import { API } from "../constants/ApiConstants";
import { postRequest, getRequest } from "../utils/request";

export const installAPI = (inputData) => {
  const actual_path = `${API.url + API.install}`;
  const requestBody = JSON.stringify(inputData);
  const headers = {
    "Content-Type": "application/json",
  };
  return postRequest(actual_path, requestBody, headers);
};
export const checkInstallationAPI = () => {
  const actual_path = `${API.url + API.checkInstallation}`;
  const headers = {
    "Content-Type": "application/json",
  };
  return getRequest(actual_path, headers);
};
