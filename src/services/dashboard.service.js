import { API } from "../constants/ApiConstants";
import { getRequest } from "../utils/request";
import { getAccessTokenHeader2 } from "../utils/api";

export const dashboardAPI = (credentials) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.getDashboard}sd=${
    credentials.start
  }&ed=${credentials.end}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};
