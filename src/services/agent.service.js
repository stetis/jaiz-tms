import { API } from "../constants/ApiConstants";
import { postRequest, getRequest } from "../utils/request";
import { getAccessTokenHeader2 } from "../utils/api";

/* AGENTS APIs */
export const allAgentsAPI = () => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.getAllAgents}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};
export const singleAgentAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.singleAgent + id}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};
export const getPaginatedAgentsAPI = (credentials) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.getPaginatedAgents}page=${
    credentials.page
  }&pagesize=${credentials.pagesize}&search=${credentials.search}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};
export const addAgentAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.addAgent}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
export const editAgentAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.editAgent}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
export const deleteAgentAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.deleteAgent + id}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};
export const enableAgentProfileAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.enableAgentProfile + id}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};
export const revokeAgentProfileAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.revokeAgentProfile + id}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};
export const initiateAgentResetPinAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.initiateAgentResetPin + id}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};
export const allSuperAgentsAPI = () => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.getAllSuperAgents}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};
export const singleSuperAgentsAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.singleSuperAgent + id}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};

export const superAgentsAPI = (credentials) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.superAgent}page=${
    credentials.page
  }&pagesize=${credentials.pagesize}&search=${credentials.search}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};
export const addSuperAgentAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.addSuperAgent}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
export const editSuperAgentAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.editSuperAgent}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
export const deleteSuperAgentAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.deleteSuperAgent + id}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};
export const enableSuperAgentProfileAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.enableSuperAgentProfile + id}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};
export const revokeSuperAgentProfileAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.revokeSuperAgentProfile + id}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};

export const initiateSuperAgentResetPinAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.initiateSuperAgentResetPin + id}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};
