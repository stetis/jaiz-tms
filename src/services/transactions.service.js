import { API } from "../constants/ApiConstants";
import { getRequest } from "../utils/request";
import { getAccessTokenHeader2 } from "../utils/api";

/* MERCHANT TRANSACTIONS  */
export const merchantTransactionsAPI = (credentials) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.getMerchantTransactions}sd=${
    credentials.from
  }&ed=${credentials.to}&status=${credentials.status}&branch-id=${
    credentials.branch
  }&search=${credentials.search}&page=${credentials.page}&pagesize=${
    credentials.pagesize
  }`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};

/* AGENT TRANSACTIONS  */
export const getAgentTransactionsAPI = (credentials) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.getAgentTransactions}sd=${
    credentials.from
  }&ed=${credentials.to}&branch-id=${credentials.branch}&status=${
    credentials.status
  }&search=${credentials.search}&page=${credentials.page}&pagesize=${
    credentials.pagesize
  }`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};

/* AGENT COMMISSIONS */
export const getAgentCommissionsAPI = (credentials) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.getAgentCommission}sd=${
    credentials.from
  }&ed=${credentials.to}&status=${credentials.status}&search=${
    credentials.search
  }&page=${credentials.page}&pagesize=${credentials.pagesize}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};
/* MERCHANT PERF0RMANCE */
export const getMerchantsPerformanceAPI = (credentials) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.getMerchantPerformance}&sd=${
    credentials.from
  }&ed=${credentials.to}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};
/* BRANCH ANALYTICS */
export const getBranchAnalyticsAPI = () => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.getBranchAnalytics}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};
/* E-JOURNAL */
export const getEjournalAPI = () => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.getEjournal}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};
