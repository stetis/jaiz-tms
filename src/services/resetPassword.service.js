import { API } from "../constants/ApiConstants";
import { postRequest } from "../utils/request";

export const initiatePasswordResetAPI = (email) => {
  const actual_path = `${API.url + API.initiatePasswordReset + email}`;
  const headers = {
    "Content-Type": "application/json",
  };
  return postRequest(actual_path, null, headers);
};

//Validate user email
export const validateUserEmailAPI = (token) => {
  const actual_path = `${API.url + API.validateEmail + token}`;
  const headers = {
    "Content-Type": "application/json",
  };
  return postRequest(actual_path, null, headers);
};
// Complete PasswordReset process User
export const completePasswordResetAPI = (inputData) => {
  const actual_path = `${API.url + API.completePasswordReset}`;
  const headers = {
    "Content-Type": "application/json",
  };
  const requestBody = JSON.stringify(inputData);
  return postRequest(actual_path, requestBody, headers);
};
