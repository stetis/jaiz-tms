import { API } from "../constants/ApiConstants";
import { postRequest, getRequest } from "../utils/request";
import { getAccessTokenHeader2 } from "../utils/api";

/* Merchants APIs */
export const singleMerchantsAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }

  const actual_path = `${API.url + API.singleMerchant + id}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};
export const allMerchantsAPI = () => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }

  const actual_path = `${API.url + API.getAllMerchants}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};
export const merchantsAPI = (credentials) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }

  const actual_path = `${API.url + API.getPaginatedMerchants}search=${
    credentials.search
  }&page=${credentials.page}&pagesize=${credentials.pagesize}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};

export const addMerchantAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }

  const actual_path = `${API.url + API.addMerchant}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
export const editMerchantAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }

  const actual_path = `${API.url + API.editMerchant}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
export const deleteMerchantAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }

  const actual_path = `${API.url + API.deleteMerchant + id}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};
export const enableMerchantProfileAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.enableMerchantProfile + id}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};
export const revokeMerchantProfileAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.revokeMerchantProfile + id}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};
export const initiateMerchantResetPinAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.initiateMerchantResetPin + id}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};
/*  BUSINESSES  */
export const allBusinessesAPI = () => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }

  const actual_path = `${API.url + API.getBusinesses}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};
export const businessesAPI = (credentials) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }

  const actual_path = `${API.url + API.getMerchants}search=${
    credentials.search
  }&page=${credentials.page}&pagesize=${credentials.pagesize}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};

export const addBusinessAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }

  const actual_path = `${API.url + API.addBusiness}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
export const editBusinessAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }

  const actual_path = `${API.url + API.editBusiness}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
export const deleteBusinessAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }

  const actual_path = `${API.url + API.deleteBusiness + id}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};
