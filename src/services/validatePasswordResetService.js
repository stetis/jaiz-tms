import { API } from "../constants/ApiConstants";
import { postRequest } from "../utils/request";

const path = "reset/actions";
export const ValidatePasswordReset = (credentials) => {
  const actual_path = `${API.url + path}`;
  const method = "POST";
  const meta = { action: "1", source: `${API.source}` };
  const data = credentials;
  const requestBody = JSON.stringify({ meta, data });
  return postRequest(actual_path, method, requestBody, null);
};
