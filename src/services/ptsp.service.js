import { API } from "../constants/ApiConstants";
import { postRequest, getRequest } from "../utils/request";
import { getAccessTokenHeader2 } from "../utils/api";

/* PTSP APIs */
export const singlePtspAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.singlePTSP + id}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};
export const ptspAPI = () => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.getPTSP}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};

export const addPTSPAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.addPTSP}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
export const editPTSPAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.editPTSP}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
export const deletePTSPAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.deletePTSP + id}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};
/* PTSP APIs CONTACT */
export const ptspContactAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.getPTSPContacts + id}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};

export const addPTSPContactAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.addPTSPContact}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
export const editPTSPContactAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.editPTSPContact}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
export const deletePTSPContactAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.deletePTSPContact + id}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};
export const enablePTSPProfileAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.enablePTSPProfile + id}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};
export const revokePTSPProfileAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.revokePTSPProfile + id}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};
export const initiatePTSPResetPinAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.initiatePTSPResetPin + id}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};
