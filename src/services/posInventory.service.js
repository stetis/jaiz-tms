import { API } from "../constants/ApiConstants";
import { postRequest, getRequest } from "../utils/request";
import { getAccessTokenHeader2 } from "../utils/api";

/* POS Inventory APIs */
export const singlePosAPI = () => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.singlePos}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};
export const allPosesAPI = (credentials) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.getPoses}search=${
    credentials.search
  }&page=${credentials.page}&pagesize=${credentials.pagesize}&status=${
    credentials.status
  }`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};
export const posInventoriesAPI = () => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.getPOSInventory}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};

export const uploadBulkPOSInventoryAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.uploadBulkPOSInventory}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
export const addPOSInventoryAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.addPOSInventory}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
export const editPOSInventoryAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.editPOSInventory}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
export const deletePOSInventoryAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.deletePOSInventory + id}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};
