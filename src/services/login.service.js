import { API } from "../constants/ApiConstants";
import {
  getLogoutAccessTokenHeader,
  getRefreshTokenHeader,
} from "../utils/api";
import { postRequest } from "../utils/request";

export const loginAPI = (credentials) => {
  const actual_path = `${API.url + API.login}`;
  const requestBody = JSON.stringify(credentials);
  const headers = {
    "Content-Type": "application/json",
  };
  return postRequest(actual_path, requestBody, headers);
};

export const logoutAPI = () => {
  const accessTokenHeader = getLogoutAccessTokenHeader();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.logout}`;
  const headers = accessTokenHeader;
  return postRequest(actual_path, null, headers);
};

export const refreshTokenAPI = () => {
  const refreshTokenHeader = getRefreshTokenHeader();
  if (!refreshTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.refreshToken}`;
  const headers = refreshTokenHeader;
  return postRequest(actual_path, null, headers);
};
