import { API } from "../constants/ApiConstants";
import { postRequest, getRequest } from "../utils/request";
import { getAccessTokenHeader2 } from "../utils/api";

/* MERCHANT TERMINALS APIs */
export const singleTerminalAPI = (id) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.singleTerminal + id}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};

export const terminalsAPI = (credentials) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.getTerminals}search=${
    credentials.search
  }&status=${credentials.status}&last-tx-date=${credentials.lastTx}&page=${
    credentials.page
  }&pagesize=${credentials.pagesize}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};
export const addMerchantTerminalAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.addMerchantTerminal}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
export const uploadmerchantTerminalsAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.bulkMerchantTerminals}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
/* AGENTS TERMINAL MANAGEMENT */
export const agentTerminalsAPI = (credentials) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.getAgentTerminals}search=${
    credentials.search
  }&status=${credentials.status}&last-tx-date=${credentials.lastTx}&page=${
    credentials.page
  }&pagesize=${credentials.pagesize}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};
export const addAgentTerminalAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.addAgentTerminal}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
export const uploadAgentTerminalsAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.bulkAgentTerminals}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
export const terminalInventoryAPI = () => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.terminalInventory}`;
  const headers = accessTokenHeader;
  return getRequest(actual_path, headers);
};
export const decommissionTerminalAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.decommissionTerminal}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};

/* EDIT TERMINAL */
export const editTerminalLocationAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.editLocation}`;
  const headers = accessTokenHeader;
  const requestBody = JSON.stringify(inputData);
  return postRequest(actual_path, requestBody, headers);
};
export const editTerminalSlipAPI = (inputData) => {
  const accessTokenHeader = getAccessTokenHeader2();
  if (!accessTokenHeader) {
    return Promise.reject("No token, you cant make this request");
  }
  const actual_path = `${API.url + API.editSlip}`;
  const requestBody = JSON.stringify(inputData);
  const headers = accessTokenHeader;
  return postRequest(actual_path, requestBody, headers);
};
