import styled, { createGlobalStyle } from "styled-components";
import { Colors } from "./assets/themes/theme";

const GlobalStyle = createGlobalStyle`
  * {
  box-sizing: border-box;
  margin: 0;
  padding: 0;
  font-family: 'Ubuntu', sans-serif;
 } 
`;

export const Container = styled.div`
  z-index: 1;
  width: 100%;
  max-width: ${({ fullWidth }) => (fullWidth ? "100%" : "1000px")};
  margin-right: auto;
  margin-left: auto;
  padding-right: 50px;
  padding-left: 50px;

  @media screen and (max-width: 991px) {
    padding-right: 30px;
    padding-left: 30px;
  }
`;

export const Button = styled.button`
  border-radius: ${({ bigRadius }) => (bigRadius ? "14px" : "4px")};
  background: ${({ primary }) => (primary ? Colors.primary : "#0467FB")};
  white-space: nowrap;
  padding: ${({ big }) => (big ? "10px 25px" : "6px 16px")};
  color: #fff;
  font-size: ${({ fontBig }) => (fontBig ? "20px" : "16px")};
  font-family: "Ubuntu bold", sans-serif;
  outline: none;
  border: none;
  cursor: pointer;

  &:hover {
    transition: all 0.3s ease-out;
    background: #fff;
    background-color: ${({ primary }) =>
      primary ? Colors.seconary : "#4B59F7"};
  }

  @media screen and (max-width: 960px) {
    width: 50%;
  }
  @media screen and (max-width: 360px) {
    width: 70%;
  }
`;

export default GlobalStyle;
