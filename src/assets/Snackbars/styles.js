import { Colors, Fonts } from "../themes/theme";

export const styles = () => ({
  success: {
    backgroundColor: Colors.primary,
    color: Colors.secondary,
    margin: 55,
    borderRadius: 5,
    fontSize: Fonts.font,
  },
  close: {
    padding: 4,
  },
  error: {
    backgroundColor: Colors.buttonError,
    color: Colors.light,
    fontSize: Fonts.font,
  },
  info: {
    backgroundcolor: Colors.seconary,
    fontSize: Fonts.font,
  },
  warning: {
    backgroundColor: "#FFF4CD",
    color: "#9C802F",
    fontSize: Fonts.font,
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: 8,
  },
  message: {
    display: "flex",
    alignItems: "center",
  },
});
