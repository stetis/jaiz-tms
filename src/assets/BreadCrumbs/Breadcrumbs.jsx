import { Breadcrumbs } from "@material-ui/core";
import { Typography, makeStyles } from "@material-ui/core/";
import HomeIcon from "@material-ui/icons/Home";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import React from "react";
import { useSelector } from "react-redux";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";
import { Colors, Fonts } from "../themes/theme";

const styles = makeStyles(() => ({
  root: {
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    color: Colors.primary,
  },
  breadLink: {
    color: Colors.primary,
    cursor: "pointer",
    textDecoration: "none",
    "& hover": {
      textDecoration: "none",
    },
  },
}));
function MainBreadcrumbs() {
  const classes = styles();
  const breadcrumbData = useSelector(
    (state) => state.updateBreadcrumb.breadcrumbData
  );
  return (
    <div style={{ marginTop: 10 }}>
      <Breadcrumbs
        separator={
          <NavigateNextIcon style={{ color: Colors.secondary, fontSize: 12 }} />
        }
        arial-label="Breadcrumb"
      >
        <Link to={breadcrumbData.homeLink ? breadcrumbData.homeLink : "#"}>
          <HomeIcon
            style={{
              verticalAlign: "top",
              color: Colors.secondary,
            }}
          />
        </Link>
        <Typography className={classes.root}>
          <Link
            to={breadcrumbData.childLink ? breadcrumbData.childLink : "#"}
            className={classes.breadLink}
          >
            {breadcrumbData.parent}
          </Link>
        </Typography>
        {breadcrumbData.child && (
          <Typography className={classes.root}>
            {breadcrumbData.child}
          </Typography>
        )}
      </Breadcrumbs>
    </div>
  );
}
export default withRouter(MainBreadcrumbs);
