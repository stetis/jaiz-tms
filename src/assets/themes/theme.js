import { lighten } from "polished";

export const Colors = {
  primary: "#0E4C28",
  secondary: "#B28B0A",
  menuListColor: lighten(0.4, "#000000"),
  light: "#ffffff",
  grey: "#808080",
  greyLight: "#e8ebee",
  textColor: "#777",
  buttonError: "#ff0033",
  buttonSuccess: "#2AA983",
  disabled: "hsl(212.3, 16.7%, 69.4%)",
};
export const Fonts = {
  primary: "'Lato',sans serif",
  secondary: "'Ubuntu',sans serif",
  form: "'Roboto',sans serif",
  SubmitButton: "12px",
  resizeLabel: "12px",
  font: "12px",
  style: {
    light: 400,
    bold: 500,
    bolder: 600,
  },
};
