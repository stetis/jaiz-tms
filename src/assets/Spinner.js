import React from "react";
import Loader from "react-loader-spinner";
import styled from "styled-components";
import { Colors } from "./themes/theme";

function Spinner(props) {
  const { className, width, height, text } = props;
  return (
    <SpinnerContainer className={className}>
      <div className="loader">
        <Loader
          type="Rings"
          color={Colors.secondary}
          secondaryColor={Colors.primary}
          height={height}
          width={width}
        />
      </div>
      {text ? <p>{text}</p> : null}
    </SpinnerContainer>
  );
}
const SpinnerContainer = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  transform: (-50%, -50%);
`;
export default Spinner;
