import { Menu, MenuItem, withStyles } from "@material-ui/core";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import React, { Component } from "react";
import { styles } from "./callout.styles";

class CallOut extends Component {
  state = {
    anchorEl: null,
  };
  CalloutOpen = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };
  render() {
    const {
      classes,
      TopAction,
      BottomAction,
      ThirdAction,
      FourthAction,
    } = this.props;
    const { anchorEl } = this.state;
    return (
      <div key={this.props.key} className={classes.callout}>
        <MoreVertIcon
          onClick={this.CalloutOpen}
          color="inherit"
          className={classes.callOutButton}
        />
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          transformOrigin={{
            vertical: "top",
            horizontal: 0,
          }}
          open={Boolean(anchorEl)}
          onClose={() => this.setState({ anchorEl: null })}
          classes={{ paper: classes.paper }}
          autoFocus={false}
          disableAutoFocusItem={true}
        >
          {TopAction && (
            <MenuItem classes={{ root: classes.menuItemRoot }}>
              {TopAction}
            </MenuItem>
          )}
          {BottomAction && (
            <MenuItem classes={{ root: classes.menuItemRoot }}>
              {BottomAction}
            </MenuItem>
          )}
          {ThirdAction && (
            <MenuItem classes={{ root: classes.menuItemRoot }}>
              {ThirdAction}
            </MenuItem>
          )}
          {FourthAction && (
            <MenuItem classes={{ root: classes.menuItemRoot }}>
              {FourthAction}
            </MenuItem>
          )}
        </Menu>
      </div>
    );
  }
}

export default withStyles(styles)(CallOut);
