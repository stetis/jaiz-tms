// /* eslint-disable comma-dangle */
// /* eslint-disable import/prefer-default-export */
import { Colors } from "../themes/theme";

export const styles = () => ({
  callOutButton: {
    textAlign: "center",
    cursor: "pointer",
    fontSize: 20,
    "&:hover": {
      backgroundColor: "transparent",
    },
    "&:focus": {
      backgroundColor: "transparent",
    },
  },
  callout: {
    position: "relative",
  },
  menu: {
    display: "flex",
  },
  paper: {
    width: "auto",
    height: "auto",
    marginLeft: -12,
    borderRadius: 0,
    marginRight: 10,
    marginTop: 7,
    backgroundColor: Colors.light,
    "&:hover": {
      backgroundColor: Colors.light,
    },
    "&:focus": {
      backgroundColor: Colors.light,
    },
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
  },
});
