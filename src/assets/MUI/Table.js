import { createTheme } from "@material-ui/core/styles";
import { Colors, Fonts } from "../../assets/themes/theme";

//Custom table for items with serial number and three vertical dots
export const theme = createTheme({
  typography: {
    useNextVariants: true,
    fontFamily: Fonts.secondary,
    fontSize: 12,
  },
  overrides: {
    MUIDataTable: {
      paper: {
        boxShadow: "none",
      },
    },
    MuiSelect: {
      select: {
        marginTop: -5,
        paddingRight: 15,
      },
    },
    MuiToolbar: {
      regular: {
        color: Colors.textColor,
        fontFamily: Fonts.secondary,
        fontSize: 12,
        height: "32px",
        minHeight: "32px",
        "@media (max-width: 600px)": {
          minHeight: "48px",
        },
      },
    },
    MuiTablePagination: {
      root: {
        fontSize: 12,
        color: Colors.textColor,
        overflow: "hidden",
      },
      input: {
        marginTop: 10,
      },
      actions: {
        marginRight: 12,
      },
      caption: {
        marginTop: -1,
        fontSize: 12,
        color: Colors.textColor,
      },
      toolbar: {
        minHeight: 10,
        height: 32,
        padding: 0,
      },
    },
    MuiMenuItem: {
      root: {
        fontSize: 12,
        color: Colors.textColor,
      },
    },
    MUIDataTableHeadCell: {
      root: {
        fontSize: 12,
        fontWeight: "bolder",
        color: Colors.textColor,
      },
    },
    MuiTableCell: {
      root: {
        fontSize: 12,
        fontFamily: Fonts.secondary,
        padding: 10,
        borderBottom: "1px solid #e8ebee",
        "&:first-child": {
          width: "4%",
          marginLeft: 4,
        },
        "&:last-child": {
          width: "3%",
        },
      },
      body: {
        color: Colors.textColor,
        fontSize: 12,
      },
      head: {
        fontSize: 12,
        fontWeight: "bolder",
        color: Colors.textColor,
        padding: 7,
      },
    },
    MuiSvgIcon: {
      root: {
        fontSize: 18,
      },
    },
    MUIDataTableToolbar: {
      root: {
        marginRight: 0,
        marginBottom: 10,
      },
      titleText: {
        color: Colors.primary,
        fontSize: 12,
        fontWeight: 700,
        fontFamily: Fonts.secondary,
        "@media only screen and (max-width: 599.95px)": {
          textAlign: "left !important",
        },
      },
      icon: {
        color: Colors.secondary,
        "&:hover": {
          color: Colors.secondary,
        },
      },
      iconActive: {
        backgroundColor: Colors.secondary,
        color: Colors.light,
      },
    },
    MuiInput: {
      root: {
        fontSize: 12,
      },
    },
    MuiInputBase: {
      root: {
        fontSize: 12,
      },
    },
    PageSizeSelector: {
      inputRoot: {
        fontSize: 8,
        marginTop: 5,
      },
      selectIcon: {
        top: -3,
      },
      label: {
        fontSize: 8,
      },
    },
    Pagination: {
      rowsLabel: {
        fontSize: 8,
      },
    },
    Pager: {
      pager: {
        padding: 0,
      },
    },
  },
});

//Custom table for items with serial number and three vertical dots
export const wideTable = createTheme({
  typography: {
    useNextVariants: true,
    fontFamily: Fonts.secondary,
    fontSize: 12,
  },
  overrides: {
    MUIDataTable: {
      paper: {
        boxShadow: "none",
      },
    },
    MuiSelect: {
      select: {
        marginTop: -5,
        paddingRight: 15,
      },
    },
    MuiToolbar: {
      regular: {
        color: Colors.textColor,
        fontFamily: Fonts.secondary,
        fontSize: 12,
        height: "32px",
        minHeight: "32px",
        "@media (max-width: 600px)": {
          minHeight: "48px",
        },
      },
    },
    MuiTablePagination: {
      root: {
        fontSize: 12,
        color: Colors.textColor,
        overflow: "hidden",
      },
      input: {
        marginTop: 10,
      },
      actions: {
        marginRight: 12,
      },
      caption: {
        marginTop: -1,
        fontSize: 12,
        color: Colors.textColor,
      },
      toolbar: {
        minHeight: 10,
        height: 32,
        padding: 0,
      },
    },
    MuiMenuItem: {
      root: {
        fontSize: 12,
        color: Colors.textColor,
      },
    },
    MUIDataTableHeadCell: {
      root: {
        fontSize: 12,
        fontWeight: "bolder",
        color: Colors.textColor,
      },
    },
    MuiTableCell: {
      root: {
        fontSize: 12,
        fontFamily: Fonts.secondary,
        padding: 10,
        borderBottom: "1px solid #e8ebee",
        "&:first-child": {
          width: "4%",
          marginLeft: 4,
        },
      },
      body: {
        color: Colors.textColor,
        fontSize: 12,
      },
      head: {
        fontSize: 12,
        fontWeight: "bolder",
        color: Colors.textColor,
        padding: 7,
      },
    },
    MuiSvgIcon: {
      root: {
        fontSize: 18,
      },
    },
    MUIDataTableToolbar: {
      root: {
        marginRight: 0,
        marginBottom: 10,
      },
      titleText: {
        color: Colors.primary,
        fontSize: 12,
        fontWeight: 700,
        fontFamily: Fonts.secondary,
        "@media only screen and (max-width: 599.95px)": {
          textAlign: "left !important",
        },
      },
      icon: {
        color: Colors.secondary,
        "&:hover": {
          color: Colors.secondary,
        },
      },
      iconActive: {
        backgroundColor: Colors.secondary,
        color: Colors.light,
      },
    },
    MuiInput: {
      root: {
        fontSize: 12,
      },
    },
    MuiInputBase: {
      root: {
        fontSize: 12,
      },
    },
    PageSizeSelector: {
      inputRoot: {
        fontSize: 8,
        marginTop: 5,
      },
      selectIcon: {
        top: -3,
      },
      label: {
        fontSize: 8,
      },
    },
    Pagination: {
      rowsLabel: {
        fontSize: 8,
      },
    },
    Pager: {
      pager: {
        padding: 0,
      },
    },
  },
});

//Custom table for items with serial number and three vertical dots
export const dynamicTable = createTheme({
  typography: {
    useNextVariants: true,
    fontFamily: Fonts.primary,
    fontSize: 12,
  },
  overrides: {
    MUIDataTable: {
      paper: {
        boxShadow: "none",
      },
    },
    MuiSelect: {
      select: {
        marginTop: -5,
        paddingRight: 15,
      },
    },
    MuiToolbar: {
      regular: {
        color: Colors.textColor,
        fontFamily: Fonts.primary,
        fontSize: 12,
        height: "32px",
        minHeight: "32px",
        "@media (max-width: 600px)": {
          minHeight: "48px",
        },
      },
    },
    MuiTablePagination: {
      root: {
        fontSize: 12,
        color: Colors.textColor,
        overflow: "hidden",
      },
      input: {
        marginTop: 10,
      },
      actions: {
        marginRight: 12,
      },
      caption: {
        marginTop: -1,
        fontSize: 12,
        color: Colors.textColor,
      },
      toolbar: {
        minHeight: 10,
        height: 32,
        padding: 0,
      },
    },
    MuiMenuItem: {
      root: {
        fontSize: 12,
        color: Colors.textColor,
      },
    },
    MuiTableCell: {
      root: {
        fontSize: 12,
        fontFamily: Fonts.primary,
        // padding: 10,
        borderBottom: "1px solid #e8ebee",
        // "&:first-child": {
        //   width: "4%",
        //   marginLeft: 4,
        // },
      },
      body: {
        color: Colors.textColor,
        fontSize: 12,
      },
      head: {
        fontSize: 12,
        fontWeight: "bolder",
        color: Colors.secondary,
        padding: 7,
      },
    },
    MUIDataTableHeadCell: {
      root: {
        fontSize: 12,
        fontWeight: 700,
        color: Colors.textColor,
      },
    },
    MuiSvgIcon: {
      root: {
        fontSize: 18,
      },
    },
    MUIDataTableToolbar: {
      root: {
        marginRight: 0,
        marginBottom: 10,
      },
      titleText: {
        color: Colors.primary,
        fontSize: 12,
        fontWeight: 700,
        fontFamily: Fonts.secondary,
        "@media only screen and (max-width: 599.95px)": {
          textAlign: "left !important",
        },
      },
      icon: {
        color: Colors.secondary,
        "&:hover": {
          color: Colors.secondary,
        },
      },
      iconActive: {
        backgroundColor: Colors.secondary,
        color: Colors.light,
      },
    },
    MuiInput: {
      root: {
        fontSize: 12,
      },
    },
    MuiInputBase: {
      root: {
        fontSize: 12,
      },
    },
    PageSizeSelector: {
      inputRoot: {
        fontSize: 8,
        marginTop: 5,
      },
      selectIcon: {
        top: -3,
      },
      label: {
        fontSize: 8,
      },
    },
    Pagination: {
      rowsLabel: {
        fontSize: 8,
      },
    },
    Pager: {
      pager: {
        padding: 0,
      },
    },
  },
});
