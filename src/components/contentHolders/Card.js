import React from "react";
import { Content, FormTitle } from "../../components/contentHolders/styles";

export const FormCard = (props) => {
  const { children, width, vmargin, hmargin, margin } = props;
  return (
    <Content
      flex
      justify="flex-start"
      width={width}
      margin={margin}
      vmargin={vmargin}
      hmargin={hmargin}
      vpadding="20px"
      hpadding="35px"
    >
      {children}
    </Content>
  );
};
export const Title = (props) => {
  const { children, width, lpadding, vmargin, hmargin, ...otherProps } = props;
  return (
    <FormTitle
      flex
      justify="flex-start"
      width={width}
      vmargin={vmargin}
      hmargin={hmargin}
      lpadding={lpadding}
      {...otherProps}
    >
      {children}
    </FormTitle>
  );
};
