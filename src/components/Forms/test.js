import React, { Component } from "react";
import Loader from "react-loader-spinner";
import { FormCard, Title } from "../../components/contentHolders/Card";
import { Grid } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import { FormsRow } from "./index";
import Password from "../TextField/password";
import PhoneInput from "../TextField/phoneInput";
import Country from "../TextField/country";
import {
  TextField,
  TextArea,
  Select,
  CheckBox,
  Radio,
} from "../TextField/index";
import {
  DeleteButton,
  SubmitButton,
  CancelButton,
  Fab,
} from "../Buttons/index";

class TestInputForms extends Component {
  state = {
    name: "",
    email: "",
    phone: "",
    password: "",
    textarea: "",
    showPassword: false,
    nationality: "",
    countryCode: "",
    date: null,
    datetime: null,
    selectedRadio: "",
  };
  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handlePasswordChange = (e) => {
    this.setState({ password: e.target.value });
  };
  handleRadioChange = (event) => {
    this.setState({
      selectedRadio: event.currentTarget.value,
    });
  };
  handleClickShowPassword = () => {
    this.setState({ showPassword: !this.state.showPassword });
  };
  handleDateChange = (date) => {
    this.setState({ date });
  };
  handleCountryChange = (object, value) => {
    this.setState({
      ...this.state,
      nationality: value.name,
      countryCode: value.code,
    });
  };
  handlePhoneInputChange = (value) => {
    if (value) {
      this.setState({
        ...this.state,
        phone: value,
      });
    }
  };
  handleSubmit = (e) => {
    e.preventDefault();
  };
  render() {
    const { isLoading } = this.props;
    return (
      <FormCard
        bg="#fff"
        width="400px"
        flex
        justify="flex-start"
        padding="20px 35px"
        vmargin="0"
        hmargin="auto"
        flexValue="2"
      >
        <Grid container spacing={8}>
          <Title width="100%" vmargin="20px" hmargin="0" lpadding="20px">
            Form Example
          </Title>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <FormsRow>
              <Grid item xs={12} sm={12} md={6} lg={6}>
                <TextField
                  id="name"
                  htmlFor="name"
                  name="name"
                  type="text"
                  label="Name"
                  value={this.state.name}
                  onChange={this.onChange}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={6} lg={6}>
                <TextField
                  id="surname"
                  htmlFor="surname"
                  name="surname"
                  type="text"
                  label="Surname"
                  value={this.state.surname}
                  onChange={this.onChange}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <TextField
                  id="email"
                  htmlFor="email"
                  name="email"
                  type="email"
                  label="Email"
                  value={this.state.email}
                  onChange={this.onChange}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <PhoneInput
                  value={this.state.phone}
                  phonelabel="Phone"
                  onChange={this.handlePhoneInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <Country
                  id="country"
                  htmlFor="country"
                  name="country"
                  countrylabel="Country"
                  defaultValue={this.state.nationality}
                  onChange={this.handleCountryChange}
                />
              </Grid>
              <Grid item xs={6} sm={12} md={12} lg={12}>
                <Password
                  id="newpassword"
                  htmlFor="newpassword"
                  name="newpassword"
                  passwordlabel="New password"
                  type={this.state.showPassword ? "text" : "password"}
                  value={this.state.password}
                  onChange={this.handlePasswordChange}
                  showPassword={this.state.showPassword}
                  onClick={this.handleClickShowPassword}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <TextField
                  id="date"
                  htmlFor="date"
                  name="date"
                  type="date"
                  label="Date"
                  value={this.state.date}
                  onChange={this.onChange}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <TextField
                  id="datetime"
                  htmlFor="datetime"
                  name="datetime"
                  label="Date Time"
                  type="datetime-local"
                  value={this.state.datetime}
                  onChange={this.onChange}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <Select
                  id="select2"
                  htmlFor="select2"
                  name="Select 2"
                  label="Select 2"
                  value={this.state.select1}
                  onChange={this.onChange}
                >
                  <option value="" hidden>
                    Select options
                  </option>
                  <option value="Yam">Yam</option>
                  <option value="Tel">Tel</option>
                  <option value="Abj">Abj</option>
                </Select>
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <Select
                  htmlFor="selectInput"
                  name="exampleInputTwo"
                  id="selectInput"
                  label="Select 2"
                  value={this.state.select2}
                  onChange={this.onChange}
                >
                  <option value="" hidden>
                    Select
                  </option>
                  <option>Jellof</option>
                  <option>Rice</option>
                  <option>Abaja</option>
                </Select>
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <TextArea
                  id="textarea"
                  htmlFor="textarea"
                  name="textarea"
                  type="text"
                  label="Text area"
                  value={this.state.textarea}
                  onChange={this.onChange}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={4} lg={4}>
                <CheckBox
                  id="checkbox"
                  htmlFor="checkbox"
                  name="checkbox"
                  type="checkbox"
                  value="checkOne"
                  onChange={this.onChange}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={4} lg={4}>
                <CheckBox
                  id="checkbox2"
                  htmlFor="checkbox2"
                  name="checkbox 2"
                  type="checkbox"
                  value="checkTwo"
                  onChange={this.onChange}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={4} lg={4}>
                <CheckBox
                  id="checkbox 3"
                  htmlFor="checkbox 3"
                  name="checkbox 3"
                  type="checkbox"
                  value="checkThree"
                  onChange={this.onChange}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={4} lg={4}>
                <Radio
                  id="radio"
                  htmlFor="radio"
                  name="name"
                  type="radio"
                  value="bread"
                  checked={this.state.selectedRadio === "bread"}
                  onChange={this.handleRadioChange}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={4} lg={4}>
                <Radio
                  id="radio1"
                  name="radio"
                  type="radio"
                  value="milk"
                  checked={this.state.selectedRadio === "milk"}
                  onChange={this.handleRadioChange}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={4} lg={4}>
                <Radio
                  id="radio2"
                  name="radio"
                  type="radio"
                  value="milo"
                  checked={this.state.selectedRadio === "milo"}
                  onChange={this.handleRadioChange}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={3} lg={3}>
                <SubmitButton
                  type="submit"
                  disabled={isLoading}
                  style={{
                    backgroundColor: isLoading ? "#2169f3" : "",
                    marginTop: 20,
                  }}
                  disableRipple={isLoading}
                  onClick={this.handleSubmit}
                >
                  <span className="submit-btn">
                    {isLoading ? (
                      <Loader
                        type="ThreeDots"
                        color="#B28B0A"
                        height="15"
                        width="30"
                      />
                    ) : (
                      "update"
                    )}
                  </span>
                </SubmitButton>
              </Grid>
              <Grid item xs={12} sm={12} md={3} lg={3}>
                <DeleteButton
                  type="submit"
                  style={{ marginTop: 20 }}
                  disabled={isLoading}
                  disableRipple={isLoading}
                  onClick={this.handleSubmit}
                >
                  <span className="submit-btn">
                    {isLoading ? (
                      <Loader
                        type="ThreeDots"
                        color="#B28B0A"
                        height="15"
                        width="30"
                      />
                    ) : (
                      "Delete"
                    )}
                  </span>
                </DeleteButton>
              </Grid>
              <Grid item xs={12} sm={12} md={3} lg={3}>
                <CancelButton
                  style={{ marginTop: 20 }}
                  onClick={this.handleDateChange}
                  disabled
                >
                  cancel
                </CancelButton>
              </Grid>
              <Grid item xs={12} sm={12} md={3} lg={3}>
                <Fab onClick={this.handleDateChange} style={{ marginTop: 20 }}>
                  <AddIcon />
                </Fab>
              </Grid>
            </FormsRow>
          </Grid>
        </Grid>
      </FormCard>
    );
  }
}
export default TestInputForms;
