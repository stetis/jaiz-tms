import styled, { css } from "styled-components";
// Import Colors and font sizes variables
import { Colors, Fonts } from "../../assets/themes/theme";
import NumberFormat from "react-number-format";

export const FormsRow = styled.div`
  display: flex;
  flex-flow: row wrap;
  align-items: flex-start;
  justify-content: flex-start;
  text-align: left;
  width: 100%;
  & + & {
    margin-top: 12px;
  }
`;

// Input label
export const InputLabel = styled.label`
  margin: ${(props) => props.vmargin || "0px"} ${(props) => props.hmargin || 0};
  margin-top: 15px;
  padding: ${(props) => props.vpadding || 0} ${(props) => props.hpadding || 0};
  display: ${(props) => (props.display ? props.display : "block")};
  width: ${(props) => (props.width ? props.width : "100%")};
  font-size: ${Fonts.font};
  font-family: ${Fonts.secondary};
  color: ${Colors.textColor};
  white-space: pre;
  cursor: ${(props) => (props.disabled ? "not-allowed" : "text")};
  font-weight: 500;
  line-height: 10px;
`;

// Text input
export const InputTextElement = styled.input.attrs((props) => ({
  type: props.type || "text",
}))`
  width: ${(props) => props.width || "100%"};
  line-height: 1;
  height: ${(props) => props.height || "35px"};
  margin: ${(props) => props.vmargin || "10px"} ${(props) => props.hmargin || 0};
  padding: ${(props) =>
      props.disabled ? 0 : props.vpadding ? props.vpadding : "10px"}
    ${(props) =>
      props.disabled ? 0 : props.hpadding ? props.hpadding : "10px"};
  font-size: ${Fonts.font};
  font-family: ${Fonts.secondary};
  border-width: 1px;
  border-style: ${(props) => (props.disabled ? "none" : "solid")};
  border-color: ${(props) =>
    props.disabled ? "#e8ebee" : props.error ? "red" : "#e8ebee"};
  border-radius: 5px;
  background: ${(props) => (props.disabled ? "#fcfdfe" : "#fcfdfe")};
  color: ${(props) => (props.disabled ? Colors.textColor : Colors.textColor)};
  cursor: ${(props) => (props.disabled ? "not-allowed" : "text")};
  transition: all 0.2s cubic-bezier(0.4, 0, 0.2, 1) 0s;
  &:hover {
    border-color: ${(props) =>
      props.disabled ? "#e8ebee" : props.error ? "red" : Colors.secondary};
    outline: none;
  }
  &:focus {
    outline: none;
  }
  :focus-within {
    border-color: ${(props) =>
      props.disabled ? "#e8ebee" : props.error ? "red" : Colors.secondary};
    outline: none;
  }
  ::placeholder {
    color: ${Colors.textColor};
  }
  & + & {
    margin-top: 12px;
  }
  ${(props) =>
    props.textarea &&
    css`
      min-height: ${(props) => props.height || "50px"};
      resize: none;
    `};
  ${(props) =>
    props.select &&
    css`
      padding: ${(props) =>
          props.disabled ? 0 : props.vpadding ? props.vpadding : "10px"}
        ${(props) =>
          props.disabled ? 0 : props.hpadding ? props.hpadding : "10px"};
      line-height: 1;
      height: ${(props) => props.height || "35px"};
      background: url(http://i.stack.imgur.com/RGBNj.png) no-repeat right
        #fcfdfe;
      -webkit-appearance: none;
      border-width: 1px;
      cursor: ${(props) => (props.disabled ? "not-allowed" : "pointer")};
      border-style: ${(props) => (props.disabled ? "none" : "solid")};
      color: ${(props) =>
        props.disabled ? Colors.textColor : Colors.textColor};
      border-color: ${(props) => (props.disabled ? "#e8ebee" : "#e8ebee")};
      background: ${(props) =>
        props.disabled
          ? "#fcfdfe"
          : "url('https://www.materialui.co/materialIcons/navigation/arrow_drop_down_grey_192x192.png') no-repeat right  #fcfdfe"};
      background-size: contain;
      background-position: right 5px top 50%;
      option {
        color: ${Colors.textColor};
        font-size: ${Fonts.fonts};
        background: #fcfdfe;
        display: flex;
        white-space: pre;
        min-height: 20px;
        padding: 0px 2px 1px;
      }
    `};
`;
// Checkbox input
export const InputOriginalEl = styled.input`
  display: none;
  &:checked ~ div {
    background-color: ${(props) =>
      props.disabled ? "#e8ebee" : Colors.primary};
    border-color: ${(props) => (props.disabled ? "#e8ebee" : Colors.primary)};
    cursor: ${(props) => (props.disabled ? "not-allowed" : "pointer")};
    &::after {
      transform: rotate(45deg) scale(1);
    }
  }
`;
export const InputOriginalElCustom = styled.div`
  position: absolute;
  top: 2px;
  left 4px;
  height: 20px;
  width: 20px;
  background: transparent;
  color: ${Colors.textColor};
  font-family:${Fonts.secondary};
  font-size:${Fonts.font};
  border: 1px solid hsla(0, 100%, 0%, .25);
  border-radius: 2px;
  transition: all .25s ease-in-out;
  cursor: ${(props) => (props.disabled ? "not-allowed" : "pointer")};
  &:hover {
    border-color: ${(props) =>
      props.disabled ? "#e8ebee" : props.error ? "red" : Colors.primary};
    outline: none;
  }
  &::after {
    position: absolute;
    content: '';
    left: 4px;
    top: 0;
    width: 8px;
    height: 12px;
    border: solid ${Colors.secondary};
    border-width: 0 2px 2px 0;
    transform: rotate(45deg) scale(0);
    transition: transform .25s ease-in-out;
  }
`;

export const InputLabelLabel = styled.span`
  margin-left: 30px;
  position: relative;
  top: -3px;
  font-family: "Ubuntu bold", sans-serif;
`;
// Radio input
export const InputRadioElCustom = styled(InputOriginalElCustom)`
  &,
  &::after {
    border-radius: 50%;
  }
  &::after {
    left: 3px;
    top: 3px;
    width: 10px;
    height: 10px;
    background-color: #fff;
    cursor: ${(props) => (props.disabled ? "not-allowed" : "pointer")};
    color: ${Colors.textColor};
    font-size: ${Fonts.font};
    transform: scale(1);
    transition: transform 0.25s ease-in-out;
  }
`;
// General Input wrapper
export const InputElWrapper = styled.fieldset`
  padding-top: 0;
  padding-bottom: 0px;
  margin: 0;
  margin-bottom: 4px;
  line-height: 0;
  border: 0;
  &:first-of-type {
    padding-right: 8px;
    padding-left: 0;
  }

  &:nth-of-type(n + 2) {
    padding-left: 8px;
    padding-right: 28px;
  }

  &:last-of-type {
    padding-right: 0;
    padding-left: ${(props) => (props.group ? "0px" : 0)};
    @media only screen and (min-width: 600px) {
      padding-right: 0;
      padding-left: ${(props) => (props.group ? "12px" : 0)};
    }
  }

  label,
  input:not(type=checkbox):not(type=radio),
  textarea {
    width: 100%;
  }

  ${(props) =>
    props.active &&
    css`
      label {
        color: ${Colors.primary};
      }

      input,
      textarea {
        border-bottom-color: ${Colors.primary};
      }

      ${InputOriginalElCustom} {
        background-color: ${Colors.primary};
        border-color: ${Colors.primary};
        color: ${Colors.textColor};
        &::after {
          transform: rotate(45deg) scale(1);
        }
      }

      ${InputRadioElCustom} {
        background-color: ${Colors.primary};
        &::after {
          background-color: #fff;
        }
      }
    `}

  ${(props) =>
    props.disabled &&
    css`
      &,
      label,
      input,
      select,
      radio,
      checkbox,
      textarea {
        cursor: not-allowed;
      }

      label {
        color: "#e8ebee";
      }

      input,
      textarea,
      ${InputOriginalElCustom} {
        border-bottom-color: "#e8ebee";
      }
      ${InputRadioElCustom} {
        border-color: "#e8ebee";
      }
    `}
  ${(props) =>
    props.error &&
    css`
      label {
        color: ${Colors.buttonError};
      }

      input,
      textarea {
        border-bottom-color: ${Colors.buttonError};
      }

      ${InputOriginalElCustom} {
        background: transparent;
        border: 2px solid ${Colors.buttonError};
      }
    `}

  ${(props) =>
    props.custom &&
    css`
      position: relative;
    `}
`;

export const Text = styled.p`
  margin: ${(props) => props.vmargin || "2px"}
    ${(props) => props.hmargin || "0"};
  padding: ${(props) => props.vpadding || 0} ${(props) => props.hpadding || 0};
  display: block;
  width: 100%;
  color: ${Colors.textColor};
  font-size: ${Fonts.fonts};
  font-weight: 400;
  font-family: ${Fonts.secondary};
  cursor: text;
  text-transform: none;
`;

// Text input
export const NumberInputElement = styled(NumberFormat)`
  width: ${(props) => props.width || "100%"};
  line-height: 1;
  height: ${(props) => props.height || "35px"};
  margin: ${(props) => props.vmargin || "8px"} ${(props) => props.hmargin || 0};
  padding: ${(props) =>
      props.disabled ? 0 : props.vpadding ? props.vpadding : "10px"}
    ${(props) =>
      props.disabled ? 0 : props.hpadding ? props.hpadding : "10px"};
  font-size: ${Fonts.font};
  font-family: ${Fonts.secondary};
  border-width: 1px;
  border-style: ${(props) => (props.disabled ? "none" : "solid")};
  border-color: ${(props) =>
    props.disabled ? "#e8ebee" : props.error ? "red" : "#e8ebee"};
  border-radius: 5px;
  background: ${(props) => (props.disabled ? "#fcfdfe" : "#fcfdfe")};
  color: ${(props) => (props.disabled ? Colors.textColor : Colors.textColor)};
  cursor: ${(props) => (props.disabled ? "not-allowed" : "text")};
  transition: all 0.2s cubic-bezier(0.4, 0, 0.2, 1) 0s;
  &:hover {
    border-color: ${(props) =>
      props.disabled ? "#e8ebee" : props.error ? "red" : Colors.secondary};
    outline: none;
  }
  &:focus {
    outline: none;
  }
  :focus-within {
    border-color: ${(props) =>
      props.disabled ? "#e8ebee" : props.error ? "red" : Colors.secondary};
    outline: none;
  }
  ::placeholder {
    color: ${Colors.disabled};
  }
  & + & {
    margin-top: 12px;
  }
`;
