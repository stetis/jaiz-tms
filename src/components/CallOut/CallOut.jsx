import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuItem from "@material-ui/core/MenuItem";
import Popover from "@material-ui/core/Popover";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import React, { useState } from "react";
import { styles } from "./styles";

function CallOut(props) {
  const classes = styles();
  const [anchorEl, setAnchorEl] = useState(null);

  const CalloutOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const {
    TopAction,
    BottomAction,
    ThirdAction,
    FourthAction,
    font,
    margin,
  } = props;
  return (
    <div key={props.key} className={classes.callout}>
      <IconButton
        aria-label="delete"
        className={classes.callOutButton}
        onClick={CalloutOpen}
      >
        <MoreVertIcon
          style={{ fontSize: font ? font : 20, margin: margin ? margin : 0 }}
        />
      </IconButton>
      <Popover
        id="simple-popover"
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <div className={classes.menuDiv}>
          {TopAction && props.open === false ? (
            <MenuItem onClick={handleClose} className={classes.item}>
              {TopAction}
            </MenuItem>
          ) : (
            TopAction && (
              <MenuItem className={classes.item}>{TopAction}</MenuItem>
            )
          )}
          <Divider />
          {BottomAction && (
            <MenuItem className={classes.item}>{BottomAction}</MenuItem>
          )}
          <Divider />
          {ThirdAction && (
            <MenuItem className={classes.item}>{ThirdAction}</MenuItem>
          )}
          <Divider />
          {FourthAction && (
            <MenuItem className={classes.item}>{FourthAction}</MenuItem>
          )}
        </div>
      </Popover>
    </div>
  );
}
export default CallOut;
