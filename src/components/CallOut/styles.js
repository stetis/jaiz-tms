/* eslint-disable comma-dangle */
/* eslint-disable import/prefer-default-export */
import { makeStyles } from "@material-ui/core";

export const styles = makeStyles(() => ({
  callOutButton: {
    textAlign: "center",
    "&:hover": {
      backgroundColor: "transparent",
    },
    "&:focus": {
      backgroundColor: "transparent",
    },
  },
  callout: {
    position: "relative",
  },
  menuDiv: {
    width: "auto",
  },
  item: {
    cursor: "pointer",
  },
  menu: {
    display: "flex",
  },
  paper: {
    padding: 2,
    borderRadius: 5,
    marginTop: 35,
    backgroundColor: "#fff",
    "&:hover": {
      backgroundColor: "#fff",
    },
    "&:focus": {
      backgroundColor: "#fff",
    },
  },
}));
