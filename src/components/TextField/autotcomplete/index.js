import React, { useState } from "react";
import { AutoComplete } from "./AutoComplete";

export default function AutoCompleteExample() {
  const [search, setSearch] = useState({
    text: "",
    suggestions: [],
  });
  const [isComponentVisible, setIsComponentVisible] = useState(true);
  const onTextChanged = (e) => {
    // const value = e.target.value;
    // let suggestions = [];
    // if (value.length > 0) {
    //   const regex = new RegExp(`^${value}`, "i");
    //   suggestions = states.sort().filter((v) => regex.test(v.name));
    // }
    // setIsComponentVisible(true);
    // setSearch({ suggestions, text: value });
  };
  const suggestionSelected = (value) => {
    setIsComponentVisible(false);
    setSearch({
      text: value.name,
      suggestions: [],
    });
  };
  const { suggestions } = search;
  return (
    <div>
      <AutoComplete
        id="autocomplete"
        htmlFor="autocomplete"
        label="AutoComplete"
        onChange={onTextChanged}
        value={search.text}
        setIsComponentVisible={setIsComponentVisible}
        suggestions={suggestions}
        isComponentVisible={isComponentVisible}
        suggestionSelected={suggestionSelected}
      />
    </div>
  );
}
