import PropTypes from "prop-types";
import { InputLabel, InputElWrapper } from "../../../components/Forms";

import {
  AutoCompleteContainer,
  AutoCompleteIcon,
  Input,
  AutoCompleteItem,
  AutoCompleteItemButton,
  Root,
} from "./styles";

export const AutoComplete = (props) => {
  const {
    iconColor,
    style,
    grouped,
    id,
    htmlFor,
    label,
    Icon,
    onChange,
    value,
    suggestionSelected,
    isComponentVisible,
    setIsComponentVisible,
    suggestions,
    disabled,
    ...otherProps
  } = props;
  return (
    <Root style={style}>
      <InputElWrapper group={grouped}>
        <InputLabel htmlFor={htmlFor}>{label}</InputLabel>
        <div
          onClick={() => setIsComponentVisible(false)}
          style={{
            display: isComponentVisible ? "block" : "none",
            width: "200vw",
            height: "200vh",
            backgroundColor: "transparent",
            position: "fixed",
            zIndex: 0,
            top: 0,
            left: 0,
          }}
        />
        <div>
          <Input
            id={id}
            autoComplete="off"
            value={value}
            onChange={onChange}
            type={"text"}
            disabled={disabled}
            {...otherProps}
          />
          <AutoCompleteIcon color={iconColor} isOpen={isComponentVisible}>
            {Icon}
          </AutoCompleteIcon>
        </div>
        {suggestions.length > 0 && isComponentVisible && (
          <AutoCompleteContainer>
            {suggestions.map((item, index) => (
              <AutoCompleteItem key={index}>
                <AutoCompleteItemButton
                  key={index}
                  onClick={() => suggestionSelected(item)}
                >
                  {item.name}
                </AutoCompleteItemButton>
              </AutoCompleteItem>
            ))}
          </AutoCompleteContainer>
        )}
      </InputElWrapper>
    </Root>
  );
};

AutoComplete.propTypes = {
  name: PropTypes.string,
  code: PropTypes.string,
  // iconColor?: PropTypes.string,
  // style?: PropTypes.object,
};
