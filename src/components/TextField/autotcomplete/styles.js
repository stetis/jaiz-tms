import styled, { css } from "styled-components";
import { Colors, Fonts } from "../../../assets/themes/theme";

export const Root = styled.div`
  position: relative;
  width: 100%;
`;

export const baseButtonMixin = css`
  background: none;
  border: none;
  padding: 0;
`;

export const ValueWrapper = styled.input`
  width: ${(props) => props.width || "100%"};
  line-height: 32px;
  height: ${(props) => props.height || "35px"};
  margin: ${(props) => props.vmargin || "8px"} ${(props) => props.hmargin || 0};
  padding: ${(props) => props.vpadding || "10px"}
    ${(props) => props.hpadding || "10px"};
  padding-left: 8px;
  padding-right: 32px;
  font-size: ${Fonts.font};
  font-family: ${Fonts.secondary};
  box-sizing: border-box;
  border-width: 1px;
  border-style: solid;
  border-color: ${(props) =>
    props.disabled ? "#e8ebee" : props.error ? "red" : "#e8ebee"};
  border-radius: 5px;
  background: ${(props) => (props.disabled ? "#fcfdfe" : "#fcfdfe")};
  color: ${(props) => (props.disabled ? Colors.textColor : Colors.textColor)};
  cursor: ${(props) => (props.disabled ? "not-allowed" : "text")};
  &:hover {
    border-color: ${(props) =>
      props.disabled ? "#e8ebee" : props.error ? "red" : Colors.secondary};
    outline: none;
  }
  &:focus {
    outline: none;
  }
  :focus-within {
    border-color: ${(props) =>
      props.disabled ? "#e8ebee" : props.error ? "red" : Colors.secondary};
    outline: none;
  }
  ::placeholder {
    color: ${Colors.disabled};
  }
  & + & {
    margin-top: 12px;
  }
`;

export const AutoCompleteIcon = styled.span`
  position: absolute;
  top: 33px;
  right: 5px;
  height: 35px;
  width: 35px;
  transition: all 150ms linear;
  transform: ${(props) => (props.isOpen ? "rotate(0.5turn)" : "none")};
  transform-origin: center;
  display: flex;

  svg {
    margin: auto;
  }

  ${ValueWrapper}:focus + & {
    color: ${(props) => props.color || "inherit"};
    fill: ${(props) => props.fill || "curentcolor"};
  }
`;

export const AutoCompleteContainer = styled.ul`
  background: #fcfdfe;
  padding: 8px 0;
  list-style-type: none;
  width: 100%;
  min-width: 250px;
  position: absolute;
  top: 100%;
  left: 0;
  border: 1px solid #e8ebee;
  border-radius: 5px;
  margin: 0;
  margin-top: -8px;
  box-sizing: border-box;
  box-shadow: 0 0 6px 2px rgba(0, 0, 0, 0.1);
  max-height: 280px;
  overflow-y: auto;
  z-index: 1000;
`;

export const AutoCompleteItem = styled.li`
  padding: 0 24px;
  width: 100%;
  box-sizing: border-box;
  &:hover {
    background-color: #e8ebee;
  }
`;

export const AutoCompleteItemButton = styled.button`
  ${baseButtonMixin} width: 100%;
  line-height: 32px;
  text-align: left;
  &:active {
    outline: none;
    color: #0076f5;
  }
`;
export const Input = styled(ValueWrapper)`
  transition: border-color 150ms linear;
  &:focus {
    border-color: Colors.secondary
    outline: none;

    + ${AutoCompleteIcon} {
      color: ${(props) => props.color || Colors.secondary};
      fill: ${(props) => props.fill || Colors.secondary};
    }
  }
`;
