import React from "react";
import zxcvbn from "zxcvbn";

const PasswordStrengthMeter = ({ password }) => {
  const testResult = zxcvbn(password);
  const num = (testResult.score * 100) / 4;
  const createPasswordStrenghtLabel = () => {
    switch (testResult.score) {
      case 0:
        return "Very weak";
      case 1:
        return "weak";
      case 2:
        return "Fair";
      case 3:
        return "Good";
      case 4:
        return "Strong";
      default:
        return "";
    }
  };
  const progressColor = () => {
    switch (testResult.score) {
      case 0:
        return "#828282";
      case 1:
        return "#EA1111";
      case 2:
        return "#FFAD00";
      case 3:
        return "#9BC158";
      case 4:
        return "#00B500";
      default:
        return "none";
    }
  };
  const changePasswordColor = () => ({
    width: `${num}%`,
    background: progressColor(),
    height: 7,
  });
  return (
    <>
      <div
        className="progress"
        style={{ height: 7, margin: "5px 3px 5px 10px" }}
      >
        <div className="progress-bar" style={changePasswordColor()}></div>
      </div>
      <p style={{ color: progressColor(), textAlign: "right" }}>
        {createPasswordStrenghtLabel()}
      </p>
    </>
  );
};
export default PasswordStrengthMeter;
