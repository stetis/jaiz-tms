import React from "react";
import {
  InputLabel,
  InputElWrapper,
  InputTextElement,
  InputOriginalEl,
  InputOriginalElCustom,
  InputLabelLabel,
  InputRadioElCustom,
  NumberInputElement,
} from "../../components/Forms";

export const TextField = (props) => {
  const {
    type,
    id,
    htmlFor,
    name,
    value,
    label,
    onChange,
    disabled,
    grouped,
    readonly,
    ...otherProps
  } = props;
  return (
    <InputElWrapper group={grouped}>
      <InputLabel htmlFor={htmlFor}>{label}</InputLabel>
      <InputTextElement
        id={id}
        name={name}
        value={value}
        label={label}
        type={type}
        onChange={onChange}
        disabled={disabled}
        readonly={readonly}
        {...otherProps}
      />
    </InputElWrapper>
  );
};

export const TextArea = (props) => {
  const {
    type,
    id,
    htmlFor,
    name,
    value,
    label,
    onChange,
    disabled,
    grouped,
    readonly,
    ...otherProps
  } = props;
  return (
    <InputElWrapper group={grouped}>
      <InputLabel htmlFor={htmlFor}>{label}</InputLabel>
      <InputTextElement
        id={id}
        htmlFor={htmlFor}
        name={name}
        value={value}
        type={type}
        label={label}
        disabled={disabled}
        onChange={onChange}
        as="textarea"
        textarea
        {...otherProps}
      />
    </InputElWrapper>
  );
};

export const Select = (props) => {
  const {
    id,
    htmlFor,
    children,
    name,
    value,
    label,
    onChange,
    disabled,
    grouped,
    readonly,
    ...otherProps
  } = props;
  return (
    <InputElWrapper group={grouped}>
      <InputLabel htmlFor={htmlFor}>{label}</InputLabel>
      <InputTextElement
        id={id}
        htmlFor={htmlFor}
        name={name}
        value={value}
        onChange={onChange}
        label={label}
        disabled={disabled}
        as="select"
        select
        {...otherProps}
      >
        <option value="" hidden></option>
        {children}
      </InputTextElement>
    </InputElWrapper>
  );
};

export const CheckBox = (props) => {
  const {
    type,
    id,
    htmlFor,
    label,
    name,
    value,
    onChange,
    disabled,
    grouped,
    readonly,
    ...otherProps
  } = props;
  return (
    <InputElWrapper group={grouped} custom>
      <InputLabel htmlFor={htmlFor}>
        <InputOriginalEl
          id={id}
          htmlFor={htmlFor}
          name={name}
          label={label}
          type={type}
          value={value}
          onChange={onChange}
          custom
          disabled={disabled}
          {...otherProps}
        />
        <InputOriginalElCustom />
        <InputLabelLabel>{label}</InputLabelLabel>
      </InputLabel>
    </InputElWrapper>
  );
};
export const Radio = (props) => {
  const {
    type,
    id,
    htmlFor,
    name,
    value,
    label,
    onChange,
    grouped,
    readonly,
    ...otherProps
  } = props;
  return (
    <InputElWrapper group={grouped} custom>
      <InputLabel htmlFor={htmlFor}>
        <InputOriginalEl
          htmlFor={htmlFor}
          id={id}
          name={name}
          type={type}
          label={label}
          value={value}
          onChange={onChange}
          {...otherProps}
        />
        <InputRadioElCustom />
        <InputLabelLabel>{label}</InputLabelLabel>
      </InputLabel>
    </InputElWrapper>
  );
};

export const MoneyTextField = (props) => {
  const {
    type,
    id,
    htmlFor,
    name,
    value,
    label,
    onChange,
    disabled,
    grouped,
    ...otherProps
  } = props;
  return (
    <InputElWrapper group={grouped}>
      <InputLabel htmlFor={htmlFor}>{label}</InputLabel>
      <NumberInputElement
        id={id}
        name={name}
        value={value}
        label={label}
        type={type}
        onChange={onChange}
        {...otherProps}
      />
    </InputElWrapper>
  );
};
