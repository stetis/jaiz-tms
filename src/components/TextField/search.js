import { InputAdornment, makeStyles, TextField } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import PropTypes from "prop-types";
import React from "react";
import { Colors } from "../../assets/themes/theme";
import { InputElWrapper, InputLabel } from "../../components/Forms";

const styles = makeStyles(() => ({
  textField: {
    width: "100%",
  },
  disabledRoot: {
    background: "#fcfdfe",
    height: 35,
    marginTop: 8,
    width: "100%",
    color: Colors.textColor,
    fontSize: 12,
    borderColor: "transparent",
    "&:hover $notchedOutline": {
      borderColor: "transparent",
    },
    "&$disabled $notchedOutline": {
      borderColor: "#e8ebee",
    },
    "&$focused $notchedOutline": {
      borderColor: "transparent",
    },
  },
  outlinedInput: {
    background: "#fcfdfe",
    height: 35,
    marginTop: 8,
    width: "100%",
    color: Colors.textColor,
    fontSize: 12,
    borderWidth: 1,
    borderColor: Colors.secondary,
    borderRadius: 5,
    "&:hover $notchedOutline": {
      borderWidth: 1,
      borderColor: Colors.secondary,
    },
    "&$disabled $notchedOutline": {
      borderWidth: 1,
      borderColor: "#e8ebee",
    },
    "&$focused $notchedOutline": {
      borderWidth: 1,
      borderColor: Colors.secondary,
    },
  },
  focused: {},
  notchedOutline: {},
  disabled: {},
  adornmentRoot: {
    fontSize: 18,
    cursor: "pointer",
  },
  searchBtn: {
    color: "inherit",
  },
}));

export default function Search(props) {
  const {
    id,
    name,
    htmlFor,
    className,
    value,
    onChange,
    disabled,
    onClickIcon,
    onKeyUp,
    searchlabel,
    grouped,
    InputProps,
    ...otherProps
  } = props;
  const classes = styles();
  const textFieldProps = {
    InputProps: {
      classes: {
        root: disabled ? classes.disabledRoot : classes.outlinedInput,
        focused: classes.focused,
        notchedOutline: classes.notchedOutline,
        disabled: classes.disabled,
      },
      endAdornment: (
        <InputAdornment position="end">
          <SearchIcon className={classes.searchBtn} onClick={onClickIcon} />
        </InputAdornment>
      ),
      ...InputProps,
    },
  };
  return (
    <InputElWrapper group={grouped}>
      <InputLabel htmlFor={htmlFor}>{searchlabel}</InputLabel>
      <TextField
        id={id}
        htmlFor={htmlFor}
        variant="outlined"
        name={name}
        searchlabel={searchlabel}
        type={"search"}
        value={value}
        disabled={disabled}
        onChange={onChange}
        onKeyUp={onKeyUp}
        className={classes.textField}
        {...textFieldProps}
        {...otherProps}
      />
    </InputElWrapper>
  );
}

Search.propTypes = {
  id: PropTypes.string,
  value: PropTypes.string,
  htmlFor: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  className: PropTypes.string,
};
