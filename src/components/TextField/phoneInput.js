import React from "react";
import PropTypes from "prop-types";
import MuiPhoneNumber from "material-ui-phone-number";
import { withStyles } from "@material-ui/core";
import { InputLabel, InputElWrapper } from "../../components/Forms";
import { Colors } from "../../assets/themes/theme";

const styles = () => ({
  phoneInputstyle: {
    width: "100%",
  },
  inputProps: {
    fontSize: 12,
    color: Colors.textColor,
  },
  disabledRoot: {
    background: "#fcfdfe",
    height: 35,
    marginTop: 8,
    width: "100%",
    color: Colors.textColor,
    fontSize: 12,
    borderColor: "transparent",
    "&:hover $notchedOutline": {
      borderColor: "transparent",
    },
    "&$disabled $notchedOutline": {
      borderColor: "transparent",
    },
  },
  outlinedInput: {
    background: "#fcfdfe",
    height: 35,
    marginTop: -5,
    width: "100%",
    color: Colors.textColor,
    fontSize: 12,
    borderColor: Colors.secondary,
    borderRadius: 5,
    "&:hover $notchedOutline": {
      borderWidth: 1,
      borderColor: Colors.secondary,
    },
    "&$disabled $notchedOutline": {
      borderWidth: 1,
      borderColor: "#e8ebee",
    },
  },
  notchedOutline: {},
  disabled: {
    cursor: "not-allowed",
  },
});
const PhoneInput = (props) => {
  const {
    classes,
    phonelabel,
    className,
    value,
    disabled,
    onChange,
    grouped,
    InputProps,
    inputProps,
    ...otherProps
  } = props;
  const textFieldProps = {
    inputProps: {
      className: classes.inputProps,
      ...inputProps,
    },
    InputProps: {
      classes: {
        root: disabled ? classes.disabledRoot : classes.outlinedInput,
        notchedOutline: classes.notchedOutline,
        disabled: classes.disabled,
      },
      ...InputProps,
    },
  };
  return (
    <InputElWrapper group={grouped}>
      <InputLabel htmlFor="phone">{phonelabel}</InputLabel>
      <MuiPhoneNumber
        id="phone"
        name="phone"
        defaultCountry={"ng"}
        value={value}
        phonelabel={phonelabel}
        className={className || classes.phoneInputstyle}
        onChange={onChange}
        margin="normal"
        variant="outlined"
        disabled={disabled}
        {...textFieldProps}
        {...otherProps}
      />
    </InputElWrapper>
  );
};
PhoneInput.propTypes = {
  id: PropTypes.string,
  value: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  placeholder: PropTypes.string,
  hintText: PropTypes.string,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  className: PropTypes.string,
};
export default withStyles(styles)(PhoneInput);
