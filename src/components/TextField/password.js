import React from "react";
import PropTypes from "prop-types";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import { InputLabel, InputElWrapper } from "../../components/Forms";
import { withStyles, TextField } from "@material-ui/core";
import InputAdornment from "@material-ui/core/InputAdornment";
import { Colors } from "../../assets/themes/theme";

const styles = () => ({
  textField: {
    width: "100%",
  },
  disabledRoot: {
    background: "#fcfdfe",
    height: 35,
    marginTop: 8,
    width: "100%",
    color: Colors.textColor,
    fontSize: 12,
    borderColor: "transparent",
    "&:hover $notchedOutline": {
      borderColor: "transparent",
    },
    "&$disabled $notchedOutline": {
      borderColor: "#e8ebee",
    },
    "&$focused $notchedOutline": {
      borderColor: "transparent",
    },
  },
  outlinedInput: {
    background: "#fcfdfe",
    height: 35,
    marginTop: 8,
    width: "100%",
    color: Colors.textColor,
    fontSize: 12,
    borderWidth: 1,
    borderColor: Colors.secondary,
    borderRadius: 5,
    "&:hover $notchedOutline": {
      borderWidth: 1,
      borderColor: Colors.secondary,
    },
    "&$disabled $notchedOutline": {
      borderWidth: 1,
      borderColor: "#e8ebee",
    },
    "&$focused $notchedOutline": {
      borderWidth: 1,
      borderColor: Colors.secondary,
    },
  },
  focused: {},
  notchedOutline: {},
  disabled: {},
  adornmentRoot: {
    fontSize: 18,
    cursor: "pointer",
  },
});

const Password = (props) => {
  const {
    id,
    name,
    type,
    htmlFor,
    classes,
    className,
    value,
    onChange,
    disabled,
    onClick,
    showPassword,
    passwordlabel,
    grouped,
    InputProps,
    ...otherProps
  } = props;
  const textFieldProps = {
    InputProps: {
      classes: {
        root: disabled ? classes.disabledRoot : classes.outlinedInput,
        focused: classes.focused,
        notchedOutline: classes.notchedOutline,
        disabled: classes.disabled,
      },
      endAdornment: (
        <InputAdornment position="end">
          {showPassword ? (
            <VisibilityOff
              onClick={onClick}
              classes={{ root: classes.adornmentRoot }}
            />
          ) : (
            <Visibility
              onClick={onClick}
              classes={{ root: classes.adornmentRoot }}
            />
          )}
        </InputAdornment>
      ),
      ...InputProps,
    },
  };
  return (
    <InputElWrapper group={grouped}>
      <InputLabel htmlFor={htmlFor}>{passwordlabel}</InputLabel>
      <TextField
        id={id}
        htmlFor={htmlFor}
        variant="outlined"
        name={name}
        passwordlabel={passwordlabel}
        type={type}
        value={value}
        disabled={disabled}
        onChange={onChange}
        className={classes.textField}
        {...textFieldProps}
        {...otherProps}
      />
    </InputElWrapper>
  );
};

Password.propTypes = {
  id: PropTypes.string,
  value: PropTypes.string,
  htmlFor: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  className: PropTypes.string,
};
export default withStyles(styles)(Password);
