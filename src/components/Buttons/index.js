import React from "react";
import PropTypes from "prop-types";
import { Button } from "./styles";
import { Colors } from "../../assets/themes/theme";

export const SubmitButton = (props) => {
  const {
    children,
    type,
    onClick,
    disabled,
    disableRipple,
    style,
    ...otherProps
  } = props;
  return (
    <Button
      small
      theme={Colors.primary}
      type={type}
      onClick={onClick}
      disabled={disabled}
      disableRipple={disableRipple}
      style={style}
      {...otherProps}
    >
      {children}
    </Button>
  );
};
SubmitButton.propTypes = {
  type: PropTypes.string,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
};

export const CancelButton = (props) => {
  const {
    children,
    type,
    onClick,
    disabled,
    disableRipple,
    style,
    ...otherProps
  } = props;
  return (
    <Button
      small
      cancel
      onClick={onClick}
      style={style}
      disabled={disabled}
      {...otherProps}
    >
      {children}
    </Button>
  );
};
CancelButton.propTypes = {
  type: PropTypes.string,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
};

export const DeleteButton = (props) => {
  const {
    children,
    onClick,
    disabled,
    disableRipple,
    style,
    ...otherProps
  } = props;
  return (
    <Button
      small
      delete
      onClick={onClick}
      style={style}
      disabled={disabled}
      {...otherProps}
    >
      {children}
    </Button>
  );
};
DeleteButton.propTypes = {
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
};

export const Fab = (props) => {
  const { children, onClick, style, ...otherProps } = props;
  return (
    <Button
      fab
      theme={Colors.primary}
      onClick={onClick}
      style={style}
      {...otherProps}
    >
      {children}
    </Button>
  );
};
Fab.propTypes = {
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
};
