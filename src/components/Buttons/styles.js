// Import dependencies
import React from "react";
import styled, { css } from "styled-components";
import { Colors, Fonts } from "../../assets/themes/theme";
// Import Container component
import { Content } from "../contentHolders/styles";

export const ButtonsRow = styled.div`
  display: flex;
  flex-flow: row wrap;
  align-items: center;
  justify-content: flex-end;
  text-align: right;
  width: 100%;
  float: right;
  margin-top: 10px;
  & + & {
    margin-top: 12px;
  }
`;

export const ButtonVariant = styled.div`
  width: 16.6666667%;

  &:nth-of-type(n + 2) {
    text-align: center;
  }
`;

export const Button = styled.button`
  display: ${(props) => (props.small ? "inline-flex" : "inline-block")};
  align-items: center;
  justify-content: center;
  outline: none;
  z-index: 0;
  width: ${(props) =>
    props.fab ? "20px" : props.small ? "auto" : props.width};
  font-size: ${Fonts.font};

  font-family: ${Fonts.secondary};
  letter-spacing: 0.25px;
  background: none;
  box-sizing: border-box;
  text-transform: capitalize;
  border-radius: 5px;
  font-weight: 500;
  outline: none;
  color: ${(props) => (props.ghost ? props.theme : Colors.secondary)};
  background-color: ${(props) => (props.ghost ? "transparent" : props.theme)};
  border: ${(props) => (props.ghost ? `1px solid ${props.theme}` : "none")};
  cursor: ${(props) => (props.disabled ? "not-allowed" : "pointer")};
  & + & {
    margin-top: 12px;
  }

  ${(props) =>
    props.active & !props.ghost &&
    css`
      background-color: ${Colors.primaryActive};
    `};

  ${(props) =>
    props.active & props.ghost &&
    css`
      color: ${Colors.primaryActive};
      border-color: ${Colors.primaryActive};
    `};

  ${(props) =>
    props.hover & props.ghost &&
    css`
      color: ${Colors.primaryHover};
      border-color: ${Colors.primaryHover};
    `};

  ${(props) =>
    props.large | props.ghost &&
    css`
      padding: 14px 18px;
    `};

  ${(props) =>
    props.disabled & !props.ghost &&
    css`
      background-color: ${Colors.primary};
    `};

  ${(props) =>
    props.disabled & props.ghost &&
    css`
      color: ${Colors.primary};
      border-color: ${Colors.primary};
    `};
  ${(props) =>
    props.fab &&
    css`
      display: flex;
      justify-content: center;
      border-radius: 50%;
      min-width: ${(props) => props.minWidth || "20px"};
      min-height: ${(props) => props.minHeight || "20px"};
      width: ${(props) => props.width || "35px"};
      height: ${(props) => props.height || "35px"};
      padding: 10px 16px;
      line-height: 24px;
      &:hover: {
        background: ${Colors.primary};
        color: ${Colors.primary};
      }
    `};

  ${(props) =>
    props.medium &&
    css`
      padding: 10px 16px;
    `};

  ${(props) =>
    props.small &&
    css`
      padding: 10px 14px;
      border: 1px solid ${Colors.primary};
    `};
  ${(props) =>
    props.small & props.delete &&
    css`
      background: transparent;
      border: none;
      color: ${Colors.secondary};
      text-transform: initial;
      padding: 10px 14px;
    `};
  ${(props) =>
    props.small & props.cancel &&
    css`
      background: transparent;
      color: ${Colors.buttonError};
      border: 1px solid ${Colors.buttonError};
      padding: 10px 14px;
    `};
  ${(props) =>
    props.small & props.ghost &&
    css`
      outline: none;
      background: transparent;
      color: ${Colors.primary};
    `};
  ${(props) =>
    props.fab & props.ghost &&
    css`
      outline: none;
      background: transparent;
      color: ${Colors.buttonError};
      border: 1px solid ${Colors.buttonError};
      &:hover: {
        background: transparent;
        color: ${Colors.buttonError};
        border: 1px solid ${Colors.buttonError};
      }
    `};
  ${(props) =>
    props.icon &&
    css`
      i {
        margin-right: 2px;
        font-size: 12px;
      }
    `};
`;

export const Buttons = () => {
  return (
    <Content>
      <h5>Default</h5>
      <ButtonsRow>
        <ButtonVariant>
          <Button theme={Colors.primary} large>
            Large
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primary} large icon>
            <i class="fas fa-bullhorn" /> with icon
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primary} ghost>
            Ghost
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primary} medium>
            Medium
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primary} small>
            Small
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primary} fab>
            +
          </Button>
        </ButtonVariant>
      </ButtonsRow>

      <ButtonsRow>
        <ButtonVariant>
          <Button theme={Colors.seconary} large>
            Large
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.seconary} large icon>
            <i class="fas fa-bullhorn" /> with icon
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.seconary} ghost>
            Ghost
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.seconary} medium>
            Medium
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.seconary} small>
            Small
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.seconary} fab>
            +
          </Button>
        </ButtonVariant>
      </ButtonsRow>

      <ButtonsRow>
        <ButtonVariant>
          <Button theme={Colors.error} large>
            Large
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.error} large icon>
            <i class="fas fa-bullhorn" /> with icon
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.error} ghost>
            Ghost
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.error} medium>
            Medium
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.error} small>
            Small
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.error} fab>
            +
          </Button>
        </ButtonVariant>
      </ButtonsRow>

      <ButtonsRow>
        <ButtonVariant>
          <Button theme={Colors.success} large>
            Large
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.success} large icon>
            <i class="fas fa-bullhorn" /> with icon
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.success} ghost>
            Ghost
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.success} medium>
            Medium
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.success} small>
            Small
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.success} fab>
            +
          </Button>
        </ButtonVariant>
      </ButtonsRow>

      <h5>Hover</h5>
      <ButtonsRow>
        <ButtonVariant>
          <Button theme={Colors.primaryHover} large hover>
            Large
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primaryHover} large hover icon>
            <i class="fas fa-bullhorn" /> with icon
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primaryHover} ghost hover>
            Ghost
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primaryHover} medium hover>
            Medium
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primaryHover} small hover>
            Small
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primaryHover} fab hover>
            +
          </Button>
        </ButtonVariant>
      </ButtonsRow>

      <ButtonsRow>
        <ButtonVariant>
          <Button theme={Colors.seconaryHover} large>
            Large
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.seconaryHover} large icon>
            <i class="fas fa-bullhorn" /> with icon
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.seconaryHover} ghost>
            Ghost
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.seconaryHover} medium>
            Medium
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.seconaryHover} small>
            Small
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.seconaryHover} fab>
            +
          </Button>
        </ButtonVariant>
      </ButtonsRow>

      <ButtonsRow>
        <ButtonVariant>
          <Button theme={Colors.errorHover} large>
            Large
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.errorHover} large icon>
            <i class="fas fa-bullhorn" /> with icon
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.errorHover} ghost>
            Ghost
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.errorHover} medium>
            Medium
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.errorHover} small>
            Small
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.errorHover} fab>
            +
          </Button>
        </ButtonVariant>
      </ButtonsRow>

      <ButtonsRow>
        <ButtonVariant>
          <Button theme={Colors.successHover} large>
            Large
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.successHover} large icon>
            <i class="fas fa-bullhorn" /> with icon
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.successHover} ghost>
            Ghost
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.successHover} medium>
            Medium
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.successHover} small>
            Small
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.successHover} fab>
            +
          </Button>
        </ButtonVariant>
      </ButtonsRow>

      <h5>Active</h5>
      <ButtonsRow>
        <ButtonVariant>
          <Button theme={Colors.primary} large active>
            Large
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primary} large active icon>
            <i class="fas fa-bullhorn" /> with icon
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primary} ghost active>
            Ghost
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primary} medium active>
            Medium
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primary} small active>
            Small
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primary} fab active>
            +
          </Button>
        </ButtonVariant>
      </ButtonsRow>

      <ButtonsRow>
        <ButtonVariant>
          <Button theme={Colors.seconaryActive} large>
            Large
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.seconaryActive} large icon>
            <i class="fas fa-bullhorn" /> with icon
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.seconaryActive} ghost>
            Ghost
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.seconaryActive} medium>
            Medium
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.seconaryActive} small>
            Small
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.seconaryActive} fab>
            +
          </Button>
        </ButtonVariant>
      </ButtonsRow>

      <ButtonsRow>
        <ButtonVariant>
          <Button theme={Colors.errorActive} large>
            Large
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.errorActive} large icon>
            <i class="fas fa-bullhorn" /> with icon
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.errorActive} ghost>
            Ghost
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.errorActive} medium>
            Medium
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.errorActive} small>
            Small
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.errorActive} fab>
            +
          </Button>
        </ButtonVariant>
      </ButtonsRow>

      <ButtonsRow>
        <ButtonVariant>
          <Button theme={Colors.successActive} large>
            Large
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.successActive} large icon>
            <i class="fas fa-bullhorn" /> with icon
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.successActive} ghost>
            Ghost
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.successActive} medium>
            Medium
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.successActive} small>
            Small
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.successActive} fab>
            +
          </Button>
        </ButtonVariant>
      </ButtonsRow>

      <h5>Disabled</h5>
      <ButtonsRow>
        <ButtonVariant>
          <Button theme={Colors.primary} large disabled>
            Large
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primary} large disabled icon>
            <i class="fas fa-bullhorn" /> with icon
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primary} ghost disabled>
            Ghost
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primary} medium disabled>
            Medium
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primary} small disabled>
            Small
          </Button>
        </ButtonVariant>

        <ButtonVariant>
          <Button theme={Colors.primary} fab disabled>
            +
          </Button>
        </ButtonVariant>
      </ButtonsRow>
    </Content>
  );
};

export default Buttons;
