//api methods for get requests
export const getRequest = (actual_path, headers) => {
  const fetchOptions = { method: "GET" };
  //this checks to see if a header is passed and if true adds the headers to the fetch options
  if (headers) {
    fetchOptions.headers = headers;
  }
  return fetch(actual_path, fetchOptions)
    .then((response) => response.json())
    .then((data) => {
      if (data.meta && data.meta.status === "200") {
        return data;
      }
      return Promise.reject({ data });
    });
};
export const postRequest = (actual_path, requestBody, headers) => {
  const fetchOptions = {
    method: "POST",
  };
  if (requestBody) {
    fetchOptions.body = requestBody;
  }
  if (headers) {
    fetchOptions.headers = headers;
  }
  return fetch(actual_path, fetchOptions)
    .then((response) => response.json())
    .then((data) => {
      if (data.meta && data.meta.status === "200") {
        return data;
      }
      return Promise.reject({ data });
    });
};
