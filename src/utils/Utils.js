/**
 * timeout and redux dispatch clearer
 * */

export const timeout = (ms, promise) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      reject("Access time out");
    }, ms);
    promise.then(resolve, reject);
  });
};
export const dispatchAndRefresh = (errorAction, refreshAction) => (
  dispatch
) => {
  dispatch(errorAction);
  return setTimeout(() => dispatch(refreshAction), 5000);
};
