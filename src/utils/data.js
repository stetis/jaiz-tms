export const Countries = [
  { code: "AF", name: "Afghanistan", "dial-code": "+93" },
  { code: "AL", name: "Albania", "dial-code": "+355" },
  { code: "DZ", name: "Algeria", "dial-code": "+213" },
  { code: "AD", name: "Andorra", "dial-code": "+376" },
  { code: "AO", name: "Angola", "dial-code": "+244" },
  { code: "AG", name: "Antigua and Barbuda", "dial-code": "+1-268" },
  { code: "AR", name: "Argentina", "dial-code": "+54" },
  { code: "AW", name: "Armenia", "dial-code": "+297" },
  { code: "AU", name: "Australia", "dial-code": "+61" },
  { code: "AT", name: "Austria", "dial-code": "+43" },
  { code: "AZ", name: "Azerbaijan", "dial-code": "+994" },
  { code: "BS", name: "Bahamas", "dial-code": "+1-242" },
  { code: "BH", name: "Bahrain", "dial-code": "+973" },
  { code: "BD", name: "Bangladesh", "dial-code": "+880" },
  { code: "BB", name: "Barbados", "dial-code": "+1-246" },
  { code: "BY", name: "Belarus", "dial-code": "+375" },
  { code: "BE", name: "Belgium", "dial-code": "+32" },
  { code: "BZ", name: "Belize", "dial-code": "+501" },
  { code: "BJ", name: "Benin", "dial-code": "+229" },
  { code: "BT", name: "Bhutan", "dial-code": "+975" },
  { code: "BO", name: "Bolivia", "dial-code": "+591" },
  { code: "BA", name: "Bosnia and Herzegovina", "dial-code": "+387" },
  { code: "BW", name: "Botswana", "dial-code": "+267" },
  { code: "BR", name: "Brazil", "dial-code": "+55" },
  { code: "BN", name: "Brunei", "dial-code": "+673" },
  { code: "BG", name: "Bulgaria", "dial-code": "+359" },
  { code: "BF", name: "Burkina Faso", "dial-code": "+226" },
  { code: "BI", name: "Burundi", "dial-code": "+257" },
  { code: "KH", name: "Cambodia", "dial-code": "+855" },
  { code: "CM", name: "Cameroon", "dial-code": "+237" },
  { code: "CA", name: "Canada", "dial-code": "+1" },
  { code: "CV", name: "Cape Verde", "dial-code": "+238" },
  { code: "CF", name: "Central Africa Republic", "dial-code": "+236" },
  { code: "TD", name: "Chad", "dial-code": "+235" },
  { code: "CL", name: "Chile", "dial-code": "+56" },
  { code: "CN", name: "China", "dial-code": "+86" },
  { code: "CO", name: "Colombia", "dial-code": "+57" },
  { code: "KM", name: "Comoros", "dial-code": "+269" },
  { code: "CR", name: "Costa Rica", "dial-code": "+506" },
  { code: "HR", name: "Croatia", "dial-code": "+385" },
  { code: "CU", name: "Cuba", "dial-code": "+53" },
  { code: "CY", name: "Cyprus", "dial-code": "+357" },
  { code: "CZ", name: "Czech Republic", "dial-code": "+420" },
  { code: "CD", name: "Democratic Rep. of Congo", "dial-code": "+243" },
  { code: "Dk", name: "Denmark", "dial-code": "000" },
  { code: "DJ", name: "Djibouti", "dial-code": "+253" },
  { code: "DM", name: "Dominica", "dial-code": "+1-767" },
  { code: "DO", name: "Dominican Republic", "dial-code": "+1-809" },
  { code: "TL", name: "East Timor", "dial-code": "+670" },
  { code: "EC", name: "Ecuador", "dial-code": "+593" },
  { code: "EG", name: "Egypt", "dial-code": "+20" },
  { code: "SV", name: "El Salvador", "dial-code": "+503" },
  { code: "GQ", name: "Equatorial Guinea", "dial-code": "+240" },
  { code: "ER", name: "Eritrea", "dial-code": "+291" },
  { code: "EE", name: "Estonia", "dial-code": "+372" },
  { code: "ET", name: "Ethiopia", "dial-code": "+251" },
  { code: "FJ", name: "Fiji", "dial-code": "+679" },
  { code: "FI", name: "Finland", "dial-code": "+358" },
  { code: "FR", name: "France", "dial-code": "+33" },
  { code: "GA", name: "Gabon", "dial-code": "+241" },
  { code: "GM", name: "Gambia", "dial-code": "+220" },
  { code: "GE", name: "Georgia", "dial-code": "+995" },
  { code: "DE", name: "Germany", "dial-code": "+49" },
  { code: "GH", name: "Ghana", "dial-code": "+233" },
  { code: "GR", name: "Greece", "dial-code": "+30" },
  { code: "GD", name: "Grenada", "dial-code": "+1-473" },
  { code: "GT", name: "Guatemala", "dial-code": "+502" },
  { code: "GN", name: "Guinea ", "dial-code": "+224" },
  { code: "GW", name: "Guinea Bissau", "dial-code": "+245" },
  { code: "GY", name: "Guyana", "dial-code": "+592" },
  { code: "HT", name: "Haiti", "dial-code": "+509" },
  { code: "HN", name: "Honduras", "dial-code": "+504" },
  { code: "HU", name: "Hungary", "dial-code": "+36" },
  { code: "IS", name: "Iceland", "dial-code": "+354" },
  { code: "IN ", name: "India", "dial-code": "000" },
  { code: "ID", name: "Indonesia", "dial-code": "+62" },
  { code: "IR", name: "Iran", "dial-code": "+98" },
  { code: "IQ", name: "Iraq", "dial-code": "+964" },
  { code: "IE", name: "Ireland", "dial-code": "+353" },
  { code: "IL", name: "Israel", "dial-code": "+972" },
  { code: "IT", name: "Italy", "dial-code": "+39" },
  { code: "CI", name: "Ivory Coast", "dial-code": "+225" },
  { code: "JM", name: "Jamaica", "dial-code": "+1-876" },
  { code: "JP", name: "Japan", "dial-code": "+81" },
  { code: "JO", name: "Jordan", "dial-code": "+962" },
  { code: "KZ", name: "Kazakhstan", "dial-code": "+7" },
  { code: "KE", name: "Kenya", "dial-code": "+254" },
  { code: "KI", name: "Kiribati", "dial-code": "+686" },
  { code: "XK", name: "Kosovo", "dial-code": "+383" },
  { code: "KW", name: "Kuwait", "dial-code": "+965" },
  { code: "KG", name: "Kyrgyzstan", "dial-code": "+996" },
  { code: "LA", name: "Laos", "dial-code": "+856" },
  { code: "LV", name: "Latvia", "dial-code": "+371" },
  { code: "LB", name: "Lebanon", "dial-code": "+961" },
  { code: "LS", name: "Lesotho", "dial-code": "+266" },
  { code: "LR", name: "Liberia", "dial-code": "+231" },
  { code: "LY", name: "Libya", "dial-code": "+218" },
  { code: "LI", name: "Liechtenstein", "dial-code": "+423" },
  { code: "LT", name: "Lithuania", "dial-code": "+370" },
  { code: "LU", name: "Luxembourg", "dial-code": "+352" },
  { code: "MK", name: "Macedonia (FYROM)", "dial-code": "+389" },
  { code: "MG", name: "Madagascar", "dial-code": "+261" },
  { code: "MW", name: "Malawi", "dial-code": "+265" },
  { code: "MY", name: "Malaysia", "dial-code": "+60" },
  { code: "MV", name: "Maldives", "dial-code": "+960" },
  { code: "ML", name: "Mali", "dial-code": "+223" },
  { code: "MT", name: "Malta", "dial-code": "+356" },
  { code: "MH", name: "Marshall Islands", "dial-code": "+692" },
  { code: "MR", name: "Mauritania", "dial-code": "+222" },
  { code: "MU", name: "Mauritius", "dial-code": "+230" },
  { code: "MX", name: "Mexico", "dial-code": "+52" },
  { code: "FM", name: "Micronesia", "dial-code": "+691" },
  { code: "MD", name: "Moldova", "dial-code": "+373" },
  { code: "MC", name: "Monaco", "dial-code": "+377" },
  { code: "MN", name: "Mongolia", "dial-code": "+976" },
  { code: "ME", name: "Montenegro", "dial-code": "+382" },
  { code: "MA", name: "Morocco", "dial-code": "+212" },
  { code: "MZ", name: "Mozambique", "dial-code": "+258" },
  { code: "MM", name: "Myanmar (Burma)", "dial-code": "+95" },
  { code: "NA", name: "Nambia", "dial-code": "+264" },
  { code: "NR", name: "Nauru", "dial-code": "+674" },
  { code: "NP", name: "Nepal", "dial-code": "+977" },
  { code: "NL", name: "Netherlands", "dial-code": "+31" },
  { code: "NZ", name: "New Zealand", "dial-code": "+64" },
  { code: "NI", name: "Nicaragua", "dial-code": "+505" },
  { code: "NE", name: "Niger", "dial-code": "+227" },
  { code: "NG", name: "Nigeria", "dial-code": "+234" },
  { code: "KP", name: "North Korea", "dial-code": "+850" },
  { code: "NO", name: "Norway", "dial-code": "+47" },
  { code: "OM", name: "Oman", "dial-code": "+968" },
  { code: "PK", name: "Pakistan", "dial-code": "+92" },
  { code: "PW", name: "Palau", "dial-code": "+680" },
  { code: "PS", name: "Palestine", "dial-code": "+970" },
  { code: "PA", name: "Panama", "dial-code": "+507" },
  { code: "PG", name: "Papua New Guinea", "dial-code": "+675" },
  { code: "PY", name: "Paraguay", "dial-code": "+595" },
  { code: "PE", name: "Peru", "dial-code": "+51" },
  { code: "PH", name: "Philippines", "dial-code": "+63" },
  { code: "PL", name: "Poland", "dial-code": "+48" },
  { code: "PT", name: "Portugal", "dial-code": "+351" },
  { code: "QA", name: "Qatar", "dial-code": "+974" },
  { code: "CG", name: "Rep. of Congo", "dial-code": "+242" },
  { code: "SS", name: "Rep. of South Sundan", "dial-code": "+211" },
  { code: "RO", name: "Romania", "dial-code": "+40" },
  { code: "RU", name: "Russia", "dial-code": "+7" },
  { code: "RW", name: "Rwanda", "dial-code": "+250" },
  { code: "KN", name: "Saint Kitts and Nevis", "dial-code": "+1-869" },
  { code: "LC", name: "Saint Lucia", "dial-code": "+1-758" },
  {
    code: "VC",
    name: "Saint Vincent and the Grenadines",
    "dial-code": "+1-784",
  },
  { code: "WS", name: "Samoa", "dial-code": "+685" },
  { code: "SM", name: "San Marino", "dial-code": "+378" },
  { code: "SA", name: "Saudi Arabia", "dial-code": "+966" },
  { code: "SN", name: "Senegal", "dial-code": "+221" },
  { code: "RS", name: "Serbia", "dial-code": "+381" },
  { code: "SC", name: "Seychelles", "dial-code": "+248" },
  { code: "SL", name: "Sierra Leone", "dial-code": "+232" },
  { code: "SG", name: "Singapore", "dial-code": "+65" },
  { code: "SK", name: "Slovakia", "dial-code": "+421" },
  { code: "SI", name: "Slovenia", "dial-code": "+386" },
  { code: "ST", name: "Soa Tome & Principle", "dial-code": "+239" },
  { code: "SB", name: "Solomon Islands", "dial-code": "+677" },
  { code: "SO", name: "Somalia", "dial-code": "+252" },
  { code: "ZA", name: "South Africa", "dial-code": "+27" },
  { code: "KR", name: "South Korea", "dial-code": "+82" },
  { code: "ES", name: "Spain", "dial-code": "+34" },
  { code: "LK", name: "Sri Lanka", "dial-code": "+94" },
  { code: "SD", name: "Sudan (North)", "dial-code": "+249" },
  { code: "SR", name: "Suriname", "dial-code": "+597" },
  { code: "SZ", name: "Swaziland", "dial-code": "+268" },
  { code: "SE", name: "Sweden", "dial-code": "+46" },
  { code: "CH", name: "Switzerland", "dial-code": "+41" },
  { code: "SY", name: "Syria", "dial-code": "+963" },
  { code: "TW", name: "Taiwan", "dial-code": "+886" },
  { code: "TJ", name: "Tajikistan", "dial-code": "+992" },
  { code: "TZ", name: "Tanzania", "dial-code": "+255" },
  { code: "TH", name: "Thailand", "dial-code": "+66" },
  { code: "TG", name: "Togo", "dial-code": "+228" },
  { code: "TO", name: "Tonga", "dial-code": "+676" },
  { code: "TT", name: "Trinidad and Tobago", "dial-code": "+1-868" },
  { code: "TN", name: "Tunisia", "dial-code": "+216" },
  { code: "TR", name: "Turkey", "dial-code": "+90" },
  { code: "TM", name: "Turkmenistan", "dial-code": "+993" },
  { code: "TV", name: "Tuvalu", "dial-code": "+688" },
  { code: "UG", name: "Uganda", "dial-code": "+256" },
  { code: "UA", name: "Ukraine", "dial-code": "+380" },
  { code: "AE", name: "United Arab Emirates", "dial-code": "+971" },
  { code: "GB", name: "United Kingdom", "dial-code": "+44" },
  { code: "US", name: "United States of America", "dial-code": "+1" },
  { code: "UY", name: "Uruguay", "dial-code": "+598" },
  { code: "UZ", name: "Uzbekistan", "dial-code": "+998" },
  { code: "VU", name: "Vanuatu", "dial-code": "+678" },
  { code: "VA", name: "Vatican City ", "dial-code": "+379" },
  { code: "VE", name: "Venezuela", "dial-code": "+58" },
  { code: "VN", name: "Vietnam", "dial-code": "+84" },
  { code: "YE", name: "Yemen", "dial-code": "+967" },
  { code: "ZM", name: "Zambia", "dial-code": "+260" },
  { code: "ZW", name: "Zimbabwe", "dial-code": "+263" },
];

export const states = [
  {
    code: "FC",
    name: "Abuja",
    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    lgs: [
      {
        code: "FC01",
        name: "Abuja municipal area council",
      },
      {
        code: "FC02",
        name: "Bwari",
      },
      {
        code: "FC03",
        name: "Kwali",
      },
      {
        code: "FC04",
        name: "Gwagwalada",
      },
      {
        code: "FC05",
        name: "Kuje",
      },
      {
        code: "FC06",
        name: "Abaji",
      },
    ],
  },
  {
    code: "AB",
    name: "Abia",
    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    lgs: [
      {
        code: "AB01",
        name: "Aba North",
      },
      {
        code: "AB02",
        name: "Aba South",
      },
      {
        code: "AB03",
        name: "Umuahia South",
      },
      {
        code: "AB04",
        name: "Arochukwu",
      },
      {
        code: "AB05",
        name: "Bende",
      },
      {
        code: "AB06",
        name: "Ikwuano",
      },
      {
        code: "AB07",
        name: "Isiala Ngwa North",
      },
      {
        code: "AB08",
        name: "Isiala Ngwa South",
      },
      {
        code: "AB09",
        name: "Isuikwuato",
      },
      {
        code: "AB010",
        name: "Obi Ngwa",
      },
      {
        code: "AB011",
        name: "Ohafia",
      },
      {
        code: "AB012",
        name: "Osisioma Ngwa",
      },
      {
        code: "AB013",
        name: "Ugwunagbo",
      },
      {
        code: "AB014",
        name: "Ukwa East",
      },
      {
        code: "AB015",
        name: "Ukwa West",
      },
      {
        code: "AB016",
        name: "Umuahia North",
      },
      {
        code: "AB017",
        name: "Umu Nneochi",
      },
    ],
  },
  {
    code: "AD",
    name: "Adamawa",
    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    lgs: [
      {
        code: "AD01",
        name: "Demsa",
      },
      {
        code: "AD02",
        name: "Fufore",
      },
      {
        code: "AD03",
        name: "Ganye",
      },
      {
        code: "AD04",
        name: "Girei",
      },
      {
        code: "AD05",
        name: "Gombi",
      },
      {
        code: "AD06",
        name: "Guyuk",
      },
      {
        code: "AD07",
        name: "Hong",
      },
      {
        code: "AD08",
        name: "Jada",
      },
      {
        code: "AD09",
        name: "Lamrude",
      },
      {
        code: "AD010",
        name: "Madagali",
      },
      {
        code: "AD011",
        name: "Maiha",
      },
      {
        code: "AD012",
        name: "Mayo-Belwa",
      },
      {
        code: "AD013",
        name: "Michika",
      },
      {
        code: "AD014",
        name: "Mubi North",
      },
      {
        code: "AD015",
        name: "Mubi South",
      },
      {
        code: "AD016",
        name: "Numan",
      },
      {
        code: "AD017",
        name: "Shelleng",
      },
      {
        code: "AD018",
        name: "Song",
      },
      {
        code: "AD019",
        name: "Toungo",
      },
      {
        code: "AD020",
        name: "Yola North",
      },
      {
        code: "AD021",
        name: "Yola South",
      },
    ],
  },
  {
    code: "AK",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Akwa Ibom",
    lgs: [
      {
        code: "AK01",
        name: "Abak",
      },
      {
        code: "AK02",
        name: "Eastern Obolo",
      },
      {
        code: "AK03",
        name: "Eket",
      },
      {
        code: "AK04",
        name: "Esit-Eket",
      },
      {
        code: "AK05",
        name: "Essien Udim",
      },
      {
        code: "AK06",
        name: "Etim-Ekpo",
      },
      {
        code: "AK07",
        name: "Etinan",
      },
      {
        code: "AK08",
        name: "Ibeno",
      },
      {
        code: "AK09",
        name: "Ibesikpo-Astuan",
      },
      {
        code: "AK010",
        name: "Ibiono-Ibiom",
      },
      {
        code: "AK011",
        name: "Ika",
      },
      {
        code: "AK012",
        name: "Ikono",
      },
      {
        code: "AK013",
        name: "Ikot Abasi",
      },
      {
        code: "AK014",
        name: "Ikot Ekpene",
      },
      {
        code: "AK015",
        name: "Ini",
      },
      {
        code: "AK016",
        name: "Itu",
      },
      {
        code: "AK017",
        name: "Mbo",
      },
      {
        code: "AK018",
        name: "Mkpat-Enin",
      },
      {
        code: "AK019",
        name: "Nsit-Atai",
      },
      {
        code: "AK020",
        name: "Nsit-Ibom",
      },
      {
        code: "AK021",
        name: "Nsit-Ubium",
      },
      {
        code: "AK022",
        name: "Obot-Akara",
      },
      {
        code: "AK023",
        name: "Okobo",
      },
      {
        code: "AK024",
        name: "Onna",
      },
      {
        code: "AK025",
        name: "Oron",
      },
      {
        code: "AK026",
        name: "Oruk Anam",
      },
      {
        code: "AK027",
        name: "Ukanafun",
      },
      {
        code: "AK028",
        name: "Udung-Uko",
      },
      {
        code: "AK029",
        name: "Uruan",
      },
      {
        code: "AK030",
        name: "Urue-Offong/Oruko",
      },
      {
        code: "AK031",
        name: "Uyo",
      },
    ],
  },
  {
    code: "AN",
    name: "Anambra",
    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    lgs: [
      {
        code: "AN01",
        name: "Aguata",
      },
      {
        code: "AN02",
        name: "Awka North",
      },
      {
        code: "AN03",
        name: "Awka South",
      },
      {
        code: "AN04",
        name: "Anambra East",
      },
      {
        code: "AN05",
        name: "Anambra West",
      },
      {
        code: "AN06",
        name: "Anaocha",
      },
      {
        code: "AN07",
        name: "Ayamelum",
      },
      {
        code: "AN08",
        name: "Dunukofia",
      },
      {
        code: "AN09",
        name: "Ekwusigo",
      },
      {
        code: "AN010",
        name: "Idemili North",
      },
      {
        code: "AN011",
        name: "Idemili South",
      },
      {
        code: "AN012",
        name: "Ihiala",
      },
      {
        code: "AN013",
        name: "Njikoka",
      },
      {
        code: "AN014",
        name: "Nnewi North",
      },
      {
        code: "AN015",
        name: "Nnewi South",
      },
      {
        code: "AN016",
        name: "Ogbaru",
      },
      {
        code: "AN017",
        name: "Onitsha North",
      },
      {
        code: "AN018",
        name: "Onitsha South",
      },
      {
        code: "AN019",
        name: "Orumba North",
      },
      {
        code: "AN020",
        name: "Orumba South",
      },
      {
        code: "AN021",
        name: "Oyi",
      },
    ],
  },
  {
    code: "BA",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Bauchi",
    lgs: [
      {
        code: "BA01",
        name: "Alkaleri",
      },
      {
        code: "BA02",
        name: "Bauchi",
      },
      {
        code: "BA03",
        name: "Bogoro",
      },
      {
        code: "BA04",
        name: "Darazo",
      },
      {
        code: "BA05",
        name: "Dass",
      },
      {
        code: "BA06",
        name: "Dukku",
      },
      {
        code: "BA07",
        name: "Gamawa",
      },
      {
        code: "BA08",
        name: "Ganjuwa",
      },
      {
        code: "BA09",
        name: "Misau",
      },
      {
        code: "BA010",
        name: "Ningi",
      },
      {
        code: "BA011",
        name: "Toro",
      },
      {
        code: "BA012",
        name: "Tafawa Balewa",
      },
      {
        code: "BA013",
        name: "Kirfi",
      },
      {
        code: "BA014",
        name: "Giade",
      },
      {
        code: "BA015",
        name: "Shira",
      },
      {
        code: "BA016",
        name: "Jamaare",
      },
      {
        code: "BA017",
        name: "Katagum",
      },
      {
        code: "BA018",
        name: "Itas/Gadau",
      },
      {
        code: "BA019",
        name: "Zaki",
      },
      {
        code: "BA020",
        name: "Damban",
      },
    ],
  },
  {
    code: "BY",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Bayelsa",
    lgs: [
      {
        code: "BY01",
        name: "Brass",
      },
      {
        code: "BY02",
        name: "Ekeremor",
      },
      {
        code: "BY03",
        name: "Kolokuma/Opokuma",
      },
      {
        code: "BY04",
        name: "Nembe",
      },
      {
        code: "BY05",
        name: "Ogbia",
      },
      {
        code: "BY06",
        name: "Sagbama",
      },
      {
        code: "BY07",
        name: "Southern Ijaw",
      },
      {
        code: "BY08",
        name: "Yenagoa",
      },
    ],
  },
  {
    code: "BE",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Benue",
    lgs: [],
  },
  {
    code: "BO",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Borno",
    lgs: [
      {
        code: "BO01",
        name: "Abadam",
      },
      {
        code: "BO02",
        name: "Askira/Uba",
      },
      {
        code: "BO03",
        name: "Bama",
      },
      {
        code: "BO04",
        name: "Bayo",
      },
      {
        code: "BO05",
        name: "Biu",
      },
      {
        code: "BO06",
        name: "Chibok",
      },
      {
        code: "BO07",
        name: "Damboa",
      },
      {
        code: "BO08",
        name: "Dikwa",
      },
      {
        code: "BO09",
        name: "Gwoza",
      },
      {
        code: "BO010",
        name: "Gubio",
      },
      {
        code: "BO011",
        name: "Guzamala",
      },
      {
        code: "BO012",
        name: "Hawul",
      },
      {
        code: "BO013",
        name: "Jere",
      },
      {
        code: "BO014",
        name: "Kaga",
      },
      {
        code: "BO015",
        name: "Kala/Balge",
      },
      {
        code: "BO016",
        name: "Konduga",
      },
      {
        code: "BO017",
        name: "Kukawa",
      },
      {
        code: "BO018",
        name: "Kwaya Kusar",
      },
      {
        code: "BO019",
        name: "Magumeri",
      },
      {
        code: "BO020",
        name: "Marte",
      },
      {
        code: "BO021",
        name: "Mobbar",
      },
      {
        code: "BO022",
        name: "Monguno",
      },
      {
        code: "BO023",
        name: "Mafa",
      },
      {
        code: "BO024",
        name: "Maiduguri",
      },
      {
        code: "BO025",
        name: "Nganzai",
      },
      {
        code: "BO026",
        name: "Ngala",
      },
      {
        code: "BO027",
        name: "Shani",
      },
      {
        code: "BO028",
        name: "Maiduguri",
      },
    ],
  },
  {
    code: "CR",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Cross River",
    lgs: [
      {
        code: "CR01",
        name: "Abi",
      },
      {
        code: "CR02",
        name: "Akamkpa",
      },
      {
        code: "CR03",
        name: "Akpabuyo",
      },
      {
        code: "CR04",
        name: "Bekwarra",
      },
      {
        code: "CR05",
        name: "Bakassi",
      },
      {
        code: "CR06",
        name: "Biase",
      },
      {
        code: "CR07",
        name: "Boki",
      },
      {
        code: "CR08",
        name: "Calabar Municipal",
      },
      {
        code: "CR09",
        name: "Calabar South",
      },
      {
        code: "CR010",
        name: "Etung",
      },
      {
        code: "CR011",
        name: "Ikom",
      },
      {
        code: "CR012",
        name: "Obanliku",
      },
      {
        code: "CR013",
        name: "Obubra",
      },
      {
        code: "CR014",
        name: "Obudu",
      },
      {
        code: "CR015",
        name: "Odukpani",
      },
      {
        code: "CR016",
        name: "Ogoja",
      },
      {
        code: "CR017",
        name: "Yakuur",
      },
      {
        code: "CR018",
        name: "Yala",
      },
    ],
  },
  {
    code: "DE",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Delta",
    lgs: [
      {
        code: "DE01",
        name: "Aniocha North",
      },
      {
        code: "DE02",
        name: "Aniocha South",
      },
      {
        code: "DE03",
        name: "Burutu",
      },
      {
        code: "DE04",
        name: "Bomadi",
      },
      {
        code: "DE05",
        name: "Ethiope East",
      },
      {
        code: "DE06",
        name: "Ethiope West",
      },
      {
        code: "DE07",
        name: "Ika North",
      },
      {
        code: "DE08",
        name: "Ika South",
      },
      {
        code: "DE09",
        name: "Isoko North",
      },
      {
        code: "DE010",
        name: "Isoko South",
      },
      {
        code: "DE011",
        name: "Ndokwa East",
      },
      {
        code: "DE012",
        name: "Ndokwa West",
      },
      {
        code: "DE013",
        name: "Oshimili North",
      },
      {
        code: "DE014",
        name: "Oshimili South",
      },
      {
        code: "DE015",
        name: "Okpe",
      },
      {
        code: "DE016",
        name: "Patani",
      },
      {
        code: "DE017",
        name: "Sapele",
      },
      {
        code: "DE018",
        name: "Udu",
      },
      {
        code: "DE019",
        name: "Ughelli North",
      },
      {
        code: "DE020",
        name: "Ughelli South",
      },
      {
        code: "DE021",
        name: "Ukwuani",
      },
      {
        code: "DE022",
        name: "Uvwie",
      },
      {
        code: "DE023",
        name: "Warri North",
      },
      {
        code: "DE024",
        name: "Warri South",
      },
      {
        code: "DE025",
        name: "Warri South West",
      },
    ],
  },
  {
    code: "EB",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Ebonyi",
    lgs: [
      {
        code: "EB01",
        name: "Abakaliki",
      },
      {
        code: "EB02",
        name: "Afikpo North",
      },
      {
        code: "EB03",
        name: "Afikpo South (Edda)",
      },
      {
        code: "EB04",
        name: "Ebonyi",
      },
      {
        code: "EB05",
        name: "Ezza North",
      },
      {
        code: "EB06",
        name: "Ezza South",
      },
      {
        code: "EB07",
        name: "Ikwo",
      },
      {
        code: "EB08",
        name: "Ishielu",
      },
      {
        code: "EB09",
        name: "Ivo",
      },
      {
        code: "EB010",
        name: "Izzi",
      },
      {
        code: "EB011",
        name: "Ohaozara",
      },
      {
        code: "EB012",
        name: "Ohaukwu",
      },
      {
        code: "EB013",
        name: "Onicha",
      },
    ],
  },
  {
    code: "ED",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Edo",
    lgs: [
      {
        code: "ED01",
        name: "Akoko-Edo",
      },
      {
        code: "ED02",
        name: "Egor",
      },
      {
        code: "ED03",
        name: "Esan Central",
      },
      {
        code: "ED04",
        name: "Esan North-East",
      },
      {
        code: "ED05",
        name: "Esan South-East",
      },
      {
        code: "ED06",
        name: "Esan West",
      },
      {
        code: "ED07",
        name: "Etsako Central",
      },
      {
        code: "ED08",
        name: "Etsako East",
      },
      {
        code: "ED09",
        name: "Etsako West",
      },
      {
        code: "ED010",
        name: "Igueben",
      },
      {
        code: "ED011",
        name: "Ikpoba-Okha",
      },
      {
        code: "ED012",
        name: "Oredo",
      },
      {
        code: "ED013",
        name: "Orhionmwon",
      },
      {
        code: "ED014",
        name: "Ovia North-East",
      },
      {
        code: "ED015",
        name: "Ovia South-West",
      },
      {
        code: "ED016",
        name: "Owan East",
      },
      {
        code: "ED017",
        name: "Owan West",
      },
      {
        code: "ED018",
        name: "Uhunmwonde",
      },
    ],
  },
  {
    code: "EK",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Ekiti",
    lgs: [
      {
        code: "Ek01",
        name: "Ado-Ekiti",
      },
      {
        code: "Ek02",
        name: "Ikere",
      },
      {
        code: "Ek03",
        name: "Oye",
      },
      {
        code: "Ek04",
        name: "Aiyekire (Gbonyin)",
      },
      {
        code: "Ek05",
        name: "Efon",
      },
      {
        code: "Ek06",
        name: "Ekiti East",
      },
      {
        code: "Ek07",
        name: "Ekiti South-West",
      },
      {
        code: "Ek08",
        name: "Ekiti West",
      },
      {
        code: "Ek09",
        name: "Emure",
      },
      {
        code: "Ek010",
        name: "Ido-Osi",
      },
      {
        code: "Ek011",
        name: "Ijero",
      },
      {
        code: "Ek012",
        name: "Ikole",
      },
      {
        code: "Ek013",
        name: "Ilejemeje",
      },
      {
        code: "Ek014",
        name: "Irepodun/Ifelodun",
      },
      {
        code: "Ek015",
        name: "Ise/Orun",
      },
      {
        code: "Ek016",
        name: "Moba",
      },
    ],
  },
  {
    code: "EN",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Enugu",
    lgs: [
      {
        code: "EN01",
        name: "Aninri",
      },
      {
        code: "EN02",
        name: "Awgu",
      },
      {
        code: "EN03",
        name: "Enugu East",
      },
      {
        code: "EN04",
        name: "Enugu North",
      },
      {
        code: "EN05",
        name: "Enugu South",
      },
      {
        code: "EN06",
        name: "Ezeagu",
      },
      {
        code: "EN07",
        name: "Igbo Etiti",
      },
      {
        code: "EN08",
        name: "Igbo Eze North",
      },
      {
        code: "EN09",
        name: "Igbo Eze South",
      },
      {
        code: "EN010",
        name: "Isi Uzo",
      },
      {
        code: "EN011",
        name: "Nkanu East",
      },
      {
        code: "EN012",
        name: "Nkanu West",
      },
      {
        code: "EN013",
        name: "Nsukka",
      },
      {
        code: "EN014",
        name: "Oji River",
      },
      {
        code: "EN015",
        name: "Udenu",
      },
      {
        code: "EN016",
        name: "Udi",
      },
      {
        code: "EN017",
        name: "Uzo-Uwani",
      },
    ],
  },
  {
    code: "GO",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Gombe",
    lgs: [
      {
        code: "GO01",
        name: "Akko",
      },
      {
        code: "GO02",
        name: "Balanga",
      },
      {
        code: "GO03",
        name: "Billiri",
      },
      {
        code: "GO04",
        name: "Dukku",
      },
      {
        code: "GO05",
        name: "Funakaye",
      },
      {
        code: "GO06",
        name: "Gombe",
      },
      {
        code: "GO07",
        name: "Kaltungo",
      },
      {
        code: "GO08",
        name: "Kwami",
      },
      {
        code: "GO09",
        name: "Nafada",
      },
      {
        code: "GO010",
        name: "Shongom",
      },
      {
        code: "GO011",
        name: "Yamaltu/Deba",
      },
    ],
  },
  {
    code: "IM",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Imo",
    lgs: [
      {
        code: "IM01",
        name: "Aboh Mbaise",
      },
      {
        code: "IM02",
        name: "Ahiazu Mbaise",
      },
      {
        code: "IM03",
        name: "Ehime Mbano",
      },
      {
        code: "IM04",
        name: "Ezinihitte Mbaise",
      },
      {
        code: "IM05",
        name: "Ideato North",
      },
      {
        code: "IM06",
        name: "Ideato South",
      },
      {
        code: "IM07",
        name: "Ihitte/Uboma",
      },
      {
        code: "IM08",
        name: "Ikeduru",
      },
      {
        code: "IM09",
        name: "Isiala Mbano",
      },
      {
        code: "IM010",
        name: "Isu",
      },
      {
        code: "IM011",
        name: "Mbaitoli",
      },
      {
        code: "IM012",
        name: "Ngor Okpala",
      },
      {
        code: "IM013",
        name: "Njaba",
      },
      {
        code: "IM014",
        name: "Nkwerre",
      },
      {
        code: "IM015",
        name: "Nwangele",
      },
      {
        code: "IM016",
        name: "Obowo",
      },
      {
        code: "IM017",
        name: "Oguta",
      },
      {
        code: "IM018",
        name: "Ohaji/Egbema",
      },
      {
        code: "IM019",
        name: "Okigwe",
      },
      {
        code: "IM020",
        name: "Onuimo",
      },
      {
        code: "IM021",
        name: "Orlu",
      },
      {
        code: "IM022",
        name: "Orsu",
      },
      {
        code: "IM023",
        name: "Oru East",
      },
      {
        code: "IM024",
        name: "Oru West",
      },
      {
        code: "IM025",
        name: "Owerri Municipal",
      },
      {
        code: "IM026",
        name: "Owerri North",
      },
      {
        code: "IM027",
        name: "Owerri West",
      },
      {
        code: "IM028",
        name: "Njaba South",
      },
    ],
  },
  {
    code: "JI",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Jigawa",
    lgs: [
      {
        code: "JI01",
        name: "Auyo",
      },
      {
        code: "JI02",
        name: "Babura",
      },
      {
        code: "JI03",
        name: "Biriniwa",
      },
      {
        code: "JI04",
        name: "Birnin Kudu",
      },
      {
        code: "JI05",
        name: "Buji",
      },
      {
        code: "JI06",
        name: "Dutse",
      },
      {
        code: "JI07",
        name: "Gagarawa",
      },
      {
        code: "JI08",
        name: "Garki",
      },
      {
        code: "JI09",
        name: "Gumel",
      },
      {
        code: "JI010",
        name: "Guri",
      },
      {
        code: "JI011",
        name: "Gwaram",
      },
      {
        code: "JI012",
        name: "Gwiwa",
      },
      {
        code: "JI013",
        name: "Hadejia",
      },
      {
        code: "JI014",
        name: "Jahun",
      },
      {
        code: "JI015",
        name: "Kafin Hausa",
      },
      {
        code: "JI016",
        name: "Kaugama",
      },
      {
        code: "JI017",
        name: "Kazaure",
      },
      {
        code: "JI018",
        name: "Kiri Kasama",
      },
      {
        code: "JI019",
        name: "Kiyawa",
      },
      {
        code: "JI020",
        name: "Maigatari",
      },
      {
        code: "JI021",
        name: "Malam Madori",
      },
      {
        code: "JI022",
        name: "Miga",
      },
      {
        code: "JI023",
        name: "Ringim",
      },
      {
        code: "JI024",
        name: "Roni",
      },
      {
        code: "JI025",
        name: "Sule Tankarkar",
      },
      {
        code: "JI026",
        name: "Taura",
      },
      {
        code: "JI027",
        name: "Yankwashi",
      },
    ],
  },
  {
    code: "KD",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Kaduna",
    lgs: [],
  },
  {
    code: "KN",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Kano",
    lgs: [
      {
        code: "KN01",
        name: "Ajingi",
      },
      {
        code: "KN02",
        name: "Albasu",
      },
      {
        code: "KN03",
        name: "Bagwai",
      },
      {
        code: "KN04",
        name: "Bebeji",
      },
      {
        code: "KN05",
        name: "Bichi",
      },
      {
        code: "KN06",
        name: "Bunkure",
      },
      {
        code: "KN07",
        name: "Dala",
      },
      {
        code: "KN08",
        name: "Dambatta",
      },
      {
        code: "KN09",
        name: "Dawakin Kudu",
      },
      {
        code: "KN010",
        name: "Dawakin Tofa",
      },
      {
        code: "KN011",
        name: "Doguwa",
      },
      {
        code: "KN012",
        name: "Fagge",
      },
      {
        code: "KN013",
        name: "Gabasawa",
      },
      {
        code: "KN014",
        name: "Garko,",
      },
      {
        code: "KN015",
        name: "Garun Mallam",
      },
      {
        code: "KN016",
        name: "Guya",
      },
      {
        code: "KN017",
        name: "Gezawa",
      },
      {
        code: "KN018",
        name: "Gwale",
      },
      {
        code: "KN019",
        name: "Gwarzo",
      },
      {
        code: "KN020",
        name: "Kabo",
      },
      {
        code: "KN021",
        name: "Kano Municipal",
      },
      {
        code: "KN022",
        name: "Karaye",
      },
      {
        code: "KN023",
        name: "Kibiya",
      },
      {
        code: "KN024",
        name: "Kiru",
      },
      {
        code: "KN025",
        name: "Kumbotso",
      },
      {
        code: "KN026",
        name: "Kunchi",
      },
      {
        code: "KN027",
        name: "Kura",
      },
      {
        code: "KN028",
        name: "Madobi",
      },
      {
        code: "KN029",
        name: "Makoda",
      },
      {
        code: "KN030",
        name: "Minjibir",
      },
      {
        code: "KN031",
        name: "Nassarawa",
      },
      {
        code: "KN032",
        name: "Rano",
      },
      {
        code: "KN033",
        name: "Rimin Gado",
      },
      {
        code: "KN034",
        name: "Rogo",
      },
      {
        code: "KN035",
        name: "Shanono",
      },
      {
        code: "KN036",
        name: "Sumaila",
      },
      {
        code: "KN037",
        name: "Takai",
      },
      {
        code: "KN038",
        name: "Tarauni",
      },
      {
        code: "KN039",
        name: "Tofa",
      },
      {
        code: "KN040",
        name: "Tsanyawa",
      },
      {
        code: "KN041",
        name: "Tudun Wada",
      },
      {
        code: "KN042",
        name: "Ungogo",
      },
      {
        code: "KN043",
        name: "Warawa",
      },
      {
        code: "KN044",
        name: "Wudil",
      },
    ],
  },
  {
    code: "KT",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Katsina",
    lgs: [
      {
        code: "KD01",
        name: "Birnin Gwari",
      },
      {
        code: "KD02",
        name: "Chikun",
      },
      {
        code: "KD03",
        name: "Giwa",
      },
      {
        code: "KD04",
        name: "Igabi",
      },
      {
        code: "KD05",
        name: "Ikara",
      },
      {
        code: "KD06",
        name: "Jemaa",
      },
      {
        code: "KD07",
        name: "Kachia",
      },
      {
        code: "KD08",
        name: "Kaduna North",
      },
      {
        code: "KD09",
        name: "Kaduna South",
      },
      {
        code: "KD010",
        name: "Kagarko",
      },
      {
        code: "KD011",
        name: "Kajuru",
      },
      {
        code: "KD012",
        name: "Kaura",
      },
      {
        code: "KD013",
        name: "Kauru",
      },
      {
        code: "KD014",
        name: "Kubau",
      },
      {
        code: "KD015",
        name: "Kudan",
      },
      {
        code: "KD016",
        name: "Lere",
      },
      {
        code: "KD017",
        name: "Makarf",
      },
      {
        code: "KD018",
        name: "Sabon Gari",
      },
      {
        code: "KD019",
        name: "Sanga",
      },
      {
        code: "KD020",
        name: "Soba",
      },
      {
        code: "KD021",
        name: "Zangon Kataf",
      },
      {
        code: "KD022",
        name: "Zaria",
      },
      {
        code: "KT01",
        name: "Bakori",
      },
      {
        code: "KT02",
        name: "Batagarawa",
      },
      {
        code: "KT03",
        name: "Batsari",
      },
      {
        code: "KT04",
        name: "Baure",
      },
      {
        code: "KT05",
        name: "Bindawa",
      },
      {
        code: "KT06",
        name: "Charanchi",
      },
      {
        code: "KT07",
        name: "Dandume",
      },
      {
        code: "KT08",
        name: "Danja",
      },
      {
        code: "KT09",
        name: "Dan Musa",
      },
      {
        code: "KT010",
        name: "Daura",
      },
      {
        code: "KT011",
        name: "Dutsi",
      },
      {
        code: "KT012",
        name: "Dutsin-Ma",
      },
      {
        code: "KT013",
        name: "Faskari",
      },
      {
        code: "KT014",
        name: "Funtua",
      },
      {
        code: "KT015",
        name: "Ingawa",
      },
      {
        code: "KT016",
        name: "Jibia",
      },
      {
        code: "KT017",
        name: "Kafur",
      },
      {
        code: "KT018",
        name: "Kaita",
      },
      {
        code: "KT019",
        name: "Kankara",
      },
      {
        code: "KT020",
        name: "Kankia",
      },
      {
        code: "KT021",
        name: "Katsina",
      },
      {
        code: "KT022",
        name: "Kurfi",
      },
      {
        code: "KT023",
        name: "Kusada",
      },
      {
        code: "KT024",
        name: "Mai Adua",
      },
      {
        code: "KT025",
        name: "Malumfashi",
      },
      {
        code: "KT026",
        name: "Mani",
      },
      {
        code: "KT027",
        name: "Mashi",
      },
      {
        code: "KT028",
        name: "Matazuu",
      },
      {
        code: "KT029",
        name: "Musawa",
      },
      {
        code: "KT030",
        name: "Rimi",
      },
      {
        code: "KT031",
        name: "Sabuwa",
      },
      {
        code: "KT032",
        name: "Safana",
      },
      {
        code: "KT033",
        name: "Sandamu",
      },
      {
        code: "KT034",
        name: "Zango",
      },
    ],
  },
  {
    code: "KE",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Kebbi",
    lgs: [
      {
        code: "KE01",
        name: "Aliero",
      },
      {
        code: "KE02",
        name: "Arewa Dandi",
      },
      {
        code: "KE03",
        name: "Argungu",
      },
      {
        code: "KE04",
        name: "Augie",
      },
      {
        code: "KE05",
        name: "Bangudo",
      },
      {
        code: "KE06",
        name: "Birnin Kebbi",
      },
      {
        code: "KE07",
        name: "Bunza",
      },
      {
        code: "KE08",
        name: "Dandi",
      },
      {
        code: "KE09",
        name: "Danko/Wasagu",
      },
      {
        code: "KE010",
        name: "Fakai",
      },
      {
        code: "KE011",
        name: "Gwandu",
      },
      {
        code: "KE012",
        name: "Jega",
      },
      {
        code: "KE013",
        name: "Kalgo",
      },
      {
        code: "KE014",
        name: "Koko/Basse",
      },
      {
        code: "KE015",
        name: "Maiyama",
      },
      {
        code: "KE016",
        name: "Ngaski",
      },
      {
        code: "KE017",
        name: "Sakaba",
      },
      {
        code: "KE018",
        name: "Shanga",
      },
      {
        code: "KE019",
        name: "Suru",
      },
      {
        code: "KE020",
        name: "Yauri",
      },
      {
        code: "KE021",
        name: "Zuru",
      },
    ],
  },
  {
    code: "KO",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Kogi",
    lgs: [
      {
        code: "KO01",
        name: "Adavi",
      },
      {
        code: "KO02",
        name: "Ajaokuta",
      },
      {
        code: "KO03",
        name: "Ankpa",
      },
      {
        code: "KO04",
        name: "Bassa",
      },
      {
        code: "KO05",
        name: "Dekina",
      },
      {
        code: "KO06",
        name: "Ibaji",
      },
      {
        code: "KO07",
        name: "Idah",
      },
      {
        code: "KO08",
        name: "Igalamela-Odolu",
      },
      {
        code: "KO09",
        name: "Ijumu",
      },
      {
        code: "KO010",
        name: "Kabba/Bunu",
      },
      {
        code: "KO011",
        name: "Kogi",
      },
      {
        code: "KO012",
        name: "Lokoja",
      },
      {
        code: "KO013",
        name: "Mopa-Muro",
      },
      {
        code: "KO014",
        name: "Ofu",
      },
      {
        code: "KO015",
        name: "Ogori/Mangongo",
      },
      {
        code: "KO016",
        name: "Okehi",
      },
      {
        code: "KO017",
        name: "Okene",
      },
      {
        code: "KO018",
        name: "Olamabolo",
      },
      {
        code: "KO019",
        name: "Omala",
      },
      {
        code: "KO020",
        name: "Yagba East",
      },
      {
        code: "KO021",
        name: "Yagba West",
      },
    ],
  },
  {
    code: "KW",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Kwara",
    lgs: [
      {
        code: "KW01",
        name: "Asa",
      },
      {
        code: "KW02",
        name: "Baruten",
      },
      {
        code: "KW03",
        name: "Edu",
      },
      {
        code: "KW04",
        name: "Ekiti",
      },
      {
        code: "KW05",
        name: "Ifelodun",
      },
      {
        code: "KW06",
        name: "Ilorin East",
      },
      {
        code: "KW07",
        name: "Ilorin South",
      },
      {
        code: "KW08",
        name: "Ilorin West",
      },
      {
        code: "KW09",
        name: "Irepodun",
      },
      {
        code: "KW010",
        name: "Isin",
      },
      {
        code: "KW011",
        name: "Kaiama",
      },
      {
        code: "KW012",
        name: "Moro",
      },
      {
        code: "KW013",
        name: "Offa",
      },
      {
        code: "KW014",
        name: "Oke Ero",
      },
      {
        code: "KW015",
        name: "Oyun",
      },
      {
        code: "KW016",
        name: "Pategi",
      },
    ],
  },
  {
    code: "LA",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Lagos",
    lgs: [
      {
        code: "LA01",
        name: "Alimosho",
      },
      {
        code: "LA02",
        name: "Ajeromi-Ifelodun",
      },
      {
        code: "LA03",
        name: "Agege",
      },
      {
        code: "LA04",
        name: "Amuwo-Odofin",
      },
      {
        code: "LA05",
        name: "Apapa",
      },
      {
        code: "LA06",
        name: "Badagry",
      },
      {
        code: "LA07",
        name: "Eti-Osa",
      },
      {
        code: "LA08",
        name: "Epe",
      },
      {
        code: "LA09",
        name: "Ifako-Ijaiye",
      },
      {
        code: "LA010",
        name: "Ikorodu",
      },
      {
        code: "LA011",
        name: "Ikeja",
      },
      {
        code: "LA012",
        name: "Ibeju-Lekki",
      },
      {
        code: "LA013",
        name: "Kosofe",
      },
      {
        code: "LA014",
        name: "Lagos Island",
      },
      {
        code: "LA015",
        name: "Lagos Mainland",
      },
      {
        code: "LA016",
        name: "Mushin",
      },
      {
        code: "LA017",
        name: "Oshodi Isolo",
      },
      {
        code: "LA018",
        name: "Ojo",
      },
      {
        code: "LA019",
        name: "Somolu",
      },
      {
        code: "LA020",
        name: "Surulere",
      },
    ],
  },
  {
    code: "NA",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Nasarawa",
    lgs: [
      {
        code: "NA01",
        name: "Akwanga",
      },
      {
        code: "NA02",
        name: "Awe",
      },
      {
        code: "NA03",
        name: "Doma",
      },
      {
        code: "NA04",
        name: "Karu",
      },
      {
        code: "NA05",
        name: "Keana",
      },
      {
        code: "NA06",
        name: "Keffi",
      },
      {
        code: "NA07",
        name: "Kokona",
      },
      {
        code: "NA08",
        name: "Lafia",
      },
      {
        code: "NA09",
        name: "Nasarawa",
      },
      {
        code: "NA010",
        name: "Nasarawa Egon",
      },
      {
        code: "NA011",
        name: "Obi",
      },
      {
        code: "NA012",
        name: "Toto",
      },
      {
        code: "NA013",
        name: "Wamba",
      },
    ],
  },
  {
    code: "NI",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Niger",
    lgs: [
      {
        code: "NI01",
        name: "Agaise",
      },
      {
        code: "NI02",
        name: "Agwara",
      },
      {
        code: "NI03",
        name: "Bida",
      },
      {
        code: "NI04",
        name: "Borgu",
      },
      {
        code: "NI05",
        name: "Bosso",
      },
      {
        code: "NI06",
        name: "Chanchaga",
      },
      {
        code: "NI07",
        name: "Edati",
      },
      {
        code: "NI08",
        name: "Gbako",
      },
      {
        code: "NI09",
        name: "Gurara",
      },
      {
        code: "NI010",
        name: "Katcha",
      },
      {
        code: "NI011",
        name: "Kontagora",
      },
      {
        code: "NI012",
        name: "Lapia",
      },
      {
        code: "NI013",
        name: "Lavun",
      },
      {
        code: "NI014",
        name: "Magama",
      },
      {
        code: "NI015",
        name: "Mariga",
      },
      {
        code: "NI016",
        name: "Mashegu",
      },
      {
        code: "NI017",
        name: "Mokwa",
      },
      {
        code: "NI018",
        name: "Munya",
      },
      {
        code: "NI019",
        name: "Paikoro",
      },
      {
        code: "NI020",
        name: "Rafi",
      },
      {
        code: "NI021",
        name: "Rijau",
      },
      {
        code: "NI022",
        name: "Shiroro",
      },
      {
        code: "NI023",
        name: "Suleja",
      },
      {
        code: "NI024",
        name: "Tafa",
      },
      {
        code: "NI025",
        name: "Wushishi",
      },
    ],
  },
  {
    code: "OG",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Ogun",
    lgs: [
      {
        code: "OG01",
        name: "Abeokuta North",
      },
      {
        code: "OG02",
        name: "Abeokuta South",
      },
      {
        code: "OG03",
        name: "Ado-Odo/Ota",
      },
      {
        code: "OG04",
        name: "Ewekoro",
      },
      {
        code: "OG05",
        name: "Ifo",
      },
      {
        code: "OG06",
        name: "Ijebu East",
      },
      {
        code: "OG07",
        name: "Ijebu North",
      },
      {
        code: "OG08",
        name: "Ijebu North East",
      },
      {
        code: "OG09",
        name: "Ijebu Ode",
      },
      {
        code: "OG010",
        name: "Ikenne",
      },
      {
        code: "OG011",
        name: "Imeko Afon",
      },
      {
        code: "OG012",
        name: "Ipokia",
      },
      {
        code: "OG013",
        name: "Obafemi Owode",
      },
      {
        code: "OG014",
        name: "Odogbolu",
      },
      {
        code: "OG015",
        name: "Odeda",
      },
      {
        code: "OG016",
        name: "Ogun Waterside",
      },
      {
        code: "OG017",
        name: "Remo North",
      },
      {
        code: "OG018",
        name: "Sagamu",
      },
      {
        code: "OG019",
        name: "Yewa North",
      },
      {
        code: "OG020",
        name: "Yewa South",
      },
    ],
  },
  {
    code: "ON",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Ondo",
    lgs: [
      {
        code: "ON01",
        name: "Akoko North East",
      },
      {
        code: "ON02",
        name: "Akoko North West",
      },
      {
        code: "ON03",
        name: "Akoko South East",
      },
      {
        code: "ON04",
        name: "Akoko South West",
      },
      {
        code: "ON05",
        name: "Akure North",
      },
      {
        code: "ON06",
        name: "Akure South",
      },
      {
        code: "ON07",
        name: "Ese Odo",
      },
      {
        code: "ON08",
        name: "Idanre",
      },
      {
        code: "ON09",
        name: "Ifedore",
      },
      {
        code: "ON010",
        name: "Ilaje",
      },
      {
        code: "ON011",
        name: "Ile Oluji/Okeigbo",
      },
      {
        code: "ON012",
        name: "Irele",
      },
      {
        code: "ON013",
        name: "Odigbo",
      },
      {
        code: "ON014",
        name: "Okitipupa",
      },
      {
        code: "ON015",
        name: "Ondo East",
      },
      {
        code: "ON016",
        name: "Ondo West",
      },
      {
        code: "ON017",
        name: "Ose",
      },
      {
        code: "ON018",
        name: "Owo",
      },
    ],
  },
  {
    code: "OS",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Osun",
    lgs: [
      {
        code: "OS01",
        name: "Aiyedaade",
      },
      {
        code: "OS02",
        name: "Aiyedire",
      },
      {
        code: "OS03",
        name: "Atakunmosa East",
      },
      {
        code: "OS04",
        name: "Atakunmosa West",
      },
      {
        code: "OS05",
        name: "Boluwaduro",
      },
      {
        code: "OS06",
        name: "Boripe",
      },
      {
        code: "OS07",
        name: "Edo North",
      },
      {
        code: "OS08",
        name: "Ede South",
      },
      {
        code: "OS09",
        name: "Egbedore",
      },
      {
        code: "OS010",
        name: "Ejigbo",
      },
      {
        code: "OS011",
        name: "Ife Central",
      },
      {
        code: "OS012",
        name: "Ife East",
      },
      {
        code: "OS013",
        name: "Ife North",
      },
      {
        code: "OS014",
        name: "Ife South",
      },
      {
        code: "OS015",
        name: "Ifedayo",
      },
      {
        code: "OS016",
        name: "Ifelodun",
      },
      {
        code: "OS017",
        name: "Ila",
      },
      {
        code: "OS018",
        name: "Ilisa East",
      },
      {
        code: "OS019",
        name: "Ilesa West",
      },
      {
        code: "OS020",
        name: "Irepodun",
      },
      {
        code: "OS021",
        name: "Irewole",
      },
      {
        code: "OS022",
        name: "Isokan",
      },
      {
        code: "OS023",
        name: "Iwo",
      },
      {
        code: "OS024",
        name: "Obokun",
      },
      {
        code: "OS025",
        name: "Odo Otin",
      },
      {
        code: "OS026",
        name: "Ola Oluwa",
      },
      {
        code: "OS027",
        name: "Olorunda ",
      },
      {
        code: "OS028",
        name: "Oriade",
      },
      {
        code: "OS029",
        name: "Orolu",
      },
      {
        code: "OS030",
        name: "Osogbo",
      },
    ],
  },
  {
    code: "OY",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Oyo",
    lgs: [
      {
        code: "OY01",
        name: "Afijio",
      },
      {
        code: "OY02",
        name: "Akinyele",
      },
      {
        code: "OY03",
        name: "Atiba",
      },
      {
        code: "OY04",
        name: "Atisbo",
      },
      {
        code: "OY05",
        name: "Egbeda",
      },
      {
        code: "OY06",
        name: "Ibadan North",
      },
      {
        code: "OY07",
        name: "Ibadan North East",
      },
      {
        code: "OY08",
        name: "Ibadan North West",
      },
      {
        code: "OY09",
        name: "Ibadan South East",
      },
      {
        code: "OY010",
        name: "Ibadan South West",
      },
      {
        code: "OY011",
        name: "Ibarapa Central",
      },
      {
        code: "OY012",
        name: "Ibarapa East",
      },
      {
        code: "OY013",
        name: "Ibarapa North",
      },
      {
        code: "OY014",
        name: "Ido",
      },
      {
        code: "OY015",
        name: "Irepo",
      },
      {
        code: "OY016",
        name: "Iseyin",
      },
      {
        code: "OY017",
        name: "Itesiwaju",
      },
      {
        code: "OY018",
        name: "Iwajowa",
      },
      {
        code: "OY019",
        name: "Kajola",
      },
      {
        code: "OY020",
        name: "Lagelu",
      },
      {
        code: "OY021",
        name: "Ogbomosho North",
      },
      {
        code: "OY022",
        name: "Ogbomosho South",
      },
      {
        code: "OY023",
        name: "Ogo Oluwa",
      },
      {
        code: "OY024",
        name: "Olorunsogo",
      },
      {
        code: "OY025",
        name: "Oluyole",
      },
      {
        code: "OY026",
        name: "Ona Ara",
      },
      {
        code: "OY027",
        name: "Orelope",
      },
      {
        code: "OY028",
        name: "Ori Ire",
      },
      {
        code: "OY029",
        name: "Oyo East",
      },
      {
        code: "OY030",
        name: "Oyo West",
      },
      {
        code: "OY031",
        name: "Saki East",
      },
      {
        code: "OY032",
        name: "Saki West",
      },
      {
        code: "OY033",
        name: "Surulere",
      },
    ],
  },
  {
    code: "PL",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Plateau",
    lgs: [
      {
        code: "PL01",
        name: "Barkin Ladi",
      },
      {
        code: "PL02",
        name: "Bassa",
      },
      {
        code: "PL03",
        name: "Bokkos",
      },
      {
        code: "PL04",
        name: "Jos East",
      },
      {
        code: "PL05",
        name: "Jos North",
      },
      {
        code: "PL06",
        name: "Jos South",
      },
      {
        code: "PL07",
        name: "Kanam",
      },
      {
        code: "PL08",
        name: "Kanke",
      },
      {
        code: "PL09",
        name: "Langtang North",
      },
      {
        code: "PL010",
        name: "Langtang South",
      },
      {
        code: "PL011",
        name: "Mangu",
      },
      {
        code: "PL012",
        name: "Mikang",
      },
      {
        code: "PL013",
        name: "Pankshin",
      },
      {
        code: "PL014",
        name: "Quaan Pan",
      },
      {
        code: "PL015",
        name: "Riyom",
      },
      {
        code: "PL016",
        name: "Shendam",
      },
      {
        code: "PL017",
        name: "Wase",
      },
    ],
  },
  {
    code: "RI",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Rivers",
    lgs: [
      {
        code: "RI01",
        name: "Abua-Odual",
      },
      {
        code: "RI02",
        name: "Ahoada East",
      },
      {
        code: "RI03",
        name: "Ahodada West",
      },
      {
        code: "RI04",
        name: "Akuku-Toru",
      },
      {
        code: "RI05",
        name: "Andoni",
      },
      {
        code: "RI06",
        name: "Asari-Toru",
      },
      {
        code: "RI07",
        name: "Bonny",
      },
      {
        code: "RI08",
        name: "Degema",
      },
      {
        code: "RI09",
        name: "Eleme",
      },
      {
        code: "RI010",
        name: "Emohua",
      },
      {
        code: "RI011",
        name: "Etche",
      },
      {
        code: "RI012",
        name: "Gokana",
      },
      {
        code: "RI013",
        name: "Ikwerre",
      },
      {
        code: "RI014",
        name: "Khana",
      },
      {
        code: "RI015",
        name: "Obio-Akpor",
      },
      {
        code: "RI016",
        name: "Ogba-Egbema-Ndoni",
      },
      {
        code: "RI017",
        name: "Ogu-Bolo",
      },
      {
        code: "RI018",
        name: "Okrika",
      },
      {
        code: "RI019",
        name: "Omuma",
      },
      {
        code: "RI020",
        name: "Opobo-Nkoro",
      },
      {
        code: "RI021",
        name: "Oyigbo",
      },
      {
        code: "RI022",
        name: "Port Harcourt",
      },
      {
        code: "RI023",
        name: "Tai",
      },
    ],
  },
  {
    code: "SO",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Sokoto",
    lgs: [
      {
        code: "SO01",
        name: "Binji",
      },
      {
        code: "SO02",
        name: "Bodinga",
      },
      {
        code: "SO03",
        name: "Dange Shuni",
      },
      {
        code: "SO04",
        name: "Gada",
      },
      {
        code: "SO05",
        name: "Goronyo",
      },
      {
        code: "SO06",
        name: "Gudu",
      },
      {
        code: "SO07",
        name: "Gwadabawa",
      },
      {
        code: "SO08",
        name: "Illela",
      },
      {
        code: "SO09",
        name: "Isa",
      },
      {
        code: "SO010",
        name: "Kebbe",
      },
      {
        code: "SO011",
        name: "Kware",
      },
      {
        code: "SO012",
        name: "Rabah",
      },
      {
        code: "SO013",
        name: "Sabon Birini",
      },
      {
        code: "SO014",
        name: "Shagari",
      },
      {
        code: "SO015",
        name: "Silama",
      },
      {
        code: "SO016",
        name: "Sokoto North",
      },
      {
        code: "SO017",
        name: "Sokoto South",
      },
      {
        code: "SO018",
        name: "Tambuwal",
      },
      {
        code: "SO019",
        name: "Tangaza",
      },
      {
        code: "SO020",
        name: "Tureta",
      },
      {
        code: "SO021",
        name: "Wamako",
      },
      {
        code: "SO022",
        name: "Wurno",
      },
      {
        code: "SO023",
        name: "Yabo",
      },
    ],
  },
  {
    code: "TA",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Taraba",
    lgs: [
      {
        code: "TA01",
        name: "Ardo Kola",
      },
      {
        code: "TA02",
        name: "Bali",
      },
      {
        code: "TA03",
        name: "Donga",
      },
      {
        code: "TA04",
        name: "Gashaka",
      },
      {
        code: "TA05",
        name: "Gassol",
      },
      {
        code: "TA06",
        name: "Ibi",
      },
      {
        code: "TA07",
        name: "Jalingo",
      },
      {
        code: "TA08",
        name: "Karim Lamido",
      },
      {
        code: "TA09",
        name: "Kurmi",
      },
      {
        code: "TA010",
        name: "Lau",
      },
      {
        code: "TA011",
        name: "Sardauna",
      },
      {
        code: "TA012",
        name: "Takum",
      },
      {
        code: "TA013",
        name: "Ussa",
      },
      {
        code: "TA014",
        name: "Wukari",
      },
      {
        code: "TA015",
        name: "Yorro",
      },
      {
        code: "TA016",
        name: "Zing",
      },
    ],
  },
  {
    code: "YO",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Yobe",
    lgs: [
      {
        code: "YO01",
        name: "Bade",
      },
      {
        code: "YO02",
        name: "Bursari",
      },
      {
        code: "YO03",
        name: "Damaturu",
      },
      {
        code: "YO04",
        name: "Geidam",
      },
      {
        code: "YO05",
        name: "Gujba",
      },
      {
        code: "YO06",
        name: "Gulani",
      },
      {
        code: "YO07",
        name: "Fika",
      },
      {
        code: "YO08",
        name: "Fune",
      },
      {
        code: "YO09",
        name: "Jakusko",
      },
      {
        code: "YO010",
        name: "Karasuwa",
      },
      {
        code: "YO011",
        name: "Machina",
      },
      {
        code: "YO012",
        name: "Nangere",
      },
      {
        code: "YO013",
        name: "Nguru",
      },
      {
        code: "YO014",
        name: "Pptiskum",
      },
      {
        code: "YO015",
        name: "Tarmuwa",
      },
      {
        code: "YO016",
        name: "Yunusari",
      },
      {
        code: "YO017",
        name: "Yusufari",
      },
    ],
  },
  {
    code: "ZA",

    regions: [
      "North Central",
      "North East",
      "North West",
      "South East",
      "South South",
      "South West",
    ],
    name: "Zamfara",
    lgs: [
      {
        code: "ZA01",
        name: "Anka",
      },
      {
        code: "ZA02",
        name: "Bakura",
      },
      {
        code: "ZA03",
        name: "Birnin Magaji/Kkiyaw",
      },
      {
        code: "ZA04",
        name: "Bukkuyum",
      },
      {
        code: "ZA05",
        name: "Bungudu",
      },
      {
        code: "ZA06",
        name: "Chafe",
      },
      {
        code: "ZA07",
        name: "Gummi",
      },
      {
        code: "ZA08",
        name: "Gusau",
      },
      {
        code: "ZA09",
        name: "Kaura Namoda",
      },
      {
        code: "ZA010",
        name: "Maradun",
      },
      {
        code: "ZA011",
        name: "Maru",
      },
      {
        code: "ZA012",
        name: "Shinkafi",
      },
      {
        code: "ZA013",
        name: "Talata Mafara",
      },
      {
        code: "ZA014",
        name: "Zurumi",
      },
    ],
  },
];
export const Banks = [
  { id: "1", name: "Access Bank", code: "044" },
  { id: "2", name: "Citibank", code: "023" },
  { id: "3", name: "Diamond Bank", code: "063" },
  { id: "4", name: "Dynamic Standard Bank", code: "" },
  { id: "5", name: "Ecobank Nigeria", code: "050" },
  { id: "6", name: "Fidelity Bank Nigeria", code: "070" },
  { id: "7", name: "First Bank of Nigeria", code: "011" },
  { id: "8", name: "First City Monument Bank", code: "214" },
  { id: "9", name: "Guaranty Trust Bank", code: "058" },
  { id: "10", name: "Heritage Bank Plc", code: "030" },
  { id: "11", name: "Jaiz Bank", code: "301" },
  { id: "12", name: "Keystone Bank Limited", code: "082" },
  { id: "13", name: "Providus Bank Plc", code: "101" },
  { id: "14", name: "Polaris Bank", code: "076" },
  { id: "15", name: "Stanbic IBTC Bank Nigeria Limited", code: "221" },
  { id: "16", name: "Standard Chartered Bank", code: "068" },
  { id: "17", name: "Sterling Bank", code: "232" },
  { id: "18", name: "Suntrust Bank Nigeria Limited", code: "100" },
  { id: "19", name: "Union Bank of Nigeria", code: "032" },
  { id: "20", name: "United Bank for Africa", code: "033" },
  { id: "21", name: "Unity Bank Plc", code: "215" },
  { id: "22", name: "Wema Bank", code: "035" },
  { id: "23", name: "Zenith Bank", code: "057" },
];
export const Qualifications = [
  {
    id: "1",
    name: "West African Secondary School Certificate Examination (WASCE)",
  },
  { id: "2", name: "General Certificate of Education (GCE)" },
  { id: "3", name: "Ordinary National Diploma (OND)" },
  { id: "4", name: "Higher National Diploma (HND)" },
  { id: "5", name: "Bachelor of Arts (BA)" },
  { id: "6", name: "Bachelor of Architecture (B.Arch)" },
  { id: "7", name: "Bachelor of Business Administration (B.B.A)" },
  { id: "8", name: "Bachelor of Education (B.Ed)" },
  { id: "9", name: "Barchelor of Engineering (B.Eng)" },
  { id: "10", name: "Bachelor of Nursing (B.N)" },
  { id: "11", name: "Bachelor of Law (LLB (Hons)/B.L)" },
  { id: "12", name: "Bachelor of Science (B.Sc)" },
  { id: "13", name: "Post Graduate Diploma (PGD)" },
  { id: "14", name: "Post Graduate Diploma in Education (PGDE)" },
  { id: "15", name: "Master of Arts (M.A)" },
  { id: "16", name: "Master of Architecture (M.Arch)" },
  { id: "17", name: "Master of Biochemistry (M.Biochem)" },
  { id: "18", name: "Master of Business Administration(M.BA)" },
  { id: "19", name: "Master of Biomedical Sciences (M.BioSci)" },
  { id: "20", name: "Master of Education (M.Ed)" },
  { id: "21", name: "Master of Engineering (M.Eng)" },
  { id: "22", name: "Master of Languages (M.Lang)" },
  { id: "23", name: "Master of Physics (M.Phys)" },
  { id: "24", name: "Master of Science (M.Sc)" },
  { id: "25", name: "Doctor of Philosophy (PH.D)" },
];
export const Currencies = [
  {
    code: "AED",
    number: "784",
    digits: 2,
    currency: "UAE Dirham",
    countries: ["United Arab Emirates (The)"],
  },
  {
    code: "AFN",
    number: "971",
    digits: 2,
    currency: "Afghani",
    countries: ["Afghanistan"],
  },
  {
    code: "ALL",
    number: "008",
    digits: 2,
    currency: "Lek",
    countries: ["Albania"],
  },
  {
    code: "AMD",
    number: "051",
    digits: 2,
    currency: "Armenian Dram",
    countries: ["Armenia"],
  },
  {
    code: "ANG",
    number: "532",
    digits: 2,
    currency: "Netherlands Antillean Guilder",
    countries: ["Curaçao", "Sint Maarten (Dutch Part)"],
  },
  {
    code: "AOA",
    number: "973",
    digits: 2,
    currency: "Kwanza",
    countries: ["Angola"],
  },
  {
    code: "ARS",
    number: "032",
    digits: 2,
    currency: "Argentine Peso",
    countries: ["Argentina"],
  },
  {
    code: "AUD",
    number: "036",
    digits: 2,
    currency: "Australian Dollar",
    countries: [
      "Australia",
      "Christmas Island",
      "Cocos (Keeling) Islands (The)",
      "Heard Island and Mcdonald Islands",
      "Kiribati",
      "Nauru",
      "Norfolk Island",
      "Tuvalu",
    ],
  },
  {
    code: "AWG",
    number: "533",
    digits: 2,
    currency: "Aruban Florin",
    countries: ["Aruba"],
  },
  {
    code: "AZN",
    number: "944",
    digits: 2,
    currency: "Azerbaijan Manat",
    countries: ["Azerbaijan"],
  },
  {
    code: "BAM",
    number: "977",
    digits: 2,
    currency: "Convertible Mark",
    countries: ["Bosnia and Herzegovina"],
  },
  {
    code: "BBD",
    number: "052",
    digits: 2,
    currency: "Barbados Dollar",
    countries: ["Barbados"],
  },
  {
    code: "BDT",
    number: "050",
    digits: 2,
    currency: "Taka",
    countries: ["Bangladesh"],
  },
  {
    code: "BGN",
    number: "975",
    digits: 2,
    currency: "Bulgarian Lev",
    countries: ["Bulgaria"],
  },
  {
    code: "BHD",
    number: "048",
    digits: 3,
    currency: "Bahraini Dinar",
    countries: ["Bahrain"],
  },
  {
    code: "BIF",
    number: "108",
    digits: 0,
    currency: "Burundi Franc",
    countries: ["Burundi"],
  },
  {
    code: "BMD",
    number: "060",
    digits: 2,
    currency: "Bermudian Dollar",
    countries: ["Bermuda"],
  },
  {
    code: "BND",
    number: "096",
    digits: 2,
    currency: "Brunei Dollar",
    countries: ["Brunei Darussalam"],
  },
  {
    code: "BOB",
    number: "068",
    digits: 2,
    currency: "Boliviano",
    countries: ["Bolivia (Plurinational State Of)"],
  },
  {
    code: "BOV",
    number: "984",
    digits: 2,
    currency: "Mvdol",
    countries: ["Bolivia (Plurinational State Of)"],
  },
  {
    code: "BRL",
    number: "986",
    digits: 2,
    currency: "Brazilian Real",
    countries: ["Brazil"],
  },
  {
    code: "BSD",
    number: "044",
    digits: 2,
    currency: "Bahamian Dollar",
    countries: ["Bahamas (The)"],
  },
  {
    code: "BTN",
    number: "064",
    digits: 2,
    currency: "Ngultrum",
    countries: ["Bhutan"],
  },
  {
    code: "BWP",
    number: "072",
    digits: 2,
    currency: "Pula",
    countries: ["Botswana"],
  },
  {
    code: "BYN",
    number: "933",
    digits: 2,
    currency: "Belarusian Ruble",
    countries: ["Belarus"],
  },
  {
    code: "BZD",
    number: "084",
    digits: 2,
    currency: "Belize Dollar",
    countries: ["Belize"],
  },
  {
    code: "CAD",
    number: "124",
    digits: 2,
    currency: "Canadian Dollar",
    countries: ["Canada"],
  },
  {
    code: "CDF",
    number: "976",
    digits: 2,
    currency: "Congolese Franc",
    countries: ["Congo (The Democratic Republic of The)"],
  },
  {
    code: "CHE",
    number: "947",
    digits: 2,
    currency: "WIR Euro",
    countries: ["Switzerland"],
  },
  {
    code: "CHF",
    number: "756",
    digits: 2,
    currency: "Swiss Franc",
    countries: ["Liechtenstein", "Switzerland"],
  },
  {
    code: "CHW",
    number: "948",
    digits: 2,
    currency: "WIR Franc",
    countries: ["Switzerland"],
  },
  {
    code: "CLF",
    number: "990",
    digits: 4,
    currency: "Unidad de Fomento",
    countries: ["Chile"],
  },
  {
    code: "CLP",
    number: "152",
    digits: 0,
    currency: "Chilean Peso",
    countries: ["Chile"],
  },
  {
    code: "CNY",
    number: "156",
    digits: 2,
    currency: "Yuan Renminbi",
    countries: ["China"],
  },
  {
    code: "COP",
    number: "170",
    digits: 2,
    currency: "Colombian Peso",
    countries: ["Colombia"],
  },
  {
    code: "COU",
    number: "970",
    digits: 2,
    currency: "Unidad de Valor Real",
    countries: ["Colombia"],
  },
  {
    code: "CRC",
    number: "188",
    digits: 2,
    currency: "Costa Rican Colon",
    countries: ["Costa Rica"],
  },
  {
    code: "CUC",
    number: "931",
    digits: 2,
    currency: "Peso Convertible",
    countries: ["Cuba"],
  },
  {
    code: "CUP",
    number: "192",
    digits: 2,
    currency: "Cuban Peso",
    countries: ["Cuba"],
  },
  {
    code: "CVE",
    number: "132",
    digits: 2,
    currency: "Cabo Verde Escudo",
    countries: ["Cabo Verde"],
  },
  {
    code: "CZK",
    number: "203",
    digits: 2,
    currency: "Czech Koruna",
    countries: ["Czechia"],
  },
  {
    code: "DJF",
    number: "262",
    digits: 0,
    currency: "Djibouti Franc",
    countries: ["Djibouti"],
  },
  {
    code: "DKK",
    number: "208",
    digits: 2,
    currency: "Danish Krone",
    countries: ["Denmark", "Faroe Islands (The)", "Greenland"],
  },
  {
    code: "DOP",
    number: "214",
    digits: 2,
    currency: "Dominican Peso",
    countries: ["Dominican Republic (The)"],
  },
  {
    code: "DZD",
    number: "012",
    digits: 2,
    currency: "Algerian Dinar",
    countries: ["Algeria"],
  },
  {
    code: "EGP",
    number: "818",
    digits: 2,
    currency: "Egyptian Pound",
    countries: ["Egypt"],
  },
  {
    code: "ERN",
    number: "232",
    digits: 2,
    currency: "Nakfa",
    countries: ["Eritrea"],
  },
  {
    code: "ETB",
    number: "230",
    digits: 2,
    currency: "Ethiopian Birr",
    countries: ["Ethiopia"],
  },
  {
    code: "EUR",
    number: "978",
    digits: 2,
    currency: "Euro",
    countries: [
      "Åland Islands",
      "Andorra",
      "Austria",
      "Belgium",
      "Cyprus",
      "Estonia",
      "European Union",
      "Finland",
      "France",
      "French Guiana",
      "French Southern Territories (The)",
      "Germany",
      "Greece",
      "Guadeloupe",
      "Holy See (The)",
      "Ireland",
      "Italy",
      "Latvia",
      "Lithuania",
      "Luxembourg",
      "Malta",
      "Martinique",
      "Mayotte",
      "Monaco",
      "Montenegro",
      "Netherlands (The)",
      "Portugal",
      "Réunion",
      "Saint Barthélemy",
      "Saint Martin (French Part)",
      "Saint Pierre and Miquelon",
      "San Marino",
      "Slovakia",
      "Slovenia",
      "Spain",
    ],
  },
  {
    code: "FJD",
    number: "242",
    digits: 2,
    currency: "Fiji Dollar",
    countries: ["Fiji"],
  },
  {
    code: "FKP",
    number: "238",
    digits: 2,
    currency: "Falkland Islands Pound",
    countries: ["Falkland Islands (The) [Malvinas]"],
  },
  {
    code: "GBP",
    number: "826",
    digits: 2,
    currency: "Pound Sterling",
    countries: [
      "Guernsey",
      "Isle of Man",
      "Jersey",
      "United Kingdom of Great Britain and Northern Ireland (The)",
    ],
  },
  {
    code: "GEL",
    number: "981",
    digits: 2,
    currency: "Lari",
    countries: ["Georgia"],
  },
  {
    code: "GHS",
    number: "936",
    digits: 2,
    currency: "Ghana Cedi",
    countries: ["Ghana"],
  },
  {
    code: "GIP",
    number: "292",
    digits: 2,
    currency: "Gibraltar Pound",
    countries: ["Gibraltar"],
  },
  {
    code: "GMD",
    number: "270",
    digits: 2,
    currency: "Dalasi",
    countries: ["Gambia (The)"],
  },
  {
    code: "GNF",
    number: "324",
    digits: 0,
    currency: "Guinean Franc",
    countries: ["Guinea"],
  },
  {
    code: "GTQ",
    number: "320",
    digits: 2,
    currency: "Quetzal",
    countries: ["Guatemala"],
  },
  {
    code: "GYD",
    number: "328",
    digits: 2,
    currency: "Guyana Dollar",
    countries: ["Guyana"],
  },
  {
    code: "HKD",
    number: "344",
    digits: 2,
    currency: "Hong Kong Dollar",
    countries: ["Hong Kong"],
  },
  {
    code: "HNL",
    number: "340",
    digits: 2,
    currency: "Lempira",
    countries: ["Honduras"],
  },
  {
    code: "HRK",
    number: "191",
    digits: 2,
    currency: "Kuna",
    countries: ["Croatia"],
  },
  {
    code: "HTG",
    number: "332",
    digits: 2,
    currency: "Gourde",
    countries: ["Haiti"],
  },
  {
    code: "HUF",
    number: "348",
    digits: 2,
    currency: "Forint",
    countries: ["Hungary"],
  },
  {
    code: "IDR",
    number: "360",
    digits: 2,
    currency: "Rupiah",
    countries: ["Indonesia"],
  },
  {
    code: "ILS",
    number: "376",
    digits: 2,
    currency: "New Israeli Sheqel",
    countries: ["Israel"],
  },
  {
    code: "INR",
    number: "356",
    digits: 2,
    currency: "Indian Rupee",
    countries: ["Bhutan", "India"],
  },
  {
    code: "IQD",
    number: "368",
    digits: 3,
    currency: "Iraqi Dinar",
    countries: ["Iraq"],
  },
  {
    code: "IRR",
    number: "364",
    digits: 2,
    currency: "Iranian Rial",
    countries: ["Iran (Islamic Republic Of)"],
  },
  {
    code: "ISK",
    number: "352",
    digits: 0,
    currency: "Iceland Krona",
    countries: ["Iceland"],
  },
  {
    code: "JMD",
    number: "388",
    digits: 2,
    currency: "Jamaican Dollar",
    countries: ["Jamaica"],
  },
  {
    code: "JOD",
    number: "400",
    digits: 3,
    currency: "Jordanian Dinar",
    countries: ["Jordan"],
  },
  {
    code: "JPY",
    number: "392",
    digits: 0,
    currency: "Yen",
    countries: ["Japan"],
  },
  {
    code: "KES",
    number: "404",
    digits: 2,
    currency: "Kenyan Shilling",
    countries: ["Kenya"],
  },
  {
    code: "KGS",
    number: "417",
    digits: 2,
    currency: "Som",
    countries: ["Kyrgyzstan"],
  },
  {
    code: "KHR",
    number: "116",
    digits: 2,
    currency: "Riel",
    countries: ["Cambodia"],
  },
  {
    code: "KMF",
    number: "174",
    digits: 0,
    currency: "Comorian Franc ",
    countries: ["Comoros (The)"],
  },
  {
    code: "KPW",
    number: "408",
    digits: 2,
    currency: "North Korean Won",
    countries: ["Korea (The Democratic People’s Republic Of)"],
  },
  {
    code: "KRW",
    number: "410",
    digits: 0,
    currency: "Won",
    countries: ["Korea (The Republic Of)"],
  },
  {
    code: "KWD",
    number: "414",
    digits: 3,
    currency: "Kuwaiti Dinar",
    countries: ["Kuwait"],
  },
  {
    code: "KYD",
    number: "136",
    digits: 2,
    currency: "Cayman Islands Dollar",
    countries: ["Cayman Islands (The)"],
  },
  {
    code: "KZT",
    number: "398",
    digits: 2,
    currency: "Tenge",
    countries: ["Kazakhstan"],
  },
  {
    code: "LAK",
    number: "418",
    digits: 2,
    currency: "Lao Kip",
    countries: ["Lao People’s Democratic Republic (The)"],
  },
  {
    code: "LBP",
    number: "422",
    digits: 2,
    currency: "Lebanese Pound",
    countries: ["Lebanon"],
  },
  {
    code: "LKR",
    number: "144",
    digits: 2,
    currency: "Sri Lanka Rupee",
    countries: ["Sri Lanka"],
  },
  {
    code: "LRD",
    number: "430",
    digits: 2,
    currency: "Liberian Dollar",
    countries: ["Liberia"],
  },
  {
    code: "LSL",
    number: "426",
    digits: 2,
    currency: "Loti",
    countries: ["Lesotho"],
  },
  {
    code: "LYD",
    number: "434",
    digits: 3,
    currency: "Libyan Dinar",
    countries: ["Libya"],
  },
  {
    code: "MAD",
    number: "504",
    digits: 2,
    currency: "Moroccan Dirham",
    countries: ["Morocco", "Western Sahara"],
  },
  {
    code: "MDL",
    number: "498",
    digits: 2,
    currency: "Moldovan Leu",
    countries: ["Moldova (The Republic Of)"],
  },
  {
    code: "MGA",
    number: "969",
    digits: 2,
    currency: "Malagasy Ariary",
    countries: ["Madagascar"],
  },
  {
    code: "MKD",
    number: "807",
    digits: 2,
    currency: "Denar",
    countries: ["Macedonia (The Former Yugoslav Republic Of)"],
  },
  {
    code: "MMK",
    number: "104",
    digits: 2,
    currency: "Kyat",
    countries: ["Myanmar"],
  },
  {
    code: "MNT",
    number: "496",
    digits: 2,
    currency: "Tugrik",
    countries: ["Mongolia"],
  },
  {
    code: "MOP",
    number: "446",
    digits: 2,
    currency: "Pataca",
    countries: ["Macao"],
  },
  {
    code: "MRU",
    number: "929",
    digits: 2,
    currency: "Ouguiya",
    countries: ["Mauritania"],
  },
  {
    code: "MUR",
    number: "480",
    digits: 2,
    currency: "Mauritius Rupee",
    countries: ["Mauritius"],
  },
  {
    code: "MVR",
    number: "462",
    digits: 2,
    currency: "Rufiyaa",
    countries: ["Maldives"],
  },
  {
    code: "MWK",
    number: "454",
    digits: 2,
    currency: "Malawi Kwacha",
    countries: ["Malawi"],
  },
  {
    code: "MXN",
    number: "484",
    digits: 2,
    currency: "Mexican Peso",
    countries: ["Mexico"],
  },
  {
    code: "MXV",
    number: "979",
    digits: 2,
    currency: "Mexican Unidad de Inversion (UDI)",
    countries: ["Mexico"],
  },
  {
    code: "MYR",
    number: "458",
    digits: 2,
    currency: "Malaysian Ringgit",
    countries: ["Malaysia"],
  },
  {
    code: "MZN",
    number: "943",
    digits: 2,
    currency: "Mozambique Metical",
    countries: ["Mozambique"],
  },
  {
    code: "NAD",
    number: "516",
    digits: 2,
    currency: "Namibia Dollar",
    countries: ["Namibia"],
  },
  {
    code: "NGN",
    number: "566",
    digits: 2,
    currency: "Naira",
    countries: ["Nigeria"],
  },
  {
    code: "NIO",
    number: "558",
    digits: 2,
    currency: "Cordoba Oro",
    countries: ["Nicaragua"],
  },
  {
    code: "NOK",
    number: "578",
    digits: 2,
    currency: "Norwegian Krone",
    countries: ["Bouvet Island", "Norway", "Svalbard and Jan Mayen"],
  },
  {
    code: "NPR",
    number: "524",
    digits: 2,
    currency: "Nepalese Rupee",
    countries: ["Nepal"],
  },
  {
    code: "NZD",
    number: "554",
    digits: 2,
    currency: "New Zealand Dollar",
    countries: [
      "Cook Islands (The)",
      "New Zealand",
      "Niue",
      "Pitcairn",
      "Tokelau",
    ],
  },
  {
    code: "OMR",
    number: "512",
    digits: 3,
    currency: "Rial Omani",
    countries: ["Oman"],
  },
  {
    code: "PAB",
    number: "590",
    digits: 2,
    currency: "Balboa",
    countries: ["Panama"],
  },
  {
    code: "PEN",
    number: "604",
    digits: 2,
    currency: "Sol",
    countries: ["Peru"],
  },
  {
    code: "PGK",
    number: "598",
    digits: 2,
    currency: "Kina",
    countries: ["Papua New Guinea"],
  },
  {
    code: "PHP",
    number: "608",
    digits: 2,
    currency: "Philippine Peso",
    countries: ["Philippines (The)"],
  },
  {
    code: "PKR",
    number: "586",
    digits: 2,
    currency: "Pakistan Rupee",
    countries: ["Pakistan"],
  },
  {
    code: "PLN",
    number: "985",
    digits: 2,
    currency: "Zloty",
    countries: ["Poland"],
  },
  {
    code: "PYG",
    number: "600",
    digits: 0,
    currency: "Guarani",
    countries: ["Paraguay"],
  },
  {
    code: "QAR",
    number: "634",
    digits: 2,
    currency: "Qatari Rial",
    countries: ["Qatar"],
  },
  {
    code: "RON",
    number: "946",
    digits: 2,
    currency: "Romanian Leu",
    countries: ["Romania"],
  },
  {
    code: "RSD",
    number: "941",
    digits: 2,
    currency: "Serbian Dinar",
    countries: ["Serbia"],
  },
  {
    code: "RUB",
    number: "643",
    digits: 2,
    currency: "Russian Ruble",
    countries: ["Russian Federation (The)"],
  },
  {
    code: "RWF",
    number: "646",
    digits: 0,
    currency: "Rwanda Franc",
    countries: ["Rwanda"],
  },
  {
    code: "SAR",
    number: "682",
    digits: 2,
    currency: "Saudi Riyal",
    countries: ["Saudi Arabia"],
  },
  {
    code: "SBD",
    number: "090",
    digits: 2,
    currency: "Solomon Islands Dollar",
    countries: ["Solomon Islands"],
  },
  {
    code: "SCR",
    number: "690",
    digits: 2,
    currency: "Seychelles Rupee",
    countries: ["Seychelles"],
  },
  {
    code: "SDG",
    number: "938",
    digits: 2,
    currency: "Sudanese Pound",
    countries: ["Sudan (The)"],
  },
  {
    code: "SEK",
    number: "752",
    digits: 2,
    currency: "Swedish Krona",
    countries: ["Sweden"],
  },
  {
    code: "SGD",
    number: "702",
    digits: 2,
    currency: "Singapore Dollar",
    countries: ["Singapore"],
  },
  {
    code: "SHP",
    number: "654",
    digits: 2,
    currency: "Saint Helena Pound",
    countries: ["Saint Helena, Ascension and Tristan Da Cunha"],
  },
  {
    code: "SLL",
    number: "694",
    digits: 2,
    currency: "Leone",
    countries: ["Sierra Leone"],
  },
  {
    code: "SOS",
    number: "706",
    digits: 2,
    currency: "Somali Shilling",
    countries: ["Somalia"],
  },
  {
    code: "SRD",
    number: "968",
    digits: 2,
    currency: "Surinam Dollar",
    countries: ["Suriname"],
  },
  {
    code: "SSP",
    number: "728",
    digits: 2,
    currency: "South Sudanese Pound",
    countries: ["South Sudan"],
  },
  {
    code: "STN",
    number: "930",
    digits: 2,
    currency: "Dobra",
    countries: ["Sao Tome and Principe"],
  },
  {
    code: "SVC",
    number: "222",
    digits: 2,
    currency: "El Salvador Colon",
    countries: ["El Salvador"],
  },
  {
    code: "SYP",
    number: "760",
    digits: 2,
    currency: "Syrian Pound",
    countries: ["Syrian Arab Republic"],
  },
  {
    code: "SZL",
    number: "748",
    digits: 2,
    currency: "Lilangeni",
    countries: ["Eswatini"],
  },
  {
    code: "THB",
    number: "764",
    digits: 2,
    currency: "Baht",
    countries: ["Thailand"],
  },
  {
    code: "TJS",
    number: "972",
    digits: 2,
    currency: "Somoni",
    countries: ["Tajikistan"],
  },
  {
    code: "TMT",
    number: "934",
    digits: 2,
    currency: "Turkmenistan New Manat",
    countries: ["Turkmenistan"],
  },
  {
    code: "TND",
    number: "788",
    digits: 3,
    currency: "Tunisian Dinar",
    countries: ["Tunisia"],
  },
  {
    code: "TOP",
    number: "776",
    digits: 2,
    currency: "Pa’anga",
    countries: ["Tonga"],
  },
  {
    code: "TRY",
    number: "949",
    digits: 2,
    currency: "Turkish Lira",
    countries: ["Turkey"],
  },
  {
    code: "TTD",
    number: "780",
    digits: 2,
    currency: "Trinidad and Tobago Dollar",
    countries: ["Trinidad and Tobago"],
  },
  {
    code: "TWD",
    number: "901",
    digits: 2,
    currency: "New Taiwan Dollar",
    countries: ["Taiwan (Province of China)"],
  },
  {
    code: "TZS",
    number: "834",
    digits: 2,
    currency: "Tanzanian Shilling",
    countries: ["Tanzania, United Republic Of"],
  },
  {
    code: "UAH",
    number: "980",
    digits: 2,
    currency: "Hryvnia",
    countries: ["Ukraine"],
  },
  {
    code: "UGX",
    number: "800",
    digits: 0,
    currency: "Uganda Shilling",
    countries: ["Uganda"],
  },
  {
    code: "USD",
    number: "840",
    digits: 2,
    currency: "US Dollar",
    countries: [
      "American Samoa",
      "Bonaire, Sint Eustatius and Saba",
      "British Indian Ocean Territory (The)",
      "Ecuador",
      "El Salvador",
      "Guam",
      "Haiti",
      "Marshall Islands (The)",
      "Micronesia (Federated States Of)",
      "Northern Mariana Islands (The)",
      "Palau",
      "Panama",
      "Puerto Rico",
      "Timor-Leste",
      "Turks and Caicos Islands (The)",
      "United States Minor Outlying Islands (The)",
      "United States of America (The)",
      "Virgin Islands (British)",
      "Virgin Islands (U.S.)",
    ],
  },
  {
    code: "USN",
    number: "997",
    digits: 2,
    currency: "US Dollar (Next day)",
    countries: ["United States of America (The)"],
  },
  {
    code: "UYI",
    number: "940",
    digits: 0,
    currency: "Uruguay Peso en Unidades Indexadas (UI)",
    countries: ["Uruguay"],
  },
  {
    code: "UYU",
    number: "858",
    digits: 2,
    currency: "Peso Uruguayo",
    countries: ["Uruguay"],
  },
  {
    code: "UYW",
    number: "927",
    digits: 4,
    currency: "Unidad Previsional",
    countries: ["Uruguay"],
  },
  {
    code: "UZS",
    number: "860",
    digits: 2,
    currency: "Uzbekistan Sum",
    countries: ["Uzbekistan"],
  },
  {
    code: "VES",
    number: "928",
    digits: 2,
    currency: "Bolívar Soberano",
    countries: ["Venezuela (Bolivarian Republic Of)"],
  },
  {
    code: "VND",
    number: "704",
    digits: 0,
    currency: "Dong",
    countries: ["Viet Nam"],
  },
  {
    code: "VUV",
    number: "548",
    digits: 0,
    currency: "Vatu",
    countries: ["Vanuatu"],
  },
  {
    code: "WST",
    number: "882",
    digits: 2,
    currency: "Tala",
    countries: ["Samoa"],
  },
  {
    code: "XAF",
    number: "950",
    digits: 0,
    currency: "CFA Franc BEAC",
    countries: [
      "Cameroon",
      "Central African Republic (The)",
      "Chad",
      "Congo (The)",
      "Equatorial Guinea",
      "Gabon",
    ],
  },
  {
    code: "XAG",
    number: "961",
    digits: 0,
    currency: "Silver",
    countries: ["Zz11_silver"],
  },
  {
    code: "XAU",
    number: "959",
    digits: 0,
    currency: "Gold",
    countries: ["Zz08_gold"],
  },
  {
    code: "XBA",
    number: "955",
    digits: 0,
    currency: "Bond Markets Unit European Composite Unit (EURCO)",
    countries: ["Zz01_bond Markets Unit European_eurco"],
  },
  {
    code: "XBB",
    number: "956",
    digits: 0,
    currency: "Bond Markets Unit European Monetary Unit (E.M.U.-6)",
    countries: ["Zz02_bond Markets Unit European_emu-6"],
  },
  {
    code: "XBC",
    number: "957",
    digits: 0,
    currency: "Bond Markets Unit European Unit of Account 9 (E.U.A.-9)",
    countries: ["Zz03_bond Markets Unit European_eua-9"],
  },
  {
    code: "XBD",
    number: "958",
    digits: 0,
    currency: "Bond Markets Unit European Unit of Account 17 (E.U.A.-17)",
    countries: ["Zz04_bond Markets Unit European_eua-17"],
  },
  {
    code: "XCD",
    number: "951",
    digits: 2,
    currency: "East Caribbean Dollar",
    countries: [
      "Anguilla",
      "Antigua and Barbuda",
      "Dominica",
      "Grenada",
      "Montserrat",
      "Saint Kitts and Nevis",
      "Saint Lucia",
      "Saint Vincent and the Grenadines",
    ],
  },
  {
    code: "XDR",
    number: "960",
    digits: 0,
    currency: "SDR (Special Drawing Right)",
    countries: ["International Monetary Fund (Imf) "],
  },
  {
    code: "XOF",
    number: "952",
    digits: 0,
    currency: "CFA Franc BCEAO",
    countries: [
      "Benin",
      "Burkina Faso",
      "Côte d'Ivoire",
      "Guinea-Bissau",
      "Mali",
      "Niger (The)",
      "Senegal",
      "Togo",
    ],
  },
  {
    code: "XPD",
    number: "964",
    digits: 0,
    currency: "Palladium",
    countries: ["Zz09_palladium"],
  },
  {
    code: "XPF",
    number: "953",
    digits: 0,
    currency: "CFP Franc",
    countries: ["French Polynesia", "New Caledonia", "Wallis and Futuna"],
  },
  {
    code: "XPT",
    number: "962",
    digits: 0,
    currency: "Platinum",
    countries: ["Zz10_platinum"],
  },
  {
    code: "XSU",
    number: "994",
    digits: 0,
    currency: "Sucre",
    countries: ['Sistema Unitario De Compensacion Regional De Pagos "Sucre"'],
  },
  {
    code: "XTS",
    number: "963",
    digits: 0,
    currency: "Codes specifically reserved for testing purposes",
    countries: ["Zz06_testing_code"],
  },
  {
    code: "XUA",
    number: "965",
    digits: 0,
    currency: "ADB Unit of Account",
    countries: ["Member Countries of the African Development Bank Group"],
  },
  {
    code: "XXX",
    number: "999",
    digits: 0,
    currency:
      "The codes assigned for transactions where no currency is involved",
    countries: ["Zz07_no_currency"],
  },
  {
    code: "YER",
    number: "886",
    digits: 2,
    currency: "Yemeni Rial",
    countries: ["Yemen"],
  },
  {
    code: "ZAR",
    number: "710",
    digits: 2,
    currency: "Rand",
    countries: ["Lesotho", "Namibia", "South Africa"],
  },
  {
    code: "ZMW",
    number: "967",
    digits: 2,
    currency: "Zambian Kwacha",
    countries: ["Zambia"],
  },
  {
    code: "ZWL",
    number: "932",
    digits: 2,
    currency: "Zimbabwe Dollar",
    countries: ["Zimbabwe"],
  },
];
