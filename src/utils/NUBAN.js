export function NUBAN(bankCode, accountNumber) {
  if (typeof bankCode !== "string" || typeof accountNumber !== "string") {
    return "accountNumber must be strings";
  }
  if (bankCode.length !== 3 || accountNumber.length !== 10) {
    return "Account number should not be less/more than 10 digits";
  }

  let checkDigit = parseInt(accountNumber.charAt(9));
  let dictionary = [3, 7, 3, 3, 7, 3, 3, 7, 3, 3, 7, 3];
  let accountSerialNo = accountNumber.substring(0, 9);
  let nubanAccountFormat = bankCode + accountSerialNo;

  let checkSum = 0;

  nubanAccountFormat.split("").forEach((char, index) => {
    checkSum += char * dictionary[index];
  });

  let validatedCheckDigit = 10 - (checkSum % 10);
  validatedCheckDigit = validatedCheckDigit === 10 ? 0 : validatedCheckDigit;
  return checkDigit === validatedCheckDigit ? true : false;
}
