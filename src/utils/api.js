// import { API } from "../constants/ApiConstants";
// import store from "../routes/store";
// import { refreshTokenCreator } from "../actions/authenticationAction";
import Cookies from "js-cookie";

/**
 * - Get the `Authorization` header with access token set
 * @return {Object} Header with Authorization
 */

export const getAccessTokenHeader = () => {
  let accessToken;
  try {
    accessToken = Cookies.get("access-token");
  } catch (error) {
    return null;
  }
  if (accessToken) {
    return {
      Authorization: `Bearer ${accessToken}`,
    };
  }
  return null;
};
export const getAccessTokenHeader2 = () => {
  let accessToken;
  try {
    accessToken = Cookies.get("access-token");
  } catch (error) {
    return null;
  }
  if (accessToken) {
    return {
      Authorization: `Bearer ${accessToken}`,
      "Content-Type": "application/json",
    };
  }
  return null;
};

/**
 * - Error object to return when there is no token in storage.
 * @return {Object}
 */
export const getNoTokenErrorObject = () => ({
  status: "0",
  info: "No token",
  message: "NO_TOKEN",
});

export const getRefreshTokenHeader = () => {
  let refreshToken;
  let accessToken;
  try {
    refreshToken = Cookies.get("refresh-token");
    accessToken = Cookies.get("access-token");
  } catch (error) {
    return null;
  }
  if (refreshToken) {
    return {
      Authorization: `bearer ${refreshToken}`,
      "Content-Type": "application/json",
      "access-token": accessToken,
    };
  }
  return null;
};

export const getLogoutAccessTokenHeader = () => {
  let refreshToken;
  let accessToken;
  try {
    refreshToken = Cookies.get("refresh-token");
    accessToken = Cookies.get("access-token");
  } catch (error) {
    return null;
  }
  if (refreshToken) {
    return {
      Authorization: `bearer ${accessToken}`,
      "Content-Type": "application/json",
      "refresh-token": refreshToken,
    };
  }
  return null;
};
