import styled, { css } from "styled-components";
import { Colors, Fonts } from "../assets/themes/theme";

// backgroundColor: Colors.primary,

// marginTop: 10,
// marginBottom: 15,
// marginRight: 10,
// display: 'flex',
// float: "right",
// width: "50%",
// fontSize:Fonts.SubmitButton,
// height: 40,
// '&:hover': {
//   backgroundColor:Colors.primary,
//   color: 'rgba(255, 255, 255)',
// }
export const Button = styled.button`
background-color: #0E4C28,
    text-transform: 'capitalize';
    font-size:12px;
    width: 50%;
    display: flex;
    float: right;
    border-radius: 3px;
    border: 2px solid #0E4C28;
    color: #fff;
    margin: 0.1em;
    padding: 0.25em 1em;

    ${(props) =>
      props.primary &&
      css`
    background-color: #0E4C28';
    color: #fff;
    `}
    `;

export const Container = styled.div`
  text-align: center;
`;
