import styled from 'styled-components'

export const TextField = styled.input`
padding: 0.5em;
margin: 0.5em;
color: ${props => props.inputColor || "palevioletred"};
background: papayawhip;
border: none;
border-radius: 3px;
`;

export const Container = styled.div`
text-align: center;
`