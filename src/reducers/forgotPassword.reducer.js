import { ActionConstants } from "../constants/ActionConstants";

const initialState = {
  loading: false,
  success: false,
  successMessage: "",
  error: false,
  errorMessage: "",
  data: {},
  validateData: {},
  validateIsLoading: false,
  validateSuccess: false,
  validateSuccessMessage: "",
  validateError: false,
  validateErrorMessage: "",
};

export function forgotPasswordReducer(state = initialState, action) {
  switch (action.type) {
    case ActionConstants.INITIATE_PASSWORD_RESET_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ActionConstants.INITIATE_PASSWORD_RESET_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        error: false,
        successMessage: action.payload.meta.message,
        data: { ...state.data, ...action.payload.data },
      };
    case ActionConstants.INITIATE_PASSWORD_RESET_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        errorMessage: action.payload,
      };
    case ActionConstants.VALIDATE_PASSWORD_RESET_REQUEST:
      return {
        ...state,
        validateIsLoading: true,
      };
    case ActionConstants.VALIDATE_PASSWORD_RESET_SUCCESS:
      return {
        ...state,
        validateError: false,
        validateIsLoading: false,
        validateSuccess: true,
        validateData: { ...state.validateData, ...action.payload.data },
      };
    case ActionConstants.VALIDATE_PASSWORD_RESET_ERROR:
      return {
        ...state,
        validateIsLoading: false,
        validateSuccess: false,
        validateError: true,
        validateErrorMessage: action.payload,
      };
    case ActionConstants.COMPLETE_PASSWORD_RESET_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ActionConstants.COMPLETE_PASSWORD_RESET_SUCCESS:
      return {
        ...state,
        error: false,
        loading: false,
        success: true,
        completed: true,
        data: { ...state.data, ...action.payload.data },
      };
    case ActionConstants.COMPLETE_PASSWORD_RESET_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorMessage: action.payload,
      };
    case ActionConstants.CLEAR_MESSAGES:
      return {
        ...state,
        loading: false,
        success: false,
        successMessage: "",
        error: false,
        errorMessage: "",
        validateIsLoading: false,
        validateSuccess: false,
        validateSuccessMessage: "",
        validateError: false,
      };
    default:
      return state;
  }
}
