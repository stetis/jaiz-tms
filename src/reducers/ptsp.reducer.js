import { ActionConstants } from "../constants/ActionConstants";

const initialState = {
  data: {},
  loading: false,
  success: false,
  error: false,
  errorMessage: "",
  singlePTSPData: {},
  singlePTSPIsLoading: false,
  singlePTSPSuccess: false,
  singlePTSPError: false,
  singlePTSPErrorMessage: "",
  contactData: {},
  contactIsLoading: false,
  contactSuccess: false,
  contactError: false,
  contactErrorMessage: "",
  actionLoading: false,
  status: null,
  statusMessage: "",
};

export function ptspReducer(state = initialState, action) {
  switch (action.type) {
    case ActionConstants.PTSP_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ActionConstants.PTSP_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        data: { ...state.data, ...action.payload.data },
      };
    case ActionConstants.PTSP_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        errorMessage: action.payload,
      };
    case ActionConstants.SINGLE_PTSP_REQUEST:
      return {
        ...state,
        singlePTSPIsLoading: true,
      };
    case ActionConstants.SINGLE_PTSP_SUCCESS:
      return {
        ...state,
        singlePTSPIsLoading: false,
        singlePTSPSuccess: true,
        singlePTSPData: { ...state.singlePTSPData, ...action.payload.data },
      };
    case ActionConstants.SINGLE_PTSP_ERROR:
      return {
        ...state,
        singlePTSPIsLoading: false,
        singlePTSPSuccess: false,
        singlePTSPError: true,
        singlePTSPErrorMessage: action.payload,
      };
    case ActionConstants.PTSP_CONTACT_REQUEST:
      return {
        ...state,
        contactIsLoading: true,
      };
    case ActionConstants.PTSP_CONTACT_SUCCESS:
      return {
        ...state,
        contactIsLoading: false,
        contactSuccess: true,
        contactData: { ...state.contactData, ...action.payload.data },
      };
    case ActionConstants.PTSP_CONTACT_ERROR:
      return {
        ...state,
        contactIsLoading: false,
        contactSuccess: false,
        error: true,
        errorMessage: action.payload,
      };
    case ActionConstants.ACTIONS_REQUEST:
      return {
        ...state,
        actionLoading: true,
      };
    case ActionConstants.ACTIONS_SUCCESS:
      return {
        ...state,
        actionLoading: false,
        status: 1,
        statusMessage: action.payload.meta.message,
        data: { ...state.data, ...action.payload.data },
      };
    case ActionConstants.ACTIONS_ERROR:
      return {
        ...state,
        actionLoading: false,
        status: 0,
        statusMessage: action.payload,
      };
    case ActionConstants.CLEAR_MESSAGES:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
        errorMessage: "",
        singlePTSPIsLoading: false,
        singlePTSPSuccess: false,
        singlePTSPError: false,
        singlePTSPErrorMessage: "",
        contactIsLoading: false,
        contactSuccess: false,
        contactError: false,
        contactErrorMessage: "",
        actionLoading: false,
        status: null,
        statusMessage: "",
      };
    default:
      return state;
  }
}
