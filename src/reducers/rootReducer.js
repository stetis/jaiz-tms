import { combineReducers } from "redux";
import updateBreadcrumb from "./breadcrumbReducer";
import { loginReducer } from "./login.reducer";
import { forgotPasswordReducer } from "./forgotPassword.reducer";
import { profileReducer } from "./profile.reducer";
import { installReducer } from "./install.reducer";
import { titleReducer } from "./ConfigurationReducers/titles.reducers";
import { accountTypeReducer } from "./ConfigurationReducers/accountType.reducer";
import { branchesReducer } from "./ConfigurationReducers/branches.reducer";
import { commissionsReducer } from "./ConfigurationReducers/commissions.reducer";
import { devicesReducer } from "./ConfigurationReducers/devices.reducer";
import { categoryReducer } from "./ConfigurationReducers/Mcategories.reducer";
import { regionsReducer } from "./ConfigurationReducers/regions.reducer";
import { softwareReducer } from "./ConfigurationReducers/sofftware.reducer";
import { auditlogReducer } from "./System/auditlog.reducers";
import { usersReducer } from "./System/users.reducer";
import { transactionsReducer } from "./transactions.reducers";
import { terminalsReducer } from "./terminals.reducer";
import { ptspReducer } from "./ptsp.reducer";
import { posInventoryReducer } from "./pos.reducer";
import { dashboardReducer } from "./dashboard.reducer";
import { agentsReducer } from "./agents.reducer";
import { merchantsReducer } from "./merchants.reducer";

export const reducers = {
  updateBreadcrumb,
  profileReducer,
  loginReducer,
  forgotPasswordReducer,
  installReducer,
  titleReducer,
  accountTypeReducer,
  branchesReducer,
  commissionsReducer,
  devicesReducer,
  categoryReducer,
  regionsReducer,
  softwareReducer,
  auditlogReducer,
  usersReducer,
  transactionsReducer,
  terminalsReducer,
  ptspReducer,
  posInventoryReducer,
  dashboardReducer,
  agentsReducer,
  merchantsReducer,
};
const rootReducer = combineReducers({
  ...reducers,
});

export default rootReducer;
