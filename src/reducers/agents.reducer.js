import { ActionConstants } from "../constants/ActionConstants";

const initialState = {
  data: {},
  loading: false,
  success: false,
  error: false,
  errorMessage: "",
  singleAgentData: {},
  singleAgentIsLoading: false,
  singleAgentSuccess: false,
  singleAgentError: false,
  singleAgentErrorMessage: "",
  allAgentsData: {},
  allAgentsIsLoading: false,
  allAgentsSuccess: false,
  allAgentsError: false,
  allAgentsErrorMessage: "",
  actionLoading: false,
  status: null,
  statusMessage: "",
};

export function agentsReducer(state = initialState, action) {
  switch (action.type) {
    case ActionConstants.AGENTS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ActionConstants.AGENTS_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        data: { ...state.data, ...action.payload.data },
      };
    case ActionConstants.AGENTS_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        errorMessage: action.payload,
      };
    case ActionConstants.SINGLE_AGENT_REQUEST:
      return {
        ...state,
        singleAgentIsLoading: true,
      };
    case ActionConstants.SINGLE_AGENT_SUCCESS:
      return {
        ...state,
        singleAgentIsLoading: false,
        singleAgentSuccess: true,
        singleAgentData: { ...state.singleAgentData, ...action.payload.data },
      };
    case ActionConstants.SINGLE_AGENT_ERROR:
      return {
        ...state,
        singleAgentIsLoading: false,
        singleAgentSuccess: false,
        singleAgentError: true,
        singleAgentErrorMessage: action.payload,
      };
    case ActionConstants.ALL_AGENTS_REQUEST:
      return {
        ...state,
        allAgentsIsLoading: true,
      };
    case ActionConstants.ALL_AGENTS_SUCCESS:
      return {
        ...state,
        allAgentsIsLoading: false,
        allAgentsSuccess: true,
        allAgentsError: false,
        allAgentsData: { ...state.allAgentsData, ...action.payload.data },
      };
    case ActionConstants.ALL_AGENTS_ERROR:
      return {
        ...state,
        allAgentsIsLoading: false,
        allAgentsSuccess: false,
        allAgentsError: true,
        allAgentsErrorMessage: action.payload,
      };
    case ActionConstants.ACTIONS_REQUEST:
      return {
        ...state,
        actionLoading: true,
      };
    case ActionConstants.ACTIONS_SUCCESS:
      return {
        ...state,
        actionLoading: false,
        status: 1,
        statusMessage: action.payload.meta.message,
      };
    case ActionConstants.ACTIONS_ERROR:
      return {
        ...state,
        actionLoading: false,
        status: 0,
        statusMessage: action.payload,
      };
    case ActionConstants.CLEAR_MESSAGES:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
        errorMessage: "",
        singleAgentIsLoading: false,
        singleAgentSuccess: false,
        singleAgentError: false,
        singleAgentErrorMessage: "",
        allAgentsIsLoading: false,
        allAgentsSuccess: false,
        allAgentsError: false,
        allAgentsErrorMessage: "",
        actionLoading: false,
        status: null,
        statusMessage: "",
      };
    default:
      return state;
  }
}
