import { ActionConstants } from "../../constants/ActionConstants";

const initialState = {
  data: {},
  loading: false,
  success: false,
  error: false,
  errorMessage: "",
};

export function auditlogReducer(state = initialState, action) {
  switch (action.type) {
    case ActionConstants.AUDIT_LOG_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ActionConstants.AUDIT_LOG_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        data: { ...state.data, ...action.payload.data },
      };
    case ActionConstants.AUDIT_LOG_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        errorMessage: action.payload,
      };
    case ActionConstants.CLEAR_MESSAGES:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
        errorMessage: "",
      };
    default:
      return state;
  }
}
