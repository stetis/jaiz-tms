import { ActionConstants } from "../constants/ActionConstants";

const initialState = {
  data: {},
  loading: false,
  success: false,
  error: false,
  errorMessage: "",
  posesData: {},
  posesLoading: false,
  posesSuccess: false,
  posesError: false,
  posesErrorMessage: "",
  singlePosData: {},
  singlePosLoading: false,
  singlePosSuccess: false,
  singlePosError: false,
  singlePosErrorMessage: "",
  actionLoading: false,
  status: null,
  statusMessage: "",
};

export function posInventoryReducer(state = initialState, action) {
  switch (action.type) {
    case ActionConstants.POS_INVENORY_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ActionConstants.POS_INVENORY_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        data: { ...state.data, ...action.payload.data.summary },
      };
    case ActionConstants.POS_INVENORY_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        errorMessage: action.payload,
      };
    case ActionConstants.POSES_REQUEST:
      return {
        ...state,
        posesLoading: true,
      };
    case ActionConstants.POSES_SUCCESS:
      return {
        ...state,
        posesLoading: false,
        posesSuccess: true,
        posesData: { ...state.posesData, ...action.payload.data },
      };
    case ActionConstants.POSES_ERROR:
      return {
        ...state,
        posesLoading: false,
        posesSuccess: false,
        posesError: true,
        posesErrorMessage: action.payload,
      };
    case ActionConstants.SINGLE_POS_REQUEST:
      return {
        ...state,
        singlePosLoading: true,
      };
    case ActionConstants.SINGLE_POS_SUCCESS:
      return {
        ...state,
        singlePosLoading: false,
        singlePosSuccess: true,
        singlePosData: { ...state.singlePosData, ...action.payload.data },
      };
    case ActionConstants.SINGLE_POS_ERROR:
      return {
        ...state,
        singlePosLoading: false,
        singlePosSuccess: false,
        singlePosError: true,
        singlePosErrorMessage: action.payload,
      };
    case ActionConstants.ACTIONS_REQUEST:
      return {
        ...state,
        actionLoading: true,
      };
    case ActionConstants.ACTIONS_SUCCESS:
      return {
        ...state,
        actionLoading: false,
        status: 1,
        statusMessage: action.payload.meta.message,
      };
    case ActionConstants.ACTIONS_ERROR:
      return {
        ...state,
        actionLoading: false,
        status: 0,
        statusMessage: action.payload,
      };
    case ActionConstants.CLEAR_MESSAGES:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
        errorMessage: "",
        posesLoading: false,
        posesSuccess: false,
        posesError: false,
        posesErrorMessage: "",
        singlePosLoading: false,
        singlePosSuccess: false,
        singlePosError: false,
        singlePosErrorMessage: "",
        actionLoading: false,
        status: null,
        statusMessage: "",
      };
    default:
      return state;
  }
}
