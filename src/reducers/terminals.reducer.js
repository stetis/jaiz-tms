import { ActionConstants } from "../constants/ActionConstants";

const initialState = {
  data: {},
  loading: false,
  success: false,
  error: false,
  errorMessage: "",
  agentTerminalData: {},
  agentTerminalLoading: false,
  agentTerminalSuccess: false,
  agentTerminalError: false,
  agentTerminalErrorMessage: "",
  singleTerminalData: {},
  singleTerminalLoading: false,
  singleTerminalSuccess: false,
  singleTerminalError: false,
  singleTerminalErrorMessage: "",
  actionLoading: false,
  status: null,
  statusMessage: "",
};

export function terminalsReducer(state = initialState, action) {
  switch (action.type) {
    case ActionConstants.TERMINALS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ActionConstants.TERMINALS_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        data: { ...state.data, ...action.payload.data },
      };
    case ActionConstants.TERMINALS_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        errorMessage: action.payload,
      };
    case ActionConstants.SINGLE_TERMINAL_REQUEST:
      return {
        ...state,
        singleTerminalLoading: true,
      };
    case ActionConstants.SINGLE_TERMINAL_SUCCESS:
      return {
        ...state,
        singleTerminalLoading: false,
        singleTerminalSuccess: true,
        singleTerminalData: {
          ...state.singleTerminalData,
          ...action.payload.data,
        },
      };
    case ActionConstants.SINGLE_TERMINAL_ERROR:
      return {
        ...state,
        singleTerminalLoading: false,
        singleTerminalSuccess: false,
        singleTerminalError: true,
        singleTerminalErrorMessage: action.payload,
      };
    case ActionConstants.AGENT_TERMINAL_REQUEST:
      return {
        ...state,
        agentTerminalLoading: true,
      };
    case ActionConstants.AGENT_TERMINAL_SUCCESS:
      return {
        ...state,
        agentTerminalLoading: false,
        agentTerminalSuccess: true,
        agentTerminalData: {
          ...state.singleTerminalData,
          ...action.payload.data,
        },
      };
    case ActionConstants.AGENT_TERMINAL_ERROR:
      return {
        ...state,
        agentTerminalLoading: false,
        agentTerminalSuccess: false,
        agentTerminalError: true,
        agentTerminalErrorMessage: action.payload,
      };
    case ActionConstants.ACTIONS_REQUEST:
      return {
        ...state,
        actionLoading: true,
      };
    case ActionConstants.ACTIONS_SUCCESS:
      return {
        ...state,
        actionLoading: false,
        status: 1,
        statusMessage: action.payload.meta.message,
      };
    case ActionConstants.ACTIONS_ERROR:
      return {
        ...state,
        actionLoading: false,
        status: 0,
        statusMessage: action.payload,
      };
    case ActionConstants.CLEAR_MESSAGES:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
        errorMessage: "",
        agentTerminalLoading: false,
        agentTerminalSuccess: false,
        agentTerminalError: false,
        agentTerminalErrorMessage: "",
        singleTerminalLoading: false,
        singleTerminalSuccess: false,
        singleTerminalError: false,
        singleTerminalErrorMessage: "",
        actionLoading: false,
        status: null,
        statusMessage: "",
      };
    default:
      return state;
  }
}
