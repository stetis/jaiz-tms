import { ActionConstants } from "../constants/ActionConstants";

const InitState = {
  data: {},
  loading: false,
  error: false,
  errorMessage: "",
  isLoggedIn: false,
  success: false,
  successMessage: "",
  loggingout: false,
  loggedout: false,
  logoutError: false,
  logoutErrorMessage: "",
  refreshing: false,
  refreshed: false,
  refreshError: false,
  refreshErrorMessage: "",
};

/**
 * - Shows if the user has logged in successfully
 * - `true` if authentiated
 * - `false` otherwise
 * - default: `false`
 * @param  {Boolean} state
 * @param  {Object} action
 * @return {Boolean}
 */

export function loginReducer(state = InitState, action) {
  switch (action.type) {
    case ActionConstants.LOGIN_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case ActionConstants.LOGIN_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        isLoggedIn: true,
        successMessage: { ...state, ...action.payload.meta.message },
        data: { ...state.data, ...action.payload.data },
      };

    case ActionConstants.LOGIN_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        isLoggedIn: false,
        error: true,
        errorMessage: action.payload,
      };
    case ActionConstants.LOGOUT_REQUEST:
      return {
        ...state,
        loggingout: true,
      };

    case ActionConstants.LOGOUT_SUCCESS:
      return {
        ...state,
        loggingout: false,
        isLoggedIn: false,
        loggedout: true,
      };

    case ActionConstants.LOGOUT_ERROR:
      return {
        ...state,
        loggingout: false,
        loggedout: false,
        isLoggedIn: false,
        logoutError: true,
        logoutErrorMessage: action.payload,
      };
    case ActionConstants.REFRESH_REQUEST:
      return {
        ...state,
        refreshing: true,
      };

    case ActionConstants.REFRESH_SUCCESS:
      return {
        ...state,
        refreshing: false,
        isLoggedIn: true,
        refreshed: true,
      };
    case ActionConstants.REFRESH_ERROR:
      return {
        ...state,
        refreshing: false,
        refreshed: false,
        isLoggedIn: false,
        refreshError: true,
        refreshErrorMessage: action.payload,
      };
    case ActionConstants.CLEAR_MESSAGES:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
        errorMessage: "",
        loggingout: false,
        loggedout: false,
        logoutError: false,
        logoutErrorMessage: "",
        refreshing: false,
        refreshed: false,
        refreshError: false,
        refreshErrorMessage: "",
      };
    default:
      return state;
  }
}
