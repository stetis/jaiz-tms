import { ActionConstants } from "../../constants/ActionConstants";

const initialState = {
  data: {},
  loading: false,
  success: false,
  error: false,
  errorMessage: "",
  typeData: {},
  typeLoading: false,
  typeSuccess: false,
  typeError: false,
  typeErrorMessage: "",
  singleFeeGroupData: {},
  singleFeeGroupLoading: false,
  singleFeeGroupSuccess: false,
  singleFeeGroupError: false,
  singleFeeGroupErrorMessage: "",
  feeGroupData: {},
  feeGroupLoading: false,
  feeGroupSuccess: false,
  feeGroupError: false,
  feeGroupErrorMessage: "",
  singleFeeData: {},
  singleFeeLoading: false,
  singleFeeSuccess: false,
  singleFeeError: false,
  singleFeeErrorMessage: "",
  feeData: {},
  feeLoading: false,
  feeSuccess: false,
  feeError: false,
  feeErrorMessage: "",
  bandData: {},
  bandLoading: false,
  bandSuccess: false,
  bandError: false,
  bandErrorMessage: "",
  actionLoading: false,
  status: null,
  statusMessage: "",
};

export function commissionsReducer(state = initialState, action) {
  switch (action.type) {
    case ActionConstants.COMMISSIONS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ActionConstants.COMMISSIONS_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        data: { ...state.data, ...action.payload.data },
      };
    case ActionConstants.COMMISSIONS_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        errorMessage: action.payload,
      };

    case ActionConstants.TRANSACTION_TYPE_REQUEST:
      return {
        ...state,
        typeLoading: true,
      };
    case ActionConstants.TRANSACTION_TYPE_SUCCESS:
      return {
        ...state,
        typeLoading: false,
        typeSuccess: true,
        typeData: { ...state.typeData, ...action.payload },
      };
    case ActionConstants.TRANSACTION_TYPE_ERROR:
      return {
        ...state,
        typeLoading: false,
        typeSuccess: false,
        typeError: true,
        typeErrorMessage: action.payload,
      };

    case ActionConstants.SINGLE_FEE_GROUP_REQUEST:
      return {
        ...state,
        singleFeeGroupLoading: true,
      };
    case ActionConstants.SINGLE_FEE_GROUP_SUCCESS:
      return {
        ...state,
        singleFeeGroupLoading: false,
        singleFeeGroupSuccess: true,
        singleFeeGroupData: {
          ...state.singleFeeGroupData,
          ...action.payload.data,
        },
      };
    case ActionConstants.SINGLE_FEE_GROUP_ERROR:
      return {
        ...state,
        singleFeeGroupLoading: false,
        singleFeeGroupSuccess: false,
        singleFeeGroupError: true,
        singleFeeGroupErrorMessage: action.payload,
      };

    case ActionConstants.FEE_GROUP_REQUEST:
      return {
        ...state,
        feeGroupLoading: true,
      };
    case ActionConstants.FEE_GROUP_SUCCESS:
      return {
        ...state,
        feeGroupLoading: false,
        feeGroupSuccess: true,
        feeGroupData: { ...state.feeGroupData, ...action.payload.data },
      };
    case ActionConstants.FEE_GROUP_ERROR:
      return {
        ...state,
        feeGroupLoading: false,
        feeGroupSuccess: false,
        feeGroupError: true,
        feeGroupErrorMessage: action.payload,
      };

    case ActionConstants.SINGLE_FEE_REQUEST:
      return {
        ...state,
        singleFeeLoading: true,
      };
    case ActionConstants.SINGLE_FEE_SUCCESS:
      return {
        ...state,
        singleFeeLoading: false,
        singleFeeSuccess: true,
        singleFeeData: { ...state.singleFeeData, ...action.payload.data },
      };
    case ActionConstants.SINGLE_FEE_ERROR:
      return {
        ...state,
        singleFeeLoading: false,
        singleFeeSuccess: false,
        singleFeeError: true,
        singleFeeErrorMessage: action.payload,
      };

    case ActionConstants.FEE_REQUEST:
      return {
        ...state,
        feeLoading: true,
      };
    case ActionConstants.FEE_SUCCESS:
      return {
        ...state,
        feeLoading: false,
        feeSuccess: true,
        feeData: { ...state.feeData, ...action.payload },
      };
    case ActionConstants.FEE_ERROR:
      return {
        ...state,
        feeLoading: false,
        feeSuccess: false,
        feeError: true,
        feeErrorMessage: action.payload,
      };
    case ActionConstants.BAND_REQUEST:
      return {
        ...state,
        bandLoading: true,
      };
    case ActionConstants.BAND_SUCCESS:
      return {
        ...state,
        bandLoading: false,
        bandSuccess: true,
        bandData: { ...state.bandData, ...action.payload.data },
      };
    case ActionConstants.BAND_ERROR:
      return {
        ...state,
        bandLoading: false,
        bandSuccess: false,
        bandError: true,
        bandErrorMessage: action.payload,
      };
    case ActionConstants.ACTIONS_REQUEST:
      return {
        ...state,
        actionLoading: true,
      };
    case ActionConstants.ACTIONS_SUCCESS:
      return {
        ...state,
        actionLoading: false,
        status: 1,
        statusMessage: action.payload.meta.message,
      };
    case ActionConstants.ACTIONS_ERROR:
      return {
        ...state,
        actionLoading: false,
        status: 0,
        statusMessage: action.payload,
      };
    case ActionConstants.CLEAR_MESSAGES:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
        errorMessage: "",
        typeLoading: false,
        typeSuccess: false,
        typeError: false,
        typeErrorMessage: "",
        singleFeeGroupLoading: false,
        singleFeeGroupSuccess: false,
        singleFeeGroupError: false,
        singleFeeGroupErrorMessage: "",
        feeGroupLoading: false,
        feeGroupSuccess: false,
        feeGroupError: false,
        feeGroupErrorMessage: "",
        singleFeeLoading: false,
        singleFeeSuccess: false,
        feeLoading: false,
        feeSuccess: false,
        feeError: false,
        feeErrorMessage: "",
        bandLoading: false,
        bandSuccess: false,
        bandError: false,
        bandErrorMessage: "",
        actionLoading: false,
        status: null,
        statusMessage: "",
      };
    default:
      return state;
  }
}
