import { ActionConstants } from "../../constants/ActionConstants";

const initialState = {
  data: {},
  loading: false,
  success: false,
  error: false,
  errorMessage: "",
  versionData: {},
  versionIsLoading: false,
  versionSuccess: false,
  versionError: false,
  versionErrorMessage: "",
  actionLoading: false,
  status: null,
  statusMessage: "",
};

export function softwareReducer(state = initialState, action) {
  switch (action.type) {
    case ActionConstants.SOFTWARE_VERSION_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ActionConstants.SOFTWARE_VERSION_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        data: { ...state.data, ...action.payload.data },
      };
    case ActionConstants.SOFTWARE_VERSION_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        errorMessage: action.payload,
      };
    case ActionConstants.GET_VERSIONS_REQUEST:
      return {
        ...state,
        versionIsLoading: true,
      };
    case ActionConstants.GET_VERSIONS_SUCCESS:
      return {
        ...state,
        versionIsLoading: false,
        versionSuccess: true,
        versionData: { ...state.versionData, ...action.payload.data },
      };
    case ActionConstants.GET_VERSIONS_ERROR:
      return {
        ...state,
        versionIsLoading: false,
        versionSuccess: false,
        versionError: true,
        versionErrorMessage: action.payload,
      };
    case ActionConstants.ACTIONS_REQUEST:
      return {
        ...state,
        actionLoading: true,
      };
    case ActionConstants.ACTIONS_SUCCESS:
      return {
        ...state,
        actionLoading: false,
        status: 1,
        statusMessage: action.payload.meta.message,
      };
    case ActionConstants.ACTIONS_ERROR:
      return {
        ...state,
        actionLoading: false,
        status: 0,
        statusMessage: action.payload,
      };
    case ActionConstants.CLEAR_MESSAGES:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
        errorMessage: "",
        versionIsLoading: false,
        versionSuccess: false,
        actionLoading: false,
        status: null,
        statusMessage: "",
      };
    default:
      return state;
  }
}
