import { ActionConstants } from "../../constants/ActionConstants";

const initialState = {
  data: {},
  loading: false,
  success: false,
  error: false,
  errorMessage: "",
  actionLoading: false,
  status: null,
  statusMessage: "",
};

export function categoryReducer(state = initialState, action) {
  switch (action.type) {
    case ActionConstants.MERCHANT_CATEGORY_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ActionConstants.MERCHANT_CATEGORY_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        data: { ...state.data, ...action.payload.data },
      };
    case ActionConstants.MERCHANT_CATEGORY_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        errorMessage: action.payload,
      };
    case ActionConstants.ACTIONS_REQUEST:
      return {
        ...state,
        actionLoading: true,
      };
    case ActionConstants.ACTIONS_SUCCESS:
      return {
        ...state,
        actionLoading: false,
        status: 1,
        statusMessage: action.payload.meta.message,
      };
    case ActionConstants.ACTIONS_ERROR:
      return {
        ...state,
        actionLoading: false,
        status: 0,
        statusMessage: action.payload,
      };
    case ActionConstants.CLEAR_MESSAGES:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
        errorMessage: "",
        actionLoading: false,
        status: null,
        statusMessage: "",
      };
    default:
      return state;
  }
}
