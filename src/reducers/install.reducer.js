import { ActionConstants } from "../constants/ActionConstants";

const initialState = {
  data: {},
  loading: false,
  success: false,
  successMessage: "",
  error: false,
  errorMessage: "",
  checking: false,
  checked: {},
  checkSucess: "",
  checkError: false,
  checkErrorMessage: "",
};

export function installReducer(state = initialState, action) {
  switch (action.type) {
    case ActionConstants.INSTALL_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ActionConstants.INSTALL_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        successMessage: action.payload.meta.message,
        data: { ...state.data, ...action.payload.data },
      };
    case ActionConstants.INSTALL_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        errorMessage: action.payload,
      };
    case ActionConstants.INSTALLATION_CHECK_REQUEST:
      return {
        ...state,
        checking: true,
      };
    case ActionConstants.INSTALLATION_CHECK_SUCCESS:
      return {
        ...state,
        checking: false,
        checkSuccess: true,
        checkSuccessMessage: action.payload.meta.message,
        data: { ...state.data, ...action.payload.data },
      };
    case ActionConstants.INSTALLATION_CHECK_ERROR:
      return {
        ...state,
        checking: false,
        checkSuccess: false,
        checkSuccessMessage: "",
        checkError: true,
        checkErrorMessage: action.payload,
      };
    case ActionConstants.CLEAR_MESSAGES:
      return {
        ...state,
        loading: false,
        success: false,
        successMessage: "",
        error: false,
        errorMessage: "",
        checking: false,
        checkSucess: "",
        checkError: false,
        checkErrorMessage: "",
      };
    default:
      return state;
  }
}
