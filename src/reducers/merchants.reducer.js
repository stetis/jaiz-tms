import { ActionConstants } from "../constants/ActionConstants";

const initialState = {
  data: {},
  loading: false,
  success: false,
  error: false,
  errorMessage: "",
  singleMerchantData: {},
  singleMerchantIsLoading: false,
  singleMerchantSuccess: false,
  singleMerchantError: false,
  singleMerchantErrorMessage: "",
  allMerchantsData: {},
  allMerchantsIsLoading: false,
  allMerchantSuccess: false,
  allMerchantsError: false,
  allMerchantsErrorMessage: "",
  businessData: {},
  businessIsLoading: false,
  businessSuccess: false,
  businessError: false,
  businessErrorMessage: "",
  allBusinessData: {},
  allBusinessIsLoading: false,
  allBusinessSuccess: false,
  allBusinessError: false,
  allBusinessErrorMessage: "",
  actionLoading: false,
  status: null,
  statusMessage: "",
};

export function merchantsReducer(state = initialState, action) {
  switch (action.type) {
    case ActionConstants.MERCHANTS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ActionConstants.MERCHANTS_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        data: { ...state.data, ...action.payload.data },
      };
    case ActionConstants.MERCHANTS_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        errorMessage: action.payload,
      };
    case ActionConstants.SINGLE_MERCHANT_REQUEST:
      return {
        ...state,
        singleMerchantIsLoading: true,
      };
    case ActionConstants.SINGLE_MERCHANT_SUCCESS:
      return {
        ...state,
        singleMerchantIsLoading: false,
        singleMerchantSuccess: true,
        singleMerchantData: {
          ...state.singleMerchantData,
          ...action.payload.data,
        },
      };
    case ActionConstants.SINGLE_MERCHANT_ERROR:
      return {
        ...state,
        singleMerchantIsLoading: false,
        singleMerchantSuccess: false,
        singleMerchantError: true,
        singleMerchantErrorMessage: action.payload,
      };
    case ActionConstants.ALL_MERCHANTS_REQUEST:
      return {
        ...state,
        allMerchantsIsLoading: true,
      };
    case ActionConstants.ALL_MERCHANTS_SUCCESS:
      return {
        ...state,
        allMerchantsIsLoading: false,
        allMerchantSuccess: true,
        allMerchantsData: {
          ...state.allMerchantsData,
          ...action.payload.data,
        },
      };
    case ActionConstants.ALL_MERCHANTS_ERROR:
      return {
        ...state,
        allMerchantsIsLoading: false,
        allMerchantSuccess: false,
        allMerchantsError: true,
        allMerchantsErrorMessage: action.payload,
      };
    case ActionConstants.BUSINESSES_REQUEST:
      return {
        ...state,
        businessIsLoading: true,
      };
    case ActionConstants.BUSINESSES_SUCCESS:
      return {
        ...state,
        businessIsLoading: false,
        businessSuccess: true,
        businessData: { ...state.businessData, ...action.payload.data },
      };
    case ActionConstants.BUSINESSES_ERROR:
      return {
        ...state,
        businessIsLoading: false,
        businessSuccess: false,
        error: true,
        errorMessage: action.payload,
      };
    case ActionConstants.ALL_BUSINESSES_REQUEST:
      return {
        ...state,
        allBusinessIsLoading: true,
      };
    case ActionConstants.ALL_BUSINESSES_SUCCESS:
      return {
        ...state,
        allBusinessIsLoading: false,
        allBusinessSuccess: true,
        allBusinessData: { ...state.allBusinessData, ...action.payload.data },
      };
    case ActionConstants.ALL_BUSINESSES_ERROR:
      return {
        ...state,
        allBusinessIsLoading: false,
        allBusinessSuccess: false,
        allBusinessError: true,
        allBusinessErrorMessage: action.payload,
      };
    case ActionConstants.ACTIONS_REQUEST:
      return {
        ...state,
        actionLoading: true,
      };
    case ActionConstants.ACTIONS_SUCCESS:
      return {
        ...state,
        actionLoading: false,
        status: 1,
        statusMessage: action.payload.meta.message,
      };
    case ActionConstants.ACTIONS_ERROR:
      return {
        ...state,
        actionLoading: false,
        status: 0,
        statusMessage: action.payload,
      };
    case ActionConstants.CLEAR_MESSAGES:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
        errorMessage: "",
        singleMerchantIsLoading: false,
        singleMerchantSuccess: false,
        singleMerchantError: false,
        allMerchantsIsLoading: false,
        allMerchantSuccess: false,
        allMerchantsError: false,
        allMerchantsErrorMessage: "",
        businessIsLoading: false,
        businessSuccess: false,
        businessError: false,
        businessErrorMessage: "",
        allBusinessIsLoading: false,
        allBusinessSuccess: false,
        allBusinessError: false,
        allBusinessErrorMessage: "",
        actionLoading: false,
        status: null,
        statusMessage: "",
      };
    default:
      return state;
  }
}
