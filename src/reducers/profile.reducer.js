import { ActionConstants } from "../constants/ActionConstants";

const initialState = {
  data: {},
  loading: false,
  success: false,
  error: false,
  errorMessage: "",
  passport: {},
  passportLoading: false,
  passportSuccess: false,
  passportError: false,
  passportErrorMessage: "",
  getPassport: {},
  getPassportLoading: false,
  getPassportSuccess: false,
  getPassportError: false,
  getPassportErrorMessage: "",
  role: {},
  roleLoading: false,
  roleSuccess: false,
  roleError: false,
  roleErrorMessage: "",
  roles: {},
  rolesLoading: false,
  rolesSuccess: false,
  rolesError: false,
  rolesErrorMessage: "",
  module: {},
  moduleLoading: false,
  moduleSuccess: false,
  moduleError: false,
  moduleErrorMessage: "",
};

export function profileReducer(state = initialState, action) {
  switch (action.type) {
    case ActionConstants.ACTIONS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ActionConstants.ACTIONS_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        data: { ...state.data, ...action.payload.data },
      };
    case ActionConstants.ACTIONS_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        errorMessage: action.payload,
      };
    case ActionConstants.PASSPORT_REQUEST:
      return {
        ...state,
        passportLoading: true,
      };
    case ActionConstants.PASSPORT_SUCCESS:
      return {
        ...state,
        passportLoading: false,
        passportSuccess: true,
        passportError: false,
        passport: { ...state.passport, ...action.payload.data },
      };
    case ActionConstants.PASSPORT_ERROR:
      return {
        ...state,
        passportLoading: false,
        passportSuccess: false,
        passportError: true,
        passportErrorMessage: action.payload,
      };
    case ActionConstants.GET_PASSPORT_REQUEST:
      return {
        ...state,
        getPassportLoading: true,
      };
    case ActionConstants.GET_PASSPORT_SUCCESS:
      return {
        ...state,
        getPassportLoading: false,
        getPassportSuccess: true,
        getPassportError: false,
        getPassport: { ...state.getPassport, ...action.payload.data },
      };
    case ActionConstants.GET_PASSPORT_ERROR:
      return {
        ...state,
        getPassportLoading: false,
        getPassportSuccess: false,
        getPassportError: true,
        getPassportErrorMessage: action.payload,
      };
    case ActionConstants.GET_ROLES_REQUEST:
      return {
        ...state,
        rolesLoading: true,
      };
    case ActionConstants.GET_ROLES_SUCCESS:
      return {
        ...state,
        rolesLoading: false,
        rolesSuccess: true,
        rolesError: false,
        roles: { ...state.roles, ...action.payload.data },
      };
    case ActionConstants.GET_ROLES_ERROR:
      return {
        ...state,
        rolesLoading: false,
        rolesSuccess: false,
        rolesError: true,
        rolesErrorMessage: action.payload,
      };

    case ActionConstants.GET_ROLE_REQUEST:
      return {
        ...state,
        roleLoading: true,
      };
    case ActionConstants.GET_ROLE_SUCCESS:
      return {
        ...state,
        roleLoading: false,
        roleSuccess: true,
        roleError: false,
        role: { ...state.role, ...action.payload.data },
      };
    case ActionConstants.GET_ROLE_ERROR:
      return {
        ...state,
        roleLoading: false,
        roleSuccess: false,
        roleError: true,
        roleErrorMessage: action.payload,
      };
    case ActionConstants.GET_MODULE_REQUEST:
      return {
        ...state,
        moduleLoading: true,
      };
    case ActionConstants.GET_MODULE_SUCCESS:
      return {
        ...state,
        moduleLoading: false,
        moduleSuccess: true,
        moduleError: false,
        module: { ...state.module, ...action.payload.data },
      };
    case ActionConstants.GET_MODULE_ERROR:
      return {
        ...state,
        moduleLoading: false,
        moduleSuccess: false,
        moduleError: true,
        moduleErrorMessage: action.payload,
      };
    case ActionConstants.CLEAR_MESSAGES:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
        errorMessage: "",
        passportLoading: false,
        passportSuccess: false,
        passportError: false,
        passportErrorMessage: "",
        getPassportLoading: false,
        getPassportSuccess: false,
        getPassportError: false,
        getPassportErrorMessage: "",
        roleLoading: false,
        roleSuccess: false,
        roleError: false,
        roleErrorMessage: "",
        rolesLoading: false,
        rolesSuccess: false,
        rolesError: false,
        rolesErrorMessage: "",
        moduleLoading: false,
        moduleSuccess: false,
        moduleError: false,
        moduleErrorMessage: "",
      };
    default:
      return state;
  }
}
