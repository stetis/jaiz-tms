import { ActionConstants } from "../constants/ActionConstants";

const initialState = {
  data: {},
  loading: false,
  success: false,
  error: false,
  errorMessage: "",
  agentData: {},
  agentLoading: false,
  agentSuccess: false,
  agentError: false,
  agentErrorMessage: "",
  commissionData: {},
  commissionLoading: false,
  commissionSuccess: false,
  commissionError: false,
  commissionErrorMessage: "",
  merchantPerformanceData: {},
  merchantPerformanceLoading: false,
  merchantPerformanceSuccess: false,
  merchantPerformanceError: false,
  merchantPerformanceErrorMessage: "",
  branchAnalyticsData: {},
  branchAnalyticsLoading: false,
  branchAnalyticsSuccess: false,
  branchAnalyticsError: false,
  branchAnalyticsErrorMessage: "",
  eJournalData: {},
  eJournalLoading: false,
  eJournalSuccess: false,
  eJournalError: false,
  eJournalErrorMessage: "",
  transactionsData: {},
  transactionsLoading: false,
  transactionsSuccess: false,
  transactionsError: false,
  transactionsErrorMessage: "",
};

export function transactionsReducer(state = initialState, action) {
  switch (action.type) {
    case ActionConstants.MERCHANT_TRANSACTIONS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ActionConstants.MERCHANT_TRANSACTIONS_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        data: { ...state.data, ...action.payload.data },
      };
    case ActionConstants.MERCHANT_TRANSACTIONS_ERROR:
      return {
        ...state,
        loading: false,
        success: false,
        error: true,
        errorMessage: action.payload,
      };
    case ActionConstants.AGENT_TRANSACTIONS_REQUEST:
      return {
        ...state,
        agentLoading: true,
      };
    case ActionConstants.AGENT_TRANSACTIONS_SUCCESS:
      return {
        ...state,
        agentLoading: false,
        agentSuccess: true,
        agentData: { ...state.agentData, ...action.payload.data },
      };
    case ActionConstants.AGENT_TRANSACTIONS_ERROR:
      return {
        ...state,
        agentLoading: false,
        agentSuccess: false,
        agentError: true,
        agentErrorMessage: action.payload,
      };
    case ActionConstants.AGENT_COMMISSIONS_REQUEST:
      return {
        ...state,
        commissionLoading: true,
      };
    case ActionConstants.AGENT_COMMISSIONS_SUCCESS:
      return {
        ...state,
        commissionLoading: false,
        commissionSuccess: true,
        commissionData: {
          ...state.commissionData,
          ...action.payload.data,
        },
      };
    case ActionConstants.AGENT_COMMISSIONS_ERROR:
      return {
        ...state,
        commissionLoading: false,
        commissionSuccess: false,
        commissionError: true,
        commissionErrorMessage: action.payload,
      };

    case ActionConstants.MERCHANT_PERFORMANCE_REQUEST:
      return {
        ...state,
        merchantPerformanceLoading: true,
      };
    case ActionConstants.MERCHANT_PERFORMANCE_SUCCESS:
      return {
        ...state,
        merchantPerformanceLoading: false,
        merchantPerformanceSuccess: true,
        merchantPerformanceData: {
          ...state.merchantPerformanceData,
          ...action.payload.data,
        },
      };
    case ActionConstants.MERCHANT_PERFORMANCE_ERROR:
      return {
        ...state,
        merchantPerformanceLoading: false,
        merchantPerformanceSuccess: false,
        merchantPerformanceError: true,
        merchantPerformanceErrorMessage: action.payload,
      };
    case ActionConstants.BRANCH_ANALYTICS_REQUEST:
      return {
        ...state,
        branchAnalyticsLoading: true,
      };
    case ActionConstants.BRANCH_ANALYTICS_SUCCESS:
      return {
        ...state,
        branchAnalyticsLoading: false,
        branchAnalyticsSuccess: true,
        branchAnalyticsData: {
          ...state.branchAnalyticsData,
          ...action.payload.data,
        },
      };
    case ActionConstants.BRANCH_ANALYTICS_ERROR:
      return {
        ...state,
        branchAnalyticsLoading: false,
        branchAnalyticsSuccess: false,
        branchAnalyticsError: true,
        branchAnalyticsErrorMessage: action.payload,
      };
    case ActionConstants.E_JOURNAL_REQUEST:
      return {
        ...state,
        eJournalLoading: true,
      };
    case ActionConstants.E_JOURNAL_SUCCESS:
      return {
        ...state,
        eJournalLoading: false,
        eJournalSuccess: true,
        eJournalData: {
          ...state.eJournalData,
          ...action.payload.data,
        },
      };
    case ActionConstants.E_JOURNAL_ERROR:
      return {
        ...state,
        eJournalLoading: false,
        eJournalSuccess: false,
        eJournalError: true,
        eJournalErrorMessage: action.payload,
      };
    case ActionConstants.TRANSACTIONS_REQUEST:
      return {
        ...state,
        transactionsLoading: true,
      };
    case ActionConstants.TRANSACTIONS_SUCCESS:
      return {
        ...state,
        transactionsLoading: false,
        transactionsSuccess: true,
        transactionsData: {
          ...state.transactionsData,
          ...action.payload.data,
        },
      };
    case ActionConstants.TRANSACTIONS_ERROR:
      return {
        ...state,
        transactionsLoading: false,
        transactionsSuccess: false,
        transactionsError: true,
        transactionsErrorMessage: action.payload,
      };
    case ActionConstants.CLEAR_MESSAGES:
      return {
        ...state,
        loading: false,
        success: false,
        error: false,
        errorMessage: "",
        agentLoading: false,
        agentSuccess: false,
        agentError: false,
        agentErrorMessage: "",
        commissionLoading: false,
        commissionSuccess: false,
        commissionError: false,
        commissionErrorMessage: "",
        merchantPerformanceLoading: false,
        merchantPerformanceSuccess: false,
        merchantPerformanceError: false,
        merchantPerformanceErrorMessage: "",
        branchAnalyticsLoading: false,
        branchAnalyticsSuccess: false,
        branchAnalyticsError: false,
        branchAnalyticsErrorMessage: "",
        eJournalLoading: false,
        eJournalSuccess: false,
        eJournalError: false,
        eJournalErrorMessage: "",
      };
    default:
      return state;
  }
}
