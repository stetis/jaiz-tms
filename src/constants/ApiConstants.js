export const API = {
  url: "http://localhost:8080/",
  // url: "http://192.168.0.175:8080/",

  /* INSTALL API */
  install: "tms/install/complete",
  checkInstallation: "tms/install/check",

  /* Authentication */
  login: "auth/login",
  logout: "auth/logout",
  refreshToken: "auth/refresh",
  initiatePasswordReset: "auth/reset/initiate/",
  validateEmail: "auth/reset/validate/",
  completePasswordReset: "auth/reset/complete",

  /*PROFILE MANAGEMENT */
  updateProfile: "profile-api/v1/update-profile",
  changePassword: "profile-api/v1/change-password",
  changePassport: "profile-api/v1/set-photo",
  getPassport: "profile-api/v1/photo",

  /* ROLE MANAGEMENT API */
  getRoles: "profile-api/v1/roles",
  getRole: "profile-api/v1/role/",
  addRole: "profile-api/v1/add-role",
  editRole: "profile-api/v1/edit-role",
  deleteRole: "profile-api/v1/delete-role/",
  getModules: "profile-api/v1/modules",

  /* TITLE */
  getTitles: "configuration/v1/titles",
  addTitle: "configuration/v1/title/add",
  editTitle: "configuration/v1/title/edit",
  deleteTitle: "configuration/v1/title/delete/",

  /* MERCHANT CATEGORY*/
  getCategories: "configuration/v1/merchantcategories",
  addCategory: "configuration/v1/mc/add",
  bulkUploadCategory: "configuration/v1/mc/upload",
  editCategory: "configuration/v1/mc/edit",
  deleteCategory: "configuration/v1/mc/delete/",

  /* REGIONS*/
  getAllRegions: "configuration/v1/regions",
  addRegion: "configuration/v1/region/add",
  editRegion: "configuration/v1/region/edit",
  deleteRegion: "configuration/v1/region/delete/",

  /* BRANCHES */
  getAllBranches: "configuration/v1/branches",
  getPaginatedBranches: "configuration/v1/branches/paginated?",
  branchBulkUpload: "configuration/v1/branch/upload",
  addBranch: "configuration/v1/branch/add",
  editBranch: "configuration/v1/branch/edit",
  deleteBranch: "configuration/v1/branch/delete/",

  /* ZONES API */
  getPaginatedZones: "configuration/v1/zones/paginated?",
  getAllZone: "configuration/v1/zones",
  getStateZone: "configuration/v1/zones/",
  addZone: "configuration/v1/zone/add",
  editZone: "configuration/v1/zone/edit",
  deleteZone: "configuration/v1/zone/delete/",

  /* COMMISSIONS API */
  getCommission: "configuration/v1/commissions",
  addCommission: "configuration/v1/commission/add",
  editCommission: "configuration/v1/commission/edit",
  deleteCommission: "configuration/v1/commission/delete/",
  /* FEE GROUPS API */
  getFeeType: "configuration/v1/feetypes",
  addFeeGroup: "configuration/v1/feegroup/add",
  editFeeGroup: "configuration/v1/feegroup/edit",
  deleteFeeGroup: "configuration/v1/feegroup/delete/",
  getFeeGroups: "configuration/v1/feegroups",
  getSingleFeegroup: "configuration/v1/feegroup/",

  /* FEE API */
  getFee: "configuration/v1/fee",
  getSingleFee: "configuration/v1/fee/",
  addFee: "configuration/v1/fee/add",
  editFee: "configuration/v1/fee/update",
  deleteFee: "configuration/v1/fee/delete/",
  /* BAND API */
  getBand: "configuration/v1/bands/",
  addBand: "configuration/v1/band/add",
  editBand: "configuration/v1/band/edit",
  deleteBand: "configuration/v1/band/delete/",

  /* DEVICES API */
  getDevices: "configuration/v1/devices",
  addDevice: "configuration/v1/device/add",
  editDevice: "configuration/v1/device/edit",
  deleteDevice: "/configuration/v1/device/delete/",

  /* ACCOUNT TYPES API */
  getAccountType: "configuration/v1/accounttypes",
  addAccountType: "configuration/v1/accounttype/add",
  editAccountType: "configuration/v1/accounttype/edit",
  deleteAccountType: "configuration/v1/accounttype/delete/",

  /* SOFTWARE VERSIONS API */
  getSoftware: "software-api/v1/softwares",
  addSoftware: "software-api/v1/add",
  editSoftware: "software-api/v1/edit",
  addSection: "software-api/v1/section/add",
  editSection: "software-api/v1/section/edit",
  deleteSection: "software-api/v1/section/delete/",
  getVersions: "software-api/v1/software/",
  newVersion: "software-api/v1/update",

  /* USERS API */
  getPaginatedProfiles: "profile-api/v1/profiles?",
  getAllProfiles: "profile-api/v1/profiles",
  addUser: "profile-api/v1/add",
  editUser: "profile-api/v1/edit",
  deleteUser: "profile-api/v1/delete/",
  suspendRecallUser: "profile-api/v1/suspend-recall/",
  changeRole: "/profile-api/v1/change-role",
  /* AUDIT LOG API */
  getAuditlog: "profile-api/v1/auditlogs",

  /* TRANSACTIONS API */
  getMerchantTransactions: "aggregator-api/v1/merchants-transactions?",
  getAgentTransactions: "aggregator-api/v1/agents-transactions?",
  getAgentCommission: "aggregator-api/v1/agents-commissions?",
  getMerchantPerformance: "aggregator-api/v1/merchants-performance?",
  getBranchAnalytics: "aggregator-api/v1/branch-analytics",
  getEjournal: "aggregator-api/v1/ejournal",
  getDashboard: "aggregator-api/v1/dashboard?",

  /* MERCHANTS API */
  getPaginatedMerchants: "merchant-api/v1/merchants?",
  getAllMerchants: "merchant-api/v1/allmerchants",
  addMerchant: "merchant-api/v1/add",
  editMerchant: "merchant-api/v1/edit",
  deleteMerchant: "merchant-api/v1/delete/",
  searchMerchant: "merchant-api/v1/search/",
  singleMerchant: "merchant-api/v1/merchant/",
  enableMerchantProfile: "merchant-api/v1/enable-profile/",
  revokeMerchantProfile: "merchant-api/v1/revoke-profile/",
  initiateMerchantResetPin: "merchant-api/v1/reset-pin/",
  getPaginatedBusinesses: "merchant-api/v1/paginated/businesses",
  getBusinesses: "merchant-api/v1/businesses",
  editBusiness: "merchant-api/v1/business/edit",
  addBusiness: "merchant-api/v1/business/add",
  deleteBusiness: "merchant-api/v1/business/delete/",

  /* AGENTS API */
  getPaginatedAgents: "agent-api/v1/agents?",
  getAllAgents: "agent-api/v1/allagents",
  singleAgent: "agent-api/v1/agent/",
  addAgent: "agent-api/v1/add",
  editAgent: "agent-api/v1/edit",
  deleteAgent: "agent-api/v1/delete/",
  enableAgentProfile: "agent-api/v1/enable-profile/",
  revokeAgentProfile: "agent-api/v1/revoke-profile/",
  initiateAgentResetPin: "agent-api/v1/reset-pin/",
  superAgent: "agent-api/v1/superagents?",
  enableSuperAgentProfile: "agent-api/v1/enable-superagent-profile/",
  revokeSuperAgentProfile: "agent-api/v1/revoke-superagent-profile/",
  initiateSuperAgentResetPin: "agent-api/v1/reset-pin/",
  getAllSuperAgents: "agent-api/v1/all-super-agents",
  singleSuperAgent: "agent-api/v1/super-agent/",
  addSuperAgent: "agent-api/v1/add/superagent",
  editSuperAgent: "agent-api/v1/edit/superagent",
  deleteSuperAgent: "sagent-api/v1/delete/superagent/",

  /* PTSP API */
  getPTSP: "ptsp-api/v1/ptsps?",
  singlePTSP: "ptsp-api/v1/ptsp/",
  addPTSP: "ptsp-api/v1/add",
  editPTSP: "ptsp-api/v1/edit",
  deletePTSP: "ptsp-api/v1/delete/",
  getPTSPContacts: "ptsp-api/v1/contacts/",
  addPTSPContact: "ptsp-api/v1/contact/add",
  editPTSPContact: "ptsp-api/v1/contact/edit",
  deletePTSPContact: "ptsp-api/v1/contact/delete/",
  enablePTSPProfile: "ptsp-api/v1/enable-profile/",
  revokePTSPProfile: "ptsp-api/v1/revoke-profile/",
  initiatePTSPResetPin: "ptsp-api/v1/reset-pin/",

  /* POS INVENTORY API */
  getPOSInventory: "pos-api/v1/inventory",
  uploadBulkPOSInventory: "pos-api/v1/upload",
  addPOSInventory: "pos-api/v1/add",
  editPOSInventory: "pos-api/v1/edit",
  deletePOSInventory: "pos-api/v1/delete/",
  getPoses: "pos-api/v1/poses?",
  singlePos: "pos-api/v1/pos/",

  /* TERMINALS API */
  singleTerminal: "terminal-api/v1/terminal/",
  getTerminals: "terminal-api/v1/terminals/merchant?",
  getAgentTerminals: "terminal-api/v1/terminals/agent?",
  addMerchantTerminal: "terminal-api/v1/add-merchant-terminals",
  bulkMerchantTerminals: "terminal-api/v1/upload-merchant-terminals",
  bulkAgentTerminals: "terminal-api/v1/upload-agent-terminals",
  terminalInventory: "terminal-api/v1/inventory",
  editLocation: "terminal-api/v1/update-location",
  editSlip: "terminal-api/v1/update-slip",
  addAgentTerminal: "terminal-api/v1/add-agent-terminal",
  decommissionTerminal: "terminal-api/v1/decommission",
};
