import { makeStyles } from "@material-ui/core";
import { Colors, Fonts } from "../../assets/themes/theme";

export const styles = makeStyles(() => ({
  card: {
    width: 320,
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    padding: "25px 25px 25px",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 550,
    },
  },
  container: {
    display: "flex",
    // padding: "25px 25px 25px",
    flexWrap: "wrap",
  },
  superAgentCard: {
    width: "95%",
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 25px 25px",
    borderRadius: 5,
    cursor: "pointer",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 580,
    },
    "@media only screen and (min-width: 960px)": {
      width: 940,
    },
  },
  superAgentlogo: {
    color: Colors.primary,
    display: "flex",
    fontSize: 16,
    fontWeight: 700,
    margin: "0px 0 15px",
    fontFamily: Fonts.resizeLabel,
  },
  logo: {
    color: Colors.primary,
    display: "flex",
    fontSize: 16,
    fontWeight: 700,
    margin: "-5px 0 0px",
    fontFamily: Fonts.secondary,
  },
  checkbox: {
    marginLeft: 5,
  },
  avatar: {
    width: 50,
    height: 50,
    float: "right",
    "@media only screen and (min-width: 600px)": {
      position: "relative",
      left: "-1%",
      top: 0,
    },
    "@media only screen and (min-width: 960px)": {
      position: "relative",
      left: "-1%",
      top: 0,
    },
  },
  passportFab: {
    color: Colors.secondary,
    cursor: "pointer",
    height: 25,
    width: 25,
    padding: 15,
    position: "relative",
    top: 47,
    left: 248,
    background: Colors.primary,
    "@media only screen and (min-width: 600px)": {
      top: 47,
      left: "95%",
    },
    "@media only screen and (min-width: 960px)": {
      top: 45,
      left: "87%",
    },
  },
  agentCard: {
    width: "95%",
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 5px 25px",
    borderRadius: 5,
    cursor: "pointer",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 960px)": {
      width: 780,
    },
  },
  addIcon: {
    fontSize: 20,
    color: Colors.secondary,
  },
  superFabIcon: {
    position: "absolute",
    top: 6,
    left: 235,
    "@media only screen and (min-width: 600px)": {
      top: 20,
      left: 450,
      zIndex: 200,
    },
    "@media only screen and (min-width: 960px)": {
      top: 20,
      left: 860,
      zIndex: 200,
    },
  },
  fabIcon: {
    color: Colors.secondary,
    fontWeight: 600,
  },
  icon: {
    fontSize: 18,
    marginRight: 5,
  },
  tableLogo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
    "@media only screen and (max-width: 540px)": {
      marginTop: -25,
    },
  },
  buttonRow: {
    "@media only screen and (max-width: 540px)": {
      position: "absolute",
      zIndex: 1000,
      top: 0,
    },
  },
  fabAddIcon: {
    fontSize: 14,
    color: Colors.secondary,
  },
  editFab: {
    width: 25,
    height: 25,
    padding: 15,
    background: Colors.primary,
    float: "right",
    "@media only screen and (min-width: 600px)": {
      margin: "20px 30px 0px",
    },
  },
  deleteRoot: {
    width: 320,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    display: "flex",
    padding: "12px 18px 20px",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 350,
    },
  },
  listItemIcon: {
    color: Colors.menuListColor,
    minWidth: 22,
    cursor: "pointer",
  },
  moreIcon: {
    margin: "1px 0 0px -10px",
    color: Colors.secondary,
    cursor: "pointer",
  },
  moreText: {
    margin: "2px 0 0px -10px",
    color: Colors.primary,
    cursor: "pointer",
  },
  button: {
    width: "100%",
    display: "flex",
    marginLeft: 5,
    marginRight: 5,
    padding: 2,
    cursor: "pointer",
    color: Colors.menuListColor,
    fontSize: Fonts.font,
  },
  DeleteIcon: {
    color: Colors.buttonError,
    marginTop: 5,
    cursor: "pointer",
  },
  deleteText: {
    color: Colors.buttonError,
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
    margin: "2px 0 0px -10px",
    cursor: "pointer",
  },
}));
