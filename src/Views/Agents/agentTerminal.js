import { Grid, MuiThemeProvider } from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import React from "react";
import { wideTable } from "../../assets/MUI/Table";
import jaiz from "../../images/jaiz-logo.png";
import { styles } from "./styles";
import { Text } from "../../components/Forms";

export default function AgentTerminals(props) {
  const classes = styles();
  const columns = [
    "",
    {
      name: "Serial number",
      options: {
        customBodyRender: (dataIndex, rowIndex) => {
          return (
            <Text
              style={{
                width: 120,
              }}
            >
              {dataIndex}
            </Text>
          );
        },
      },
    },
    {
      name: "Terminal ID",
      options: {
        customBodyRender: (dataIndex, rowIndex) => {
          return (
            <Text
              style={{
                width: 100,
              }}
            >
              {dataIndex}
            </Text>
          );
        },
      },
    },
    {
      name: "Last seen",
      options: {
        customBodyRender: (dataIndex, rowIndex) => {
          return (
            <Text
              style={{
                width: 130,
              }}
            >
              {dataIndex}
            </Text>
          );
        },
      },
    },
    {
      name: "Last transaction",
      options: {
        customBodyRender: (dataIndex, rowIndex) => {
          return (
            <Text
              style={{
                width: 130,
              }}
            >
              {dataIndex}
            </Text>
          );
        },
      },
    },
    "Last update",
    "Status",
    "Enabled",
  ];
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: true,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    textLabels: {
      body: {
        noMatch: "No terminals records found",
      },
    },
  };
  return (
    <div className={classes.card}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <div className={classes.tableLogo}>
            <img
              src={jaiz}
              alt="jaiz logo"
              style={{ width: 20, height: 20, marginRight: "1rem" }}
            />
            Terminals
          </div>
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <MuiThemeProvider theme={wideTable}>
            <MUIDataTable
              data={
                props.terminals && props.terminals.constructor === Array
                  ? props.terminals.map((terminal, i) => {
                      return {
                        "": i + 1,
                        "Terminal ID": terminal["terminal-id"],
                        "Serial number": terminal["serial-number"],
                        "Last seen": terminal["last-seen"],
                        "Last transaction": terminal["last-tx"],
                        "Last update": terminal["last-update"],
                        Status: terminal["status"],
                        Enabled:
                          terminal["enabled"] === true ? "true" : "false",
                      };
                    })
                  : []
              }
              columns={columns}
              options={options}
            />
          </MuiThemeProvider>
        </Grid>
      </Grid>
    </div>
  );
}
