import { Grid, List, ListItemIcon } from "@material-ui/core";
import { EditSharp, PersonAddSharp } from "@material-ui/icons";
import PhotoCameraIcon from "@material-ui/icons/PhotoCamera";
import React, { useEffect, useState } from "react";
import Avatar from "react-avatar";
import Loader from "react-loader-spinner";
import { isValidPhoneNumber } from "react-phone-number-input";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../actions/agents.actions";
import { history } from "../../App";
import Snackbars from "../../assets/Snackbars/index";
import { Colors } from "../../assets/themes/theme";
import {
  DeleteButton,
  Fab,
  SubmitButton,
} from "../../components/Buttons/index";
import { ButtonsRow } from "../../components/Buttons/styles";
import CallOut from "../../components/CallOut/CallOut";
import { Title } from "../../components/contentHolders/Card";
import { Text } from "../../components/Forms/index";
import {
  CheckBox,
  Select,
  TextArea,
  TextField,
} from "../../components/TextField";
import PhoneInput from "../../components/TextField/phoneInput";
import {
  editAgentHelper,
  enableAgentProfileHelper,
  getAllSuperAgentsHelper,
} from "../../helpers/agents.helper";
import { getAccountTypeHelper } from "../../helpers/Configurations/accountType.helper";
import { getBranchesHelper } from "../../helpers/Configurations/branches.helper";
import { feeGroupHelper } from "../../helpers/Configurations/commissions.helper";
import jaiz from "../../images/jaiz-logo.png";
import profileAvatar from "../../images/user.png";
import { states } from "../../utils/data";
import DeleteAgent from "./delete";
import InitiateAgentResetPin from "./reset";
import RevokeAgentProfile from "./revoke";
import { styles } from "./styles";
import AgentTerminals from "./agentTerminal";

export default function EditAgent(props) {
  const classes = styles();
  const imageUploader = React.useRef();
  const inputFields = {
    //eslint-disable-next-line
    email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  };
  const [state, setState] = useState({
    passport: props.row.photo,
    agentID: props.row["agent-id"],
    name: props.row.name,
    gender: props.row.gender,
    phone: props.row.phone,
    email: props.row.email,
    superAgent: props.row["super-agent"],
    branch: props.row.branch,
    feeGroup: props.row["fee-group"],
    accountNumber: props.row["account-number"],
    accountType: props.row["account-type"],
    accountName: props.row["account-name"],
    state: props.row["state-name"],
    lga: props.row["lg-name"],
    address: props.row.address,
    lgaLists: [{ code: props.row["lg-code"], name: props.row["lg-name"] }],
    superaccount: false,
    formError: false,
    disabled: true,
  });
  const dispatch = useDispatch();
  const agents = useSelector(
    (state) => state.agentsReducer.allAgentsData["super-agents"]
  );
  const loading = useSelector((state) => state.agentsReducer.actionLoading);
  const status = useSelector((state) => state.agentsReducer.status);
  const statusMessage = useSelector(
    (state) => state.agentsReducer.statusMessage
  );
  const feeGroups = useSelector(
    (state) => state.commissionsReducer.feeGroupData.groups
  );
  const accountTypes = useSelector(
    (state) => state.accountTypeReducer.data["account-types"]
  );
  const branches = useSelector((state) => state.branchesReducer.data.branches);
  useEffect(() => {
    dispatch(getAccountTypeHelper());
    dispatch(getAllSuperAgentsHelper());
    dispatch(feeGroupHelper());
    dispatch(getBranchesHelper());
  }, []); //eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);
  const handleImageUpload = (e) => {
    e.persist();
    e.preventDefault();
    const { files } = e.target;
    let file = files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setState((prev) => ({
        ...prev,
        passport: reader.result,
      }));
    };
  };
  const handleClick = () => {
    dispatch(enableAgentProfileHelper(props.row.id));
  };
  const handleDisabled = (e) => {
    e.preventDefault();
    setState((prevState) => ({
      ...prevState,
      disabled: false,
    }));
  };
  const handlePhoneInputChange = (value) => {
    if (value) {
      setState((prevState) => ({
        ...prevState,
        phone: value,
      }));
    }
  };
  const handleCheckBoxChange = () => {
    setState((prev) => ({
      ...prev,
      superaccount: !state.superaccount,
    }));
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };
  const handleClose = (e) => {
    e.preventDefault();
    return history.push("/tms/agents");
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const handleStateChange = ({ target: { value } }) => {
    const choosenState = states.find((state) => state.name === value);
    if (choosenState) {
      setState((prev) => ({
        ...prev,
        lgaLists: choosenState.lgs,
        state: value,
      }));
    }
  };
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const notValidPhone = state.phone
      ? isValidPhoneNumber(state.phone) === false
      : false;
    const stateDetail = states.find((st) => st.name === state.state);
    const branchDetail = branches.find(
      (branch) => branch.name === state.branch
    );
    const lgaDetail = state.lgaLists.find((lg) => lg.name === state.lga);
    const feeDetail = feeGroups.find((fee) => fee.name === state.feeGroup);
    const agentDetail = agents.find((agent) => agent.name === state.superAgent);
    const accounttypeDetail = accountTypes.find(
      (type) => type.name === state.accountType
    );
    if (
      !state.superAgent ||
      state.name === "" ||
      state.email === "" ||
      !inputFields.email.test(state.email) ||
      state.phone === "" ||
      notValidPhone === true ||
      state.accountNumber === "" ||
      !state.state ||
      !state.lga ||
      !state.gender
    ) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      const inputData = {
        id: props.row.id,
        photo: state.passport,
        name: state.name,
        address: state.address,
        phone: state.phone,
        email: state.email,
        gender: state.gender,
        "super-agent-id": agentDetail.id,
        "super-agent": state.superAgent,
        "fee-group-id": feeDetail.id,
        "fee-group": state.feeGroup,
        "account-number": state.superaccount
          ? agentDetail["account-number"]
          : state.accountNumber,
        "account-name": state.accountName,
        "account-type": state.accountType,
        "account-type-id": accounttypeDetail.id,
        "branch-id": branchDetail.id,
        branch: state.branch,
        region: branchDetail.region,
        "region-id": branchDetail["region-id"],
        "state-name": state.state,
        "state-code": stateDetail.code,
        "lg-name": state.lga,
        "lg-code": lgaDetail.code,
      };
      dispatch(editAgentHelper(inputData));
    }
  };
  const notValidPhone = state.phone
    ? isValidPhoneNumber(state.phone) === false
    : false;
  const agentDetail =
    agents && agents.find((agent) => agent.name === state.superAgent);
  return (
    <div>
      {state.formError === true
        ? renderFieldError(
            !state.superAgent
              ? "Super agent name is required"
              : state.name === ""
              ? "Name is required"
              : !state.gender
              ? "Gender is required"
              : notValidPhone === true
              ? "Invalid phone number"
              : state.phone === ""
              ? "Phone number is required"
              : state.email === ""
              ? "Email is required"
              : !inputFields.email.test(state.email)
              ? "Invalid email pattern"
              : state.accountNumber === ""
              ? "Account number is required"
              : !state.state
              ? "State is required"
              : !state.feeGroup
              ? "Commission fee group is required"
              : !state.lga
              ? "LGA is required"
              : ""
          )
        : null}
      <div className={classes.card}>
        <Grid container spacing={0}>
          <Grid item xs={12} sm={6} md={6} lg={6}>
            <div className={classes.logo} style={{ marginBottom: -38 }}>
              <img
                src={jaiz}
                alt="jaiz logo"
                style={{ width: 20, height: 20, marginRight: "1rem" }}
              />
              Manage Agent
            </div>
          </Grid>
          <Grid item xs={12} sm={6} md={6} lg={6}>
            <ButtonsRow>
              {props.row["profile-enabled"] === true ? (
                <CallOut
                  open={state.disabled === false}
                  font="30"
                  margin="-30px -20px 0 0"
                  TopAction={
                    <List
                      classes={{ root: classes.button }}
                      onClick={handleDisabled}
                    >
                      <ListItemIcon classes={{ root: classes.listItemIcon }}>
                        <EditSharp className={classes.moreIcon} />
                      </ListItemIcon>
                      <Text className={classes.moreText}>Edit</Text>
                    </List>
                  }
                  BottomAction={<InitiateAgentResetPin row={props.row} />}
                  ThirdAction={<RevokeAgentProfile row={props.row} />}
                  FourthAction={<DeleteAgent row={props.row} />}
                />
              ) : (
                <CallOut
                  font="30"
                  margin="-30px -20px 0 0"
                  TopAction={
                    <List
                      open={state.disabled === false}
                      classes={{ root: classes.button }}
                      onClick={handleDisabled}
                    >
                      <ListItemIcon classes={{ root: classes.listItemIcon }}>
                        <EditSharp className={classes.moreIcon} />
                      </ListItemIcon>
                      <Text className={classes.moreText}>Edit</Text>
                    </List>
                  }
                  BottomAction={
                    <List
                      classes={{ root: classes.button }}
                      onClick={handleClick.bind(this, props.row.id)}
                    >
                      <ListItemIcon classes={{ root: classes.listItemIcon }}>
                        <PersonAddSharp className={classes.moreIcon} />
                      </ListItemIcon>
                      <Text className={classes.moreText}>
                        <span>
                          {loading ? (
                            <Loader
                              type="ThreeDots"
                              color={Colors.primary}
                              height="15"
                              width="30"
                            />
                          ) : (
                            "Enable profile"
                          )}
                        </span>
                      </Text>
                    </List>
                  }
                  ThirdAction={<DeleteAgent row={props.row} />}
                />
              )}
            </ButtonsRow>
          </Grid>
          <Grid item xs={1} sm={6} md={6} lg={6} />
          <Grid item xs={11} sm={6} md={6} lg={6}>
            <Avatar
              size="90"
              round={false}
              src={
                state.passport === "" ||
                state.passport === undefined ||
                state.passport === null
                  ? profileAvatar
                  : state.passport
              }
              className={classes.avatar}
            />
            {state.disabled ? (
              ""
            ) : (
              <>
                <input
                  type="file"
                  accept="image/*"
                  onChange={handleImageUpload}
                  ref={imageUploader}
                  multiple={false}
                  style={{
                    display: "none",
                  }}
                />
                <Fab
                  className={classes.passportFab}
                  onClick={() => imageUploader.current.click()}
                  disabled={state.disabled}
                >
                  <PhotoCameraIcon
                    style={{
                      fontSize: 12,
                    }}
                  />
                </Fab>
              </>
            )}
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <form
              noValidate
              autoComplete="off"
              onSubmit={handleSubmit}
              className={classes.container}
            >
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="superAgent"
                  htmlFor="superAgent"
                  label="Super Agent"
                  name="superAgent"
                  value={state.superAgent}
                  onChange={handleChange}
                  disabled={state.disabled}
                >
                  {agents &&
                    agents.map((agent) => {
                      return (
                        <option key={agent.id} value={agent.name}>
                          {agent.name}
                        </option>
                      );
                    })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="name"
                  htmlFor="name"
                  label="Name"
                  name="name"
                  value={state.name}
                  onChange={handleChange}
                  disabled={state.disabled}
                  grouped
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="gender"
                  htmlFor="gender"
                  label="Gender"
                  name="gender"
                  value={state.gender}
                  onChange={handleChange}
                  disabled={state.disabled}
                >
                  <option value="Female">Female</option>
                  <option value="Male">Male</option>
                </Select>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="email"
                  htmlFor="email"
                  label="Email"
                  name="email"
                  value={state.email}
                  onChange={handleChange}
                  disabled={state.disabled}
                  grouped
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <PhoneInput
                  phonelabel="Phone"
                  name="phone"
                  value={state.phone}
                  onChange={handlePhoneInputChange}
                  disabled={state.disabled}
                />
              </Grid>
              {state.disabled ? null : (
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Title style={{ display: "flex", marginTop: 28 }}>
                    Use super agent account number
                    <span className={classes.checkbox}>
                      <CheckBox
                        id="superaccount"
                        htmlFor="superaccount"
                        type="checkbox"
                        name="superaccount"
                        checked={state.superaccount}
                        value={state.superaccount}
                        onChange={handleCheckBoxChange}
                        disabled={state.disabled}
                      />
                    </span>
                  </Title>
                </Grid>
              )}

              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="accountNumber"
                  htmlFor="accountNumber"
                  label="Account number"
                  name="accountNumber"
                  value={
                    state.superaccount
                      ? agentDetail["account-number"]
                      : state.accountNumber
                  }
                  onChange={handleChange}
                  disabled={state.disabled}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="accountName"
                  htmlFor="accountName"
                  label="Account name"
                  name="accountName"
                  value={state.accountName}
                  onChange={handleChange}
                  disabled={state.disabled}
                  grouped
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="accountType"
                  htmlFor="accountType"
                  label="Account type"
                  name="accountType"
                  value={state.accountType}
                  onChange={handleChange}
                  disabled={state.disabled}
                >
                  {accountTypes &&
                    accountTypes.map((type) => {
                      return (
                        <option value={type.name} key={type.name}>
                          {type.name}
                        </option>
                      );
                    })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <TextArea
                  id="address"
                  htmlFor="address"
                  label="Address"
                  name="address"
                  value={state.address}
                  onChange={handleChange}
                  disabled={state.disabled}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="branch"
                  htmlFor="branch"
                  name="branch"
                  label="Branch"
                  value={state.branch}
                  onChange={handleChange}
                  disabled={state.disabled}
                >
                  {branches &&
                    branches.map((branch) => {
                      return (
                        <option key={branch.id} value={branch.name}>
                          {branch.name}
                        </option>
                      );
                    })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="feeGroup"
                  htmlFor="feeGroup"
                  name="feeGroup"
                  label="Commission fee group"
                  value={state.feeGroup}
                  onChange={handleChange}
                  disabled={state.disabled}
                  grouped
                >
                  {feeGroups &&
                    feeGroups.map((feegroup) => {
                      return (
                        <option key={feegroup.id} value={feegroup.name}>
                          {feegroup.name}
                        </option>
                      );
                    })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="state"
                  htmlFor="state"
                  name="state"
                  label="State"
                  value={state.state}
                  onChange={handleStateChange}
                  disabled={state.disabled}
                >
                  {states.map((state) => {
                    return (
                      <option key={state.name} value={state.name}>
                        {state.name}
                      </option>
                    );
                  })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="lga"
                  htmlFor="lga"
                  name="lga"
                  label="LGA"
                  value={state.lga}
                  onChange={handleChange}
                  disabled={state.disabled}
                  grouped
                >
                  {state.lgaLists.map((lg) => {
                    return (
                      <option key={lg.name} value={lg.name}>
                        {lg.name}
                      </option>
                    );
                  })}
                </Select>
              </Grid>
              {state.disabled ? (
                ""
              ) : (
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      onClick={handleClose}
                      style={{ marginRight: 10 }}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        backgroundColor: loading ? Colors.primary : "",
                        marginTop: 0,
                      }}
                      disableRipple={loading}
                    >
                      <span className="submit-btn">
                        {loading ? (
                          <Loader
                            type="ThreeDots"
                            color="#B28B0A"
                            height="15"
                            width="30"
                          />
                        ) : (
                          "save"
                        )}
                      </span>
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              )}
            </form>
          </Grid>
        </Grid>
      </div>
      <AgentTerminals terminals={props.row.terminals} />
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
