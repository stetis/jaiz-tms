import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../actions/breadcrumbAction";
import Spinner from "../../assets/Spinner";
import { getSingleAgentHelper } from "../../helpers/agents.helper";
import EditAgent from "./edit";

const styleProps = {
  height: 50,
  width: 50,
};
export default function AddAgent(props) {
  const dispatch = useDispatch();
  const singleAgentLoading = useSelector(
    (state) => state.agentsReducer.singleAgentIsLoading
  );
  const singleAgent = useSelector(
    (state) => state.agentsReducer.singleAgentData
  );
  useEffect(() => {
    document.title = `${props.location.state.row.name} | Manage agents`;
    dispatch(
      updateBreadcrumb({
        parent: "Agents",
        child: "Manage agent",
        homeLink: "/tms",
        childLink: "/tms/manage-agents",
      })
    );
    dispatch(getSingleAgentHelper(props.location.state.row.id));
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  return (
    <div>
      {singleAgentLoading ? (
        <Spinner {...styleProps} />
      ) : (
        <EditAgent row={singleAgent} />
      )}
    </div>
  );
}
