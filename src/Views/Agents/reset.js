import { Grid, List, ListItemIcon } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import React, { useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { ResetIcon } from "../../assets/icons/Svg";
import { Colors } from "../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../components/Buttons/index";
import { ButtonsRow } from "../../components/Buttons/styles";
import { Text } from "../../components/Forms/index";
import { initiateAgentResetPinHelper } from "../../helpers/agents.helper";
import jaiz from "../../images/jaiz-logo.png";
import { styles } from "./styles";

export default function InitiateAgentResetPin(props) {
  const classes = styles();
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.agentsReducer.actionLoading);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = (e) => {
    e.preventDefault();
    setOpen(false);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(initiateAgentResetPinHelper(props.row["profile-id"]));
  };
  return (
    <div>
      <List
        classes={{ root: classes.button }}
        onClick={handleClickOpen}
        disabled={loading}
      >
        <ListItemIcon classes={{ root: classes.listItemIcon }}>
          <ResetIcon className={classes.moreIcon} />
        </ListItemIcon>
        <Text className={classes.moreText}>Reset login pin</Text>
      </List>
      <Dialog
        open={open}
        keepMounted
        aria-labelledby="alert-dialog-slide-title"
        className="add-department-dialog"
        aria-describedby="alert-dialog-slide-description"
      >
        <div className={classes.deleteRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <div className={classes.logo}>
                <img
                  src={jaiz}
                  alt="jaiz logo"
                  style={{ width: 20, height: 20, marginRight: "1rem" }}
                />
                Reset login pin
              </div>
            </Grid>
            <Grid
              item
              xs={12}
              sm={12}
              md={12}
              lg={12}
              className={classes.deleteTextStyle}
            >
              <Text style={{ width: "100%", margin: "15px auto" }}>
                Are you sure you want to reset{" "}
                <strong>{props.row.name}'s</strong> login pin?
              </Text>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <ButtonsRow>
                <DeleteButton onClick={handleClose} style={{ marginRight: 10 }}>
                  Cancel
                </DeleteButton>
                <SubmitButton
                  type="submit"
                  disabled={loading}
                  style={{
                    marginTop: 0,
                    width: 65,
                  }}
                  onClick={handleSubmit}
                >
                  {loading ? (
                    <Loader
                      type="ThreeDots"
                      color={Colors.secondary}
                      height="15"
                      width="30"
                    />
                  ) : (
                    "Reset"
                  )}
                </SubmitButton>
              </ButtonsRow>
            </Grid>
          </Grid>
        </div>
      </Dialog>
    </div>
  );
}
