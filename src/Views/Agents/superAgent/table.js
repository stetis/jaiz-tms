import { Grid, MuiThemeProvider } from "@material-ui/core";
import TablePagination from "@material-ui/core/TablePagination";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";
import FilterListOutlinedIcon from "@material-ui/icons/FilterListOutlined";
import NavigateNextOutlinedIcon from "@material-ui/icons/NavigateNextOutlined";
import MUIDataTable from "mui-datatables";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../actions/agents.actions";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import { theme } from "../../../assets/MUI/Table";
import Snackbars from "../../../assets/Snackbars/index";
import Spinner from "../../../assets/Spinner";
import { SubmitButton } from "../../../components/Buttons";
import { ButtonsRow } from "../../../components/Buttons/styles";
import Search from "../../../components/TextField/search";
import { getSuperAgentHelper } from "../../../helpers/agents.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "../styles";

const styleProps = {
  height: 50,
  width: 50,
};
function capitalizeFirstLetters(str) {
  var splitStr = str.toLowerCase().split(" ");
  for (var i = 0; i < splitStr.length; i++) {
    splitStr[i] =
      splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  return splitStr.join(" ");
}
function Merchants(props) {
  const classes = styles();
  const [showButton, setShowButton] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [search, setSearch] = useState("");
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.agentsReducer.loading);
  const agents = useSelector(
    (state) => state.agentsReducer.data["super-agents"]
  );
  const pages = useSelector(
    (state) =>
      state.agentsReducer.data.paging &&
      state.agentsReducer.data.paging["total-record"]
  );
  const error = useSelector((state) => state.agentsReducer.error);
  const errorMessage = useSelector((state) => state.agentsReducer.errorMessage);
  useEffect(() => {
    document.title = "Agents | Jaiz bank TMS";
    const credentials = {
      page: page,
      pagesize: rowsPerPage,
      search: search,
    };
    dispatch(getSuperAgentHelper(credentials));
    dispatch(
      updateBreadcrumb({
        parent: "Super agents",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleChangePage = (event, newPage) => {
    const credentials = {
      page: newPage,
      pagesize: rowsPerPage,
      search: search,
    };
    dispatch(getSuperAgentHelper(credentials));
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    const credentials = {
      page: page,
      pagesize: rowsPerPage,
      search: search,
    };
    dispatch(getSuperAgentHelper(credentials));
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };
  const gotoAddAgent = () => {
    props.history.push("/tms/add-super-agent");
  };
  const gotoEditAgent = (agent) => {
    props.history.push({
      pathname: `/tms/edit-super-agent`,
      state: {
        row: agent,
      },
    });
  };
  const handleRowGotoEditAgent = (rowData) => {
    return props.history.push({
      pathname: `/tms/edit-super-agent`,
      state: {
        row: {
          id: rowData[1],
          name: rowData[2],
        },
      },
    });
  };
  const handleSearchChange = (event) => setSearch(event.target.value);

  const handleSearch = (e) => {
    e.preventDefault();
    const credentials = {
      page: page,
      pagesize: rowsPerPage,
      search: search,
    };
    dispatch(getSuperAgentHelper(credentials));
    setPage(page);
    setSearch(search);
    setRowsPerPage(rowsPerPage);
  };
  const handleKeyPress = (event) => {
    const credentials = {
      page: page,
      pagesize: rowsPerPage,
      search: search,
    };
    if (event.keyCode === 13) {
      dispatch(getSuperAgentHelper(credentials));
      setPage(page);
      setSearch(search);
      setRowsPerPage(rowsPerPage);
    }
  };
  const handleClickOpen = (e) => {
    e.preventDefault();
    setShowButton(!showButton);
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const columns = [
    "",
    "Code",
    "Name",
    "Email",
    "Phone",
    "Branch",
    "Fee group",
    " ",
  ];
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: true,
    sort: false,
    rowHover: false,
    onRowClick: (rowData) => handleRowGotoEditAgent(rowData),
    selectableRows: "none",
    customToolbar: () => (
      <ButtonsRow className={classes.buttonRow}>
        <SubmitButton
          onClick={handleClickOpen}
          className={classes.fabIcon}
          style={{ marginRight: 15 }}
        >
          <FilterListOutlinedIcon className={classes.icon} />
          Filters
        </SubmitButton>
        <SubmitButton
          onClick={gotoAddAgent}
          className={classes.fabIcon}
          style={{ marginTop: 0 }}
        >
          <AddOutlinedIcon className={classes.icon} />
          Add super agent
        </SubmitButton>
      </ButtonsRow>
    ),
    textLabels: {
      body: {
        noMatch: "No super agent records found",
      },
    },
  };
  return (
    <div className={classes.superAgentCard}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!loading ? (
            <MuiThemeProvider theme={theme}>
              <MUIDataTable
                data={
                  agents && agents.constructor === Array
                    ? agents.map((agent, i) => {
                        return {
                          "": rowsPerPage * page + i + 1,
                          Code: agent.id,
                          Name: agent.name,
                          Email: agent.email,
                          Phone: agent.phone,
                          Branch: capitalizeFirstLetters(agent.branch),
                          "Fee group": agent["fee-group"],
                          " ": (
                            <NavigateNextOutlinedIcon
                              style={{ fontSize: 20 }}
                              onClick={gotoEditAgent.bind(this, agent)}
                            />
                          ),
                        };
                      })
                    : []
                }
                title={
                  showButton ? (
                    <Search
                      id="search"
                      htmlFor="search"
                      name="search"
                      value={search}
                      placeholder="search super agent"
                      searchlabel="Search"
                      onChange={handleSearchChange}
                      onKeyUp={handleKeyPress}
                      onClickIcon={handleSearch}
                    />
                  ) : (
                    <div className={classes.tableLogo}>
                      <img
                        src={jaiz}
                        alt="jaiz logo"
                        style={{ width: 20, height: 20, marginRight: "1rem" }}
                      />
                      Super agents
                    </div>
                  )
                }
                columns={columns}
                options={options}
              />
              <TablePagination
                component="div"
                count={pages ? pages : 0}
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={rowsPerPage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                rowsPerPageOptions={[5, 10, 15, 20]}
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
    </div>
  );
}
export default Merchants;
