import { Grid, List, ListItemIcon } from "@material-ui/core";
import { EditSharp, PersonAddSharp } from "@material-ui/icons";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { isValidPhoneNumber } from "react-phone-number-input";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../../actions/agents.actions";
import { history } from "../../../../App";
import Snackbars from "../../../../assets/Snackbars/index";
import { Colors } from "../../../../assets/themes/theme";
import {
  DeleteButton,
  SubmitButton,
} from "../../../../components/Buttons/index";
import { ButtonsRow } from "../../../../components/Buttons/styles";
import CallOut from "../../../../components/CallOut/CallOut";
import { Text } from "../../../../components/Forms/index";
import { Select, TextArea, TextField } from "../../../../components/TextField";
import PhoneInput from "../../../../components/TextField/phoneInput";
import {
  editSuperAgentHelper,
  enableSuperAgentProfileHelper,
} from "../../../../helpers/agents.helper";
import { getBranchesHelper } from "../../../../helpers/Configurations/branches.helper";
import { feeGroupHelper } from "../../../../helpers/Configurations/commissions.helper";
import jaiz from "../../../../images/jaiz-logo.png";
import { styles } from "../../styles";
import DeleteSuperAgent from "./delete";
import InitiateSuperAgentResetPin from "./resetPin";
import RevokeSuperAgentProfile from "./revokeProfile";

export default function EditAgent(props) {
  const classes = styles();
  const inputFields = {
    //eslint-disable-next-line
    email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  };
  const [state, setState] = useState({
    name: props.row.name,
    accountNumber: props.row["account-number"],
    email: props.row.email,
    phone: props.row.phone,
    address: props.row.address,
    branch: props.row.branch,
    feeGroup: props.row["fee-group"],
    formError: false,
    disabled: true,
  });
  const dispatch = useDispatch();
  const feeGroups = useSelector(
    (state) => state.commissionsReducer.feeGroupData.groups
  );
  const branches = useSelector((state) => state.branchesReducer.data.branches);
  const loading = useSelector((state) => state.agentsReducer.actionLoading);
  const status = useSelector((state) => state.agentsReducer.status);
  const statusMessage = useSelector(
    (state) => state.agentsReducer.statusMessage
  );

  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  useEffect(() => {
    dispatch(feeGroupHelper());
    dispatch(getBranchesHelper());
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleDisabled = (e) => {
    e.preventDefault();
    setState((prevState) => ({
      ...prevState,
      disabled: false,
    }));
  };
  const handleClick = () => {
    dispatch(enableSuperAgentProfileHelper(props.row.id));
  };
  const handlePhoneInputChange = (value) => {
    if (value) {
      setState((prevState) => ({
        ...prevState,
        phone: value,
      }));
    }
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };
  const handleClose = (e) => {
    e.preventDefault();
    return history.push("/tms/super-agents");
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const notValidPhone = state.phone
      ? isValidPhoneNumber(state.phone) === false
      : false;
    const feeGroupDetail = feeGroups.find((fee) => fee.name === state.feeGroup);
    const branchDetail = branches.find(
      (branch) => branch.name === state.branch
    );

    if (
      state.name === "" ||
      state.accountNumber === "" ||
      (state.email && !inputFields.email.test(state.email)) ||
      state.phone === "" ||
      notValidPhone === true ||
      state.address === "" ||
      !state.feeGroup ||
      !state.branch
    ) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      const inputData = {
        id: props.row.id,
        name: state.name,
        phone: state.phone,
        email: state.email,
        address: state.address,
        "account-number": state.accountNumber,
        "fee-group": state.feeGroup,
        "fee-group-id": feeGroupDetail.id,
        branch: state.branch,
        "branch-id": branchDetail.id,
      };
      dispatch(editSuperAgentHelper(inputData));
    }
  };
  const notValidPhone = state.phone
    ? isValidPhoneNumber(state.phone) === false
    : false;
  return (
    <div>
      {state.formError === true
        ? renderFieldError(
            state.name === ""
              ? "Name is required"
              : state.accountNumber === ""
              ? "Account number is required"
              : state.phone === ""
              ? "Phone number is required"
              : notValidPhone === true
              ? "Invalid phone number"
              : state.email && !inputFields.email.test(state.email)
              ? "Invalid email pattern"
              : state.address === ""
              ? "Agent address is required"
              : !state.feeGroup
              ? "Comission fee group is required"
              : !state.branch
              ? "Branch is required"
              : ""
          )
        : null}
      <div className={classes.card}>
        <Grid container spacing={0}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <form
              noValidate
              autoComplete="off"
              onSubmit={handleSubmit}
              className={classes.container}
            >
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <div className={classes.superAgentlogo}>
                  <img
                    src={jaiz}
                    alt="jaiz logo"
                    style={{ width: 20, height: 20, marginRight: "1rem" }}
                  />
                  Manage super agent
                </div>
              </Grid>
              {props.row["profile-enabled"] === true ? (
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <ButtonsRow>
                    <CallOut
                      font="30"
                      open={state.disabled === false}
                      TopAction={
                        <List
                          classes={{ root: classes.button }}
                          onClick={handleDisabled}
                        >
                          <ListItemIcon
                            classes={{ root: classes.listItemIcon }}
                          >
                            <EditSharp className={classes.moreIcon} />
                          </ListItemIcon>
                          <Text className={classes.moreText}>Edit</Text>
                        </List>
                      }
                      BottomAction={
                        <InitiateSuperAgentResetPin row={props.row} />
                      }
                      ThirdAction={<RevokeSuperAgentProfile row={props.row} />}
                      FourthAction={<DeleteSuperAgent row={props.row} />}
                    />
                  </ButtonsRow>
                </Grid>
              ) : (
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <ButtonsRow>
                    <CallOut
                      font="30"
                      open={state.disabled === false}
                      TopAction={
                        <List
                          classes={{ root: classes.button }}
                          onClick={handleDisabled}
                        >
                          <ListItemIcon
                            classes={{ root: classes.listItemIcon }}
                          >
                            <EditSharp className={classes.moreIcon} />
                          </ListItemIcon>
                          <Text className={classes.moreText}>Edit</Text>
                        </List>
                      }
                      BottomAction={
                        <List
                          classes={{ root: classes.button }}
                          onClick={handleClick.bind(this, props.row.id)}
                        >
                          <ListItemIcon
                            classes={{ root: classes.listItemIcon }}
                          >
                            <PersonAddSharp className={classes.moreIcon} />
                          </ListItemIcon>
                          <Text className={classes.moreText}>
                            <span>
                              {props.loading ? (
                                <Loader
                                  type="ThreeDots"
                                  color={Colors.primary}
                                  height="15"
                                  width="30"
                                />
                              ) : (
                                "Enable profile"
                              )}
                            </span>
                          </Text>
                        </List>
                      }
                      ThirdAction={<DeleteSuperAgent row={props.row} />}
                    />
                  </ButtonsRow>
                </Grid>
              )}

              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="name"
                  htmlFor="name"
                  label="Name"
                  name="name"
                  value={state.name}
                  onChange={handleChange}
                  disabled={state.disabled}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="accountNumber"
                  htmlFor="accountNumber"
                  label="Account number"
                  name="accountNumber"
                  value={state.accountNumber}
                  onChange={handleChange}
                  disabled={state.disabled}
                  grouped
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="email"
                  htmlFor="email"
                  label="Email"
                  name="email"
                  value={state.email}
                  onChange={handleChange}
                  disabled={state.disabled}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <PhoneInput
                  phonelabel="Phone"
                  name="phone"
                  value={state.phone}
                  onChange={handlePhoneInputChange}
                  grouped
                  disabled={state.disabled}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <TextArea
                  id="address"
                  htmlFor="address"
                  label="Address"
                  name="address"
                  value={state.address}
                  onChange={handleChange}
                  disabled={state.disabled}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="branch"
                  htmlFor="branch"
                  name="branch"
                  label="Branch"
                  value={state.branch}
                  onChange={handleChange}
                  disabled={state.disabled}
                  grouped
                >
                  {branches &&
                    branches.map((branch) => {
                      return (
                        <option key={branch.id} value={branch.name}>
                          {branch.name}
                        </option>
                      );
                    })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="feeGroup"
                  htmlFor="feeGroup"
                  name="feeGroup"
                  label="Commission fee group"
                  value={state.feeGroup}
                  onChange={handleChange}
                  disabled={state.disabled}
                  grouped
                >
                  {feeGroups &&
                    feeGroups.map((feegroup) => {
                      return (
                        <option key={feegroup.id} value={feegroup.name}>
                          {feegroup.name}
                        </option>
                      );
                    })}
                </Select>
              </Grid>
              {state.disabled ? (
                ""
              ) : (
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      onClick={handleClose}
                      style={{ marginRight: 10 }}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        marginTop: 0,
                        width: 65,
                      }}
                      disableRipple={loading}
                    >
                      <span className="submit-btn">
                        {loading ? (
                          <Loader
                            type="ThreeDots"
                            color={Colors.secondary}
                            height="15"
                            width="30"
                          />
                        ) : (
                          "save"
                        )}
                      </span>
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              )}
            </form>
          </Grid>
        </Grid>
      </div>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
