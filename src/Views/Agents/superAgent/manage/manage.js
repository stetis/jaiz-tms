import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../../actions/breadcrumbAction";
import { getSingleSuperAgentHelper } from "../../../../helpers/agents.helper";
import { getBranchesHelper } from "../../../../helpers/Configurations/branches.helper";
import { feeGroupHelper } from "../../../../helpers/Configurations/commissions.helper";
import EditAgent from "./edit";
import Spinner from "../../../../assets/Spinner";
import { Text } from "../../../../components/Forms/index";

const styleProps = {
  height: 50,
  width: 50,
};
export default function ManageSuperAgent(props) {
  const dispatch = useDispatch();
  const singleSuperAgentLoading = useSelector(
    (state) => state.agentsReducer.singleAgentIsLoading
  );
  const singleSuperAgent = useSelector(
    (state) => state.agentsReducer.singleAgentData
  );
  useEffect(() => {
    document.title = `${props.location.state.row.name} | Manage super agents`;
    dispatch(
      updateBreadcrumb({
        parent: "Super agents",
        child: "Manage super agent",
        homeLink: "/tms",
        childLink: "/tms/super-agents",
      })
    );
    dispatch(feeGroupHelper());
    dispatch(getBranchesHelper());
    dispatch(getSingleSuperAgentHelper(props.location.state.row.id));
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div>
      {singleSuperAgentLoading ? (
        <Spinner {...styleProps} />
      ) : singleSuperAgent ? (
        <EditAgent row={singleSuperAgent} />
      ) : (
        <Text>Could not fetch resources</Text>
      )}
    </div>
  );
}
