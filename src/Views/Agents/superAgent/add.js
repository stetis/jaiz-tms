import { Grid } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { isValidPhoneNumber } from "react-phone-number-input";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../actions/agents.actions";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Select, TextArea, TextField } from "../../../components/TextField";
import PhoneInput from "../../../components/TextField/phoneInput";
import { addSuperAgentHelper } from "../../../helpers/agents.helper";
import { getBranchesHelper } from "../../../helpers/Configurations/branches.helper";
import { feeGroupHelper } from "../../../helpers/Configurations/commissions.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "../styles";
import { NUBAN } from "../../../utils/NUBAN";

export default function AddAgent(props) {
  const classes = styles();
  const inputFields = {
    //eslint-disable-next-line
    email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  };
  const [state, setState] = useState({
    name: "",
    accountNumber: "",
    email: "",
    phone: "",
    address: "",
    branch: "",
    feeGroup: "",
    formError: false,
  });
  const dispatch = useDispatch();
  const feeGroups = useSelector(
    (state) => state.commissionsReducer.feeGroupData.groups
  );
  const branches = useSelector((state) => state.branchesReducer.data.branches);
  const loading = useSelector((state) => state.agentsReducer.actionLoading);
  const status = useSelector((state) => state.agentsReducer.status);
  const statusMessage = useSelector(
    (state) => state.agentsReducer.statusMessage
  );
  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  useEffect(() => {
    document.title = "Add agent | Agents";
    dispatch(
      updateBreadcrumb({
        parent: "Super agents",
        child: "Add super agent",
        homeLink: "/tms",
        childLink: "/tms/agents",
      })
    );
    dispatch(feeGroupHelper());
    dispatch(getBranchesHelper());
  }, []); //eslint-disable-line react-hooks/exhaustive-deps

  const handlePhoneInputChange = (value) => {
    if (value) {
      setState((prevState) => ({
        ...prevState,
        phone: value,
      }));
    }
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };
  const handleClose = (e) => {
    e.preventDefault();
    props.history.goBack();
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const notValidPhone = state.phone
      ? isValidPhoneNumber(state.phone) === false
      : false;
    const notValidAccountNumber = NUBAN("301", state.accountNumber);
    const feeGroupDetail = feeGroups.find((fee) => fee.name === state.feeGroup);
    const branchDetail = branches.find(
      (branch) => branch.name === state.branch
    );

    if (
      state.name === "" ||
      state.accountNumber === "" ||
      notValidAccountNumber === false ||
      notValidAccountNumber ===
        "Account number should not be less/more than 10 digits" ||
      (state.email && !inputFields.email.test(state.email)) ||
      state.phone === "" ||
      notValidPhone === true ||
      state.address === "" ||
      !state.feeGroup ||
      !state.branch
    ) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      const inputData = {
        name: state.name,
        phone: state.phone,
        email: state.email,
        address: state.address,
        "account-number": state.accountNumber,
        "fee-group": state.feeGroup,
        "fee-group-id": feeGroupDetail.id,
        branch: state.branch,
        "branch-id": branchDetail.id,
      };
      dispatch(addSuperAgentHelper(inputData));
    }
  };
  const notValidPhone = state.phone
    ? isValidPhoneNumber(state.phone) === false
    : false;
  const notValidAccountNumber = NUBAN("301", state.accountNumber);
  return (
    <div>
      {state.formError === true
        ? renderFieldError(
            state.name === ""
              ? "Name is required"
              : state.accountNumber === ""
              ? "Account number is required"
              : notValidAccountNumber === false
              ? "Invalid account number/ bank"
              : notValidAccountNumber ===
                "Account number should not be less/more than 10 digits"
              ? "Account number should not be less/more than 10 digits"
              : state.phone === ""
              ? "Phone number is required"
              : notValidPhone === true
              ? "Invalid phone number"
              : state.email && !inputFields.email.test(state.email)
              ? "Invalid email pattern"
              : state.address === ""
              ? "Agent address is required"
              : !state.feeGroup
              ? "Comission fee group is required"
              : !state.branch
              ? "Branch is required"
              : ""
          )
        : null}
      <div className={classes.card}>
        <Grid container spacing={0}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <form
              noValidate
              autoComplete="off"
              onSubmit={handleSubmit}
              className={classes.container}
            >
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <div className={classes.superAgentlogo}>
                  <img
                    src={jaiz}
                    alt="jaiz logo"
                    style={{ width: 20, height: 20, marginRight: "1rem" }}
                  />
                  New super agent
                </div>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="name"
                  htmlFor="name"
                  label="Name"
                  name="name"
                  value={state.name}
                  onChange={handleChange}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="accountNumber"
                  htmlFor="accountNumber"
                  label="Account number"
                  name="accountNumber"
                  value={state.accountNumber}
                  onChange={handleChange}
                  grouped
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="email"
                  htmlFor="email"
                  label="Email"
                  name="email"
                  value={state.email}
                  onChange={handleChange}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <PhoneInput
                  phonelabel="Phone"
                  name="phone"
                  value={state.phone}
                  onChange={handlePhoneInputChange}
                  grouped
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <TextArea
                  id="address"
                  htmlFor="address"
                  label="Address"
                  name="address"
                  value={state.address}
                  onChange={handleChange}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="branch"
                  htmlFor="branch"
                  name="branch"
                  label="Branch"
                  value={state.branch}
                  onChange={handleChange}
                  grouped
                >
                  {branches &&
                    branches.map((branch) => {
                      return (
                        <option key={branch.id} value={branch.name}>
                          {branch.name}
                        </option>
                      );
                    })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="feeGroup"
                  htmlFor="feeGroup"
                  name="feeGroup"
                  label="Commission fee group"
                  value={state.feeGroup}
                  onChange={handleChange}
                  grouped
                >
                  {feeGroups &&
                    feeGroups.map((feegroup) => {
                      return (
                        <option key={feegroup.id} value={feegroup.name}>
                          {feegroup.name}
                        </option>
                      );
                    })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <ButtonsRow>
                  <DeleteButton
                    onClick={handleClose}
                    style={{ marginRight: 10 }}
                  >
                    Cancel
                  </DeleteButton>
                  <SubmitButton
                    type="submit"
                    disabled={loading}
                    style={{
                      marginTop: 0,
                      width: 65,
                    }}
                    disableRipple={loading}
                  >
                    <span className="submit-btn">
                      {loading ? (
                        <Loader
                          type="ThreeDots"
                          color={Colors.secondary}
                          height="15"
                          width="30"
                        />
                      ) : (
                        "save"
                      )}
                    </span>
                  </SubmitButton>
                </ButtonsRow>
              </Grid>
            </form>
          </Grid>
        </Grid>
      </div>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
