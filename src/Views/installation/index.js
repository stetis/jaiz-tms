import { Grid } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { isValidPhoneNumber } from "react-phone-number-input";
import { useDispatch, useSelector } from "react-redux";
import { withRouter } from "react-router";
import { clearMessages } from "../../actions/install.action";
import Snackbars from "../../assets/Snackbars/index";
import { Colors } from "../../assets/themes/theme";
import { SubmitButton } from "../../components/Buttons/index";
import { ButtonsRow } from "../../components/Buttons/styles";
import { Title } from "../../components/contentHolders/Card";
import { Select, TextArea, TextField } from "../../components/TextField/index";
import Password from "../../components/TextField/password";
import PasswordStrengthMeter from "../../components/TextField/passwordStrengthMeter";
import PhoneInput from "../../components/TextField/phoneInput";
import {
  installHelper,
  checkInstallationHelper,
} from "../../helpers/install.helper";
import jaiz from "../../images/jaiz-logo.png";
import { Banks, Countries, Currencies, states } from "../../utils/data";
import { styles } from "./style";

const inputFields = {
  //eslint-disable-next-line
  email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
};

function Installation() {
  const classes = styles();
  const [state, setState] = useState({
    name: "301",
    currency: "Naira",
    country: "Nigeria",
    state: "Abuja",
    lga: "Abuja municipal area council",
    region: "North central",
    lgaLists: [{ code: "FC01", name: "Abuja municipal area council" }],
    regionLists: ["North central"],
    email: "customercare@jaizbankplc.com",
    signature: "Jaiz Bank.",
    phone: "+2347007730000",
    address:
      "No. 73 Ralph Shodeinde Street Central Business District. P.M.B. 31 Garki Abuja, Nigeria",
    receipt: "Thank you for using Jaiz Bank POS. Dail *443# to buy airtime",
    footnote: "Jaiz bank TMS",
    visa: "556",
    masterCard: "556",
    verve: "556",
    displayName: "Abdulkadir Aisha Usman",
    displayEmail: "aisha.abdulkadir@stetis.com",
    displayPhone: "+2348142445807",
    password: "password",
    confirmPassword: "password",
    showPassword: false,
    showConfirmPassword: false,
    formError: false,
  });

  const loading = useSelector((state) => state.installReducer.loading);
  const installError = useSelector((state) => state.installReducer.error);
  const installErrorMessage = useSelector(
    (state) => state.installReducer.errorMessage
  );
  const dispatch = useDispatch();

  useEffect(() => {
    document.title = "installation | setup";
    dispatch(checkInstallationHelper());
  }, []); //eslint-disable-line react-hooks/exhaustive-deps

  // useEffect(() => {
  //   window.addEventListener("online", () => {
  //     <Snackbars
  //       variant="success"
  //       handleClose={handleCloseSnack}
  //       message={"We are back!"}
  //       isOpen={navigator.onLine === true}
  //     />;
  //   });
  //   window.addEventListener("offline", () => {
  //     <Snackbars
  //       variant="error"
  //       handleClose={handleCloseSnack}
  //       message={"No network, Please check your internet connectivity"}
  //       isOpen={navigator.onLine === false}
  //     />;
  //   });
  // }, []);

  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        showError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.showError]);

  const handleStateChange = ({ target: { value } }) => {
    const choosenState = states.find((state) => state.name === value);
    if (choosenState) {
      setState((prev) => ({
        ...prev,
        lgaLists: choosenState.lgs,
        regionLists: choosenState.regions,
        state: value,
      }));
    }
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  const handleClickShowPassword = () => {
    setState((prev) => ({
      ...prev,
      showPassword: !state.showPassword,
    }));
  };
  const handleClickShowConfirmPassword = () => {
    setState((prevState) => ({
      ...prevState,
      showConfirmPassword: !state.showConfirmPassword,
    }));
  };
  const handlePhoneInputChange = (value) => {
    if (value) {
      setState((prevState) => ({
        ...prevState,
        phone: value,
      }));
    }
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const handleSubmit = (e) => {
    e.preventDefault();
    const notValidPhone = state.phone
      ? isValidPhoneNumber(state.phone) === false
      : false;
    const notValidDisplayPhone = state.displayPhone
      ? isValidPhoneNumber(state.displayPhone) === false
      : false;
    const bankDetail = Banks.find((bank) => bank.code === state.name);
    const currencyDetail = Currencies.find(
      (currency) => currency.currency === state.currency
    );
    const countryDetail = Countries.find(
      (country) => country.name === state.country
    );
    const stateDetail = states.find((st) => st.name === state.state);
    if (
      notValidPhone === true ||
      notValidDisplayPhone === true ||
      state.email === "" ||
      !inputFields.email.test(state.email) ||
      state.name === ""
    ) {
      setState((prev) => ({
        ...prev,
        showError: true,
      }));
    } else {
      const inputData = {
        "bank-code": bankDetail.code,
        "bank-name": bankDetail.name,
        currency: currencyDetail.currency,
        country: state.country,
        "country-code": countryDetail.code,
        "currency-code": countryDetail.code,
        symbol: currencyDetail.code,
        "numeric-code": currencyDetail.number,
        "bank-email": state.email,
        "bank-phone": state.phone,
        "bank-address": state.address,
        "receipt-footer": state.receipt,
        "email-signature": state.signature,
        "foot-note": state.footnote,
        "state-code": stateDetail.code,
        "state-name": state.state,
        region: state.region,
        "display-name": state.displayName,
        username: state.displayEmail,
        email: state.displayEmail,
        mobile: state.displayPhone,
        password: state.password,
        "verve-number": state.verve,
        "visa-number": state.visa,
        "mastercard-number": state.masterCard,
      };
      dispatch(installHelper(inputData));
    }
  };

  const renderFieldError = (showErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={showErrorMessage}
        isOpen={state.showError}
      />
    );
  };
  const notValidPhone = state.phone
    ? isValidPhoneNumber(state.phone) === false
    : false;
  const notValidDisplayPhone = state.displayPhone
    ? isValidPhoneNumber(state.displayPhone) === false
    : false;
  return (
    <div>
      {state.showError === true
        ? renderFieldError(
            !state.name
              ? "Bank name is required"
              : state.email === ""
              ? "email is required"
              : state.name === ""
              ? "name is required"
              : !inputFields.email.test(state.email)
              ? "Invalid email pattern"
              : notValidPhone === true
              ? "Invalid phone number"
              : notValidDisplayPhone === true
              ? "Invalid display phone number"
              : ""
          )
        : null}
      <div className={classes.card}>
        <Grid container spacing={1}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <div className={classes.logo}>
              <img
                src={jaiz}
                alt="jaiz logo"
                style={{ width: 25, height: 25, marginRight: "1rem" }}
              />
              Stetis Terminal Management System
            </div>
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <form
              noValidate
              autoComplete="off"
              onSubmit={handleSubmit}
              className={classes.container}
            >
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <Title>Organization</Title>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="name"
                  htmlFor="name"
                  name="name"
                  label="Name"
                  value={state.name}
                  onChange={handleChange}
                >
                  {Banks.map((bank) => {
                    return (
                      <option key={bank.id} value={bank.code}>
                        {bank.name}
                      </option>
                    );
                  })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="currency"
                  htmlFor="currency"
                  name="currency"
                  label="Currency"
                  value={state.currency}
                  onChange={handleChange}
                  grouped
                >
                  {Currencies.map((currency) => {
                    return (
                      <option key={currency.currency} value={currency.currency}>
                        {currency.currency}
                      </option>
                    );
                  })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="country"
                  htmlFor="country"
                  name="country"
                  label="Country"
                  value={state.country}
                  onChange={handleChange}
                >
                  {Countries.map((country) => {
                    return (
                      <option key={country.name} value={country.name}>
                        {country.name}
                      </option>
                    );
                  })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="state"
                  htmlFor="state"
                  name="state"
                  label="State"
                  value={state.state}
                  onChange={handleStateChange}
                  grouped
                >
                  {states.map((state) => {
                    return (
                      <option key={state.name} value={state.name}>
                        {state.name}
                      </option>
                    );
                  })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="lga"
                  htmlFor="lga"
                  name="lga"
                  label="LGA"
                  value={state.lga}
                  onChange={handleChange}
                >
                  {state.lgaLists.map((lg) => {
                    return (
                      <option key={lg.name} value={lg.name}>
                        {lg.name}
                      </option>
                    );
                  })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="region"
                  htmlFor="region"
                  name="region"
                  label="Region"
                  value={state.region}
                  onChange={handleChange}
                  grouped
                >
                  {state.regionLists.map((region) => {
                    return (
                      <option key={region} value={region}>
                        {region}
                      </option>
                    );
                  })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="email"
                  htmlFor="email"
                  name="email"
                  type="email"
                  label="Email"
                  value={state.email}
                  onChange={handleChange}
                />
              </Grid>

              <Grid item xs={12} sm={6} md={6} lg={6}>
                <PhoneInput
                  value={state.phone}
                  phonelabel="Phone"
                  onChange={handlePhoneInputChange}
                  style={{ marginTop: 13 }}
                  grouped={true}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="signature"
                  htmlFor="signature"
                  name="signature"
                  type="signature"
                  label="Email signature"
                  value={state.signature}
                  onChange={handleChange}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="footnote"
                  htmlFor="footnote"
                  name="footnote"
                  type="footnote"
                  label="Foot note"
                  value={state.footnote}
                  onChange={handleChange}
                  grouped
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <TextArea
                  id="address"
                  htmlFor="address"
                  name="address"
                  type="address"
                  label="Address"
                  value={state.address}
                  onChange={handleChange}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <TextArea
                  id="receipt"
                  htmlFor="receipt"
                  name="receipt"
                  type="receipt"
                  label="Receipt footer"
                  value={state.receipt}
                  onChange={handleChange}
                />
              </Grid>

              <Grid item xs={12} sm={12} md={12} lg={12}>
                <Title style={{ marginTop: 5 }}>Acquirer numbers</Title>
              </Grid>
              <Grid item xs={12} sm={4} md={4} lg={4}>
                <TextField
                  id="visa"
                  htmlFor="visa"
                  label="Visa"
                  type="number"
                  name="visa"
                  value={state.visa}
                  onChange={handleChange}
                />
              </Grid>
              <Grid item xs={12} sm={4} md={4} lg={4}>
                <TextField
                  id="verve"
                  htmlFor="verve"
                  label="Verve"
                  name="verve"
                  type="number"
                  value={state.verve}
                  onChange={handleChange}
                  grouped
                />
              </Grid>
              <Grid item xs={12} sm={4} md={4} lg={4}>
                <TextField
                  id="masterCard"
                  htmlFor="masterCard"
                  label="Master card"
                  name="masterCard"
                  type="number"
                  value={state.masterCard}
                  onChange={handleChange}
                  grouped
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <Title>Access credential</Title>
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <TextField
                  id="displayName"
                  htmlFor="displayName"
                  name="displayName"
                  label="Display name"
                  value={state.displayName}
                  onChange={handleChange}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="displayEmail"
                  htmlFor="displayEmail"
                  name="displayEmail"
                  type="displayEmail"
                  label="Email"
                  value={state.displayEmail}
                  onChange={handleChange}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <PhoneInput
                  value={state.displayPhone}
                  phonelabel="Phone"
                  onChange={handlePhoneInputChange}
                  style={{ marginTop: 13 }}
                  grouped={true}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Password
                  id="password"
                  htmlFor="password"
                  name="password"
                  passwordlabel="Enter new password"
                  type={state.showPassword ? "text" : "password"}
                  value={state.password}
                  onChange={handleChange}
                  showPassword={state.showPassword}
                  onClick={handleClickShowPassword}
                />
                {state.password === "" ? null : (
                  <PasswordStrengthMeter password={state.password} />
                )}
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Password
                  id="confirmPassword"
                  htmlFor="confirmPassword"
                  name="confirmPassword"
                  passwordlabel="Confirm new password"
                  type={state.showConfirmPassword ? "text" : "password"}
                  value={state.confirmPassword}
                  onChange={handleChange}
                  showPassword={state.showConfirmPassword}
                  onClick={handleClickShowConfirmPassword}
                  grouped={true}
                />
              </Grid>
              {state.password === "" ? (
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        backgroundColor: loading ? Colors.primary : "",
                        marginTop: 15,
                        width: 65,
                      }}
                      disableRipple={loading}
                    >
                      <span className="submit-btn">
                        {loading ? (
                          <Loader
                            type="ThreeDots"
                            color="#B28B0A"
                            height="15"
                            width="30"
                          />
                        ) : (
                          "Save"
                        )}
                      </span>
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              ) : (
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        backgroundColor: loading ? Colors.primary : "",
                        marginTop: -18,
                        width: 65,
                      }}
                      disableRipple={loading}
                    >
                      <span className="submit-btn">
                        {loading ? (
                          <Loader
                            type="ThreeDots"
                            color="#B28B0A"
                            height="15"
                            width="30"
                          />
                        ) : (
                          "Save"
                        )}
                      </span>
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              )}
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <div className={classes.copyright}>
                  <span className={classes.copyrightSymbol}> &#169;</span>{" "}
                  Stetis Terminal Management System{" "}
                  <span className={classes.yearColor}>
                    {new Date().getFullYear()}{" "}
                  </span>
                  .
                </div>
              </Grid>
            </form>
          </Grid>
        </Grid>
      </div>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={installErrorMessage}
        isOpen={installError}
      />
    </div>
  );
}
export default withRouter(Installation);
