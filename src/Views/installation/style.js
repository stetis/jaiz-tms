import { makeStyles } from "@material-ui/core";
import { Colors, Fonts } from "../../assets/themes/theme";

export const styles = makeStyles(() => ({
  card: {
    width: 320,
    margin: "96px auto",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media screen and (min-width: 600px)": {
      width: 550,
    },
    "@media screen and (min-width: 960px)": {
      width: 650,
    },
  },
  container: {
    display: "flex",
    flexWrap: "wrap",
    padding: "20px 30px 25px",
  },
  logo: {
    color: Colors.primary,
    justifyContent: "left",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 18,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
    display: "flex",
    margin: "10px 0 -10px 25px",
    "&:hover": {
      textDecoration: "none",
    },
  },
  signupText: {
    color: Colors.textColor,
    justifyContent: "flex-start",
    fontSize: 12,
    fontFamily: "'Ubuntu', sans-serif",
    display: "flex",
    alignItems: "center",
    marginTop: 20,
    marginBottom: 20,
    letterSpacing: 0.8,
  },
  copyright: {
    marginTop: 25,
    color: Colors.primary,
    textTransform: "initial",
    display: "flex",
    fontSize: 10,
    fontFamily: Fonts.secondary,
    justifyContent: "flex-end",
    position: "relative",
    top: 17,
    left: 4,
  },
  copyrightSymbol: {
    color: "#B28B0A",
    marginRight: 4,
    marginTop: -2,
    fontSize: 13,
  },
  yearColor: {
    color: "#B28B0A",
    marginLeft: 1,
  },
}));
