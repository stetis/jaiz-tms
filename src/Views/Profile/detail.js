import { Grid } from "@material-ui/core";
// import jaiz from "../../images/jaiz-logo.png";
import PhotoCameraIcon from "@material-ui/icons/PhotoCamera";
import React, { useEffect, useState } from "react";
import Avatar from "react-avatar";
import Loader from "react-loader-spinner";
import { isValidPhoneNumber } from "react-phone-number-input";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../actions/System/users.actions";
import { history } from "../../App";
import Snackbars from "../../assets/Snackbars/index";
import { Colors } from "../../assets/themes/theme";
import {
  DeleteButton,
  Fab,
  SubmitButton,
} from "../../components/Buttons/index";
import { ButtonsRow } from "../../components/Buttons/styles";
import { TextField } from "../../components/TextField/index";
import PhoneInput from "../../components/TextField/phoneInput";
import {
  updatePhotoHelper,
  updateProfileHelper,
} from "../../helpers/profile.helper";
import profileAvatar from "../../images/user.png";
import { styles } from "./styles";
import jaiz from "../../images/jaiz-logo.png";

export default function UpdateProfile(props) {
  const classes = styles();
  const imageUploader = React.useRef();
  const inputFields = {
    //eslint-disable-next-line
    email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  };
  const [state, setState] = useState({
    passport: props.photo,
    name: props.row.name,
    role: props.row.role,
    email: props.row.email,
    phone: props.row.phone,
    formError: false,
    disabled: true,
  });
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.profileReducer.loading);
  const error = useSelector((state) => state.profileReducer.error);
  const errorMessage = useSelector(
    (state) => state.profileReducer.errorMessage
  );
  const passportError = useSelector(
    (state) => state.profileReducer.passportError
  );
  const passportErrorMessage = useSelector(
    (state) => state.profileReducer.passportErrorMessage
  );
  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);
  const handleImageUpload = (e) => {
    e.preventDefault();
    const { files } = e.target;
    let file = files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      handlePassportChange(reader.result);
      setState((prev) => ({
        ...prev,
        passport: reader.result,
      }));
    };
  };
  const handlePassportChange = (passport) => {
    dispatch(updatePhotoHelper({ photo: passport }));
  };
  const handlePhoneInputChange = (value) => {
    if (value) {
      setState((prevState) => ({
        ...prevState,
        phone: value,
      }));
    }
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };
  const handleClose = (e) => {
    e.preventDefault();
    return history.goBack();
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const notValidPhone = state.phone
      ? isValidPhoneNumber(state.phone) === false
      : false;
    if (
      state.name === "" ||
      state.email === "" ||
      !inputFields.email.test(state.email) ||
      state.phone === "" ||
      notValidPhone === true
    ) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      const inputData = {
        photo: state.passport,
        name: state.name,
        phone: state.phone,
        email: state.email,
      };
      dispatch(updateProfileHelper(inputData));
    }
  };
  const notValidPhone = state.phone
    ? isValidPhoneNumber(state.phone) === false
    : false;
  return (
    <div>
      {state.formError === true
        ? renderFieldError(
            state.name === ""
              ? "Name is required"
              : notValidPhone === true
              ? "Invalid phone number"
              : state.phone === ""
              ? "Phone number is required"
              : state.email === ""
              ? "Email is required"
              : !inputFields.email.test(state.email)
              ? "Invalid email pattern"
              : ""
          )
        : null}
      <div className={classes.card}>
        <Grid container spacing={0}>
          <Grid item xs={12} sm={6} md={6} lg={6}>
            <div className={classes.logo}>
              <img
                src={jaiz}
                alt="jaiz logo"
                style={{ width: 20, height: 20, marginRight: "1rem" }}
              />
              Profile
            </div>
          </Grid>
          <Grid item xs={12} sm={6} md={6} lg={6}>
            <Avatar
              size="90"
              round={false}
              src={
                state.passport === "" ||
                state.passport === undefined ||
                state.passport === null
                  ? profileAvatar
                  : state.passport
              }
              className={classes.avatar}
            />
            <input
              type="file"
              accept="image/*"
              onChange={handleImageUpload}
              ref={imageUploader}
              multiple={false}
              style={{
                display: "none",
              }}
            />
            <Fab
              className={classes.passportFab}
              onClick={() => imageUploader.current.click()}
            >
              <PhotoCameraIcon
                style={{
                  color: Colors.secondary,
                  fontSize: 12,
                }}
              />
            </Fab>
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <form
              className={classes.container}
              noValidate
              autoComplete="off"
              onSubmit={handleSubmit}
            >
              {/* <Grid item xs={12} sm={12} md={12} lg={12}>
                <ButtonsRow>
                  {" "}
                  <Fab onClick={handleDisabled} className={classes.fabIcon}>
                    <EditSharpIcon className={classes.editcon} />
                  </Fab>
                </ButtonsRow>
              </Grid> */}
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="name"
                  htmlFor="name"
                  label="Name"
                  name="name"
                  value={state.name}
                  onChange={handleChange}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="role"
                  htmlFor="role"
                  label="Role"
                  name="role"
                  value={state.role}
                  onChange={handleChange}
                  grouped
                  disabled
                  ghost
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="email"
                  htmlFor="email"
                  type="email"
                  label="Email"
                  name="email"
                  value={state.email}
                  onChange={handleChange}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <PhoneInput
                  value={state.phone}
                  phonelabel="Phone"
                  onChange={handlePhoneInputChange}
                  grouped
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <ButtonsRow>
                  <DeleteButton
                    style={{
                      marginRight: 10,
                    }}
                    onClick={handleClose}
                    // disabled={loading}
                  >
                    Cancel
                  </DeleteButton>
                  <SubmitButton
                    type="submit"
                    // disabled={loading}
                    style={{
                      marginTop: 0,
                      width: 65,
                    }}
                  >
                    {loading ? (
                      <Loader
                        type="ThreeDots"
                        color={Colors.secondary}
                        height="15"
                        width="30"
                      />
                    ) : (
                      "update"
                    )}
                  </SubmitButton>
                </ButtonsRow>
              </Grid>
            </form>
          </Grid>
        </Grid>
      </div>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={passportErrorMessage}
        isOpen={passportError}
      />
    </div>
  );
}
