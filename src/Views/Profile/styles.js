import { makeStyles } from "@material-ui/core";
import { Colors, Fonts } from "../../assets/themes/theme";

export const styles = makeStyles(() => ({
  cardHolder: {
    width: "90%",
    margin: "120px auto",
    display: "flex",
    "@media only screen and (min-width: 600px)": {
      width: 500,
    },
  },
  card: {
    width: "100%",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    cursor: "pointer",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
  },

  logo: {
    color: Colors.primary,
    display: "flex",
    fontSize: 16,
    fontWeight: 700,
    margin: "15px 25px 0px",
    fontFamily: Fonts.secondary,
  },
  passwordlogo: {
    color: Colors.primary,
    display: "flex",
    fontSize: 16,
    fontWeight: 700,
    margin: "10px 0 10px",
    fontFamily: Fonts.secondary,
  },
  container: {
    display: "flex",
    padding: "8px 25px 20px",
    flexWrap: "wrap",
  },
  icon: {
    fontSize: 20,
    color: Colors.secondary,
  },
  actionsRoot: {
    width: "100%",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 480px)": {
      width: 400,
    },
  },
  avatar: {
    width: 50,
    height: 50,
    float: "right",
    marginRight: 10,
    "@media only screen and (min-width: 600px)": {
      position: "relative",
      left: 0,
      top: 10,
    },
  },
  passportFab: {
    color: Colors.secondary,
    cursor: "pointer",
    height: 25,
    width: 25,
    padding: 15,
    position: "relative",
    top: 64,
    left: "86%",
    background: Colors.primary,
    "@media only screen and (min-width: 600px)": {
      top: 84,
      left: "83%",
    },
  },
}));
