import React, { useEffect } from "react";
import { Grid } from "@material-ui/core";
import { updateBreadcrumb } from "../../actions/breadcrumbAction";
import { useDispatch, useSelector } from "react-redux";
import { styles } from "./styles";
import UpdateProfile from "./detail";
import ChangePasssword from "./changePassword";
import { ButtonsRow } from "../../components/Buttons/styles";
import { getPhotoHelper } from "../../helpers/profile.helper";

export default function Profile() {
  const classes = styles();
  const dispatch = useDispatch();
  const data = useSelector((state) => state.loginReducer.data);
  const photo = useSelector((state) => state.profileReducer.getPassport.photo);

  useEffect(() => {
    document.title = "Profile management";
    dispatch(
      updateBreadcrumb({
        parent: "Profile management",
        child: "",
        homeLink: "/tms",
        childLink: "/tms/profile-management",
      })
    );
    dispatch(getPhotoHelper());
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div className={classes.cardHolder}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <ButtonsRow style={{ marginBottom: 15 }}>
            <ChangePasssword />
          </ButtonsRow>
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <UpdateProfile row={data["profile"]} photo={photo} />
        </Grid>
      </Grid>
    </div>
  );
}
