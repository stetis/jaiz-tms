import { Grid } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../actions/profile.action";
import Snackbars from "../../assets/Snackbars/index";
import { Colors } from "../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../components/Buttons/index";
import { ButtonsRow } from "../../components/Buttons/styles";
import Password from "../../components/TextField/password";
import PasswordStrengthMeter from "../../components/TextField/passwordStrengthMeter";
import { changePasswordHelper } from "../../helpers/profile.helper";
import jaiz from "../../images/jaiz-logo.png";
import { styles } from "./styles";

export default function ChangePassword() {
  const classes = styles();
  const [state, setState] = useState({
    open: false,
    currentPassword: "",
    password: "",
    confirmPassword: "",
    showCurrentPassword: false,
    showPassword: false,
    showConfirmPassword: false,
    formError: false,
  });
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.profileReducer.actionLoading);
  const error = useSelector((state) => state.profileReducer.error);
  const errorMessage = useSelector(
    (state) => state.profileReducer.errorMessage
  );
  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };
  const handleClickShowPassword = () => {
    setState((prev) => ({
      ...prev,
      showPassword: !state.showPassword,
    }));
  };
  const handleClickShowConfirmPassword = () => {
    setState((prevState) => ({
      ...prevState,
      showConfirmPassword: !state.showConfirmPassword,
    }));
  };
  const handleClickShowCurrentPassword = () => {
    setState((prevState) => ({
      ...prevState,
      showCurrentPassword: !state.showCurrentPassword,
    }));
  };
  const handleClickOpen = () => {
    setState((prev) => ({
      ...prev,
      open: true,
    }));
  };
  const handleClose = (e) => {
    e.preventDefault();
    setState((prev) => ({
      ...prev,
      open: false,
      currentPassword: "",
      password: "",
      confirmPassword: "",
    }));
  };
  const handleCloseSnack = () => dispatch(clearMessages());

  const handleSubmit = (e) => {
    e.preventDefault();
    if (state.currentPassword === "" || state.password === "") {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      const inputData = {
        "current-password": state.currentPassword,
        password: state.password,
        "confirm-password": state.confirmPassword,
      };
      dispatch(changePasswordHelper(inputData));
    }
  };
  const renderFieldError = (formErrorMessage) => {
    <Snackbars
      variant="error"
      handleClose={handleCloseSnack}
      message={formErrorMessage}
      isOpen={state.formError}
    />;
  };
  return (
    <div>
      <SubmitButton onClick={handleClickOpen} ghost>
        Change password
      </SubmitButton>
      {state.formError === true
        ? renderFieldError(
            state.currentPassword === ""
              ? "Current password is required"
              : state.password === ""
              ? "Password is required"
              : state.confirmPassword !== state.password
              ? "Password did not match"
              : ""
          )
        : null}
      <Dialog
        open={state.open}
        keepMounted
        aria-labelledby="alert-dialog-slide-title"
        className="add-branch-dialog"
        aria-describedby="alert-dialog-slide-name"
      >
        <div className={classes.actionsRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12}>
              <form
                className={classes.container}
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.passwordlogo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Change password
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Password
                    id="currentPassword"
                    htmlFor="currentPassword"
                    name="currentPassword"
                    passwordlabel="Current password"
                    type={state.showCurrentPassword ? "text" : "password"}
                    value={state.currentPassword}
                    onChange={handleChange}
                    showPassword={state.showCurrentPassword}
                    onClick={handleClickShowCurrentPassword}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Password
                    id="password"
                    htmlFor="password"
                    name="password"
                    passwordlabel="New password"
                    type={state.showPassword ? "text" : "password"}
                    value={state.password}
                    onChange={handleChange}
                    showPassword={state.showPassword}
                    onClick={handleClickShowPassword}
                  />
                  {state.password === "" ? null : (
                    <PasswordStrengthMeter password={state.password} />
                  )}
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Password
                    id="confirmPassword"
                    htmlFor="confirmPassword"
                    name="confirmPassword"
                    passwordlabel="Confirm new password"
                    type={state.showConfirmPassword ? "text" : "password"}
                    value={state.confirmPassword}
                    onChange={handleChange}
                    showPassword={state.showConfirmPassword}
                    onClick={handleClickShowConfirmPassword}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      style={{
                        marginRight: 10,
                      }}
                      onClick={handleClose}
                      disabled={loading}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        width: 65,
                        marginTop: 0,
                      }}
                    >
                      {loading ? (
                        <Loader
                          type="ThreeDots"
                          color={Colors.secondary}
                          height="15"
                          width="30"
                        />
                      ) : (
                        "change"
                      )}
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </div>
      </Dialog>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
    </div>
  );
}
