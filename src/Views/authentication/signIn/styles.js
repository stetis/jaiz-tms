import { makeStyles } from "@material-ui/core";
import { Colors, Fonts } from "../../../assets/themes/theme";

export const styles = makeStyles(() => ({
  card: {
    width: "85%",
    margin: "120px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 480px)": {
      width: 400,
    },
  },
  container: {
    display: "flex",
    padding: "20px 30px 25px",
    flexWrap: "wrap",
  },
  logo: {
    color: Colors.primary,
    justifyContent: "center",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 20,
    fontWeight: 700,
    fontFamily: "'Ubuntu', sans-serif",
    display: "flex",
    alignItems: "center",
    marginTop: 30,
    "&:hover": {
      color: Colors.textColor,
      textDecoration: "none",
    },
  },
  forgotPasswordText: {
    float: "right",
    fontSize: 12,
    color: Colors.primary,
    marginTop: 7,
    textTransform: "capitalize",
    cursor: "pointer",
    "@media only screen and (min-width: 300px) and (max-width: 700px)": {
      marginBottom: 10,
      marginLeft: 33,
      fontSize: 10,
    },
  },
  copyright: {
    marginTop: 25,
    color: Colors.primary,
    textTransform: "initial",
    display: "flex",
    fontSize: 10,
    fontFamily: Fonts.secondary,
    justifyContent: "flex-end",
  },
  copyrightSymbol: {
    color: "#B28B0A",
    marginRight: 4,
    marginTop: -2,
    fontSize: 13,
  },
  yearColor: {
    color: "#B28B0A",
    marginLeft: 1.5,
  },
}));
