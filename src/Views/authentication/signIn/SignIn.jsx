import { Grid } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { withRouter } from "react-router";
import { clearMessages } from "../../../actions/login.action";
import { history } from "../../../App";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { TextField } from "../../../components/TextField/index";
import Password from "../../../components/TextField/password";
import { userLogin } from "../../../helpers/login.helper";
import jaiz from "../../../images/jaiz.png";
import { styles } from "./styles";

function SignIn() {
  const classes = styles();
  const inputFields = {
    //eslint-disable-next-line
    email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  };
  const [state, setState] = useState({
    email: "aisha.abdulkadir@stetis.com",
    password: "password12345",
    formError: false,
    showPassword: false,
  });
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.loginReducer.loading);
  const error = useSelector((state) => state.loginReducer.error);
  const errorMessage = useSelector((state) => state.loginReducer.errorMessage);

  useEffect(() => {
    document.title = "Login | Authentication";
  }, []);

  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  const forgotPassword = () => history.push("/forgot-password");

  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  const handleClickShowPassword = () => {
    setState((prev) => ({
      ...prev,
      showPassword: !state.showPassword,
    }));
  };
  const handleCloseSnack = () => {
    dispatch(clearMessages());
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (
      state.email === "" ||
      !inputFields.email.test(state.email) ||
      state.password === ""
    ) {
      setState({
        formError: true,
      });
      setTimeout(() => {
        setState({ formError: false });
      }, 3000);
      return false;
    } else {
      const credentials = {
        username: state.email,
        password: state.password,
      };
      dispatch(userLogin(credentials));
    }
  };
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  return (
    <div>
      {state.formError === true
        ? renderFieldError(
            state.email === ""
              ? "email is required"
              : state.email && !inputFields.email.test(state.email)
              ? "Invalid email pattern"
              : state.password === ""
              ? "password is required"
              : ""
          )
        : null}
      <div className={classes.card}>
        <Grid container spacing={0}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <div className={classes.logo}>
              <img
                src={jaiz}
                alt="jaiz logo"
                style={{ width: 70, height: 40 }}
              />
            </div>
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <form
              noValidate
              autoComplete="off"
              onSubmit={handleSubmit}
              className={classes.container}
            >
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <TextField
                  id="email"
                  htmlFor="email"
                  name="email"
                  type="email"
                  label="Username"
                  placeholder="Email"
                  value={state.email}
                  onChange={handleChange}
                  disabled={loading}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <Password
                  id="password"
                  htmlFor="password"
                  name="password"
                  disabled={loading}
                  type={state.showPassword ? "text" : "password"}
                  value={state.password}
                  passwordlabel="Password"
                  onChange={handleChange}
                  showPassword={state.showPassword}
                  onClick={handleClickShowPassword}
                  style={{ marginBottom: 8 }}
                  placeholder="Password"
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <ButtonsRow style={{ margin: "-5px 0 10px" }}>
                  <div
                    className={classes.forgotPasswordText}
                    disabled={loading}
                    onClick={forgotPassword}
                  >
                    forgot password?
                  </div>
                </ButtonsRow>
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <SubmitButton
                  type="submit"
                  disabled={loading}
                  style={{
                    width: "100%",
                    backgroundColor: loading ? Colors.primary : "",
                    marginTop: 10,
                  }}
                  disableRipple={loading}
                >
                  {loading ? (
                    <Loader
                      type="ThreeDots"
                      color="#B28B0A"
                      height="15"
                      width="30"
                    />
                  ) : (
                    "Login"
                  )}
                </SubmitButton>
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <div className={classes.copyright}>
                  <span className={classes.copyrightSymbol}> &#169;</span>{" "}
                  Stetis Terminal Management System{" "}
                  <span className={classes.yearColor}>
                    {new Date().getFullYear()}{" "}
                  </span>
                  .
                </div>
              </Grid>
            </form>
          </Grid>
        </Grid>
      </div>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
    </div>
  );
}
export default withRouter(SignIn);
