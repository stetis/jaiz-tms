import { Grid } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { withRouter } from "react-router";
import { Link, Redirect } from "react-router-dom";
import { clearMessages } from "../../../actions/fortgotPassword.action";
import { history } from "../../../App";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { TextField } from "../../../components/TextField/index";
import { initPasswordReset } from "../../../helpers/resetPassword.helper";
import jaiz from "../../../images/jaiz.png";
import "../../authentication/authPage.css";
import { styles } from "./style";

function ForgotPassword() {
  const classes = styles();
  const [username, setUsername] = useState("");
  const [formError, setFormError] = useState("");
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.forgotPasswordReducer.loading);
  const success = useSelector((state) => state.forgotPasswordReducer.success);
  const error = useSelector((state) => state.forgotPasswordReducer.error);
  const errorMessage = useSelector(
    (state) => state.forgotPasswordReducer.errorMessage
  );

  useEffect(() => {
    document.title = "Initiate password reset | Password reset";
  });
  useEffect(() => {
    const timer = setTimeout(() => {
      setFormError(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [formError]);

  const handleChange = (e) => setUsername(e.target.value);
  const handleCloseSnack = () => dispatch(clearMessages());
  const handleClose = (e) => {
    e.preventDefault();
    return history.push("/");
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (username === "") {
      setFormError(true);
    } else {
      dispatch(initPasswordReset(username));
    }
  };
  const displayResetError = () => {
    return (
      <>
        {error === true ? (
          <Snackbars
            variant="error"
            handleClose={handleCloseSnack}
            message={errorMessage}
            isOpen={error}
          />
        ) : (
          ""
        )}

        {formError === true && username === "" ? (
          <Snackbars
            handleClose={handleCloseSnack}
            variant="error"
            message="Please provide email/phone number"
            isOpen={formError}
          />
        ) : (
          ""
        )}
      </>
    );
  };

  const displayResetFormView = () => {
    return (
      <>
        <div className={classes.card}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Link to="/" className={classes.logo}>
                <img
                  src={jaiz}
                  alt="jaiz logo"
                  style={{ width: 70, height: 40 }}
                />
              </Link>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <form
                className={classes.container}
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.instructionText}>
                    We’ll give you instructions on how to reset your password.
                    An email will be sent to you shortly.
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextField
                    id="username"
                    htmlFor="username"
                    name="username"
                    type="text"
                    label=""
                    value={username}
                    placeholder="Enter your username or phone number"
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow style={{ marginTop: 8 }}>
                    <DeleteButton
                      onClick={handleClose}
                      style={{
                        marginRight: 15,
                      }}
                    >
                      Back to login
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      className={classes.submitAddButton}
                      disabled={loading}
                      style={{
                        marginTop: 0,
                      }}
                    >
                      {loading ? (
                        <Loader
                          type="ThreeDots"
                          color={Colors.secondary}
                          height="15"
                          width="30"
                        />
                      ) : (
                        "initiate reset"
                      )}
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.copyright}>
                    <span className={classes.copyrightSymbol}> &#169;</span>{" "}
                    Stetis Terminal Management System{" "}
                    <span className={classes.yearColor}>
                      {new Date().getFullYear()}{" "}
                    </span>
                    .
                  </div>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </div>
      </>
    );
  };
  const displayResetPasswordSuccessViews = () => {
    return (
      <>
        {success === true ? (
          <Redirect to="/confirm-email" />
        ) : success === false ? (
          <div>
            <div>{displayResetFormView()}</div>
          </div>
        ) : (
          ""
        )}
      </>
    );
  };

  return (
    <div>
      <div>{displayResetPasswordSuccessViews()}</div>
      <div>{displayResetError()}</div>
    </div>
  );
}
export default withRouter(ForgotPassword);
