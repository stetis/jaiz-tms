import React, { useEffect } from "react";
import { styles } from "./style";
import { Grid } from "@material-ui/core";
import { withRouter } from "react-router";
import jaiz from "../../../images/jaiz.png";
import { SubmitButton } from "../../../components/Buttons/index";
import { Link } from "react-router-dom";

function ComfirmSucessPage(props) {
  useEffect(() => {
    document.title = "Success alert | Forgot password";
  });

  const handleConfirm = () => {
    props.history.push("/");
  };
  const classes = styles();
  return (
    <div className={classes.card}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <Link to="/" className={classes.logo}>
            <img src={jaiz} alt="jaiz logo" style={{ width: 70, height: 40 }} />
          </Link>
        </Grid>
        <div className={classes.container}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <div className={classes.successConfirmation}>
              Password Successfully Changed
            </div>
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <SubmitButton
              type="submit"
              style={{
                width: "100%",
                marginTop: 30,
              }}
              onClick={handleConfirm}
            >
              login
            </SubmitButton>
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <div className={classes.copyright}>
              <span className={classes.copyrightSymbol}> &#169;</span> Stetis
              Terminal Management System{" "}
              <span className={classes.yearColor}>
                {new Date().getFullYear()}{" "}
              </span>
              .
            </div>
          </Grid>
        </div>
      </Grid>
    </div>
  );
}
export default withRouter(ComfirmSucessPage);
