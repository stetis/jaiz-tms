import { Grid } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";
import { clearMessages } from "../../../actions/fortgotPassword.action";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { SubmitButton } from "../../../components/Buttons/index";
import { Text } from "../../../components/Forms/index";
import Password from "../../../components/TextField/password";
import PasswordStrengthMeter from "../../../components/TextField/passwordStrengthMeter";
import {
  completePasswordReset,
  validatePasswordReset,
} from "../../../helpers/resetPassword.helper";
import jaiz from "../../../images/jaiz.png";
import { styles } from "./style";

function CompleteForgotPassword(props) {
  const classes = styles();
  const [state, setState] = useState({
    password: "",
    confirmPassword: "",
    showPassword: false,
    showConfirmPassword: false,
    formError: false,
  });
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.forgotPasswordReducer.loading);
  const error = useSelector((state) => state.forgotPasswordReducer.error);
  const errorMessage = useSelector(
    (state) => state.forgotPasswordReducer.errorMessage
  );
  const validating = useSelector(
    (state) => state.forgotPasswordReducer.validateIsLoading
  );
  const validationSuccess = useSelector(
    (state) => state.forgotPasswordReducer.validateSuccess
  );
  const validationError = useSelector(
    (state) => state.forgotPasswordReducer.validateError
  );
  const validationErrorMessage = useSelector(
    (state) => state.forgotPasswordReducer.validateErrorMessage
  );
  useEffect(() => {
    document.title = "Complete password reset | Password reset";
    dispatch(validatePasswordReset(props.match.params.token));
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  const handleClickShowPassword = () => {
    setState((prevState) => ({
      ...prevState,
      showPassword: !state.showPassword,
    }));
  };
  const handleClickShowConfirmPassword = () => {
    setState((prevState) => ({
      ...prevState,
      showConfirmPassword: !state.showConfirmPassword,
    }));
  };

  const handleCloseSnack = () => dispatch(clearMessages());
  const handleSubmit = (e) => {
    e.preventDefault();
    if (
      state.password === "" ||
      state.confirmPassword === "" ||
      state.confirmPassword !== state.password
    ) {
      setState((prevState) => ({
        ...prevState,
        formError: true,
      }));
    } else {
      const credentials = {
        token: props.match.params.token,
        password: state.password,
      };
      dispatch(completePasswordReset(credentials));
    }
  };
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  const ValidationErrors = (
    <div className={classes.card} style={{ padding: "0px 10px 20px" }}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <Link to="/" className={classes.logo}>
            <img src={jaiz} alt="jaiz logo" style={{ width: 70, height: 40 }} />
          </Link>
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <Text style={{ maxWidth: "90%", margin: "10px auto" }}>
            {validationError ? validationErrorMessage : ""}
          </Text>
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <div className={classes.copyright}>
            <span className={classes.copyrightSymbol}> &#169;</span> Stetis
            Terminal Management System{" "}
            <span className={classes.yearColor}>{new Date().getFullYear}</span>.
          </div>
        </Grid>
      </Grid>
    </div>
  );
  const userCompletePasswordReset = (
    <div className={classes.card}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <Link to="#" className={classes.logo}>
            <img src={jaiz} alt="jaiz logo" style={{ width: 70, height: 40 }} />
          </Link>
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <form
            className={classes.container}
            noValidate
            autoComplete="off"
            onSubmit={handleSubmit}
          >
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Password
                id="password"
                htmlFor="password"
                name="password"
                passwordlabel="New password"
                type={state.showPassword ? "text" : "password"}
                value={state.password}
                onChange={handleChange}
                showPassword={state.showPassword}
                onClick={handleClickShowPassword}
              />
              {state.password === "" ? null : (
                <PasswordStrengthMeter password={state.password} />
              )}
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Password
                id="confirmPassword"
                htmlFor="confirmPassword"
                name="confirmPassword"
                passwordlabel="Confirm new password"
                type={state.showConfirmPassword ? "text" : "password"}
                value={state.confirmPassword}
                onChange={handleChange}
                showPassword={state.showConfirmPassword}
                onClick={handleClickShowConfirmPassword}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <SubmitButton
                type="submit"
                disabled={loading}
                style={{
                  marginTop: 20,
                  width: "100%",
                }}
                disableRipple={loading}
                onClick={handleSubmit}
              >
                {loading ? (
                  <Loader
                    type="ThreeDots"
                    color="#B28B0A"
                    height="15"
                    width="30"
                  />
                ) : (
                  "update"
                )}
              </SubmitButton>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <div className={classes.copyright}>
                <span className={classes.copyrightSymbol}> &#169;</span> Stetis
                Terminal Management System{" "}
                <span className={classes.yearColor}>
                  {new Date().getFullYear}{" "}
                </span>
                .
              </div>
            </Grid>
          </form>
        </Grid>
      </Grid>
    </div>
  );
  return (
    <div>
      {state.formError === true
        ? renderFieldError(
            state.password === ""
              ? "Password is required"
              : state.confirmPassword === ""
              ? "Confirm password is required"
              : state.confirmPassword !== state.password
              ? "Password did not match"
              : ""
          )
        : null}

      {validating ? (
        <Loader type="Rings" height={50} width={50} color={Colors.secondary} />
      ) : validationSuccess ? (
        userCompletePasswordReset
      ) : (
        ValidationErrors
      )}
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
    </div>
  );
}
export default withRouter(CompleteForgotPassword);
