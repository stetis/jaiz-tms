import { Grid } from "@material-ui/core";
import React, { useEffect } from "react";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";
import { Colors } from "../../../assets/themes/theme";
import jaiz from "../../../images/jaiz.png";
import { styles } from "./style";

function ComfirmEmailPage() {
  useEffect(() => {
    document.title = "Email confirmation | Password reset";
  });
  const classes = styles();
  return (
    <div className={classes.card}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <Link to="/" className={classes.logo}>
            <img src={jaiz} alt="jaiz logo" style={{ width: 70, height: 40 }} />
          </Link>
        </Grid>
        <div className={classes.container}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <div className={classes.emailConfirmation}>
              A reset link has been sent to your inbox, please check your inbox
              and follow the link to reset your password.
            </div>
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <div className={classes.resend}>
              Did not receive mail?{" "}
              <Link
                to="/forgot-password"
                style={{
                  color: Colors.primary,
                  fontWeight: 700,
                  marginLeft: 2,
                }}
              >
                {" "}
                resend link
              </Link>
            </div>
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <div className={classes.copyright}>
              <span className={classes.copyrightSymbol}> &#169;</span> Stetis
              Terminal Management System{" "}
              <span className={classes.yearColor}>
                {new Date().getFullYear()}{" "}
              </span>
              .
            </div>
          </Grid>
        </div>
      </Grid>
    </div>
  );
}
export default withRouter(ComfirmEmailPage);
