import { makeStyles } from "@material-ui/core";
import { Colors, Fonts } from "../../assets/themes/theme";

export const styles = makeStyles((theme) => ({
  card: {
    width: "90%",
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "25px 25px 25px",
    borderRadius: 5,
    cursor: "pointer",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
  },
  fabIcon: {
    color: Colors.secondary,
    fontWeight: 600,
  },
  icon: {
    fontSize: 18,
    marginRight: 5,
  },
  merchantCard: {
    width: "100%",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    margin: "10px 0",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
  },
  editCard: {
    width: 320,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    margin: "26px auto",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 550,
    },
  },
  container: {
    display: "flex",
    padding: "8px 25px 25px",
    flexWrap: "wrap",
  },
  editOutletFab: {
    width: 25,
    height: 20,
    padding: 15,
    float: "right",
    "@media only screen and (min-width: 600px)": {
      left: -5,
      top: -28,
    },
    "@media only screen and (min-width: 960px)": {
      left: 8,
      top: -30,
    },
    "@media only screen and (min-width: 1280)": {
      top: 15,
    },
    "@media only screen and (min-width: 1920)": {
      top: 15,
    },
  },
  AddOutletButton: {
    position: "relative",
    top: -8,
    left: 0,
    float: "right",
    "@media only screen and (min-width: 600px)": {
      left: -5,
      top: -28,
    },
    "@media only screen and (min-width: 960px)": {
      left: 8,
      top: -30,
    },
    "@media only screen and (min-width: 1280)": {
      // left: 594,
      top: 15,
    },
    "@media only screen and (min-width: 1920)": {
      // left: 594,
      top: 15,
    },
  },
  AddOutletFab: {
    width: 25,
    height: 20,
    padding: 15,
    float: "right",
    marginTop: 0,
    "@media only screen and (min-width: 960px)": {
      position: "absolute",
      left: 695,
      top: 23,
    },
  },
  EditRemoveFab: {
    width: 20,
    height: 18,
    padding: 10,
    margin: 8,
    "@media only screen and (min-width: 600px)": {
      position: "relative",
      top: -52,
      left: 115,
    },
    "@media only screen and (min-width: 960px)": {
      left: 110,
    },
  },
  removeFab: {
    width: 20,
    height: 18,
    padding: 10,
    margin: 8,
    float: "right",
    "@media only screen and (min-width: 600px)": {
      position: "relative",
      top: -52,
      left: 30,
    },
    "@media only screen and (min-width: 960px)": {
      position: "relative",
      top: -52,
      left: 37,
    },
  },
  removeFabIcon: {
    fontSize: 14,
  },
  transLabel: {
    cursor: "pointer",
    fontFamily: Fonts.secondary,
  },
  tableLogo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
    "@media only screen and (max-width: 540px)": {
      marginTop: -25,
    },
  },
  logo: {
    color: Colors.primary,
    display: "flex",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
  },
  deleteRoot: {
    width: 320,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    display: "flex",
    padding: "12px 18px 20px",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 350,
    },
  },
  addIcon: {
    fontSize: 20,
    color: Colors.secondary,
  },
  actionsRoot: {
    width: "95%",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 480px) and (max-width: 1200px)": {
      width: 350,
    },
  },
  buttonRow: {
    "@media only screen and (max-width: 540px)": {
      position: "absolute",
      zIndex: 1000,
      top: 0,
    },
  },
  listItemIcon: {
    color: Colors.secondary,
    minWidth: 22,
    cursor: "pointer",
  },
  moreIcon: {
    color: Colors.secondary,
    margin: "1px 0 0px -10px",
    cursor: "pointer",
  },
  moreText: {
    color: Colors.primary,
    margin: "2px 0 0px 5px",
    cursor: "pointer",
  },
  button: {
    width: "100%",
    display: "flex",
    cursor: "pointer",
    marginLeft: 5,
    marginRight: 5,
    padding: 2,
    color: Colors.menuListColor,
    fontSize: Fonts.font,
  },
  DeleteIcon: {
    color: Colors.buttonError,
    margin: "1px 0 0px -10px",
    cursor: "pointer",
  },
  deleteText: {
    color: Colors.buttonError,
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
    margin: "2px 0 0px -10px",
    cursor: "pointer",
  },
}));
