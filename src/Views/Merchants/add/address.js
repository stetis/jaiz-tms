import { Grid } from "@material-ui/core";
import React from "react";
import Loader from "react-loader-spinner";
import { withRouter } from "react-router";
import { Colors } from "../../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import {
  Select,
  TextArea,
  TextField,
} from "../../../components/TextField/index";
import { styles } from "../styles";
import jaiz from "../../../images/jaiz-logo.png";

function PhysicalAddress(props) {
  const classes = styles();
  const handleClose = (e) => {
    e.preventDefault();
    props.history.push("/tms/merchants");
  };
  const previousStep = (e) => {
    e.preventDefault();
    props.prevStep();
  };

  const {
    values,
    handleChange,
    handleStateChange,
    handleSubmit,
    loading,
  } = props;

  return (
    <div>
      <div className={classes.merchantCard}>
        <Grid container spacing={0}>
          <Grid item xs={12} sm={12}>
            <form
              className={classes.container}
              noValidate
              onSubmit={handleSubmit}
              autoComplete="off"
            >
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <div className={classes.logo}>
                  <img
                    src={jaiz}
                    alt="jaiz logo"
                    style={{ width: 20, height: 20, marginRight: "1rem" }}
                  />
                  Physical address
                </div>
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <TextArea
                  id="address"
                  htmlFor="address"
                  type="text"
                  label="Address"
                  name="address"
                  value={values.address}
                  onChange={handleChange}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="state"
                  htmlFor="state"
                  label="State"
                  name="state"
                  value={values.state}
                  onChange={handleStateChange}
                >
                  {props.states &&
                    props.states.map((state) => {
                      return (
                        <option key={state.name} value={state.name}>
                          {state.name}
                        </option>
                      );
                    })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="lga"
                  htmlFor="lga"
                  label="LGA"
                  name="lga"
                  value={values.lga}
                  onChange={handleChange}
                  grouped
                >
                  {values.lgaLists &&
                    values.lgaLists.map((lg) => {
                      return (
                        <option value={lg.name} key={lg.name}>
                          {lg.name}
                        </option>
                      );
                    })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="zipcode"
                  htmlFor="zipcode"
                  type="text"
                  label="Zip code"
                  name="zipcode"
                  value={values.zipcode}
                  onChange={handleChange}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <ButtonsRow>
                  <DeleteButton onClick={handleClose}>Cancel</DeleteButton>
                  <SubmitButton
                    onClick={previousStep}
                    style={{ margin: "0 10px" }}
                    ghost
                  >
                    previous
                  </SubmitButton>
                  <SubmitButton
                    type="submit"
                    className={classes.submitAddButton}
                    disabled={loading}
                    style={{
                      backgroundColor: loading ? Colors.primary : "",
                      marginTop: 0,
                      width: 65,
                    }}
                  >
                    {loading ? (
                      <Loader
                        type="ThreeDots"
                        color={Colors.secondary}
                        height="15"
                        width="30"
                      />
                    ) : (
                      "Save"
                    )}
                  </SubmitButton>
                </ButtonsRow>
              </Grid>
            </form>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
export default withRouter(PhysicalAddress);
