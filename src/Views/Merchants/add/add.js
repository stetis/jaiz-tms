import {
  makeStyles,
  Step,
  StepConnector,
  StepLabel,
  Stepper,
  Typography,
  withStyles,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { isValidPhoneNumber } from "react-phone-number-input";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import { clearMessages } from "../../../actions/merchants.actions";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors, Fonts } from "../../../assets/themes/theme";
import { getAccountTypeHelper } from "../../../helpers/Configurations/accountType.helper";
import { getBranchesHelper } from "../../../helpers/Configurations/branches.helper";
import { mCatgoryHelper } from "../../../helpers/Configurations/merchant.helper";
import { getTitleHelper } from "../../../helpers/Configurations/titles.helper";
import { addMerchantHelper } from "../../../helpers/mechants.helper";
import { states } from "../../../utils/data";
import PhysicalAddress from "./address";
import Details from "./detail";

const QontoConnector = withStyles({
  alternativeLabel: {
    top: 10,
    left: "calc(-50% + 16px)",
    right: "calc(50% + 16px)",
  },
  active: {
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
    color: Colors.secondary,
    "& $line": {
      borderColor: Colors.contact,
    },
  },
  completed: {
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
    color: Colors.secondary,
    "& $line": {
      borderColor: Colors.secondary,
    },
  },
  line: {
    height: 1,
    border: 0,
    backgroundColor: Colors.secondary,
    borderRadius: 1,
  },
})(StepConnector);

const useStyles = makeStyles(() => ({
  root: {
    width: "95%",
    margin: "26px auto",
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
    backgroundColor: Colors.light,
    "@media only screen and (min-width: 480px)": {
      width: 460,
    },
  },
  instructions: {
    margin: "8px 0",
  },
}));

function getSteps() {
  return ["Merchant detail", "Physical address"];
}

export default function AddMerchantContainer(props) {
  const classes = useStyles();
  const inputFields = {
    //eslint-disable-next-line
    email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  };
  const [state, setState] = useState({
    activeStep: 0,
    merchantID: "",
    merchantName: "",
    contactTitle: "",
    category: "",
    accountType: "",
    accountNumber: "",
    accountName: "",
    contactName: "",
    contactPhone: "",
    contactEmail: "",
    address: "",
    zipcode: "",
    state: "",
    lga: "",
    lgaLists: [],
    formError: false,
  });
  const steps = getSteps();
  const dispatch = useDispatch();
  const categories = useSelector(
    (state) => state.categoryReducer.data && state.categoryReducer.data.records
  );
  const accounttypes = useSelector(
    (state) => state.accountTypeReducer.data["account-types"]
  );
  const branches = useSelector((state) => state.branchesReducer.data.branches);
  const titles = useSelector((state) => state.titleReducer.data.titles);
  const loading = useSelector((state) => state.merchantsReducer.actionLoading);
  const status = useSelector((state) => state.merchantsReducer.status);
  const statusMessage = useSelector(
    (state) => state.merchantsReducer.statusMessage
  );
  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  useEffect(() => {
    document.title = "Add merchant | Jaiz bank TMS";
    dispatch(
      updateBreadcrumb({
        parent: "Merchants",
        child: "Add merchant",
        homeLink: "/tms",
        childLink: "/tms/merchants",
      })
    );
    dispatch(getAccountTypeHelper());
    dispatch(mCatgoryHelper());
    dispatch(getTitleHelper());
    dispatch(getBranchesHelper());
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleStateChange = ({ target: { value } }) => {
    const choosenState = states.find((state) => state.name === value);
    if (choosenState) {
      setState((prev) => ({
        ...prev,
        lgaLists: choosenState.lgs,
        state: value,
      }));
    }
  };
  const handlePhoneInputChange = (value) => {
    if (value) {
      setState((prevState) => ({
        ...prevState,
        contactPhone: value,
      }));
    }
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  const handleNext = () => {
    const notValidPhone = state.contactPhone
      ? isValidPhoneNumber(state.contactPhone) === false
      : false;
    if (
      state.merchantID === "" ||
      state.merchantName === "" ||
      state.category === "" ||
      state.accountType === "" ||
      state.contactPhone === "" ||
      notValidPhone === true ||
      state.contactEmail === "" ||
      (state.contactEmail && !inputFields.email.test(state.contactEmail)) ||
      state.accountNumber === "" ||
      state.accountName === ""
    ) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      setState((prev) => ({
        ...prev,
        activeStep: 1,
      }));
    }
  };

  const handleBack = () => {
    setState((prev) => ({
      ...prev,
      activeStep: 0,
    }));
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const notValidPhone = state.contactPhone
      ? isValidPhoneNumber(state.contactPhone) === false
      : false;
    const categoryDetail = categories.find(
      (category) => category.name === state.category
    );
    const accounttypeDetail = accounttypes.find(
      (type) => type.name === state.accountType
    );
    const stateDetail = states.find((st) => st.name === state.state);
    const branchDetail = branches.find(
      (branch) => branch.name === state.branch
    );
    const lgDetail = state.lgaLists.find((lg) => lg.name === state.lga);

    if (
      state.merchantID === "" ||
      state.merchantName === "" ||
      state.category === "" ||
      state.accountType === "" ||
      state.accountNumber === "" ||
      state.accountName === "" ||
      state.contactName === "" ||
      state.contactPhone === "" ||
      notValidPhone === true ||
      state.contactEmail === "" ||
      (state.contactEmail && !inputFields.email.test(state.contactEmail)) ||
      state.address === "" ||
      state.zipcode === "" ||
      !state.state ||
      !state.lga
    ) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      const inputData = {
        "merchant-id": state.merchantID,
        name: state.merchantName,
        "contact-title": state.contactTitle,
        "contact-name": state.contactName,
        "contact-email": state.contactEmail,
        "contact-phone": state.contactPhone,
        "category-code": categoryDetail.id,
        category: state.category,
        "account-number": state.accountNumber,
        "account-name": state.accountName,
        "account-type": state.accountType,
        "account-type-code": accounttypeDetail.id,
        address: state.address,
        "zip-code": state.zipcode,
        "state-code": stateDetail.code,
        "state-name": state.state,
        "lg-code": lgDetail.code,
        "lg-name": state.lga,
        "branch-id": branchDetail.id,
        branch: state.branch,
        region: branchDetail.region,
        "region-id": branchDetail["region-id"],
      };
      dispatch(addMerchantHelper(inputData));
    }
  };
  const notValidPhone = state.contactPhone
    ? isValidPhoneNumber(state.contactPhone) === false
    : false;
  function getStepContent(step) {
    switch (step) {
      case 0:
        return (
          <Details
            nextStep={handleNext}
            prevStep={handleBack}
            values={state}
            handleChange={handleChange}
            handlePhoneInputChange={handlePhoneInputChange}
            categories={categories}
            accountTypes={accounttypes}
            titles={titles}
            branches={branches}
          />
        );
      case 1:
        return (
          <PhysicalAddress
            values={state}
            prevStep={handleBack}
            handleSubmit={handleSubmit}
            handleChange={handleChange}
            isLoading={props.isLoading}
            states={states}
            handleStateChange={handleStateChange}
            loading={loading}
          />
        );
      default:
        return "Unknown step";
    }
  }
  return (
    <div>
      {state.formError === true
        ? renderFieldError(
            state.merchantID === ""
              ? "Merchant ID is required"
              : state.merchantName === ""
              ? "Merchant's name is required"
              : !state.category
              ? "Category is required"
              : !state.accountType
              ? "Account type is required"
              : !state.state
              ? "State is required"
              : !state.lga
              ? "LGA is required"
              : state.accountNumber === ""
              ? "Account number is required"
              : state.accountName === ""
              ? "Account name is required"
              : state.contactName === ""
              ? "Primary contact name is required"
              : state.contactPhone === ""
              ? "Primary phone number is required"
              : notValidPhone === true
              ? "Invalid phone number"
              : state.contactEmail === ""
              ? "Primary email is required"
              : state.contactEmail &&
                !inputFields.email.test(state.contactEmail)
              ? "Invalid email pattern"
              : ""
          )
        : null}

      <div className={classes.root}>
        <Stepper activeStep={state.activeStep} connector={<QontoConnector />}>
          {steps.map((label) => {
            const stepProps = {};
            const labelProps = {};
            return (
              <Step key={label} {...stepProps}>
                <StepLabel {...labelProps}>{label}</StepLabel>
              </Step>
            );
          })}
        </Stepper>
        <div>
          {state.activeStep === steps.length ? (
            ""
          ) : (
            <div>
              <Typography component="div" className={classes.instructions}>
                {getStepContent(state.activeStep)}
              </Typography>
            </div>
          )}
        </div>
      </div>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
