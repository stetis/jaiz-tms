import { Grid } from "@material-ui/core";
import React from "react";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Title } from "../../../components/contentHolders/Card";
import { Select, TextField } from "../../../components/TextField/index";
import PhoneInput from "../../../components/TextField/phoneInput";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "../styles";
import { withRouter } from "react-router";

function Details(props) {
  const classes = styles();
  const handleClose = (e) => {
    e.preventDefault();
    props.history.push("/tms/merchants");
  };
  const nextStepper = (e) => {
    e.preventDefault();
    props.nextStep();
  };
  const handleKeyPress = (event) => {
    if (event.which === 10 || event.which === 13) {
      event.preventDefault();
    }
  };
  const { values, handleChange, handlePhoneInputChange } = props;
  return (
    <div className={classes.merchantCard}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <form className={classes.container} noValidate autoComplete="off">
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <div className={classes.logo}>
                <img
                  src={jaiz}
                  alt="jaiz logo"
                  style={{ width: 20, height: 20, marginRight: "1rem" }}
                />
                Merchant detail
              </div>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <TextField
                id="merchantID"
                htmlFor="merchantID"
                label="ID"
                name="merchantID"
                value={values.merchantID}
                onChange={handleChange}
                onKeyPress={handleKeyPress}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <TextField
                id="merchantName"
                htmlFor="merchantName"
                label="Name"
                name="merchantName"
                value={values.merchantName}
                onChange={handleChange}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <Select
                id="category"
                htmlFor="category"
                label="Category"
                name="category"
                value={values.category}
                onChange={handleChange}
                grouped
              >
                {props.categories &&
                  props.categories.map((category) => {
                    return (
                      <option value={category.name} key={category.name}>
                        {category.name}
                      </option>
                    );
                  })}
              </Select>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Title>Bank detail</Title>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <TextField
                id="accountNumber"
                htmlFor="accountNumber"
                label="Account number"
                name="accountNumber"
                value={values.accountNumber}
                onChange={handleChange}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <TextField
                id="accountName"
                htmlFor="accountName"
                label="Account name"
                name="accountName"
                value={values.accountName}
                onChange={handleChange}
                grouped
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <Select
                id="accountType"
                htmlFor="accountType"
                label="Account type"
                name="accountType"
                value={values.accountType}
                onChange={handleChange}
              >
                {props.accountTypes &&
                  props.accountTypes.map((type) => {
                    return (
                      <option value={type.name} key={type.name}>
                        {type.name}
                      </option>
                    );
                  })}
              </Select>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <Select
                id="branch"
                htmlFor="branch"
                label="Branch"
                name="branch"
                value={values.branch}
                onChange={handleChange}
                grouped
              >
                {props.branches &&
                  props.branches.map((branch) => {
                    return (
                      <option value={branch.code} key={branch.name}>
                        {branch.name}
                      </option>
                    );
                  })}
              </Select>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Title>Primary contact</Title>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <Select
                id="contactTitle"
                htmlFor="contactTitle"
                label="Title"
                name="contactTitle"
                value={values.contactTitle}
                onChange={handleChange}
              >
                {props.titles &&
                  props.titles.map((title) => {
                    return (
                      <option value={title.name} key={title.name}>
                        {title.name}
                      </option>
                    );
                  })}
              </Select>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <TextField
                id="contactName"
                htmlFor="contactName"
                label="Name"
                name="contactName"
                value={values.contactName}
                onChange={handleChange}
                grouped
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <PhoneInput
                value={values.contactPhone}
                phonelabel="Phone"
                onChange={handlePhoneInputChange}
                style={{ marginTop: 13 }}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <TextField
                id="contactEmail"
                htmlFor="contactEmail"
                type="email"
                label="Email"
                name="contactEmail"
                value={values.contactEmail}
                onChange={handleChange}
                grouped
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <ButtonsRow>
                <DeleteButton onClick={handleClose} style={{ marginRight: 15 }}>
                  Cancel
                </DeleteButton>
                <SubmitButton
                  type="submit"
                  className={classes.submitAddButton}
                  style={{
                    marginTop: 0,
                    width: 65,
                  }}
                  onClick={nextStepper}
                >
                  Next
                </SubmitButton>
              </ButtonsRow>
            </Grid>
          </form>
        </Grid>
      </Grid>
    </div>
  );
}
export default withRouter(Details);
