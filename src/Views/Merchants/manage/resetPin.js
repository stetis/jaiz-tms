import { Grid, List, ListItemIcon } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import React, { useState } from "react";
import { ResetIcon } from "../../../assets/icons/Svg";
import { useDispatch, useSelector } from "react-redux";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Title } from "../../../components/contentHolders/Card";
import { Text } from "../../../components/Forms/index";
import Loader from "react-loader-spinner";
import { Colors } from "../../../assets/themes/theme";
import { initiateMerchantResetPinHelper } from "../../../helpers/mechants.helper";
import { styles } from "../styles";

export default function InitiateMerchantResetPin(props) {
  const classes = styles();
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.merchantsReducer.actionLoading);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = (e) => {
    e.preventDefault();
    setOpen(false);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(initiateMerchantResetPinHelper(props.row["profile-id"]));
  };
  return (
    <div>
      <List classes={{ root: classes.button }} onClick={handleClickOpen}>
        <ListItemIcon classes={{ root: classes.listItemIcon }}>
          <ResetIcon className={classes.moreIcon} />
        </ListItemIcon>
        <Text className={classes.moreText}>Reset login pin</Text>
      </List>
      <Dialog
        open={open}
        keepMounted
        aria-labelledby="alert-dialog-slide-title"
        className="add-department-dialog"
        aria-describedby="alert-dialog-slide-description"
      >
        <div className={classes.deleteRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Title>Reset login pin</Title>
            </Grid>
            <Grid
              item
              xs={12}
              sm={12}
              md={12}
              lg={12}
              className={classes.deleteTextStyle}
            >
              <Text style={{ width: "100%", margin: "15px auto" }}>
                Are you sure you want to reset{" "}
                <strong>{props.row.name}'s</strong> login pin?
              </Text>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <ButtonsRow>
                <DeleteButton onClick={handleClose} style={{ marginRight: 10 }}>
                  Cancel
                </DeleteButton>
                <SubmitButton
                  type="submit"
                  disabled={loading}
                  style={{
                    width: 65,
                    marginTop: 0,
                  }}
                  onClick={handleSubmit}
                >
                  {loading ? (
                    <Loader
                      type="ThreeDots"
                      color={Colors.secondary}
                      height="15"
                      width="30"
                    />
                  ) : (
                    "Reset"
                  )}
                </SubmitButton>
              </ButtonsRow>
            </Grid>
          </Grid>
        </div>
      </Dialog>
    </div>
  );
}
