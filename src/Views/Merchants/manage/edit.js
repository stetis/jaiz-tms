import { Grid, List, ListItemIcon } from "@material-ui/core";
import { EditSharp, PersonAddSharp } from "@material-ui/icons";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { isValidPhoneNumber } from "react-phone-number-input";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../actions/merchants.actions";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import CallOut from "../../../components/CallOut/CallOut";
import { Title } from "../../../components/contentHolders/Card";
import { Text } from "../../../components/Forms/index";
import {
  Select,
  TextArea,
  TextField,
} from "../../../components/TextField/index";
import PhoneInput from "../../../components/TextField/phoneInput";
import { getAccountTypeHelper } from "../../../helpers/Configurations/accountType.helper";
import { getBranchesHelper } from "../../../helpers/Configurations/branches.helper";
import { mCatgoryHelper } from "../../../helpers/Configurations/merchant.helper";
import { getTitleHelper } from "../../../helpers/Configurations/titles.helper";
import jaiz from "../../../images/jaiz-logo.png";
import {
  editMerchantHelper,
  enableMerchantProfileHelper,
} from "../../../helpers/mechants.helper";
import { states } from "../../../utils/data";
import DeleteMerchant from "../delete";
import { styles } from "../styles";
import InitiateMerchantResetPin from "./resetPin";
import RevokeMerchantProfile from "./revokeProfile";
import { history } from "../../../App";
export default function ManageDetail(props) {
  const classes = styles();
  const inputFields = {
    //eslint-disable-next-line
    email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  };
  const [state, setState] = useState({
    merchantID: props.row["merchant-id"],
    merchantName: props.row.name,
    category: props.row.category,
    branch: props.row.branch,
    accountType: props.row["account-type"],
    accountNumber: props.row["account-number"],
    accountName: props.row["account-name"],
    contactName: props.row["contact-name"],
    contactPhone: props.row["contact-phone"],
    contactEmail: props.row["contact-email"],
    contactTitle: props.row["contact-title"],
    address: props.row.address,
    zipcode: props.row["zip-code"],
    state: props.row["state-name"],
    lga: props.row["lg-name"],
    lgaLists: [{ name: props.row["lg-name"], code: props.row["lg-code"] }],
    formError: false,
    disabled: true,
  });
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.merchantsReducer.actionLoading);
  const categories = useSelector(
    (state) => state.categoryReducer.data && state.categoryReducer.data.records
  );
  const accounttypes = useSelector(
    (state) => state.accountTypeReducer.data["account-types"]
  );
  const branches = useSelector((state) => state.branchesReducer.data.branches);
  const titles = useSelector((state) => state.titleReducer.data.titles);
  const status = useSelector((state) => state.merchantsReducer.status);
  const statusMessage = useSelector(
    (state) => state.merchantsReducer.statusMessage
  );
  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);
  useEffect(() => {
    dispatch(getAccountTypeHelper());
    dispatch(mCatgoryHelper());
    dispatch(getTitleHelper());
    dispatch(getBranchesHelper());
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleDisabled = (e) => {
    e.preventDefault();
    setState((prev) => ({
      ...prev,
      disabled: false,
    }));
  };
  const handleClick = () => {
    dispatch(enableMerchantProfileHelper(props.row["merchant-id"]));
  };
  const handleClose = (e) => {
    e.preventDefault();
    return history.push("/tms/merchants");
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };
  const handlePhoneInputChange = (value) => {
    if (value) {
      setState((prevState) => ({
        ...prevState,
        contactPhone: value,
      }));
    }
  };
  const handleStateChange = ({ target: { value } }) => {
    const choosenState = states.find((state) => state.name === value);
    if (choosenState) {
      setState((prev) => ({
        ...prev,
        lgaLists: choosenState.lgs,
        state: value,
      }));
    }
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const notValidPhone = state.contactPhone
      ? isValidPhoneNumber(state.contactPhone) === false
      : false;
    const categoryDetail = categories.find(
      (category) => category.name === state.category
    );
    const accounttypeDetail = accounttypes.find(
      (type) => type.name === state.accountType
    );
    const branchDetail = branches.find(
      (branch) => branch.name === state.branch
    );
    const stateDetail = states.find((st) => st.name === state.state);
    const lgDetail = state.lgaLists.find((lg) => lg.name === state.lga);
    if (
      state.merchantID === "" ||
      state.merchantName === "" ||
      state.category === "" ||
      state.accountType === "" ||
      state.accountNumber === "" ||
      state.accountName === "" ||
      state.contactName === "" ||
      state.contactPhone === "" ||
      notValidPhone === true ||
      state.contactEmail === "" ||
      (state.contactEmail && !inputFields.email.test(state.contactEmail)) ||
      state.address === "" ||
      state.zipcode === "" ||
      !state.state ||
      !state.lga
    ) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      const inputData = {
        id: props.row.id,
        "merchant-id": state.merchantID,
        name: state.merchantName,
        "contact-title": state.contactTitle,
        "contact-name": state.contactName,
        "contact-email": state.contactEmail,
        "contact-phone": state.contactPhone,
        "category-code": categoryDetail.id,
        category: state.category,
        "account-number": state.accountNumber,
        "account-name": state.accountName,
        "account-type": state.accountType,
        "account-type-code": accounttypeDetail.id,
        address: state.address,
        "zip-code": state.zipcode,
        "state-code": stateDetail.code,
        "state-name": state.state,
        "lg-code": lgDetail.code,
        "lg-name": state.lga,
        "branch-id": branchDetail.id,
        branch: state.branch,
        region: branchDetail.region,
        "region-id": branchDetail["region-id"],
      };
      dispatch(editMerchantHelper(inputData));
    }
  };
  const notValidPhone = state.contactPhone
    ? isValidPhoneNumber(state.contactPhone) === false
    : false;
  return (
    <div>
      {state.formError === true
        ? renderFieldError(
            state.merchantID === ""
              ? "Merchant ID is required"
              : state.merchantName === ""
              ? "Merchant's name is required"
              : !state.category
              ? "Category is required"
              : !state.accountType
              ? "Account type is required"
              : !state.state
              ? "State is required"
              : !state.lga
              ? "LGA is required"
              : state.accountNumber === ""
              ? "Account number is required"
              : state.accountName === ""
              ? "Account name is required"
              : state.contactName === ""
              ? "Primary contact name is required"
              : state.contactPhone === ""
              ? "Primary phone number is required"
              : notValidPhone === true
              ? "Invalid phone number"
              : state.contactEmail === ""
              ? "Primary email is required"
              : state.contactEmail &&
                !inputFields.email.test(state.contactEmail)
              ? "Invalid email pattern"
              : ""
          )
        : null}
      <div className={classes.editCard}>
        <Grid container spacing={0}>
          <Grid item xs={12} sm={12}>
            <form
              className={classes.container}
              noValidate
              autoComplete="off"
              onSubmit={handleSubmit}
            >
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <div
                  className={classes.logo}
                  style={{ margin: "20px 0px 0 -6px" }}
                >
                  <img
                    src={jaiz}
                    alt="jaiz logo"
                    style={{ width: 20, height: 20, marginRight: "1rem" }}
                  />
                  Manage Merchant
                </div>
              </Grid>
              {props.row["profile-enabled"] === true ? (
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <ButtonsRow>
                    <CallOut
                      font="30"
                      open={state.disabled === false}
                      TopAction={
                        <List
                          classes={{ root: classes.button }}
                          onClick={handleDisabled}
                        >
                          <ListItemIcon
                            classes={{ root: classes.listItemIcon }}
                          >
                            <EditSharp className={classes.moreIcon} />
                          </ListItemIcon>
                          <Text className={classes.moreText}>Edit</Text>
                        </List>
                      }
                      BottomAction={
                        <InitiateMerchantResetPin row={props.row} />
                      }
                      ThirdAction={<RevokeMerchantProfile row={props.row} />}
                      FourthAction={<DeleteMerchant row={props.row} />}
                    />
                  </ButtonsRow>
                </Grid>
              ) : (
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <ButtonsRow>
                    <CallOut
                      font="30"
                      open={state.disabled === false}
                      TopAction={
                        <List
                          classes={{ root: classes.button }}
                          onClick={handleDisabled}
                        >
                          <ListItemIcon
                            classes={{ root: classes.listItemIcon }}
                          >
                            <EditSharp className={classes.moreIcon} />
                          </ListItemIcon>
                          <Text className={classes.moreText}>Edit</Text>
                        </List>
                      }
                      BottomAction={
                        <List
                          classes={{ root: classes.button }}
                          onClick={handleClick}
                        >
                          <ListItemIcon
                            classes={{ root: classes.listItemIcon }}
                          >
                            <PersonAddSharp className={classes.moreIcon} />
                          </ListItemIcon>
                          <Text className={classes.moreText}>
                            <span>
                              {loading ? (
                                <Loader
                                  type="ThreeDots"
                                  color={Colors.primary}
                                  height="15"
                                  width="30"
                                />
                              ) : (
                                "Enable profile"
                              )}
                            </span>
                          </Text>
                        </List>
                      }
                      ThirdAction={<DeleteMerchant row={props.row} />}
                    />
                  </ButtonsRow>
                </Grid>
              )}
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <TextField
                  id="merchantID"
                  htmlFor="merchantID"
                  label="Merchant ID"
                  name="merchantID"
                  value={state.merchantID}
                  onChange={handleChange}
                  disabled
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="merchantName"
                  htmlFor="merchantName"
                  label="Name"
                  name="merchantName"
                  value={state.merchantName}
                  onChange={handleChange}
                  disabled={state.disabled}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="category"
                  htmlFor="category"
                  label="Category"
                  name="category"
                  value={state.category}
                  onChange={handleChange}
                  disabled={state.disabled}
                  grouped
                >
                  {categories &&
                    categories.map((category) => {
                      return (
                        <option value={category.name} key={category.name}>
                          {category.name}
                        </option>
                      );
                    })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <Title style={{ marginTop: 10 }}>Bank detail</Title>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="accountNumber"
                  htmlFor="accountNumber"
                  label="Account number"
                  name="accountNumber"
                  value={state.accountNumber}
                  onChange={handleChange}
                  disabled={state.disabled}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="accountName"
                  htmlFor="accountName"
                  label="Account name"
                  name="accountName"
                  value={state.accountName}
                  onChange={handleChange}
                  disabled={state.disabled}
                  grouped
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="accountType"
                  htmlFor="accountType"
                  label="Account type"
                  name="accountType"
                  value={state.accountType}
                  onChange={handleChange}
                  disabled={state.disabled}
                >
                  {accounttypes &&
                    accounttypes.map((type) => {
                      return (
                        <option value={type.name} key={type.name}>
                          {type.name}
                        </option>
                      );
                    })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="branch"
                  htmlFor="branch"
                  label="Branch"
                  name="branch"
                  value={state.branch}
                  onChange={handleChange}
                  disabled={state.disabled}
                  grouped
                >
                  {branches &&
                    branches.map((branch) => {
                      return (
                        <option value={branch.code} key={branch.name}>
                          {branch.name}
                        </option>
                      );
                    })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <Title style={{ marginTop: 10 }}>Primary contact</Title>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="contactTitle"
                  htmlFor="contactTitle"
                  label="Title"
                  name="contactTitle"
                  value={state.contactTitle}
                  onChange={handleChange}
                  disabled={state.disabled}
                >
                  {titles &&
                    titles.map((title) => {
                      return (
                        <option value={title.name} key={title.name}>
                          {title.name}
                        </option>
                      );
                    })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="contactName"
                  htmlFor="contactName"
                  label="Name"
                  name="contactName"
                  value={state.contactName}
                  onChange={handleChange}
                  disabled={state.disabled}
                  grouped
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <PhoneInput
                  value={state.contactPhone}
                  phonelabel="Phone"
                  onChange={handlePhoneInputChange}
                  style={{ marginTop: 13 }}
                  disabled={state.disabled}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="contactEmail"
                  htmlFor="contactEmail"
                  type="email"
                  label="Email"
                  name="contactEmail"
                  value={state.contactEmail}
                  onChange={handleChange}
                  disabled={state.disabled}
                  grouped
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <Title style={{ marginTop: 10 }}>Physical address</Title>
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <TextArea
                  id="address"
                  htmlFor="address"
                  type="text"
                  label="Address"
                  name="address"
                  value={state.address}
                  onChange={handleChange}
                  disabled={state.disabled}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="state"
                  htmlFor="state"
                  label="State"
                  name="state"
                  value={state.state}
                  onChange={handleStateChange}
                  disabled={state.disabled}
                >
                  {states &&
                    states.map((state) => {
                      return (
                        <option key={state.name} value={state.name}>
                          {state.name}
                        </option>
                      );
                    })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Select
                  id="lga"
                  htmlFor="lga"
                  label="LGA"
                  name="lga"
                  value={state.lga}
                  onChange={handleChange}
                  disabled={state.disabled}
                  grouped
                >
                  {state.lgaLists &&
                    state.lgaLists.map((lg) => {
                      return (
                        <option value={lg.name} key={lg.name}>
                          {lg.name}
                        </option>
                      );
                    })}
                </Select>
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="zipcode"
                  htmlFor="zipcode"
                  type="text"
                  label="Zip code"
                  name="zipcode"
                  value={state.zipcode}
                  onChange={handleChange}
                  disabled={state.disabled}
                />
              </Grid>
              {state.disabled ? (
                ""
              ) : (
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      onClick={handleClose}
                      style={{ marginRight: 15 }}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      className={classes.submitAddButton}
                      style={{
                        marginTop: 0,
                      }}
                    >
                      <span className="submit-btn">
                        {loading ? (
                          <Loader
                            type="ThreeDots"
                            color={Colors.secondary}
                            height="15"
                            width="30"
                          />
                        ) : (
                          "save"
                        )}
                      </span>
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              )}
            </form>
          </Grid>
        </Grid>
      </div>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
