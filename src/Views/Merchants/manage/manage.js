import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import Spinner from "../../../assets/Spinner";
import { Text } from "../../../components/Forms/index";
import { getSingleMerchantsHelper } from "../../../helpers/mechants.helper";
import EditMerchant from "./edit";

const styleProps = {
  height: 50,
  width: 50,
};
export default function ManageDetail(props) {
  const dispatch = useDispatch();
  const loading = useSelector(
    (state) => state.merchantsReducer.singleMerchantIsLoading
  );
  const singleMerchant = useSelector(
    (state) => state.merchantsReducer.singleMerchantData
  );
  const errorMessage = useSelector(
    (state) => state.merchantsReducer.singleMerchantErrorMessage
  );
  useEffect(() => {
    document.title = `${props.location.state.row.name} |Manage Merchants`;
    dispatch(getSingleMerchantsHelper(props.location.state.row.id));
    dispatch(
      updateBreadcrumb({
        parent: "Merchants",
        child: "Manage merchant",
        homeLink: "/tms",
        childLink: "/tms/merchants",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  return (
    <div>
      {loading ? (
        <Spinner {...styleProps} />
      ) : singleMerchant ? (
        <div>
          <EditMerchant row={singleMerchant} />
        </div>
      ) : (
        <div>
          <Text>{errorMessage}</Text>
        </div>
      )}
    </div>
  );
}
