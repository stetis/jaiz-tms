import { Grid, MuiThemeProvider, TablePagination } from "@material-ui/core";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";
import FilterListOutlinedIcon from "@material-ui/icons/FilterListOutlined";
import NavigateNextOutlinedIcon from "@material-ui/icons/NavigateNextOutlined";
import MUIDataTable from "mui-datatables";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../actions/breadcrumbAction";
import { clearMessages } from "../../actions/merchants.actions";
import { theme } from "../../assets/MUI/Table";
import Snackbars from "../../assets/Snackbars/index";
import Spinner from "../../assets/Spinner";
import { SubmitButton } from "../../components/Buttons";
import { ButtonsRow } from "../../components/Buttons/styles";
import Search from "../../components/TextField/search";
import { getMerchantsHelper } from "../../helpers/mechants.helper";
import jaiz from "../../images/jaiz-logo.png";
import { styles } from "./styles";

const styleProps = {
  height: 50,
  width: 50,
};
function capitalizeFirstLetters(str) {
  var splitStr = str.toLowerCase().split(" ");
  for (var i = 0; i < splitStr.length; i++) {
    splitStr[i] =
      splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  return splitStr.join(" ");
}
export default function Merchants(props) {
  const classes = styles();
  const [showButton, setShowButton] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [search, setSearch] = useState("");
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.merchantsReducer.loading);
  const merchants = useSelector(
    (state) => state.merchantsReducer.data.merchants
  );
  const pages = useSelector((state) => state.merchantsReducer.data.paging);
  const error = useSelector((state) => state.merchantsReducer.error);
  const errorMessage = useSelector(
    (state) => state.merchantsReducer.errorMessage
  );
  useEffect(() => {
    document.title = "Merchants | Jaiz bank TMS";
    dispatch(
      updateBreadcrumb({
        parent: "Merchants",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );
    const credentials = {
      page: page,
      pagesize: rowsPerPage,
      search: search,
    };
    dispatch(getMerchantsHelper(credentials));
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const handleChangePage = (event, newPage) => {
    let credentials = {
      page: newPage,
      pagesize: rowsPerPage,
      search,
    };
    dispatch(getMerchantsHelper(credentials));
    setPage(newPage);
    setRowsPerPage(rowsPerPage);
  };
  const handleChangeRowsPerPage = (event) => {
    let credentials = {
      page,
      pagesize: parseInt(event.target.value, 10),
      search: search,
    };
    dispatch(getMerchantsHelper(credentials));
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
    setSearch(setSearch);
  };
  const gotoAddMerchant = (e) => {
    e.preventDefault();
    props.history.push("/tms/add-merchant");
  };
  const gotoEditMerchant = (merchant) => {
    props.history.push({
      pathname: `/tms/edit-merchant`,
      state: {
        row: merchant,
      },
    });
    props.history.push("/tms/edit-merchant");
  };
  const rowClickGotoEditMerchant = (rowData) => {
    props.history.push({
      pathname: `/tms/edit-merchant`,
      state: {
        row: { id: rowData[1], name: rowData[2] },
      },
    });
  };
  const handleClickOpen = (e) => {
    e.preventDefault();
    setShowButton(!showButton);
  };
  const handleSearchChange = (event) => setSearch(event.target.value);
  const handleSearch = (e) => {
    e.preventDefault();
    let credentials = {
      page,
      pagesize: rowsPerPage,
      search,
    };
    dispatch(getMerchantsHelper(credentials));
    setPage(page);
    setRowsPerPage(rowsPerPage);
  };
  const handleKeyPress = (event) => {
    let credentials = {
      page,
      pagesize: rowsPerPage,
      search,
    };
    if (event.keyCode === 13) {
      dispatch(getMerchantsHelper(credentials));
    }
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const columns = [
    "",
    {
      name: "id",
      options: {
        display: false,
      },
    },
    "Name",
    "Category",
    "Email",
    "Phone",
    "Address",
    " ",
  ];
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: true,
    sort: false,
    rowHover: false,
    onRowClick: (rowData) => rowClickGotoEditMerchant(rowData),
    selectableRows: "none",
    customToolbar: () => (
      <ButtonsRow className={classes.buttonRow}>
        <SubmitButton
          onClick={handleClickOpen}
          className={classes.fabIcon}
          style={{ marginRight: 15 }}
        >
          <FilterListOutlinedIcon className={classes.icon} />
          Filters
        </SubmitButton>
        <SubmitButton
          onClick={gotoAddMerchant}
          className={classes.fabIcon}
          style={{ marginTop: 0 }}
        >
          <AddOutlinedIcon className={classes.icon} />
          Add merchant
        </SubmitButton>
      </ButtonsRow>
    ),
    textLabels: {
      body: {
        noMatch: "No merchant records found",
      },
    },
  };
  return (
    <div className={classes.card}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!loading ? (
            <MuiThemeProvider theme={theme}>
              <MUIDataTable
                data={
                  merchants && merchants.constructor === Array
                    ? merchants.map((merchant, i) => {
                        return {
                          "": i + 1,
                          id: merchant.id,
                          Name: capitalizeFirstLetters(merchant.name),
                          Category: merchant.category,
                          Email:
                            merchant["contact-email"] &&
                            merchant["contact-email"].toLowerCase(),
                          Phone: merchant["contact-phone"],
                          Address: capitalizeFirstLetters(merchant.address),
                          " ": (
                            <NavigateNextOutlinedIcon
                              style={{ fontSize: 20 }}
                              onClick={gotoEditMerchant.bind(this, merchant)}
                            />
                          ),
                        };
                      })
                    : []
                }
                title={
                  showButton ? (
                    <Search
                      id="search"
                      htmlFor="search"
                      name="search"
                      value={search}
                      placeholder="search merchant"
                      searchlabel="Search"
                      onChange={handleSearchChange}
                      onKeyUp={handleKeyPress}
                      onClickIcon={handleSearch}
                    />
                  ) : (
                    <div className={classes.tableLogo}>
                      <img
                        src={jaiz}
                        alt="jaiz logo"
                        style={{ width: 20, height: 20, marginRight: "1rem" }}
                      />
                      Merchants
                    </div>
                  )
                }
                columns={columns}
                options={options}
              />
              <TablePagination
                component="div"
                count={
                  pages && pages["total-record"] ? pages["total-record"] : 0
                }
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={rowsPerPage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                rowsPerPageOptions={[5, 10, 15, 20]}
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
    </div>
  );
}
