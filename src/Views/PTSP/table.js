import { Grid } from "@material-ui/core";
import { MuiThemeProvider } from "@material-ui/core/styles";
import NavigateNextOutlinedIcon from "@material-ui/icons/NavigateNextOutlined";
import MUIDataTable from "mui-datatables";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../actions/breadcrumbAction";
import { clearMessages } from "../../actions/ptsp.actions";
import { theme } from "../../assets/MUI/Table";
import Snackbars from "../../assets/Snackbars/index";
import Spinner from "../../assets/Spinner";
import { getPTSPHelper } from "../../helpers/ptsp.helper";
import jaiz from "../../images/jaiz-logo.png";
import AddPTSP from "./add";
import { styles } from "./styles";

const styleProps = {
  height: 50,
  width: 50,
};
export default function PTSPTable(props) {
  const classes = styles();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.ptspReducer.loading);
  const ptsps = useSelector((state) => state.ptspReducer.data.ptsps);
  const error = useSelector((state) => state.ptspReducer.error);
  const errorMessage = useSelector((state) => state.ptspReducer.errorMessage);

  useEffect(() => {
    document.title = "PTSP | Jaiz bank TMS";
    dispatch(getPTSPHelper());
    dispatch(
      updateBreadcrumb({
        parent: "PTSPs",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const gotoManagePTSP = (row) => {
    props.history.push({
      pathname: `/tms/manage-ptsp`,
      state: {
        row: row,
      },
    });
  };
  const rowClickGotoManagePTSP = (rowData) => {
    return props.history.push({
      pathname: `/tms/manage-ptsp`,
      state: {
        row: {
          id: rowData[1],
          name: rowData[2],
        },
      },
    });
  };
  const handleCloseSnack = () => dispatch(clearMessages());

  const columns = [
    "",
    {
      name: "id",
      options: {
        display: false,
      },
    },
    "Name",
    "Email",
    "Phone",
    "Address",
    " ",
  ];
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: true,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    rowsPerPageOptions: [5, 10],
    onRowClick: (rowData) => rowClickGotoManagePTSP(rowData),
    customToolbar: () => {
      return <AddPTSP />;
    },
    textLabels: {
      body: {
        noMatch: "No PTSP records found",
      },
    },
  };
  return (
    <div className={classes.ptspcard}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!loading ? (
            <MuiThemeProvider theme={theme}>
              <MUIDataTable
                data={
                  ptsps && ptsps.constructor === Array
                    ? ptsps.map((ptsp, i) => {
                        return {
                          "": i + 1,
                          id: ptsp.id,
                          Name: ptsp.name,
                          Email: ptsp.email,
                          Phone: ptsp.phone,
                          Address: ptsp.address,
                          " ": (
                            <NavigateNextOutlinedIcon
                              style={{ fontSize: 20 }}
                              onClick={gotoManagePTSP.bind(this, ptsp)}
                            />
                          ),
                        };
                      })
                    : []
                }
                columns={columns}
                options={options}
                title={
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    PTSPs
                  </div>
                }
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
    </div>
  );
}
