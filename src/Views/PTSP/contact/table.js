import { Grid, List, ListItemIcon } from "@material-ui/core";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { PersonAddSharp } from "@material-ui/icons";
import MUIDataTable from "mui-datatables";
import React, { useEffect } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { theme } from "../../../assets/MUI/Table";
import Spinner from "../../../assets/Spinner";
import { Colors } from "../../../assets/themes/theme";
import CallOut from "../../../components/CallOut/CallOut";
import { Text } from "../../../components/Forms/index";
import {
  enablePTSPProfileHelper,
  getPTSPContactHelper,
} from "../../../helpers/ptsp.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "../styles";
import AddPTSPContact from "./add";
import DeletePTSPContact from "./delete";
import EditPTSPContact from "./edit";
import InitiatePTSPResetPin from "./resetPin";
import RevokePTSPContactProfile from "./revokeProfile";

const styleProps = {
  height: 50,
  width: 50,
};
function PTSPContactTable(props) {
  const classes = styles();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.ptspReducer.contactIsLoading);
  const contacts = useSelector(
    (state) => state.ptspReducer.contactData.contacts
  );

  useEffect(() => {
    dispatch(getPTSPContactHelper(props.id));
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleClick = (id) => {
    const inputData = {
      id,
      "ptsp-id": props.id,
    };
    dispatch(enablePTSPProfileHelper(inputData));
  };
  const columns = ["", "Name", "Email", "Phone", "State", " "];
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: true,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    customToolbar: () => {
      return <AddPTSPContact id={props.id} />;
    },
    textLabels: {
      body: {
        noMatch: "No ptsp contact record found",
      },
    },
  };
  return (
    <div className={classes.card}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!loading ? (
            <MuiThemeProvider theme={theme}>
              <MUIDataTable
                data={
                  contacts && contacts.constructor === Array
                    ? contacts.map((contact, i) => {
                        return {
                          "": i + 1,
                          Name: contact.name,
                          Email: contact.email,
                          Phone: contact.phone,
                          State: contact["state-name"],
                          " ":
                            contact["profile-enabled"] === true ? (
                              <CallOut
                                TopAction={
                                  <EditPTSPContact
                                    row={contact}
                                    id={props.id}
                                  />
                                }
                                BottomAction={
                                  <InitiatePTSPResetPin
                                    row={contact}
                                    id={props.id}
                                  />
                                }
                                ThirdAction={
                                  <RevokePTSPContactProfile
                                    row={contact}
                                    id={props.id}
                                  />
                                }
                                FourthAction={
                                  <DeletePTSPContact
                                    row={contact}
                                    id={props.id}
                                  />
                                }
                              />
                            ) : (
                              <CallOut
                                TopAction={
                                  <EditPTSPContact
                                    row={contact}
                                    id={props.id}
                                  />
                                }
                                BottomAction={
                                  <List
                                    classes={{ root: classes.button }}
                                    onClick={handleClick.bind(this, contact.id)}
                                  >
                                    <ListItemIcon
                                      classes={{ root: classes.listItemIcon }}
                                    >
                                      <PersonAddSharp
                                        className={classes.moreIcon}
                                      />
                                    </ListItemIcon>
                                    <Text className={classes.moreText}>
                                      <span>
                                        {loading ? (
                                          <Loader
                                            type="ThreeDots"
                                            color={Colors.primary}
                                            height="15"
                                            width="30"
                                          />
                                        ) : (
                                          "Enable profile"
                                        )}
                                      </span>
                                    </Text>
                                  </List>
                                }
                                ThirdAction={
                                  <DeletePTSPContact
                                    row={contact}
                                    id={props.id}
                                  />
                                }
                              />
                            ),
                        };
                      })
                    : []
                }
                columns={columns}
                options={options}
                title={
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    PTSP contacts
                  </div>
                }
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
    </div>
  );
}
export default PTSPContactTable;
