import { Grid, Popover } from "@material-ui/core";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { isValidPhoneNumber } from "react-phone-number-input";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../actions/ptsp.actions";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import {
  DeleteButton,
  Fab,
  SubmitButton,
} from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Select, TextField } from "../../../components/TextField/index";
import PhoneInput from "../../../components/TextField/phoneInput";
import { addPTSPContactHelper } from "../../../helpers/ptsp.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { states } from "../../../utils/data";
import { styles } from "../styles";

const inputFields = {
  //eslint-disable-next-line
  email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
};
export default function AddPTSPContact(props) {
  const classes = styles();
  const [anchorEl, setAnchorEl] = useState(null);
  const [state, setState] = useState({
    name: "",
    email: "",
    phone: "",
    state: "",
    formError: false,
  });
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.ptspReducer.actionLoading);
  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  const handleClickOpen = (e) => setAnchorEl(e.currentTarget);
  const handleClose = (e) => {
    e.preventDefault();
    setAnchorEl(null);
    setState((prev) => ({
      ...prev,
      name: "",
      email: "",
      phone: "",
      state: "",
    }));
  };
  const handlePopperClose = () => setAnchorEl(null);
  const handlePhoneInputChange = (value) => {
    if (value) {
      setState((prevState) => ({
        ...prevState,
        phone: value,
      }));
    }
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const notValidPhone = state.phone
      ? isValidPhoneNumber(state.phone) === false
      : false;
    const stateDetail = states.find((st) => st.name === state.state);
    if (
      state.name === "" ||
      state.email === "" ||
      !inputFields.email.test(state.email) ||
      state.phone === "" ||
      notValidPhone === true ||
      !state.state
    ) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      let inputData = {
        "ptsp-id": props.id,
        name: state.name,
        email: state.email,
        phone: state.phone,
        "state-name": state.state,
        "state-code": stateDetail.code,
      };
      dispatch(addPTSPContactHelper(inputData));
    }
  };
  const notValidPhone = state.phone
    ? isValidPhoneNumber(state.phone) === false
    : false;
  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  return (
    <div>
      <Fab onClick={handleClickOpen} className={classes.fabIcon}>
        <AddOutlinedIcon className={classes.addIcon} />
      </Fab>
      {state.formError === true
        ? renderFieldError(
            state.name === ""
              ? "Name is required"
              : state.email === ""
              ? "Email is required"
              : !inputFields.email.test(state.email)
              ? "Invalid email pattern"
              : state.phone === ""
              ? "Phone is required"
              : notValidPhone === true
              ? "Invalid phone number"
              : !state.state
              ? "State is required"
              : ""
          )
        : null}
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handlePopperClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <div className={classes.actionsRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12}>
              <form
                className={classes.container}
                noValidate
                onSubmit={handleSubmit}
                autoComplete="off"
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Add PTSP contact
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextField
                    id="name"
                    htmlFor="name"
                    type="text"
                    label="Name"
                    name="name"
                    value={state.name}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextField
                    id="email"
                    htmlFor="email"
                    type="email"
                    label="Email"
                    name="email"
                    value={state.email}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <PhoneInput
                    value={state.phone}
                    phonelabel="Phone"
                    onChange={handlePhoneInputChange}
                    style={{ marginTop: 13 }}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Select
                    id="state"
                    htmlFor="state"
                    name="state"
                    label="State"
                    value={state.state}
                    onChange={handleChange}
                    grouped
                  >
                    {states.map((state) => {
                      return (
                        <option key={state.name} value={state.name}>
                          {state.name}
                        </option>
                      );
                    })}
                  </Select>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      onClick={handleClose}
                      style={{ marginRight: 15 }}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      className={classes.submitAddButton}
                      disabled={loading}
                      style={{
                        backgroundColor: loading ? Colors.primary : "",
                        marginTop: 0,
                      }}
                    >
                      {loading ? (
                        <Loader
                          type="ThreeDots"
                          color={Colors.secondary}
                          height="15"
                          width="30"
                        />
                      ) : (
                        "Save"
                      )}
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </div>
      </Popover>
    </div>
  );
}
