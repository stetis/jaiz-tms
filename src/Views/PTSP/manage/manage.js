import { Grid } from "@material-ui/core";
import { PersonOutline } from "@material-ui/icons";
import EmailOutlinedIcon from "@material-ui/icons/EmailOutlined";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import HomeOutlinedIcon from "@material-ui/icons/HomeOutlined";
import SmartphoneOutlinedIcon from "@material-ui/icons/SmartphoneOutlined";
import React, { useEffect } from "react";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Text } from "../../../components/Forms/index";
import Snackbars from "../../../assets/Snackbars/index";
import { clearMessages } from "../../../actions/ptsp.actions";
import CallOut from "../../../components/CallOut/CallOut";
import PTSPContactTable from "../contact/table";
import { styles } from "../styles";
import DeletePTSP from "./delete";
import EditPTSP from "./edit";
import { Title } from "../../../components/contentHolders/Card";
import { getSinglePTSPHelper } from "../../../helpers/ptsp.helper";
import Spinner from "../../../assets/Spinner";

const styleProps = {
  height: 50,
  width: 50,
};
export default function ManageGroups(props) {
  const classes = styles();
  const { row } = props.location.state;
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.ptspReducer.singlePTSPIsLoading);
  const ptsp = useSelector((state) => state.ptspReducer.singlePTSPData);
  const error = useSelector((state) => state.ptspReducer.singlePTSPError);
  const errorMessage = useSelector(
    (state) => state.ptspReducer.singlePTSPErrorMessage
  );
  const status = useSelector((state) => state.ptspReducer.status);
  const statusMessage = useSelector((state) => state.ptspReducer.statusMessage);
  const tableError = useSelector((state) => state.ptspReducer.contactError);
  const tableErrorMessage = useSelector(
    (state) => state.ptspReducer.contactErrorMessage
  );
  useEffect(() => {
    document.title = `Manage ${row.name}  | PTSPs`;
    dispatch(
      updateBreadcrumb({
        parent: "PTSP",
        child: "Manage PTSP",
        homeLink: "/tms",
        childLink: "/tms/ptsp",
      })
    );
    dispatch(getSinglePTSPHelper(row.id));
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const handleCloseSnack = () => dispatch(clearMessages());

  return (
    <div className={classes.cardRoot}>
      {loading ? (
        <Spinner {...styleProps} />
      ) : error ? (
        { errorMessage }
      ) : (
        <Grid container spacing={0}>
          <div className={classes.manageCard}>
            <Grid container spacing={1}>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <Title>{ptsp.name}</Title>{" "}
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <ButtonsRow style={{ margin: "0 0 -25px" }}>
                  <CallOut
                    font="30"
                    TopAction={<EditPTSP row={ptsp} />}
                    BottomAction={<DeletePTSP row={ptsp} />}
                  />
                </ButtonsRow>
              </Grid>
              <Grid
                item
                xs={12}
                sm={12}
                md={12}
                lg={12}
                style={{ display: "flex" }}
              >
                <PersonOutline className={classes.icons} />{" "}
                <Text style={{ marginLeft: 8 }}>{ptsp.name}</Text>
              </Grid>
              <Grid
                item
                xs={12}
                sm={6}
                md={6}
                lg={6}
                style={{ display: "flex" }}
              >
                <EmailOutlinedIcon className={classes.icons} />{" "}
                <Text style={{ marginLeft: 8 }}>{ptsp.email}</Text>
              </Grid>
              <Grid
                item
                xs={12}
                sm={6}
                md={6}
                lg={6}
                style={{ display: "flex" }}
              >
                <SmartphoneOutlinedIcon className={classes.icons} />
                <Text style={{ marginLeft: 8 }}>{ptsp.phone}</Text>
              </Grid>
              <Grid
                item
                xs={12}
                sm={12}
                md={12}
                lg={12}
                style={{ display: "flex" }}
              >
                <HomeOutlinedIcon className={classes.icons} />
                <Text style={{ marginLeft: 8 }}>{ptsp.address}</Text>
              </Grid>
            </Grid>
          </div>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <PTSPContactTable id={ptsp.id} />
          </Grid>
        </Grid>
      )}
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={tableErrorMessage}
        isOpen={tableError}
      />
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
