import { Grid, List, ListItemIcon } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import { EditSharp } from "@material-ui/icons";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { isValidPhoneNumber } from "react-phone-number-input";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../actions/ptsp.actions";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Text } from "../../../components/Forms/index";
import { TextArea, TextField } from "../../../components/TextField/index";
import PhoneInput from "../../../components/TextField/phoneInput";
import { editPTSPHelper } from "../../../helpers/ptsp.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "../styles";

const inputFields = {
  //eslint-disable-next-line
  email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
};
export default function EditPTSP(props) {
  const classes = styles();
  const [state, setState] = useState({
    open: false,
    name: props.row.name,
    email: props.row.email,
    phone: props.row.phone,
    address: props.row.address,
    formError: false,
  });
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.ptspReducer.actionLoading);

  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };
  const handleClickOpen = () => {
    setState((prev) => ({
      ...prev,
      open: true,
    }));
  };
  const handleClose = (e) => {
    e.preventDefault();
    setState((prev) => ({
      ...prev,
      open: false,
      name: "",
      email: "",
      phone: "",
      address: "",
    }));
  };
  const handlePhoneInputChange = (value) => {
    if (value) {
      setState((prev) => ({
        ...prev,
        phone: value,
      }));
    }
  };
  const handleCloseSnack = () => dispatch(clearMessages());

  const handleSubmit = (e) => {
    e.preventDefault();
    const notValidPhone = state.phone
      ? isValidPhoneNumber(state.phone) === false
      : false;
    if (
      state.name === "" ||
      state.email === "" ||
      !inputFields.email.test(state.email) ||
      state.phone === "" ||
      notValidPhone === true ||
      state.address === ""
    ) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      let inputData = {
        id: props.row.id,
        name: state.name,
        email: state.email,
        phone: state.phone,
        address: state.address,
      };
      dispatch(editPTSPHelper(inputData));
    }
  };
  const renderFieldError = (formErrorMessage) => {
    <Snackbars
      variant="error"
      handleClose={handleCloseSnack}
      message={formErrorMessage}
      isOpen={state.formError}
    />;
  };
  const notValidPhone = state.phone
    ? isValidPhoneNumber(state.phone) === false
    : false;
  return (
    <div>
      <List classes={{ root: classes.button }} onClick={handleClickOpen}>
        <ListItemIcon classes={{ root: classes.listItemIcon }}>
          <EditSharp className={classes.moreIcon} />
        </ListItemIcon>
        <Text className={classes.moreText}>Edit</Text>
      </List>
      {state.formError === true
        ? renderFieldError(
            state.name === ""
              ? "PTSP name is required"
              : state.email === ""
              ? "PTSP email is required"
              : !inputFields.email.test(state.email)
              ? "Invalid email pattern"
              : state.phone === ""
              ? "PTSP phone is required"
              : notValidPhone === true
              ? "Invalid phone number"
              : state.address === ""
              ? "PTSP address is required"
              : ""
          )
        : null}
      <Dialog
        open={state.open}
        keepMounted
        aria-labelledby="alert-dialog-slide-title"
        className="add-branch-dialog"
        aria-describedby="alert-dialog-slide-description"
      >
        <div className={classes.actionsRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12}>
              <form
                className={classes.container}
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Edit PTSP
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextField
                    id="name"
                    htmlFor="name"
                    type="text"
                    label="Name"
                    name="name"
                    value={state.name}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextField
                    id="email"
                    htmlFor="email"
                    type="email"
                    label="Email"
                    name="email"
                    value={state.email}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <PhoneInput
                    value={state.phone}
                    phonelabel="Phone"
                    onChange={handlePhoneInputChange}
                    style={{ marginTop: 13 }}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextArea
                    id="address"
                    htmlFor="address"
                    type="text"
                    label="Address"
                    name="address"
                    value={state.address}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      style={{
                        marginRight: 10,
                      }}
                      onClick={handleClose}
                      disabled={loading}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        marginTop: 0,
                      }}
                    >
                      {loading ? (
                        <Loader
                          type="ThreeDots"
                          color={Colors.secondary}
                          height="15"
                          width="30"
                        />
                      ) : (
                        "save"
                      )}
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </div>
      </Dialog>
    </div>
  );
}
