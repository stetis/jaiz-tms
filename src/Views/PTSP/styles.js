import { makeStyles } from "@material-ui/core";
import { Colors, Fonts } from "../../assets/themes/theme";

export const styles = makeStyles((theme) => ({
  ptspcard: {
    width: "95%",
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 15px 25px",
    borderRadius: 5,
    cursor: "pointer",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 580,
    },
    "@media only screen and (min-width: 960px)": {
      width: 800,
    },
  },
  card: {
    width: "100%",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "10px 0px 15px",
    borderRadius: 5,
    marginTop: 30,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      padding: "10px 25px 25px",
    },
  },
  fabIcon: {
    color: Colors.secondary,
    fontWeight: 600,
    "@media only screen and (max-width: 540px)": {
      float: "right",
      marginTop: -29,
    },
  },
  icon: {
    fontSize: 18,
    marginRight: 5,
  },
  outletCard: {
    width: "100%",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 5px 25px",
    borderRadius: 5,
    margin: "-10px auto",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
  },
  container: {
    display: "flex",
    padding: "20px 25px 25px",
    flexWrap: "wrap",
  },
  fabAddIcon: {
    fontSize: 14,
    color: Colors.secondary,
  },
  icons: {
    fontSize: 20,
    color: Colors.secondary,
  },
  logo: {
    color: Colors.primary,
    display: "flex",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
  },
  deleteRoot: {
    width: 320,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    display: "flex",
    padding: "12px 18px 20px",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 480px)": {
      width: 350,
    },
  },
  addIcon: {
    fontSize: 20,
    color: Colors.secondary,
  },
  actionsRoot: {
    width: 350,
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (max-width: 359px)": {
      width: "100%",
    },
    "@media only screen and (min-width: 360px) and (max-width: 700px)": {
      width: "100%",
    },
    "@media only screen and (min-width: 768px) and (max-width: 1200px)": {
      width: 350,
    },
  },
  EditIcon: {
    fill: Colors.primary,
    margin: "1px 0 0px -10px",
  },
  editlistItemIcon: {
    color: Colors.primary,
    minWidth: 10,
    cursor: "pointer",
  },
  listItemIcon: {
    color: Colors.secondary,
    minWidth: 22,
    cursor: "pointer",
  },
  moreIcon: {
    color: Colors.secondary,
    margin: "1px 0 0px -10px",
    cursor: "pointer",
  },
  moreText: {
    color: Colors.primary,
    margin: "2px 0 0px -10px",
    cursor: "pointer",
  },
  button: {
    width: "100%",
    display: "flex",
    marginLeft: 5,
    marginRight: 5,
    padding: 2,
    cursor: "pointer",
    color: Colors.menuListColor,
    fontSize: Fonts.font,
  },
  DeleteIcon: {
    color: Colors.buttonError,
    margin: "1px 0 0px -10px",
    cursor: "pointer",
  },
  deleteText: {
    color: Colors.buttonError,
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
    margin: "2px 0 0px -10px",
    cursor: "pointer",
  },
  cardRoot: {
    width: "95%",
    margin: "26px auto",
    "@media only screen and (min-width: 600px)": { width: "80%" },
    "@media only screen and (min-width: 960px)": {
      width: "55%",
    },
  },
  manageCard: {
    width: "100%",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 5px 12px 25px",
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
  },
}));
