import { Grid, Popover } from "@material-ui/core";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { isValidPhoneNumber } from "react-phone-number-input";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../actions/System/users.actions";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { getRolesHelper } from "../../../helpers/profile.helper";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Title } from "../../../components/contentHolders/Card";
import {
  CheckBox,
  Select,
  TextField,
} from "../../../components/TextField/index";
import Password from "../../../components/TextField/password";
import PasswordStrengthMeter from "../../../components/TextField/passwordStrengthMeter";
import PhoneInput from "../../../components/TextField/phoneInput";
import { getBranchesHelper } from "../../../helpers/Configurations/branches.helper";
import { addUserHelper } from "../../../helpers/System/users.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "./styles";

export default function AddUsers() {
  const classes = styles();
  const inputFields = {
    //eslint-disable-next-line
    email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  };
  const [anchorEl, setAnchorEl] = useState(null);
  const [state, setState] = useState({
    name: "",
    branch: "",
    role: "",
    email: "",
    phone: "",
    password: "",
    confirmPassword: "",
    requestReset: true,
    showPassword: false,
    showConfirmPassword: false,
    formError: false,
  });
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.usersReducer.actionLoading);
  const roles = useSelector((state) => state.profileReducer.roles.roles);
  const branches = useSelector((state) => state.branchesReducer.data.branches);
  const status = useSelector((state) => state.usersReducer.status);
  const statusMessage = useSelector(
    (state) => state.usersReducer.statusMessage
  );

  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  useEffect(() => {
    dispatch(getBranchesHelper());
    dispatch(getRolesHelper());
  }, []); //eslint-disable-line react-hooks/exhaustive-deps

  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };
  const handleResetCheckbox = () => {
    setState((prev) => ({
      ...prev,
      requestReset: !state.requestReset,
    }));
  };
  const handlePhoneInputChange = (value) => {
    if (value) {
      setState((prevState) => ({
        ...prevState,
        phone: value,
      }));
    }
  };
  const handleClickShowPassword = () => {
    setState((prev) => ({
      ...prev,
      showPassword: !state.showPassword,
    }));
  };
  const handleClickShowConfirmPassword = () => {
    setState((prev) => ({
      ...prev,
      showConfirmPassword: !state.showConfirmPassword,
    }));
  };
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = (e) => {
    e.preventDefault();
    setAnchorEl(null);
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const renderFieldError = (formErrorMessage) => {
    <Snackbars
      variant="error"
      handleClose={handleCloseSnack}
      message={formErrorMessage}
      isOpen={state.formError}
    />;
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const notValidPhone = isValidPhoneNumber(state.phone) === false;
    const branchDetail = branches.find(
      (branch) => branch.name === state.branch
    );
    if (
      state.name === "" ||
      state.branch === "" ||
      !state.role ||
      state.password !== state.confirmPassword ||
      notValidPhone === true ||
      !inputFields.email.test(state.email)
    ) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      let inputData = {
        name: state.name,
        email: state.email,
        phone: state.phone,
        role: state.role,
        password: state.password,
        branch: state.branch,
        "branch-id": branchDetail.id,
        "request-password-reset": state.requestReset,
      };
      dispatch(addUserHelper(inputData));
    }
  };
  const notValidPhone = isValidPhoneNumber(state.phone) === false;
  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  return (
    <div>
      <SubmitButton
        onClick={handleClick}
        className={classes.fabIcon}
        style={{ marginTop: 0 }}
      >
        <AddOutlinedIcon className={classes.icon} />
        Add user
      </SubmitButton>
      {state.formError === true
        ? renderFieldError(
            state.name === ""
              ? "User name is required"
              : state.password !== state.confirmPassword
              ? "Password did not match"
              : notValidPhone === true
              ? "Invalid phone number"
              : !inputFields.email.test(state.email)
              ? "Invalid email pattern"
              : ""
          )
        : null}
      <Popover
        id={id}
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <div className={classes.actionsRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12}>
              <form
                className={classes.container}
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Add user
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextField
                    id="name"
                    htmlFor="name"
                    label="Name"
                    name="name"
                    value={state.name}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <TextField
                    id="email"
                    htmlFor="email"
                    type="email"
                    label="Email (Username)"
                    name="email"
                    value={state.email}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <PhoneInput
                    value={state.phone}
                    phonelabel="Phone"
                    onChange={handlePhoneInputChange}
                    grouped
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <Select
                    id="branch"
                    htmlFor="branch"
                    name="branch"
                    label="Branch"
                    value={state.branch}
                    onChange={handleChange}
                  >
                    {branches &&
                      branches.map((branch) => {
                        return (
                          <option key={branch.id} value={branch.name}>
                            {branch.name}
                          </option>
                        );
                      })}
                  </Select>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <Select
                    id="role"
                    htmlFor="role"
                    label="Role"
                    name="role"
                    value={state.role}
                    onChange={handleChange}
                    grouped
                  >
                    {roles &&
                      roles.map((role) => {
                        return (
                          <option value={role.title} id={role["role-id"]}>
                            {role.title}
                          </option>
                        );
                      })}
                  </Select>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <Password
                    id="password"
                    htmlFor="password"
                    type={state.showPassword ? "text" : "password"}
                    passwordlabel="Password"
                    name="password"
                    value={state.password}
                    onChange={handleChange}
                    showPassword={state.showPassword}
                    onClick={handleClickShowPassword}
                  />
                  {state.password === "" ? null : (
                    <PasswordStrengthMeter password={state.password} />
                  )}
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <Password
                    id="confirmPassword"
                    htmlFor="confirmPassword"
                    type={state.showConfirmPassword ? "text" : "password"}
                    passwordlabel="Confirm password"
                    name="confirmPassword"
                    value={state.confirmPassword}
                    onChange={handleChange}
                    showPassword={state.showConfirmPassword}
                    onClick={handleClickShowConfirmPassword}
                    grouped
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Title style={{ display: "flex", margin: "10px 0" }}>
                    Request user to reset defaultpassword on first login
                    <span className={classes.checkbox}>
                      <CheckBox
                        id="requestReset"
                        htmlFor="requestReset"
                        type="checkbox"
                        name="requestReset"
                        checked={state.requestReset}
                        onChange={handleResetCheckbox}
                      />
                    </span>
                  </Title>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      style={{
                        marginRight: 10,
                      }}
                      onClick={handleClose}
                      disabled={loading}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        marginTop: 0,
                        width: 65,
                      }}
                    >
                      {loading ? (
                        <Loader
                          type="ThreeDots"
                          color={Colors.secondary}
                          height="15"
                          width="30"
                        />
                      ) : (
                        "save"
                      )}
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </div>
        <Snackbars
          variant="success"
          handleClose={handleCloseSnack}
          message={statusMessage}
          isOpen={status === 1}
        />
        <Snackbars
          variant="error"
          handleClose={handleCloseSnack}
          message={statusMessage}
          isOpen={status === 0}
        />
      </Popover>
    </div>
  );
}
