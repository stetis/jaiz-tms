import { Grid, MuiThemeProvider, TablePagination } from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import FilterListOutlinedIcon from "@material-ui/icons/FilterListOutlined";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import { clearMessages } from "../../../actions/System/users.actions";
import { theme } from "../../../assets/MUI/Table";
import Snackbars from "../../../assets/Snackbars/index";
import Spinner from "../../../assets/Spinner";
import CallOut from "../../../components/CallOut/CallOut";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { getPaginatedUsersHelper } from "../../../helpers/System/users.helper";
import jaiz from "../../../images/jaiz-logo.png";
import DeleteUser from "./delete";
import AddUsers from "./add";
import RecallProfile from "./recallUser";
import { styles } from "./styles";
import SuspendUser from "./suspend";
import ChangeRole from "./changeRole";
import Search from "../../../components/TextField/search";
import { SubmitButton } from "../../../components/Buttons";

const styleProps = {
  height: 50,
  width: 50,
};
export default function UsersTable() {
  const classes = styles();
  const [showButton, setShowButton] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [search, setSearch] = useState("");
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.usersReducer.loading);
  const users = useSelector((state) => state.usersReducer.data.profiles);
  const pages = useSelector((state) => state.usersReducer.data.paging);
  const error = useSelector((state) => state.usersReducer.error);
  const errorMessage = useSelector((state) => state.usersReducer.errorMessage);
  const status = useSelector((state) => state.usersReducer.status);
  const statusMessage = useSelector(
    (state) => state.usersReducer.statusMessage
  );

  useEffect(() => {
    document.title = "Users | Jaiz bank TMS";
    dispatch(
      updateBreadcrumb({
        parent: "Users",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );

    const credentials = {
      page: page,
      pagesize: rowsPerPage,
      search: search,
    };
    dispatch(getPaginatedUsersHelper(credentials));
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleChangePage = (event, newPage) => {
    let credentials = {
      page: newPage,
      pagesize: rowsPerPage,
      search: search,
    };
    dispatch(getPaginatedUsersHelper(credentials));
    setPage(newPage);
    setRowsPerPage(rowsPerPage);
  };
  const handleChangeRowsPerPage = (event) => {
    let credentials = {
      page,
      pagesize: rowsPerPage,
      search: search,
    };
    dispatch(getPaginatedUsersHelper(credentials));
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };
  const handleClickOpen = (e) => {
    e.preventDefault();
    setShowButton(!showButton);
  };
  const handleSearchChange = (event) => setSearch(event.target.value);
  const handleSearch = (e) => {
    e.preventDefault();
    let credentials = {
      page,
      pagesize: rowsPerPage,
      search: search,
    };
    dispatch(getPaginatedUsersHelper(credentials));
  };
  const handleKeyPress = (event) => {
    let credentials = {
      page,
      pagesize: rowsPerPage,
      search: search,
    };
    if (event.keyCode === 13) {
      dispatch(getPaginatedUsersHelper(credentials));
    }
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const columns = ["", "Name", "Email", "Phone", "Role", "Status", " "];

  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: true,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    customToolbar: () => (
      <ButtonsRow className={classes.buttonRow}>
        <SubmitButton
          onClick={handleClickOpen}
          className={classes.fabIcon}
          style={{ marginRight: 15 }}
        >
          <FilterListOutlinedIcon className={classes.icon} />
          Filters
        </SubmitButton>
        <AddUsers />
      </ButtonsRow>
    ),
    textLabels: {
      body: {
        noMatch: "No users records found",
      },
    },
  };
  return (
    <div className={classes.card}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!loading ? (
            <MuiThemeProvider theme={theme}>
              <MUIDataTable
                data={
                  users && users.constructor === Array
                    ? users.map((user, i) => {
                        return {
                          "": rowsPerPage * page + i + 1,
                          Name: user.name,
                          Role:
                            user.role === "admin"
                              ? "Admin"
                              : user.role === "superadmin"
                              ? "Super admin"
                              : "User",
                          Email: user.email,
                          Phone: user.phone,
                          Status:
                            user.status === "suspended"
                              ? "Suspended"
                              : "Active",
                          " ":
                            user.status === "active" ? (
                              <CallOut
                                TopAction={<SuspendUser row={user} />}
                                BottomAction={<ChangeRole row={user} />}
                                ThirdAction={<DeleteUser row={user} />}
                              />
                            ) : (
                              <CallOut
                                TopAction={<RecallProfile row={user} />}
                                BottomAction={<DeleteUser row={user} />}
                              />
                            ),
                        };
                      })
                    : []
                }
                title={
                  showButton ? (
                    <Search
                      id="search"
                      htmlFor="search"
                      name="search"
                      value={search}
                      placeholder="search merchant"
                      searchlabel="Search"
                      onChange={handleSearchChange}
                      onKeyUp={handleKeyPress}
                      onClickIcon={handleSearch}
                    />
                  ) : (
                    <div className={classes.tableLogo}>
                      <img
                        src={jaiz}
                        alt="jaiz logo"
                        style={{ width: 20, height: 20, marginRight: "1rem" }}
                      />
                      Users
                    </div>
                  )
                }
                columns={columns}
                options={options}
              />
              <TablePagination
                component="div"
                count={
                  pages && pages["total-record"] ? pages["total-record"] : 0
                }
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={rowsPerPage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                rowsPerPageOptions={[5, 10, 15, 20]}
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
