import { makeStyles } from "@material-ui/core";
import { Colors, Fonts } from "../../../assets/themes/theme";

export const styles = makeStyles(() => ({
  card: {
    width: "95%",
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: 25,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: "90%",
    },
    "@media only screen and (min-width: 960px)": {
      width: "70%",
    },
  },
  container: {
    display: "flex",
    padding: "15px 25px 20px",
    flexWrap: "wrap",
  },
  addIcon: {
    fontSize: 20,
    color: Colors.secondary,
  },
  addUserFab: {
    float: "right",
    zIndex: 200,
    "@media only screen and (min-width: 600px)": {
      position: "absolute",
      top: 12,
      left: "90%",
    },
    "@media only screen and (min-width: 960px)": {
      position: "absolute",
      top: 25,
      left: "95%",
    },
  },
  fabIcon: {
    color: Colors.secondary,
    fontWeight: 600,
  },
  icon: {
    fontSize: 18,
    marginRight: 5,
  },
  tableLogo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
  },
  infoText: {
    marginTop: 5,
    "@media only screen and (min-width: 600px)": {
      margin: "15px 0px -3px",
    },
  },
  detail: {
    color: Colors.secondary,
  },
  logo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
  },
  addressIcons: {
    fontSize: 20,
    color: Colors.secondary,
  },
  checkbox: {
    marginLeft: 10,
  },
  deleteRoot: {
    width: "100%",
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
    background: Colors.light,
    borderRadius: 5,
    display: "flex",
    padding: "12px 18px 20px",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 480px)": {
      width: 400,
    },
  },
  root: {
    width: "95%",
    display: "flex",
    flexGrow: 1,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 480px)": {
      width: 350,
    },
  },
  actionsRoot: {
    width: "100%",
    display: "flex",
    flexGrow: 1,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 480px)": {
      width: 400,
    },
  },
  listItemIcon: {
    color: Colors.menuListColor,
    minWidth: 22,
    cursor: "pointer",
  },
  editIcon: {
    margin: "1px 0 0px -10px",
    color: Colors.secondary,
    cursor: "pointer",
  },
  editText: {
    margin: "2px 0 0px -10px",
    cursor: "pointer",
    color: Colors.primary,
  },
  button: {
    width: "100%",
    display: "flex",
    marginLeft: 5,
    marginRight: 5,
    padding: 2,
    cursor: "pointer",
    color: Colors.menuListColor,
    fontSize: Fonts.font,
  },
  DeleteIcon: {
    color: Colors.buttonError,
    margin: "1px 0 0px -10px",
    cursor: "pointer",
  },
  deleteText: {
    color: Colors.buttonError,
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
    margin: "2px 0 0px -10px",
    cursor: "pointer",
  },
}));
