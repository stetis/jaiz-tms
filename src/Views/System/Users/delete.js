import { Grid, List, ListItemIcon } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DeleteSharpIcon from "@material-ui/icons/DeleteSharp";
import React, { useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { Colors } from "../../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Text } from "../../../components/Forms/index";
import { deleteUserHelper } from "../../../helpers/System/users.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "./styles";

export default function DeleteUser(props) {
  const classes = styles();
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.usersReducer.actionLoading);

  const handleClickOpen = (e) => {
    e.preventDefault();
    setOpen(true);
  };

  const handleClose = (e) => {
    e.preventDefault();
    setOpen(false);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(deleteUserHelper(props.row.id));
  };
  return (
    <div>
      <List classes={{ root: classes.button }} onClick={handleClickOpen}>
        <ListItemIcon classes={{ root: classes.listItemIcon }}>
          <DeleteSharpIcon className={classes.DeleteIcon} />
        </ListItemIcon>
        <Text className={classes.deleteText}>Delete</Text>
      </List>
      <Dialog
        open={open}
        keepMounted
        aria-labelledby="alert-dialog-slide-title"
        className="add-department-dialog"
        aria-describedby="alert-dialog-slide-description"
      >
        <div className={classes.deleteRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <div className={classes.logo}>
                <img
                  src={jaiz}
                  alt="jaiz logo"
                  style={{ width: 20, height: 20, marginRight: "1rem" }}
                />
                Delete profile
              </div>
            </Grid>
            <Grid
              item
              xs={12}
              sm={12}
              md={12}
              lg={12}
              className={classes.deleteTextStyle}
            >
              <Text style={{ width: "80%", margin: "10px auto" }}>
                Are you sure you want to delete{" "}
                <strong> {props.row.name}?</strong>
              </Text>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <ButtonsRow>
                <DeleteButton onClick={handleClose} style={{ marginRight: 10 }}>
                  Cancel
                </DeleteButton>
                <SubmitButton
                  type="submit"
                  disabled={loading}
                  style={{
                    marginTop: 0,
                  }}
                  onClick={handleSubmit}
                >
                  <span className="submit-btn">
                    {loading ? (
                      <Loader
                        type="ThreeDots"
                        color={Colors.secondary}
                        height="15"
                        width="30"
                      />
                    ) : (
                      "Delete"
                    )}
                  </span>
                </SubmitButton>
              </ButtonsRow>
            </Grid>
          </Grid>
        </div>
      </Dialog>
    </div>
  );
}
