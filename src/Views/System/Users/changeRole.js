import { Grid, List, ListItemIcon } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../actions/profile.action";
import { Roles } from "../../../assets/icons/Svg";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Text } from "../../../components/Forms/index";
import { Select } from "../../../components/TextField/index";
import { getRolesHelper } from "../../../helpers/profile.helper";
import { changeRoleHelper } from "../../../helpers/System/users.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "./styles";

export default function ChangeRole(props) {
  const classes = styles();
  const [role, setRole] = useState(props.row.role);
  const [open, setOpen] = useState(false);
  const [formError, setFormError] = useState(false);
  const dispatch = useDispatch();
  const roles = useSelector((state) => state.profileReducer.roles.roles);
  const loading = useSelector((state) => state.usersReducer.actionLoading);
  const status = useSelector((state) => state.usersReducer.status);
  const statusMessage = useSelector(
    (state) => state.usersReducer.statusMessage
  );
  useEffect(() => {
    const timer = setTimeout(() => {
      setFormError(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [formError]);

  useEffect(() => {
    dispatch(getRolesHelper());
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleChange = (e) => setRole(e.target.value);
  const handleClickOpen = () => setOpen(true);
  const handleClose = (e) => {
    e.preventDefault();
    setOpen(false);
    setRole("");
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const roleDetail = roles && roles.find((rol) => rol["title"] === role);
    if (!role) {
      setFormError(true);
    } else {
      const inputData = {
        id: props.row.id,
        name: role,
        "role-id": roleDetail["role-id"],
        role: role,
        "profile-id": props.row.id,
      };
      console.log(inputData, "i am the input data");
      dispatch(changeRoleHelper(inputData));
    }
  };

  const handleCloseSnack = () => dispatch(clearMessages());
  const renderFieldError = (formErrorMessage) => {
    <Snackbars
      variant="error"
      handleClose={handleCloseSnack}
      message={formErrorMessage}
      isOpen={formError}
    />;
  };
  return (
    <div>
      <List classes={{ root: classes.button }} onClick={handleClickOpen}>
        <ListItemIcon classes={{ root: classes.listItemIcon }}>
          <Roles className={classes.editIcon} />
        </ListItemIcon>
        <Text className={classes.editText}>Change user role</Text>
      </List>
      {formError === true
        ? renderFieldError(!role ? "Role is required" : "")
        : null}
      <Dialog
        open={open}
        keepMounted
        aria-labelledby="alert-dialog-slide-role"
        className="add-branch-dialog"
        aria-describedby="alert-dialog-slide-description"
      >
        <div className={classes.actionsRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12}>
              <form
                className={classes.container}
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Change role
                  </div>
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={12}
                  lg={12}
                  style={{ display: "flex" }}
                >
                  <Text className={classes.infoText}>
                    <span className={classes.detail}>Current role: </span>
                    {props.row.role}
                  </Text>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Select
                    id="role"
                    htmlFor="role"
                    name="role"
                    type="text"
                    label="Role"
                    value={role}
                    onChange={handleChange}
                  >
                    {roles &&
                      roles.map((role) => (
                        <option value={role.title} key={role.title}>
                          {role.title}
                        </option>
                      ))}
                  </Select>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      style={{
                        marginRight: 10,
                      }}
                      onClick={handleClose}
                      disabled={loading}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        marginTop: 0,
                      }}
                    >
                      {loading ? (
                        <Loader
                          type="ThreeDots"
                          color={Colors.secondary}
                          height="15"
                          width="30"
                        />
                      ) : (
                        "Save"
                      )}
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </div>
      </Dialog>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
