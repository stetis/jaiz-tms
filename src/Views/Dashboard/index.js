import { Grid } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import FilterListOutlinedIcon from "@material-ui/icons/FilterListOutlined";
import { updateBreadcrumb } from "../../actions/breadcrumbAction";
import { Colors } from "../../assets/themes/theme";
import { Title } from "../../components/contentHolders/Card";
import { Text } from "../../components/Forms/index";
import { dashboardHelper } from "../../helpers/dashboard.helper";
import Commissions from "./Agent/commission";
import Transactions from "./Agent/transactions";
import AgentChart from "./general/agentChart";
import AgentsTable from "./general/agentTable";
import MerchantChart from "./general/merchantChart";
import Outlets from "./Merchant/outlets";
import TransactionPerTerminal from "./Merchant/transactionPerTerminal";
import MerchantsTable from "./merchantTable";
import { styles } from "./styles";
import { ButtonsRow } from "../../components/Buttons/styles";
import { SubmitButton } from "../../components/Buttons";
import { TextField } from "../../components/TextField";
import Spinner from "../../assets/Spinner";

export default function Dashboard() {
  const classes = styles();
  let today = new Date();
  let startDate =
    today.getFullYear() +
    "/" +
    ("0" + (today.getMonth() - 5)).slice(-2) +
    "/" +
    ("0" + today.getDate()).slice(-2);
  let endDate =
    today.getFullYear() +
    "/" +
    ("0" + (today.getMonth() + 1)).slice(-2) +
    "/" +
    ("0" + today.getDate()).slice(-2);
  const [showButton, setShowButton] = useState(false);
  const [type, setType] = useState("text");
  const [toType, setToType] = useState("text");
  const [from, setFrom] = useState(startDate);
  const [to, setTo] = useState(endDate);
  const toOnFocus = () => setToType("date");
  const toOnBlur = () => setToType("text");
  const onFocus = () => setType("date");
  const onBlur = () => setType("text");
  const data = useSelector((state) => state.loginReducer.data);
  const dashboardData = useSelector((state) => state.dashboardReducer.data);
  const loading = useSelector((state) => state.dashboardReducer.loading);

  const dispatch = useDispatch();
  useEffect(() => {
    document.title = "Dashboard | Jaiz bank TMS";
    dispatch(
      updateBreadcrumb({
        parent: "Dashboard",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );
    let credentials = {
      start: startDate,
      end: endDate,
    };
    dispatch(dashboardHelper(credentials));
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const handleClickOpen = () => {
    setShowButton(!showButton);
  };
  const handleFromChange = (e) => setFrom(e.target.value);
  const handleToChange = (e) => setTo(e.target.value);
  const handleKeyPress = (event) => {
    let fromDate = new Date(from);
    let toDate = new Date(to);
    let startDate =
      fromDate.getFullYear() +
      "/" +
      ("0" + (fromDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + fromDate.getDate()).slice(-2);
    let endDate =
      toDate.getFullYear() +
      "/" +
      ("0" + (toDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + toDate.getDate()).slice(-2);
    let credentials = {
      start: startDate,
      end: endDate,
    };
    if (event.keyCode === 13) {
      dispatch(dashboardHelper(credentials));
    }
  };

  const generalDashboard = (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <ButtonsRow>
          <SubmitButton
            onClick={handleClickOpen}
            className={classes.fabIcon}
            ghost
          >
            <FilterListOutlinedIcon className={classes.icon} />
            Filters
          </SubmitButton>
        </ButtonsRow>
      </Grid>
      {showButton ? (
        <Grid container spacing={0}>
          <Grid item xs={1} sm={6} md={6} lg={6} />
          <Grid item xs={11} sm={6} md={6} lg={6}>
            <form className={classes.container} noValidate autoComplete="off">
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="from"
                  htmlFor="from"
                  name="from"
                  type={type}
                  placeholder="from"
                  onFocus={onFocus}
                  onBlur={onBlur}
                  value={from}
                  label="Start date"
                  onChange={handleFromChange}
                  onKeyUp={handleKeyPress}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="to"
                  htmlFor="to"
                  name="to"
                  type={toType}
                  placeholder="To"
                  onFocus={toOnFocus}
                  onBlur={toOnBlur}
                  value={to}
                  label="End date"
                  onChange={handleToChange}
                  onKeyUp={handleKeyPress}
                  grouped
                />
              </Grid>
            </form>
          </Grid>
        </Grid>
      ) : null}
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <Title>Analytics</Title>
      </Grid>
      <Grid item xs={12} sm={3} md={3} lg={3}>
        <div className={classes.terminalCard}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Title style={{ marginBottom: 5 }}>Terminal distributions</Title>
            </Grid>
            <Grid item xs={12} sm={4} md={4} lg={4}>
              <Text
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "left",
                  color: Colors.secondary,
                  fontWeight: 700,
                }}
              >
                Merchants
                <span
                  style={{
                    marginTop: 5,
                  }}
                >
                  {dashboardData["analytics"] &&
                    dashboardData["analytics"]["merchant-terminals"]}
                </span>
              </Text>
            </Grid>
            <Grid item xs={12} sm={4} md={4} lg={4}>
              <Text
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "left",
                  color: Colors.secondary,
                  fontWeight: 700,
                }}
              >
                Agents
                <span
                  style={{
                    marginTop: 5,
                  }}
                >
                  {dashboardData["analytics"] &&
                    dashboardData["analytics"]["agent-terminals"]}
                </span>
              </Text>
            </Grid>
            <Grid item xs={12} sm={4} md={4} lg={4}>
              <Text
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "left",
                  color: Colors.secondary,
                  fontWeight: 700,
                }}
              >
                Both
                <span
                  style={{
                    marginTop: 5,
                  }}
                >
                  {dashboardData["analytics"] &&
                    dashboardData["analytics"]["merchant-agent-terminals"]}
                </span>
              </Text>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <hr
                style={{
                  // margin: "10px 0",
                  height: 0.5,
                  border: "none",
                  color: Colors.greyLight,
                  backgroundColor: Colors.greyLight,
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Text
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "left",
                  color: Colors.secondary,
                  fontWeight: 700,
                  marginBottom: 10,
                }}
              >
                Total:{" "}
                {parseFloat(
                  dashboardData["analytics"] &&
                    dashboardData["analytics"]["merchant-terminals"]
                ) +
                  parseFloat(
                    dashboardData["analytics"] &&
                      dashboardData["analytics"]["agent-terminals"]
                  ) +
                  parseFloat(
                    dashboardData["analytics"] &&
                      dashboardData["analytics"]["merchant-agent-terminals"]
                  )}
              </Text>
            </Grid>
          </Grid>
        </div>
      </Grid>
      <Grid item xs={12} sm={2} md={2} lg={2}>
        <div className={classes.merchantAgentCard}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Title style={{ marginBottom: 5 }}>Merchants and Agents</Title>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <Text
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "left",
                  color: Colors.secondary,
                  fontWeight: 700,
                }}
              >
                Merchants
                <span
                  style={{
                    marginTop: 5,
                  }}
                >
                  {dashboardData["analytics"] &&
                    dashboardData["analytics"]["merchants"]}
                </span>
              </Text>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <Text
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "left",
                  color: Colors.secondary,
                  fontWeight: 700,
                }}
              >
                Agents
                <span
                  style={{
                    marginTop: 5,
                  }}
                >
                  {dashboardData["analytics"] &&
                    dashboardData["analytics"]["agents"]}
                </span>
              </Text>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <hr
                style={{
                  // margin: "10px 0",
                  height: 0.5,
                  border: "none",
                  color: Colors.greyLight,
                  backgroundColor: Colors.greyLight,
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Text
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "left",
                  color: Colors.secondary,
                  fontWeight: 700,
                  marginBottom: 10,
                }}
              >
                Total:{" "}
                {parseFloat(
                  dashboardData["analytics"] &&
                    dashboardData["analytics"]["merchants"]
                ) +
                  parseFloat(
                    dashboardData["analytics"] &&
                      dashboardData["analytics"]["agents"]
                  )}
              </Text>
            </Grid>
          </Grid>
        </div>
      </Grid>
      <Grid item xs={12} sm={2} md={2} lg={2}>
        <div className={classes.transactionCard}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Title style={{ marginBottom: 5 }}>
                Transactions for 6 months
              </Title>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <Text
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "left",
                  color: Colors.secondary,
                  fontWeight: 700,
                }}
              >
                Merchants
                <span
                  style={{
                    marginTop: 5,
                  }}
                >
                  {dashboardData["analytics"] &&
                    dashboardData["analytics"]["merchant-tx"]}
                </span>
              </Text>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <Text
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "left",
                  color: Colors.secondary,
                  fontWeight: 700,
                }}
              >
                Agents
                <span
                  style={{
                    marginTop: 5,
                  }}
                >
                  {dashboardData["analytics"] &&
                    dashboardData["analytics"]["agent-tx"]}
                </span>
              </Text>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <hr
                style={{
                  height: 0.5,
                  border: "none",
                  color: Colors.greyLight,
                  backgroundColor: Colors.greyLight,
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Text
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "left",
                  color: Colors.secondary,
                  fontWeight: 700,
                  marginBottom: 10,
                }}
              >
                Total:{" "}
                {parseFloat(
                  dashboardData["analytics"] &&
                    dashboardData["analytics"]["merchant-tx"]
                ) +
                  parseFloat(
                    dashboardData["analytics"] &&
                      dashboardData["analytics"]["agent-tx"]
                  )}
              </Text>
            </Grid>
          </Grid>
        </div>
      </Grid>
      <Grid item xs={12} sm={3} md={3} lg={3}>
        <div className={classes.transactionCard}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Title style={{ marginBottom: 5 }}>
                Transactions for the session
              </Title>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <Text
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "left",
                  color: Colors.secondary,
                  fontWeight: 700,
                }}
              >
                Merchants
                <span
                  style={{
                    marginTop: 5,
                  }}
                >
                  {dashboardData["analytics"] &&
                    dashboardData["analytics"]["total-merchant-tx"]}
                </span>
              </Text>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <Text
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "left",
                  color: Colors.secondary,
                  fontWeight: 700,
                }}
              >
                Agents
                <span
                  style={{
                    marginTop: 5,
                  }}
                >
                  {dashboardData["analytics"] &&
                    dashboardData["analytics"]["total-agent-tx"]}
                </span>
              </Text>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <hr
                style={{
                  height: 0.5,
                  border: "none",
                  color: Colors.greyLight,
                  backgroundColor: Colors.greyLight,
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Text
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "left",
                  color: Colors.secondary,
                  fontWeight: 700,
                  marginBottom: 10,
                }}
              >
                Total:{" "}
                {parseFloat(
                  dashboardData["analytics"] &&
                    dashboardData["analytics"]["total-merchant-tx"]
                ) +
                  parseFloat(
                    dashboardData["analytics"] &&
                      dashboardData["analytics"]["total-agent-tx"]
                  )}
              </Text>
            </Grid>
          </Grid>
        </div>
      </Grid>
      <Grid item xs={12} sm={2} md={2} lg={2}>
        <div className={classes.healthCard}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Title>Terminal health</Title>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <Text
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "left",
                  color: Colors.secondary,
                  fontWeight: 700,
                }}
              >
                Healthy
                <span
                  style={{
                    marginTop: 5,
                    color: Colors.secondary,
                    fontSize: 16,
                  }}
                >
                  {dashboardData["analytics"] &&
                    dashboardData["analytics"]["ok-terminals"]}
                </span>
              </Text>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <Text
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "left",
                  color: Colors.buttonError,
                  fontWeight: 700,
                }}
              >
                Faulty
                <span
                  style={{
                    marginTop: 5,
                    color: Colors.buttonError,
                    fontSize: 16,
                  }}
                >
                  {dashboardData["analytics"] &&
                    dashboardData["analytics"]["faulty-terminals"]}
                </span>
              </Text>
            </Grid>
          </Grid>
        </div>
      </Grid>
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <Title>Transaction trends</Title>
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={6}>
        <MerchantChart
          data={
            dashboardData["transaction-chart"] &&
            dashboardData["transaction-chart"]["merchant-chart"]
          }
        />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={6}>
        <AgentChart
          data={
            dashboardData["transaction-chart"] &&
            dashboardData["transaction-chart"]["agent-chart"]
          }
        />
      </Grid>
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <Title>Transactions</Title>
      </Grid>
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <MerchantsTable
          data={
            dashboardData["latest-transactions"] &&
            dashboardData["latest-transactions"]["merchant-transactions"]
          }
        />
      </Grid>
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <AgentsTable
          data={
            dashboardData["latest-transactions"] &&
            dashboardData["latest-transactions"]["agent-transaction"]
          }
        />
      </Grid>
    </Grid>
  );

  const merchantDashboard = (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <div className={classes.summaryHolder}>
          <Grid item xs={12} sm={2} md={2} lg={2}>
            <div
              className={classes.summaryCard}
              style={{ textAlign: "center", fontWeight: 700 }}
            >
              <Title style={{ textAlign: "center", color: Colors.primary }}>
                Outlet
                <span className={classes.summaryDigits}>4</span>
              </Title>
            </div>
          </Grid>
          <Grid item xs={12} sm={2} md={2} lg={2}>
            <div
              className={classes.summaryCard}
              style={{ textAlign: "center", fontWeight: 700 }}
            >
              <Title style={{ textAlign: "center", color: Colors.primary }}>
                Terminals
                <span className={classes.summaryDigits}>10 </span>
              </Title>
            </div>
          </Grid>
        </div>
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={6}>
        <Outlets />
      </Grid>
      <Grid item xs={12} sm={6} md={6} lg={6}>
        <TransactionPerTerminal />
      </Grid>
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <Title style={{ margin: "5px 0 -10" }}>Transactions</Title>
      </Grid>
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <MerchantsTable />
      </Grid>
    </Grid>
  );
  const agentDashboard = (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <ButtonsRow style={{ marginTop: 0 }}>
          <SubmitButton
            onClick={handleClickOpen}
            className={classes.fabIcon}
            ghost
          >
            <FilterListOutlinedIcon className={classes.icon} />
            Filters
          </SubmitButton>
        </ButtonsRow>
      </Grid>
      {showButton ? (
        <Grid container spacing={0}>
          <Grid item xs={1} sm={6} md={6} lg={6} />
          <Grid item xs={11} sm={6} md={6} lg={6}>
            <form className={classes.container} noValidate autoComplete="off">
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="from"
                  htmlFor="from"
                  name="from"
                  type={type}
                  placeholder="from"
                  onFocus={onFocus}
                  onBlur={onBlur}
                  value={from}
                  label="Start date"
                  onChange={handleFromChange}
                  onKeyUp={handleKeyPress}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6} lg={6}>
                <TextField
                  id="to"
                  htmlFor="to"
                  name="to"
                  type={toType}
                  placeholder="To"
                  onFocus={toOnFocus}
                  onBlur={toOnBlur}
                  value={to}
                  label="End date"
                  onChange={handleToChange}
                  onKeyUp={handleKeyPress}
                  grouped
                />
              </Grid>
            </form>
          </Grid>
        </Grid>
      ) : null}
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <Commissions />
      </Grid>
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <Transactions />
      </Grid>
    </Grid>
  );

  return (
    <div className={classes.card}>
      {loading ? (
        <Spinner />
      ) : data.profile["user-type"] === "merchant" ? (
        merchantDashboard
      ) : data.profile["user-type"] === "agent" ? (
        agentDashboard
      ) : data.profile["user-type"] === "bank" ? (
        generalDashboard
      ) : (
        ""
      )}
    </div>
  );
}
