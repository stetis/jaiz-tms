import { makeStyles } from "@material-ui/core";
import { Colors, Fonts } from "../../assets/themes/theme";

export const styles = makeStyles(() => ({
  card: {
    width: "98%",
    margin: "16px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    "@media only screen and (min-width: 960px)": {
      width: "100%",
    },
  },
  logo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
  },
  terminalCard: {
    fontFamily: Fonts.secondary,
    padding: "10px 15px 0px 15px",
    background: Colors.light,
    borderRadius: 5,
    width: "100%",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    transition: "all .2s ease",
  },

  merchantAgentCard: {
    fontFamily: Fonts.secondary,
    padding: "10px 15px 0px 15px",
    width: "100%",
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    transition: "all .2s ease",
  },
  transactionCard: {
    fontFamily: Fonts.secondary,
    padding: "10px 15px 0px 15px",
    width: "100%",
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    transition: "all .2s ease",
  },
  healthCard: {
    fontFamily: Fonts.secondary,
    padding: "10px 15px 0px 15px",
    width: "100%",
    height: 135,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    transition: "all .2s ease",
  },
  summaryDigits: {
    color: Colors.secondary,
    display: "flex",
    justifyContent: "center",
    textAlign: "center",
    marginTop: 5,
  },
  vr: {
    borderLeft: `1px solid ${Colors.greyLight}`,
    height: "100%",
    "@media only screen and (min-width: 600px)": {
      position: "relative",
      left: "2%",
      top: 0,
    },
  },
  chartCard: {
    width: "100%",
    background: Colors.light,
    padding: "13px 25px 25px",
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
  },
  tableCard: {
    width: "100%",
    background: Colors.light,
    padding: "13px 25px 25px",
    borderRadius: 5,
    marginTop: 10,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
  },
  tableLogo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
  },
  moreLink: {
    textDecoration: "none",
    color: Colors.primary,
    float: "right",
    margin: "10px 20px 0",
    cursor: "pointer",
    fontFamily: Fonts.secondary,
    fontWeight: 700,
    fontSize: 12,
  },
  root: {
    width: 320,
    display: "flex",
    flexGrow: 1,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 350,
    },
  },
  container: {
    display: "flex",
    flexWrap: "wrap",
    paddingRight: 15,
    marginBottom: -25,
  },
  fabIcon: {
    float: "right",
    "@media only screen and (min-width: 600px)": {
      position: "absolute",
      top: 60,
      left: "88%",
    },
    "@media only screen and (min-width: 960px)": {
      position: "absolute",
      top: 65,
      left: "90%",
    },
  },
  icon: {
    fontSize: 20,
    color: Colors.secondary,
    marginRight: 5,
  },
}));
