import { Grid } from "@material-ui/core";
import { MuiThemeProvider } from "@material-ui/core/styles";
import MUIDataTable from "mui-datatables";
import React, { useState } from "react";
import { theme } from "../../../assets/MUI/Table";
import Spinner from "../../../assets/Spinner";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "../styles";

const styleProps = {
  height: 50,
  width: 50,
};
function TransactionPerTerminal(props) {
  const classes = styles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowPerPage] = useState(10);

  /* This use have frontend pagination */
  const onPageChange = (page) => {
    //  const { rowsPerPage, searchText } = state;
    //  const { fetchPeopleTable } = props;
    //  let credentials = {
    //    page: page + 1,
    //    pagesize: rowsPerPage,
    //    search: searchText,
    //  };
    //  fetchPeopleTable(credentials);
    setPage(page);

    //  setState({
    //    page: page,
    //    rowsPerPage,
    //    searchText,
    //  });
  };
  const onChangeRowsPerPage = (numberOfRows) => {
    //  const { page, searchText } = state;
    //  setState({ rowsPerPage: numberOfRows });
    //  const { fetchPeopleTable } = props;
    //  let credentials = {
    //    pagesize: numberOfRows,
    //    page: page,
    //    search: searchText,
    //  };
    //  fetchPeopleTable(credentials);
    setRowPerPage(numberOfRows);
  };

  const columns = ["", "Terminal ID", "Serial No", "Amount"];
  const data = [
    [1, "Tm567890", "34567890", "234,563"],
    [2, "678678", "fg7654678", "23,000"],
    [3, "09876789", "ft876556", "10,000"],
    [4, "rt0987654", "456789", "344,983"],
  ];
  const options = {
    pagination: true,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    rowsPerPage: rowsPerPage,
    page: page,
    serverSide: false,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    rowsPerPageOptions: [5, 10],
    textLabels: {
      body: {
        noMatch: "No outlet record found",
      },
    },
    onTableChange: (action, tableState) => {
      switch (action) {
        case "onRowsPerPageChange":
          onChangeRowsPerPage(tableState.rowsPerPage);
          break;
        case "onPageChange":
          onPageChange(tableState.page);
          break;
        default:
      }
    },
  };
  return (
    <div className={classes.tableCard}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!props.isLoading ? (
            <MuiThemeProvider theme={theme}>
              <MUIDataTable
                data={data}
                columns={columns}
                options={options}
                title={
                  <div className={classes.tableLogo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    transactions per terminal
                  </div>
                }
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
    </div>
  );
}
export default TransactionPerTerminal;
