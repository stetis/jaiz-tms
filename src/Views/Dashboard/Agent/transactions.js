import { Grid, MuiThemeProvider } from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import React from "react";
import { withRouter } from "react-router";
import { wideTable } from "../../../assets/MUI/Table";
import Spinner from "../../../assets/Spinner";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "../styles";

const styleProps = {
  height: 50,
  width: 50,
};
function TransactionsTable(props) {
  const classes = styles();
  const columns = [
    "",
    "Date",
    "Terminal ID",
    "Amount",
    "RRN",
    "Type",
    "Status",
    "Charge",
    "Commission",
  ];
  const data = [
    [
      1,
      "20-07-2020",
      "5678974tel",
      "jz45678343",
      "35,456",
      "RR34567897",
      "Temrinal widthdrawal",
      "200",
      "100",
    ],
    [
      2,
      "20-07-2020",
      "5678974tel",
      "jz45678343",
      "35,456",
      "RR34567897",
      "Temrinal widthdrawal",
      "100",
      "200",
    ],
    [
      3,
      "20-07-2020",
      "5678974tel",
      "jz45678343",
      "35,456",
      "RR34567897",
      "Temrinal widthdrawal",
      "1000",
      "500",
    ],
    [
      4,
      "20-07-2021",
      "5678974tel",
      "jz45678343",
      "35,456",
      "RR34567897",
      "Temrinal widthdrawal",
      "3000",
      "900",
    ],
    [
      5,
      "20-07-2019",
      "5678974tel",
      "jz456783423",
      "234,456",
      "RR34678998",
      "Temrinal transafer",
      "1250",
      "500",
    ],
  ];
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: false,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    rowsPerPageOptions: [],
    textLabels: {
      body: {
        noMatch: "No transaction record found",
      },
    },
  };
  return (
    <div className={classes.tableCard}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!props.isLoading ? (
            <MuiThemeProvider theme={wideTable}>
              <MUIDataTable
                data={data}
                columns={columns}
                options={options}
                title={
                  <div className={classes.tableLogo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Transactions
                  </div>
                }
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
    </div>
  );
}
export default withRouter(TransactionsTable);
