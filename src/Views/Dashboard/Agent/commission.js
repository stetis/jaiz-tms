import { Grid, MuiThemeProvider } from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import React from "react";
import { withRouter } from "react-router";
import { wideTable } from "../../../assets/MUI/Table";
import Spinner from "../../../assets/Spinner";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "../styles";

const styleProps = {
  height: 50,
  width: 50,
};
function CommissionsTable(props) {
  const classes = styles();
  const columns = [
    "",
    "Transaction",
    "Type",
    "Total Amount",
    "Total charge",
    "Total commission",
  ];
  const data = [
    [1, "Account base", "Withdrawal", "5,000", "100", "125"],
    [2, "POS base", "deposit", "20,0000", "64", "100"],
    [3, "Account base", "deposit", "100,123", "350", "150"],
    [4, "terminals", "withdrawal", "150,000", "300", "1200"],
    [5, "Account type", "deposit", "200,000", "2,000", "1,200"],
    ["Total", "", "", "23,343", "120,000", "125,000"],
  ];
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: false,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    rowsPerPageOptions: [],
    textLabels: {
      body: {
        noMatch: "No commission record found",
      },
    },
  };
  return (
    <div className={classes.tableCard}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!props.isLoading ? (
            <MuiThemeProvider theme={wideTable}>
              <MUIDataTable
                data={data}
                columns={columns}
                options={options}
                title={
                  <div className={classes.tableLogo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Commissions
                  </div>
                }
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
    </div>
  );
}
export default withRouter(CommissionsTable);
