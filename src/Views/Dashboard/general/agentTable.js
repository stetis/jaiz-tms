import { Grid, MuiThemeProvider } from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import React from "react";
import { withRouter } from "react-router";
import { wideTable } from "../../../assets/MUI/Table";
import Spinner from "../../../assets/Spinner";
import { Text } from "../../../components/Forms";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "../styles";

const styleProps = {
  height: 50,
  width: 50,
};

function capitalizeFirstLetters(str) {
  var splitStr = str.toLowerCase().split(" ");
  for (var i = 0; i < splitStr.length; i++) {
    // You do not need to check if i is larger than splitStr length, as your for does that for you
    // Assign it back to the array
    splitStr[i] =
      splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  // Directly return the joined string
  return splitStr.join(" ");
}
function AgentsTable(props) {
  const classes = styles();
  if (props.data) {
    props.data.header.unshift("");
  }
  props.data && props.data.body.map((dat, i) => dat.unshift(i + 1));
  const newColumns =
    props.data &&
    props.data.header.map((dat, i) => {
      return dat === "ID"
        ? {
            name: dat,
            options: {
              display: false,
            },
          }
        : dat === "Agent"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 150,
                    }}
                  >
                    {capitalizeFirstLetters(dataIndex)}
                  </Text>
                );
              },
            },
          }
        : dat === "Agent ID"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 80,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : dat === "To account"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 90,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : dat === "From account"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 80,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : dat === "State"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return <Text>{capitalizeFirstLetters(dataIndex)}</Text>;
              },
            },
          }
        : dat === "Region"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 120,
                    }}
                  >
                    {capitalizeFirstLetters(dataIndex)}
                  </Text>
                );
              },
            },
          }
        : dat === "Branch"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 120,
                    }}
                  >
                    {capitalizeFirstLetters(dataIndex)}
                  </Text>
                );
              },
            },
          }
        : dat === "Amount (₦)"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 80,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : dat === "RRN"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 80,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : dat === "Merchant ID"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 100,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : dat === "Date"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 70,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : dat === "Terminal ID"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 70,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : dat === "Description"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 185,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : dat;
    });
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: false,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    textLabels: {
      body: {
        noMatch: "No agent record found",
      },
    },
  };
  return (
    <div className={classes.tableCard}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!props.isLoading ? (
            <MuiThemeProvider theme={wideTable}>
              <MUIDataTable
                data={props.data && props.data ? props.data.body : []}
                columns={newColumns}
                options={options}
                title={
                  <div className={classes.tableLogo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Agents
                  </div>
                }
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
    </div>
  );
}
export default withRouter(AgentsTable);
