import React from "react";

import { Line } from "react-chartjs-2";
import { Colors } from "../../../assets/themes/theme";
import { styles } from "../styles";

export default function MerchantChart(props) {
  const classes = styles();
  const newLabel = props.data && props.data.map((arr) => arr.month);
  const newData = props.data && props.data.map((arr) => arr.count);
  const data = {
    labels: newLabel,
    datasets: [
      {
        label: "Merchant monthly transations",
        data: newData,
        fill: false,
        backgroundColor: Colors.light,
        borderColor: Colors.secondary,
        borderWidth: 1,
      },
    ],
  };

  const options = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  };
  return (
    <div className={classes.chartCard}>
      <Line data={data} options={options} />
    </div>
  );
}
