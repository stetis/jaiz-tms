import { makeStyles, withStyles } from "@material-ui/core";
import { Colors, Fonts } from "../../assets/themes/theme";
import { Tooltip } from "@material-ui/core";

export const LightTooltip = withStyles(() => ({
  tooltip: {
    backgroundColor: Colors.light,
    color: Colors.textColor,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
  },
}))(Tooltip);

export const styles = makeStyles(() => ({
  root: {
    width: "95%",
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 5px 25px",
    borderRadius: 5,
    cursor: "pointer",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 960px)": {
      width: "100%",
    },
  },
  summaryHolder: {
    width: "100%",
    "@media only screen and (min-width: 600px)": {
      display: "flex",
    },
  },
  SummaryCard: {
    width: "100%",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "10px 15px",
    borderRadius: 5,
    margin: 10,
    justifyContent: "center",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 125,
      display: "flex",
      flexGrow: 1,
    },
    "@media only screen and (min-width: 960px)": {
      width: "80%",
      margin: "0 auto",
      flexGrow: 1,
    },
  },
  summaryDigits: {
    color: Colors.secondary,
    display: "flex",
    justifyContent: "center",
    marginTop: 5,
  },
  searchGrid: {
    "@media only screen and (min-width: 600px)": {
      display: "flex",
    },
  },
  bulkCard: {
    width: "98%",
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "0 10px 10px 10px",
    borderRadius: 5,
    cursor: "pointer",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: "100%",
    },
  },
  fileGrid: {
    marginTop: 23,
    "@media only screen and (min-width: 960px)": {
      margin: "27px 0 10px -52px",
    },
  },
  manageTerminal: {
    width: "90%",
    margin: "26px auto",
    "@media only screen and (min-width: 960px)": {
      width: "60%",
    },
  },
  manageTerminalCards: {
    width: "100%",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "15px 10px 20px 20px",
    borderRadius: 5,
    marginTop: 26,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      margin: "27px 0 10px -52px",
    },
  },
  merchantGrid: {
    marginTop: 10,
    "@media only screen and (min-width: 960px)": {
      margin: "10px 0 10px 47px",
    },
  },
  card: {
    width: "100%",
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 20px 25px",
    borderRadius: 5,
    cursor: "pointer",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
  },
  merchantInfo: {
    color: Colors.secondary,
    marginRight: 2,
  },
  fabIcon: {
    float: "right",
    "@media only screen and (min-width: 600px)": {
      top: 25,
      left: "95%",
      zIndex: 200,
    },
  },
  icon: {
    color: Colors.secondary,
    marginRight: 5,
  },
  addMerchantCard: {
    width: 320,
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 500,
    },
  },
  addCard: {
    width: 320,
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 550,
    },
  },
  container: {
    display: "flex",
    padding: "8px 25px 25px",
    flexWrap: "wrap",
  },
  fabAddIcon: {
    fontSize: 14,
    color: Colors.secondary,
  },

  editOutletFab: {
    width: 25,
    height: 20,
    padding: 15,
    float: "right",
    "@media only screen and (min-width: 600px)": {
      left: -5,
      top: -28,
    },
    "@media only screen and (min-width: 960px)": {
      left: 8,
      top: -30,
    },
    "@media only screen and (min-width: 1280)": {
      // left: 594,
      top: 15,
    },
    "@media only screen and (min-width: 1920)": {
      // left: 594,
      top: 15,
    },
  },
  AddOutletButton: {
    position: "relative",
    top: -8,
    left: 0,
    float: "right",
    "@media only screen and (min-width: 600px)": {
      left: -5,
      top: -28,
    },
    "@media only screen and (min-width: 960px)": {
      left: 8,
      top: -30,
    },
    "@media only screen and (min-width: 1280)": {
      // left: 594,
      top: 15,
    },
    "@media only screen and (min-width: 1920)": {
      // left: 594,
      top: 15,
    },
  },
  addTerminalCard: {
    width: "95%",
    margin: "26px auto",
    display: "flex",
    paddingRight: 10,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 550,
    },
  },
  AddTerminalFab: {
    width: 25,
    height: 20,
    padding: 15,
    float: "right",
    marginTop: 0,
    "@media only screen and (min-width: 600px)": {
      position: "relative",
      left: 0,
      top: 13,
    },
  },
  EditRemoveFab: {
    width: 20,
    height: 18,
    padding: 10,
    margin: 8,
    "@media only screen and (min-width: 600px)": {
      position: "relative",
      top: -52,
      left: 115,
    },
    "@media only screen and (min-width: 960px)": {
      left: 110,
    },
  },
  removeFab: {
    width: 20,
    height: 18,
    padding: 10,
    margin: 8,
    float: "right",
    "@media only screen and (min-width: 600px)": {
      position: "relative",
      top: -46,
      left: 34,
    },
    "@media only screen and (min-width: 960px)": {
      position: "relative",
      top: -48,
      left: 35,
    },
  },
  removeFabIcon: {
    fontSize: 14,
  },
  label: {
    cursor: "pointer",
    fontFamily: Fonts.secondary,
    marginLeft: 15,
  },
  tableLogo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
    padding: "10px 18px 10px",
    "@media only screen and (max-width: 540px)": {
      marginTop: -25,
    },
  },
  buttonRow: {
    "@media only screen and (max-width: 540px)": {
      position: "absolute",
      zIndex: 1000,
      top: 0,
    },
  },
  bulkLogo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
  },
  logo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
    margin: "10px 0px",
  },
  deleteRoot: {
    width: 320,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    display: "flex",
    padding: "12px 18px 20px",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 350,
    },
  },
  addIcon: {
    fontSize: 20,
    color: Colors.secondary,
  },
  actionsRoot: {
    width: 350,
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (max-width: 359px)": {
      width: "100%",
    },
    "@media only screen and (min-width: 360px) and (max-width: 700px)": {
      width: "100%",
    },
    "@media only screen and (min-width: 768px) and (max-width: 1200px)": {
      width: 350,
    },
  },
  editListItemIcon: {
    color: Colors.primary,
    minWidth: 22,
    cursor: "pointer",
    marginTop: 6,
  },
  listItemIcon: {
    color: Colors.secondary,
    minWidth: 22,
    cursor: "pointer",
  },
  editIcon: {
    color: Colors.secondary,
    margin: "1px 0 0px -10px",
    cursor: "pointer",
  },
  editText: {
    color: Colors.primary,
    margin: "2px 0 0px -10px",
    cursor: "pointer",
  },
  button: {
    width: "100%",
    display: "flex",
    marginLeft: 5,
    marginRight: 5,
    padding: 2,
    color: Colors.menuListColor,
    fontSize: Fonts.font,
    cursor: "pointer",
  },
  checkbox: {
    marginLeft: 10,
  },
  DeleteIcon: {
    color: Colors.buttonError,
    margin: "1px 0 0px -10px",
    cursor: "pointer",
  },
  deleteText: {
    color: Colors.buttonError,
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
    margin: "2px 0 0px -10px",
    cursor: "pointer",
  },
}));
