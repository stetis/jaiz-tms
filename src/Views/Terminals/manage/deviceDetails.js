import { Grid } from "@material-ui/core";
import React from "react";
import { TerminalIcon } from "../../../assets/icons/Svg";
import { Colors } from "../../../assets/themes/theme";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Text } from "../../../components/Forms/index";
import { LightTooltip, styles } from "../styles";
import jaiz from "../../../images/jaiz-logo.png";

export default function DeviceDetail(props) {
  const classes = styles();
  return (
    <div className={classes.manageTerminalCards}>
      <Grid container spacing={1}>
        <Grid item xs={12} sm={11} md={11} lg={11}>
          <div className={classes.logo}>
            <img
              src={jaiz}
              alt="jaiz logo"
              style={{ width: 20, height: 20, marginRight: "1rem" }}
            />
            Device
          </div>
        </Grid>
        <Grid item xs={12} sm={1} md={1} lg={1}>
          <LightTooltip
            title={`This terminal is ${props.row.status}`}
            arrow={true}
            placement="right"
          >
            <ButtonsRow>
              <TerminalIcon
                width="35px"
                height="35px"
                style={{
                  float: "right",
                  cursor: "pointer",
                  marginBottom: -15,
                  color:
                    props.row.status === "ok"
                      ? Colors.primary
                      : props.row.status === "faulty"
                      ? Colors.buttonError
                      : Colors.secondary,
                }}
              />
            </ButtonsRow>
          </LightTooltip>
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={4}>
          <Text>
            <span className={classes.merchantInfo}>Serial number: </span>
            {props.row["serial-number"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={4}>
          <Text>
            <span className={classes.merchantInfo}>Terminal ID: </span>
            {props.row["terminal-id"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={4} />
        <Grid item xs={12} sm={4} md={4} lg={4}>
          <Text>
            <span className={classes.merchantInfo}>Name: </span>
            {props.row["name"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={4}>
          <Text>
            <span className={classes.merchantInfo}>Model: </span>
            {props.row["model"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={4}>
          <Text>
            <span className={classes.merchantInfo}>Type: </span>
            {props.row["type"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={4}>
          <Text>
            <span className={classes.merchantInfo}>App name: </span>
            {props.row["app-name"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={4}>
          <Text>
            <span className={classes.merchantInfo}>App version: </span>
            {props.row["app-version"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={4}>
          <Text>
            <span className={classes.merchantInfo}>Network type: </span>
            {props.row["internet-access-mode"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={4}>
          <Text>
            <span className={classes.merchantInfo}>Last seen: </span>
            {props.row["last-seen"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={4}>
          <Text>
            <span className={classes.merchantInfo}>Last updated: </span>
            {props.row["last-update"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={4}>
          <Text>
            <span className={classes.merchantInfo}>Last prep: </span>
            {props.row["last-prepped"]}
          </Text>
        </Grid>
      </Grid>
    </div>
  );
}
