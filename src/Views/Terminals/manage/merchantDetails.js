import { Grid } from "@material-ui/core";
import PersonIcon from "@material-ui/icons/Person";
import React from "react";
import { Colors } from "../../../assets/themes/theme";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Text } from "../../../components/Forms/index";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "../styles";

export default function MerchanytDetail(props) {
  const classes = styles();
  return (
    <div className={classes.manageTerminalCards}>
      <Grid container spacing={1}>
        <Grid item xs={12} sm={11} md={11} lg={11}>
          <div className={classes.logo}>
            <img
              src={jaiz}
              alt="jaiz logo"
              style={{ width: 20, height: 20, marginRight: "1rem" }}
            />
            Merchant
          </div>
        </Grid>
        <Grid
          item
          xs={12}
          sm={1}
          md={1}
          lg={1}
          style={{ display: "flex", color: Colors.secondary }}
        >
          <ButtonsRow>
            <PersonIcon
              width="35px"
              height="25px"
              style={{
                fontSize: 30,
                float: "right",
                marginBottom: -10,
              }}
            />
          </ButtonsRow>
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={4} style={{ display: "flex" }}>
          <Text>
            <span className={classes.merchantInfo}>ID: </span>
            {props.row["merchant-id"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={4} style={{ display: "flex" }}>
          <Text>
            <span className={classes.merchantInfo}>Name: </span>
            {props.row["merchant"] && props.row["merchant"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={4} />
        <Grid item xs={12} sm={4} md={4} lg={4} />
        <Grid item xs={12} sm={12} md={12} lg={12} style={{ display: "flex" }}>
          <Text>
            <span className={classes.merchantInfo}>Contact person: </span>
            {props.row["contact-name"] && props.row["contact-name"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={4} style={{ display: "flex" }}>
          <Text>
            <span className={classes.merchantInfo}>Phone: </span>
            {props.row["contact-phone"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={4} style={{ display: "flex" }}>
          <Text>
            <span className={classes.merchantInfo}>Email: </span>
            {props.row["contact-email"] &&
              props.row["contact-email"].toLowerCase()}
          </Text>
        </Grid>
      </Grid>
    </div>
  );
}
