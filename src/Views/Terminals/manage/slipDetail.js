import { Grid } from "@material-ui/core";
import ReceiptIcon from "@material-ui/icons/Receipt";
import React from "react";
import { Colors } from "../../../assets/themes/theme";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Text } from "../../../components/Forms/index";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "../styles";
import EditSlip from "./editSlip";

export default function SlipDetail(props) {
  const classes = styles();
  return (
    <div className={classes.manageTerminalCards}>
      <Grid container spacing={1}>
        <Grid item xs={12} sm={10} md={10} lg={10}>
          <div className={classes.logo}>
            <img
              src={jaiz}
              alt="jaiz logo"
              style={{ width: 20, height: 20, marginRight: "1rem" }}
            />
            Slip
          </div>
        </Grid>
        <Grid item xs={4} sm={2} md={2} lg={2}>
          <ButtonsRow style={{ marginTop: -5 }}>
            <EditSlip row={props.row} />
            <ReceiptIcon
              style={{
                marginBottom: -10,
                color: Colors.secondary,
                fontSize: 30,
              }}
            />
          </ButtonsRow>
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={6} style={{ display: "flex" }}>
          <Text>
            <span className={classes.merchantInfo}>Header: </span>
            {props.row["slip-header"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={6} style={{ display: "flex" }}>
          <Text>
            <span className={classes.merchantInfo}>Footer: </span>
            {props.row["slip-footer"]}
          </Text>
        </Grid>
      </Grid>
    </div>
  );
}
