import { Grid } from "@material-ui/core";
import MapIcon from "@material-ui/icons/Map";
import React from "react";
import { Colors } from "../../../assets/themes/theme";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Text } from "../../../components/Forms/index";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "../styles";
import EditLocation from "./editLocation";
import { Title } from "../../../components/contentHolders/Card";

export default function LocationDetail(props) {
  const classes = styles();
  return (
    <div className={classes.manageTerminalCards}>
      <Grid container spacing={1}>
        <Grid item xs={12} sm={10} md={10} lg={10}>
          <div className={classes.logo}>
            <img
              src={jaiz}
              alt="jaiz logo"
              style={{ width: 20, height: 20, marginRight: "1rem" }}
            />
            Location and Slip
          </div>
        </Grid>
        <Grid item xs={12} sm={2} md={2} lg={2} style={{ display: "flex" }}>
          <ButtonsRow style={{ marginTop: -5 }}>
            <EditLocation row={props.row} />
            <MapIcon
              style={{
                cursor: "pointer",
                marginBottom: -10,
                color: Colors.secondary,
                fontSize: 30,
              }}
            />
          </ButtonsRow>
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <Title>Location detail</Title>
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={12} style={{ display: "flex" }}>
          <Text>
            <span className={classes.merchantInfo}>Address: </span>
            {props.row.address}
          </Text>
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={6} style={{ display: "flex" }}>
          <Text>
            <span className={classes.merchantInfo}>Branch: </span>
            {props.row.branch}
          </Text>
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={6} style={{ display: "flex" }}>
          <Text>
            <span className={classes.merchantInfo}>Region: </span>
            {props.row.region}
          </Text>
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={6} style={{ display: "flex" }}>
          <Text>
            <span className={classes.merchantInfo}>State: </span>
            {props.row["state-name"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={6} style={{ display: "flex" }}>
          <Text>
            <span className={classes.merchantInfo}>LGA: </span>
            {props.row["lg-name"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <Title>Slip detail</Title>
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={6} style={{ display: "flex" }}>
          <Text>
            <span className={classes.merchantInfo}>Header: </span>
            {props.row["slip-header"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={6} style={{ display: "flex" }}>
          <Text>
            <span className={classes.merchantInfo}>Footer: </span>
            {props.row["slip-footer"]}
          </Text>
        </Grid>
      </Grid>
    </div>
  );
}
