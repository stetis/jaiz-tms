import { Grid } from "@material-ui/core";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
import React from "react";
import { Colors } from "../../../assets/themes/theme";
import { Text } from "../../../components/Forms/index";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "../styles";

export default function BankDetail(props) {
  const classes = styles();
  return (
    <div className={classes.manageTerminalCards}>
      <Grid container spacing={1}>
        <Grid item xs={12} sm={11} md={11} lg={11}>
          <div className={classes.logo}>
            <img
              src={jaiz}
              alt="jaiz logo"
              style={{ width: 20, height: 20, marginRight: "1rem" }}
            />
            Bank
          </div>
        </Grid>
        <Grid item xs={12} sm={1} md={1} lg={1}>
          <AccountBalanceIcon
            style={{
              float: "right",
              cursor: "pointer",
              marginBottom: -10,
              color: Colors.secondary,
              fontSize: 30,
            }}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={6} style={{ display: "flex" }}>
          <Text>
            <span className={classes.merchantInfo}>Bank code: </span>
            {props.row["account-type-id"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={6} style={{ display: "flex" }}>
          <Text>
            <span className={classes.merchantInfo}>Account number: </span>
            {props.row["account-number"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={6} style={{ display: "flex" }}>
          <Text>
            <span className={classes.merchantInfo}>Account type: </span>
            {props.row["account-type"]}
          </Text>
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={6} style={{ display: "flex" }}>
          <Text>
            <span className={classes.merchantInfo}>Account name: </span>
            {props.row["account-name"]}
          </Text>
        </Grid>
      </Grid>
    </div>
  );
}
