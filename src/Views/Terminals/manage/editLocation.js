import { Grid, List, ListItemIcon } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../actions/Configurations/branch.actions";
import { EditIcon } from "../../../assets/icons/Svg";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Select, TextArea } from "../../../components/TextField/index";
import { getBranchesHelper } from "../../../helpers/Configurations/branches.helper";
import { editTerminalLocationHelper } from "../../../helpers/terminals.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { states } from "../../../utils/data";
import { LightTooltip, styles } from "../styles";
export default function EditLocation(props) {
  const classes = styles();
  const [state, setState] = useState({
    open: false,
    address: props.row.address,
    branch: props.row.branch,
    lgaLists: [{ name: props.row["lg-name"], code: props.row["lg-code"] }],
    state: props.row["state-name"],
    lga: props.row["lg-name"],
    slipheader: props.row["slip-header"],
    slipfooter: props.row["slip-footer"],

    formError: false,
  });

  const dispatch = useDispatch();
  const loading = useSelector((state) => state.terminalsReducer.actionLoading);
  const status = useSelector((state) => state.terminalsReducer.status);
  const statusMessage = useSelector(
    (state) => state.terminalsReducer.statusMessage
  );
  const branches = useSelector((state) => state.branchesReducer.data.branches);

  useEffect(() => {
    dispatch(getBranchesHelper());
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        FormError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  const handleStateChange = ({ target: { value } }) => {
    const choosenState = states.find((state) => state.name === value);
    if (choosenState) {
      setState((prev) => ({
        ...prev,
        lgaLists: choosenState.lgs,
        state: value,
      }));
    }
  };
  const handleClickOpen = () => {
    setState((prev) => ({
      ...prev,
      open: true,
    }));
  };
  const handleClose = (e) => {
    e.preventDefault();
    setState((prev) => ({
      ...prev,
      open: false,
    }));
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const stateDetail = states.find((st) => st.name === state.state);
    const branchDetail = branches.find(
      (branch) => branch.name === state.branch
    );
    const lgDetail = state.lgaLists.find((lg) => lg.name === state.lga);
    if (
      state.address === "" ||
      state.name === "" ||
      !state.state ||
      !state.lga ||
      state.slipheader === "" ||
      state.slipfooter === ""
    ) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      let inputData = {
        id: props.row.id,
        branch: state.branch,
        "branch-id": branchDetail.id,
        address: state.address,
        "state-code": stateDetail["code"],
        "state-name": state.state,
        region: branchDetail["region"],
        "region-id": branchDetail["region-id"],
        "lg-name": state.lga,
        "lg-code": lgDetail.code,
        "slip-header": state.slipheader,
        "slip-footer": state.slipfooter,
      };
      console.log(stateDetail, "i am the state detail");
      dispatch(editTerminalLocationHelper(inputData));
    }
  };
  const renderFieldError = (formErrorMessage) => {
    <Snackbars
      variant="error"
      handleClose={handleCloseSnack}
      message={formErrorMessage}
      isOpen={state.formError}
    />;
  };
  return (
    <div>
      <List classes={{ root: classes.button }} onClick={handleClickOpen}>
        <LightTooltip title="Edit location" arrow={true} placement="bottom">
          <ListItemIcon classes={{ root: classes.editListItemIcon }}>
            <EditIcon
              className={classes.editIcon}
              style={{ fill: Colors.primary }}
            />
          </ListItemIcon>
        </LightTooltip>
      </List>
      {state.formError === true
        ? renderFieldError(
            state.address === ""
              ? "Branch address is required"
              : state.name === ""
              ? "Branch name is required"
              : !state.state
              ? "State is required"
              : !state.lga
              ? "LGA is required"
              : state.slipheader === ""
              ? "Slip header is required"
              : state.slipfooter === ""
              ? "Slip footer is required"
              : ""
          )
        : null}
      <Dialog
        open={state.open}
        keepMounted
        aria-labelledby="alert-dialog-slide-title"
        className="add-branch-dialog"
        aria-describedby="alert-dialog-slide-description"
      >
        <div className={classes.actionsRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12}>
              <form
                className={classes.container}
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Edit location
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Select
                    id="branch"
                    htmlFor="branch"
                    label="Branch"
                    name="branch"
                    value={state.branch}
                    onChange={handleChange}
                  >
                    {branches &&
                      branches.map((branch) => {
                        return (
                          <option value={branch.name} key={branch.name}>
                            {branch.name}
                          </option>
                        );
                      })}
                  </Select>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <Select
                    id="state"
                    htmlFor="state"
                    name="state"
                    label="State"
                    value={state.state}
                    onChange={handleStateChange}
                  >
                    {states.map((state) => {
                      return (
                        <option value={state.name} key={state.code}>
                          {state.name}
                        </option>
                      );
                    })}
                  </Select>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <Select
                    id="lga"
                    htmlFor="lga"
                    name="lga"
                    label="LGA"
                    value={state.lga}
                    onChange={handleChange}
                    grouped
                  >
                    {state.lgaLists &&
                      state.lgaLists.map((lga) => {
                        return (
                          <option value={lga.name} key={lga.code}>
                            {lga.name}
                          </option>
                        );
                      })}
                  </Select>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextArea
                    id="address"
                    htmlFor="address"
                    name="address"
                    label="Address"
                    value={state.address}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextArea
                    id="slipheader"
                    htmlFor="slipheader"
                    name="slipheader"
                    label="Slip header"
                    value={state.slipheader}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextArea
                    id="slipfooter"
                    htmlFor="slipfooter"
                    name="slipfooter"
                    label="Slip footer"
                    value={state.slipfooter}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      style={{
                        marginRight: 10,
                      }}
                      onClick={handleClose}
                      disabled={loading}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        marginTop: 0,
                      }}
                    >
                      {loading ? (
                        <Loader
                          type="ThreeDots"
                          color={Colors.secondary}
                          height="15"
                          width="30"
                        />
                      ) : (
                        "Save"
                      )}
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </div>
      </Dialog>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
