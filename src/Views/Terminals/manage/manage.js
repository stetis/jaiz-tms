import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import { getSingleTerminalHelper } from "../../../helpers/terminals.helper";
import { styles } from "../styles";
import DeviceDetail from "./deviceDetails";
import MerchantDetail from "./merchantDetails";
import LocationDetail from "./locationDetail";
import Spinner from "../../../assets/Spinner";

export default function ManageTerminal(props) {
  const classes = styles();
  const dispatch = useDispatch();
  const loading = useSelector(
    (state) => state.terminalsReducer.singleTerminalLoading
  );
  const singleTerminal = useSelector(
    (state) => state.terminalsReducer.singleTerminalData
  );
  useEffect(() => {
    document.title = `Manage ${props.location.state.row.name}  | Terminals`;
    dispatch(getSingleTerminalHelper(props.location.state.row.id));
    dispatch(
      updateBreadcrumb({
        parent: "Merchant terminals",
        child: "Manage terminal",
        homeLink: "/tms",
        childLink: "/tms/merchant-terminals",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  return (
    <div>
      {loading ? (
        <Spinner />
      ) : singleTerminal ? (
        <div className={classes.manageTerminal}>
          <DeviceDetail row={singleTerminal} />
          <MerchantDetail row={singleTerminal} />
          <LocationDetail row={singleTerminal} />
        </div>
      ) : null}
    </div>
  );
}
