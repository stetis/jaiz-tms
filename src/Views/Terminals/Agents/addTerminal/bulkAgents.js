import { Grid, MuiThemeProvider } from "@material-ui/core";
import CancelOutlinedIcon from "@material-ui/icons/CancelOutlined";
import CheckCircleOutlinedIcon from "@material-ui/icons/CheckCircleOutlined";
import MUIDataTable from "mui-datatables";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import XLSX from "xlsx";
import { updateBreadcrumb } from "../../../../actions/breadcrumbAction";
import { dynamicTable } from "../../../../assets/MUI/Table";
import { Colors } from "../../../../assets/themes/theme";
import {
  DeleteButton,
  SubmitButton,
} from "../../../../components/Buttons/index";
import { ButtonsRow } from "../../../../components/Buttons/styles";
import { Text } from "../../../../components/Forms";
import jaiz from "../../../../images/jaiz-logo.png";
import { styles } from "../../styles";

export default function BulkUpload(props) {
  const classes = styles();
  const fileUploader = React.useRef();
  const [state, setState] = useState({
    data: [],
    cols: [],
    formError: false,
  });
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.terminalsReducer.actionLoading);
  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  useEffect(() => {
    document.title = "Agent terminals bulk upload| Terminals";
    dispatch(
      updateBreadcrumb({
        parent: "Agent terminals",
        child: "Agent terminals bulk upload",
        homeLink: "/tms",
        childLink: "/tms/agent-terminals",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  function capitalizeFirstLetters(str) {
    var splitStr = str.toLowerCase().split(" ");
    for (var i = 0; i < splitStr.length; i++) {
      splitStr[i] =
        splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(" ");
  }

  const handleFile = (file) => {
    /* Boilerplate to set up FileReader */
    const reader = new FileReader();
    const rABS = !!reader.readAsBinaryString;
    reader.onload = (e) => {
      /* Parse data */
      const bstr = e.target.result;
      const wb = XLSX.read(bstr, { type: rABS ? "binary" : "array" });
      /* Get first worksheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      /* Convert array of arrays */
      const data = XLSX.utils.sheet_to_json(ws, {
        header: 1,
      });
      /* Update state */
      setState({
        data: data,
        cols: make_cols(ws["!ref"]),
      });
    };
    if (rABS) reader.readAsBinaryString(file);
    else reader.readAsArrayBuffer(file);
  };
  const handleChange = (e) => {
    const files = e.target.files;
    if (files && files[0]) handleFile(files[0]);
  };
  const handleFileUpload = (e) => {
    e.preventDefault();
    fileUploader.current.click();
  };

  const handleClose = (e) => {
    e.preventDefault();
    props.history.push("/tms/agent-terminals");
  };
  // const inputData = state.data && state.data.map((el) => el[4]);
  // inputData.shift();
  // console.log(inputData, "i am the input data");

  // const handleCloseSnack = () => dispatch(clearMessages());
  // const handleSubmit = (e) => {
  //   e.preventDefault();
  //   let inputData = state.data.map((el) => {
  //     return {
  //       "merchant-id": el[0],
  //       merchant: el[1],
  //       "contact-title": el[2],
  //       "contact-name": el[3],
  //       "contact-phone": el[4],
  //       "contact-email": el[5],
  //       "email-alert": el[6],
  //       address: el[7],
  //       model: el[8],
  //       "terminal-id": el[9],
  //       "bank-code": el[10],
  //       "account-number": el[11],
  //       "account-type-code": el[12],
  //       "slip-header": el[13],
  //       "slip-footer": el[14],
  //       "business-occupation-code": el[15],
  //       "category-code": el[16],
  //       "state-code": el[17],
  //       "visa-number": el[18],
  //       "verve-number": el[19],
  //       "mastercard-number": el[20],
  //       owner: el[21],
  //       "lg-name": el[22],
  //       "post-code": el[23],
  //       url: el[24],
  //       "account-name": el[25],
  //       ptsp: el[26],
  //       "device-type": el[27],
  //       "device-name": el[28],
  //       "serial-number": el[29],
  //       "app-name": el[30],
  //       "app-version": el[31],
  //       "network-type": el[32],
  //     };
  //   });
  //   inputData.shift();
  //   dispatch(uploadBulkMerchantTerminalsHelper(inputData));
  // };
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: false,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    rowsPerPageOptions: [],
    customToolbar: () => (
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <ButtonsRow
          style={{
            margin: "25px 0",
          }}
        >
          <DeleteButton
            style={{
              marginRight: 10,
            }}
            onClick={handleClose}
            disabled={loading}
          >
            Go back to agent terminal table
          </DeleteButton>
          <input
            type="file"
            className="form-control"
            id="file"
            accept={SheetJSFT}
            onChange={handleChange}
            ref={fileUploader}
            style={{
              display: "none",
            }}
          />
          <SubmitButton
            type="submit"
            disabled={loading}
            style={{
              marginTop: 0,
            }}
            onClick={handleFileUpload}
            ghost
          >
            {loading ? (
              <Loader
                type="ThreeDots"
                color={Colors.secondary}
                height="15"
                width="30"
              />
            ) : (
              "Re upload agent terminals"
            )}
          </SubmitButton>
        </ButtonsRow>
      </Grid>
    ),
  };
  const newColumns = props.location.state.data.data.header.map((dat, i) => {
    return dat === "Terminal status"
      ? {
          name: dat,
          options: {
            customBodyRender: (dataIndex, rowIndex) => {
              return dataIndex === "faulty" ? (
                <CancelOutlinedIcon
                  style={{
                    color: Colors.buttonError,
                    fontSize: 20,
                  }}
                />
              ) : (
                <CheckCircleOutlinedIcon
                  style={{
                    color: Colors.primary,
                    fontSize: 20,
                  }}
                />
              );
            },
          },
        }
      : dat === "Agent"
      ? {
          name: dat,
          options: {
            customBodyRender: (dataIndex, rowIndex) => {
              return (
                <Text
                  style={{
                    width: 170,
                  }}
                >
                  {capitalizeFirstLetters(dataIndex)}
                </Text>
              );
            },
          },
        }
      : dat === "Agent remark"
      ? {
          name: dat,
          options: {
            customBodyRender: (dataIndex, rowIndex) => {
              return (
                <Text
                  style={{
                    color:
                      dataIndex === "Not uploaded"
                        ? "#FFCC00"
                        : dataIndex === "Agent not found"
                        ? Colors.primary
                        : Colors.textColor,
                    width: 130,
                  }}
                >
                  {dataIndex}
                </Text>
              );
            },
          },
        }
      : dat === "Agent status"
      ? {
          name: dat,
          options: {
            customBodyRender: (dataIndex, rowIndex) => {
              return dataIndex === "faulty" ? (
                <CancelOutlinedIcon
                  style={{
                    color: Colors.buttonError,
                    fontSize: 20,
                  }}
                />
              ) : (
                <CheckCircleOutlinedIcon
                  style={{
                    color: Colors.primary,
                    fontSize: 20,
                  }}
                />
              );
            },
          },
        }
      : dat;
  });
  // console.log(newData, "DayPicker-Body");
  return (
    <div className={classes.bulkCard}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <MuiThemeProvider theme={dynamicTable}>
            <MUIDataTable
              data={props.location.state.data.data.body}
              columns={newColumns}
              options={options}
              title={
                <div className={classes.bulkLogo}>
                  <img
                    src={jaiz}
                    alt="jaiz logo"
                    style={{ width: 20, height: 20, marginRight: "1rem" }}
                  />
                  Agent terminals upload outcome
                </div>
              }
            />
          </MuiThemeProvider>
        </Grid>
      </Grid>
    </div>
  );
}

/* list of supported file types */
const SheetJSFT = ["xlsx", "xls", "csv"]
  .map(function (x) {
    return "." + x;
  })
  .join(",");

/* generate an array of column objects */
const make_cols = (refstr) => {
  let o = [],
    C = XLSX.utils.decode_range(refstr).e.c + 1;
  for (var i = 0; i < C; ++i)
    o[i] = {
      name: XLSX.utils.encode_col(i),
      key: i,
    };
  return o;
};
