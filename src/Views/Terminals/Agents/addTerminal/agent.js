import { Grid } from "@material-ui/core";
import ArrowDropDownOutlinedIcon from "@material-ui/icons/ArrowDropDownOutlined";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../../actions/breadcrumbAction";
import { clearMessages } from "../../../../actions/terminals.actions";
import Snackbars from "../../../../assets/Snackbars/index";
import { Colors } from "../../../../assets/themes/theme";
import {
  DeleteButton,
  SubmitButton,
} from "../../../../components/Buttons/index";
import { ButtonsRow } from "../../../../components/Buttons/styles";
import { Title } from "../../../../components/contentHolders/Card";
import { Text } from "../../../../components/Forms/index";
import { Select, TextField } from "../../../../components/TextField";
import { AutoComplete } from "../../../../components/TextField/autotcomplete/autocomplete";
import { getAllAgentsHelper } from "../../../../helpers/agents.helper";
import { getPTSPHelper } from "../../../../helpers/ptsp.helper";
import { addAgentTerminalHelper } from "../../../../helpers/terminals.helper";
import jaiz from "../../../../images/jaiz-logo.png";
import { styles } from "../../styles";

export default function AddSingleTerminal(props) {
  const classes = styles();
  const [agent, setAgent] = useState({
    text: "",
    suggestions: [],
  });
  const [isComponentVisible, setIsComponentVisible] = useState(true);
  const [state, setState] = useState({
    agentID: "",
    serialNo: "",
    TerminalID: "",
    ptsp: "",
    formError: false,
  });
  const dispatch = useDispatch();
  const agents = useSelector(
    (state) => state.agentsReducer.allAgentsData.agents
  );
  const ptsps = useSelector((state) => state.ptspReducer.data.ptsps);
  const loading = useSelector(
    (state) => state.posInventoryReducer.actionLoading
  );
  const status = useSelector((state) => state.terminalsReducer.status);
  const statusMessage = useSelector(
    (state) => state.terminalsReducer.statusMessage
  );
  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  useEffect(() => {
    document.title = "Add agent terminal | Terminals";
    dispatch(getPTSPHelper());
    dispatch(getAllAgentsHelper());
    dispatch(
      updateBreadcrumb({
        parent: "Agents",
        child: "Add agent terminal",
        homeLink: "/tms",
        childLink: "/tms/terminals",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const onTextChanged = (e) => {
    const value = e.target.value;
    // console.log(Array.isArray(agents), agents, "i am the merchant here");

    let suggestions = [];
    if (value.length > 0) {
      const regex = new RegExp(`^${value}`, "i");
      suggestions =
        agents && agents.length !== 0
          ? agents.sort().filter((agent) => regex.test(agent.name))
          : [];
    }
    setIsComponentVisible(true);
    setAgent({ suggestions, text: value });
  };
  const suggestionSelected = (value) => {
    setIsComponentVisible(false);
    setAgent({
      text: value.name,
      suggestions: [],
    });
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  const handleClose = (e) => {
    e.preventDefault();
    props.history.push("/tms/terminals");
  };
  const handleCloseSnack = () => dispatch(clearMessages());

  const { suggestions } = agent;
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const ptspDetail = ptsps && ptsps.find((pt) => pt.id === state.ptsp);
    const agentDetail = agents && agents.find((ag) => ag.name === agent.text);
    if (agent.text === "" || state.serialNo === "" || state.terminalID === "") {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      const inputData = {
        "serial-number": state.serialNo,
        "terminal-id": state.terminalID,
        "agent-id": agentDetail.id,
        "ptsp-id": ptspDetail.id,
      };
      dispatch(addAgentTerminalHelper(inputData));
    }
  };
  const agentDetail = agents && agents.find((ag) => ag.name === agent.text);
  return (
    <div>
      {state.formError === true
        ? renderFieldError(
            !agent.text === ""
              ? "Agent name is required"
              : state.serialNo === ""
              ? "Serial number is required"
              : state.terminalID === ""
              ? "Terminal ID is required"
              : ""
          )
        : null}
      <div className={classes.addCard}>
        <Grid container spacing={0}>
          <div>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <form
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
                className={classes.container}
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Add Agent terminal
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <AutoComplete
                    id="agent"
                    htmlFor="agent"
                    label="Agent"
                    onChange={onTextChanged}
                    value={agent.text}
                    setIsComponentVisible={setIsComponentVisible}
                    suggestions={suggestions}
                    isComponentVisible={isComponentVisible}
                    suggestionSelected={suggestionSelected}
                    Icon={
                      <ArrowDropDownOutlinedIcon style={{ fontSize: 34 }} />
                    }
                  />
                </Grid>
                {agentDetail && agentDetail !== null ? (
                  <>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={6}
                      lg={6}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>Agent: </span>
                        {agentDetail.id}
                      </Text>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={6}
                      lg={6}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>
                          Agent/Merchant ID:{" "}
                        </span>
                        {agentDetail["merchant-id"]}
                      </Text>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={6}
                      lg={6}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>Phone: </span>
                        {agentDetail.phone}
                      </Text>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={6}
                      lg={6}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>Email: </span>
                        {agentDetail.email}
                      </Text>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={12}
                      md={12}
                      lg={12}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>Address: </span>
                        {agentDetail.address}
                      </Text>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={6}
                      lg={6}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>State: </span>
                        {agentDetail["state-name"]}
                      </Text>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={6}
                      lg={6}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>LGA: </span>
                        {agentDetail["lg-name"]}
                      </Text>
                    </Grid>
                  </>
                ) : null}
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Title style={{ marginTop: 5 }}>Terminal</Title>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <TextField
                    id="serialNo"
                    htmlFor="serialNo"
                    label="Serial Number"
                    name="serialNo"
                    value={state.serialNo}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <TextField
                    id="terminalID"
                    htmlFor="terminalID"
                    label="Terminal ID"
                    name="terminalID"
                    value={state.terminalID}
                    onChange={handleChange}
                    grouped
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <Select
                    id="ptsp"
                    htmlFor="ptsp"
                    label="PTSP"
                    name="ptsp"
                    value={state.ptsp}
                    onChange={handleChange}
                  >
                    {ptsps &&
                      ptsps.map((ptsp) => {
                        return (
                          <option value={ptsp.id} key={ptsp.id}>
                            {ptsp.name}
                          </option>
                        );
                      })}
                  </Select>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      onClick={handleClose}
                      style={{ margin: "3px 10px 0 0" }}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        backgroundColor: loading ? Colors.primary : "",
                        marginTop: 0,
                        width: 65,
                      }}
                      disableRipple={loading}
                    >
                      <span className="submit-btn">
                        {loading ? (
                          <Loader
                            type="ThreeDots"
                            color="#B28B0A"
                            height="15"
                            width="30"
                          />
                        ) : (
                          "save"
                        )}
                      </span>
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </div>
        </Grid>
      </div>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
