import { Grid, MuiThemeProvider } from "@material-ui/core";
import FilterListOutlinedIcon from "@material-ui/icons/FilterListOutlined";
import TablePagination from "@material-ui/core/TablePagination";
import NavigateNextOutlinedIcon from "@material-ui/icons/NavigateNextOutlined";
import MUIDataTable from "mui-datatables";
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import { clearMessages } from "../../../actions/terminals.actions";
import { history } from "../../../App";
import { theme } from "../../../assets/MUI/Table";
import Snackbars from "../../../assets/Snackbars/index";
import Spinner from "../../../assets/Spinner";
import { Colors } from "../../../assets/themes/theme";
import { Text } from "../../../components/Forms";
import { Select, TextField } from "../../../components/TextField";
import { getAgentTerminalsHelper } from "../../../helpers/terminals.helper";
import jaiz from "../../../images/jaiz-logo.png";
import AddTerminal from "./addTerminal/add";
import { styles } from "../styles";
import { SubmitButton } from "../../../components/Buttons";
import { ButtonsRow } from "../../../components/Buttons/styles";
import Search from "../../../components/TextField/search";

const styleProps = {
  height: 50,
  width: 50,
};
function capitalizeFirstLetters(str) {
  var splitStr = str.toLowerCase().split(" ");
  for (var i = 0; i < splitStr.length; i++) {
    // You do not need to check if i is larger than splitStr length, as your for does that for you
    // Assign it back to the array
    splitStr[i] =
      splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  // Directly return the joined string
  return splitStr.join(" ");
}

export default function AgentsTerminals() {
  const classes = styles();
  let today = new Date();
  let fromDate =
    today.getFullYear() +
    "-" +
    ("0" + (today.getMonth() + 1)).slice(-2) +
    "-" +
    ("0" + today.getDate()).slice(-2);
  const [showButton, setShowButton] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [search, setSearch] = useState("");
  const [from, setFrom] = useState(fromDate);
  const [type, setType] = useState("text");
  const onFocus = () => setType("date");
  const onBlur = () => setType("text");
  const dispatch = useDispatch();
  const [status, setStatus] = useState("all");
  const loading = useSelector((state) => state.terminalsReducer.loading);
  const paginatedTerminals = useSelector(
    (state) => state.terminalsReducer.agentTerminalData
  );
  const pages = useSelector(
    (state) => state.terminalsReducer.agentTerminalData.paging
  );

  const error = useSelector(
    (state) => state.terminalsReducer.agentTerminalError
  );
  const errorMessage = useSelector(
    (state) => state.terminalsReducer.agentTerminalErrorMessage
  );
  useEffect(() => {
    document.title = "Agent terminals | Jaiz bank TMS";
    const credentials = {
      page,
      pagesize: rowsPerPage,
      search,
      status,
      lastTx: fromDate,
    };
    dispatch(getAgentTerminalsHelper(credentials));
    dispatch(
      updateBreadcrumb({
        parent: "Agent terminals",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    paginatedTerminals.header && paginatedTerminals.header.shift("");
    paginatedTerminals.header && paginatedTerminals.header.push("");
    paginatedTerminals.body &&
      paginatedTerminals.body.map(
        (dat, i) => (
          // eslint-disable-next-line
          dat.unshift(rowsPerPage * page + i + 1),
          dat.push(
            <NavigateNextOutlinedIcon
              style={{ fontSize: 20 }}
              onClick={() => gotoManageTerminal(dat)}
            />
          )
        )
      );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const gotoManageTerminal = useCallback((row) => {
    return history.push({
      pathname: `/tms/manage-terminal`,
      state: {
        row: {
          id: row[0],
          name: row[4],
        },
      },
    });
  }, []);
  const rowClickGotoManageTerminal = (row) => {
    history.push({
      pathname: `/tms/manage-terminal`,
      state: {
        row: {
          id: row[0],
          name: row[4].props.children,
        },
      },
    });
  };
  const handleCloseSnack = (e) => dispatch(clearMessages());
  const handleFromChange = (e) => setFrom(e.target.value);
  const handleSearchChange = (e) => setSearch(e.target.value);
  const handleSearch = (e) => {
    e.preventDefault();
    let today = new Date(from);
    let fromDate =
      today.getFullYear() +
      "-" +
      ("0" + (today.getMonth() + 1)).slice(-2) +
      "-" +
      ("0" + today.getDate()).slice(-2);
    const credentials = {
      page,
      pagesize: rowsPerPage,
      search,
      status,
      lastTx: fromDate,
    };
    dispatch(getAgentTerminalsHelper(credentials));
    setSearch(search);
  };
  const handleKeyPress = (event) => {
    let today = new Date(from);
    let fromDate =
      today.getFullYear() +
      "-" +
      ("0" + (today.getMonth() + 1)).slice(-2) +
      "-" +
      ("0" + today.getDate()).slice(-2);
    const credentials = {
      page,
      pagesize: rowsPerPage,
      search,
      status,
      lastTx: fromDate,
    };
    if (event.keyCode === 13) {
      dispatch(getAgentTerminalsHelper(credentials));
      setSearch(search);
    }
  };
  const handleClickOpen = (e) => {
    e.preventDefault();
    setShowButton(!showButton);
  };
  const handleStatusChange = (e) => setStatus(e.target.value);
  const handleChangePage = (event, newPage) => {
    let today = new Date(from);
    let fromDate =
      today.getFullYear() +
      "-" +
      ("0" + (today.getMonth() + 1)).slice(-2) +
      "-" +
      ("0" + today.getDate()).slice(-2);
    const credentials = {
      page: newPage,
      pagesize: rowsPerPage,
      search,
      status,
      lastTx: fromDate,
    };
    dispatch(getAgentTerminalsHelper(credentials));
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    let today = new Date(from);
    let fromDate =
      today.getFullYear() +
      "-" +
      ("0" + (today.getMonth() + 1)).slice(-2) +
      "-" +
      ("0" + today.getDate()).slice(-2);
    const credentials = {
      page: page,
      pagesize: rowsPerPage,
      search,
      status,
      lastTx: fromDate,
    };
    dispatch(getAgentTerminalsHelper(credentials));
    setPage(0);
    setRowsPerPage(parseInt(event.target.value, 10));
  };
  const newColumns =
    paginatedTerminals.header &&
    paginatedTerminals.header.map((dat, i) => {
      return dat === "ID"
        ? {
            name: dat,
            options: {
              display: false,
            },
          }
        : dat === "Agent"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 190,
                    }}
                  >
                    {capitalizeFirstLetters(dataIndex)}
                  </Text>
                );
              },
            },
          }
        : dat === "Terminal ID"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 80,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : dat === "Last updated"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 150,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : dat === "Last seen"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 150,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : dat === "Last Transaction date"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 120,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : dat === "Last transaction days"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 120,
                      color: Colors.secondary,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : dat === "Status"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 70,
                      color:
                        dataIndex === true
                          ? Colors.primary
                          : Colors.buttonError,
                    }}
                  >
                    {dataIndex === true ? "Enabled" : "Disabled"}
                  </Text>
                );
              },
            },
          }
        : dat;
    });
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: true,
    sort: false,
    rowHover: false,
    onRowClick: (row) => rowClickGotoManageTerminal(row),
    selectableRows: "none",
    customToolbar: () => (
      <ButtonsRow className={classes.buttonRow}>
        <SubmitButton
          onClick={handleClickOpen}
          className={classes.fabIcon}
          style={{ marginRight: 15 }}
        >
          <FilterListOutlinedIcon className={classes.icon} />
          Filters
        </SubmitButton>
        <AddTerminal />
      </ButtonsRow>
    ),
    textLabels: {
      body: {
        noMatch: "No agent terminals records found",
      },
    },
  };
  return (
    <div className={classes.root}>
      {!loading ? (
        <Grid container spacing={0}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <MuiThemeProvider theme={theme}>
              <MUIDataTable
                data={paginatedTerminals ? paginatedTerminals.body : []}
                columns={newColumns}
                options={options}
                title={
                  showButton ? (
                    <Grid container spacing={0}>
                      <Grid item xs={12} sm={4} md={4} lg={4}>
                        <Search
                          id="search"
                          htmlFor="search"
                          name="search"
                          value={search}
                          placeholder="search merchant ID, agent"
                          searchlabel="Search"
                          onChange={handleSearchChange}
                          onKeyUp={handleKeyPress}
                          onClickIcon={handleSearch}
                        />
                      </Grid>
                      <Grid item xs={12} sm={4} md={4} lg={4}>
                        <Select
                          id="status"
                          htmlFor="status"
                          name="status"
                          placeholder="status"
                          label="Status"
                          value={status}
                          onChange={handleStatusChange}
                          onKeyUp={handleKeyPress}
                          grouped
                        >
                          <option value="all">All</option>
                          <option value="enabled">Enabled</option>
                          <option value="disabled">Disabled</option>
                        </Select>
                      </Grid>
                      <Grid item xs={12} sm={4} md={4} lg={4}>
                        <TextField
                          id="from"
                          htmlFor="from"
                          name="from"
                          type={type}
                          placeholder="Transaction date"
                          label="Transaction date"
                          onFocus={onFocus}
                          onBlur={onBlur}
                          value={from}
                          onChange={handleFromChange}
                          onKeyUp={handleKeyPress}
                          grouped
                        />
                      </Grid>
                    </Grid>
                  ) : (
                    <div className={classes.tableLogo}>
                      <img
                        src={jaiz}
                        alt="jaiz logo"
                        style={{ width: 20, height: 20, marginRight: "1rem" }}
                      />
                      Agents terminals
                    </div>
                  )
                }
              />
              <TablePagination
                component="div"
                count={
                  pages && pages["total-record"] ? pages["total-record"] : 0
                }
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={rowsPerPage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                rowsPerPageOptions={[5, 10, 15, 20]}
              />
            </MuiThemeProvider>
          </Grid>
        </Grid>
      ) : (
        <Spinner {...styleProps} />
      )}
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
    </div>
  );
}
