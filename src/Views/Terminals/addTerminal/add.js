import { List, ListItemIcon, MenuItem } from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import Popover from "@material-ui/core/Popover";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";
import AddSharpIcon from "@material-ui/icons/AddSharp";
import React, { useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { withRouter } from "react-router";
import XLSX from "xlsx";
import { clearMessages } from "../../../actions/terminals.actions";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { SubmitButton } from "../../../components/Buttons/index";
import { Text } from "../../../components/Forms/index";
import { uploadBulkMerchantTerminalsHelper } from "../../../helpers/terminals.helper";
import { styles } from "../styles";

function AddTerminal(props) {
  const classes = styles();
  const [anchorEl, setAnchorEl] = useState(null);
  const fileUploader = React.useRef();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.terminalsReducer.actionLoading);
  const status = useSelector((state) => state.terminalsReducer.status);
  const statusMessage = useSelector(
    (state) => state.terminalsReducer.statusMessage
  );

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleAddMulti = () => {
    props.history.push("/tms/add-multiple-terminals");
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const handleFileUpload = () => {
    fileUploader.current.click();
    // props.history.push("/tms/add-bulkmerchant-terminals");
  };
  const handleFile = (file) => {
    /* Boilerplate to set up FileReader */
    const reader = new FileReader();
    const rABS = !!reader.readAsBinaryString;
    reader.onload = (e) => {
      /* Parse data */
      const bstr = e.target.result;
      const wb = XLSX.read(bstr, { type: rABS ? "binary" : "array" });
      /* Get first worksheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      /* Convert array of arrays */
      const data = XLSX.utils.sheet_to_json(ws, {
        header: 1,
      });
      /* Update state and call handleSubmit() */
      let inputData = data.map((el) => {
        return {
          "merchant-id": el[0],
          merchant: el[1],
          "contact-title": el[2],
          "contact-name": el[3],
          "contact-phone": el[4],
          "contact-email": el[5],
          "email-alert": el[6],
          address: el[7],
          model: el[8],
          "terminal-id": el[9],
          "bank-code": el[10],
          "account-number": el[11],
          "account-type-code": el[12],
          "slip-header": el[13],
          "slip-footer": el[14],
          "business-occupation-code": el[15],
          "category-code": el[16],
          "state-code": el[17],
          "visa-number": el[18],
          "verve-number": el[19],
          "mastercard-number": el[20],
          owner: el[21],
          "lg-name": el[22],
          "post-code": el[23],
          url: el[24],
          "account-name": el[25],
          ptsp: el[26],
          "device-type": el[27],
          "device-name": el[28],
          "serial-number": el[29],
          "app-name": el[30],
          "app-version": el[31],
          "network-type": el[32],
        };
      });
      inputData.shift();
      dispatch(uploadBulkMerchantTerminalsHelper(inputData));
    };

    if (rABS) reader.readAsBinaryString(file);
    else reader.readAsArrayBuffer(file);
  };
  const handleChange = (e) => {
    const files = e.target.files;
    if (files && files[0]) handleFile(files[0]);
  };
  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  return (
    <div>
      <SubmitButton
        onClick={handleClick}
        className={classes.fabIcon}
        style={{ marginTop: 0 }}
      >
        <AddOutlinedIcon className={classes.icon} />
        Add terminal
      </SubmitButton>
      <Popover
        id={id}
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
      >
        <MenuItem>
          <List classes={{ root: classes.button }} onClick={handleAddMulti}>
            <ListItemIcon classes={{ root: classes.listItemIcon }}>
              <AddSharpIcon className={classes.editIcon} />
            </ListItemIcon>
            <Text className={classes.editText}>Add merchant terminal(s)</Text>
          </List>
        </MenuItem>
        <Divider />
        <MenuItem>
          <input
            type="file"
            className="form-control"
            id="file"
            accept={SheetJSFT}
            onChange={handleChange}
            ref={fileUploader}
            style={{
              display: "none",
            }}
          />
          <List
            classes={{ root: classes.button }}
            onClick={handleFileUpload}
            style={{ cursor: loading ? "not-allowed" : "pointer" }}
          >
            <ListItemIcon classes={{ root: classes.listItemIcon }}>
              <AddSharpIcon className={classes.editIcon} />
            </ListItemIcon>
            <Text className={classes.editText}>
              {loading ? (
                <Loader
                  type="ThreeDots"
                  color={Colors.secondary}
                  height="15"
                  width="30"
                />
              ) : (
                "Bulk upload"
              )}
            </Text>
          </List>
        </MenuItem>
        <Divider />
      </Popover>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
/* list of supported file types */
const SheetJSFT = ["xlsx", "xls", "csv"]
  .map(function (x) {
    return "." + x;
  })
  .join(",");

export default withRouter(AddTerminal);
