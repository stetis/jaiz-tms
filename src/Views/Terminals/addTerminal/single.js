import { Grid } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Title } from "../../../components/contentHolders/Card";
import { Text } from "../../../components/Forms/index";
import { Select, TextField, CheckBox } from "../../../components/TextField";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "../styles";

function AddSingleTerminal(props) {
  const classes = styles();
  const [state, setState] = useState({
    merchant: "",
    deviceType: "",
    serialNo: "",
    TerminalID: "",
    app: "",
    ptsp: "",
    visa: "",
    masterCard: "",
    verve: "",
    emailAlert: false,
    supervisionPin: false,
    adminPin: false,
    formError: false,
  });

  useEffect(() => {
    document.title = "Add single terminal | Terminals";
  }, []);

  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  const handleCheckbox = () => {
    setState((prev) => ({
      ...prev,
      emailAlert: !state.emailAlert,
    }));
  };
  const handleSuperVisionbox = () => {
    setState((prevState) => ({
      ...prevState,
      supervisionPin: !state.supervisionPin,
    }));
  };
  const handleAdminPinbox = () => {
    setState((prevState) => ({
      ...prevState,
      adminPin: !state.adminPin,
    }));
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  const handleClose = (e) => {
    e.preventDefault();
    props.history.push("/tms/terminals");
  };
  const handleCloseSnack = () => {
    // this.props.refreshActions()
  };
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (
      !state.merchant ||
      !state.oulet ||
      !state.deviceType ||
      state.serialNo === "" ||
      state.terminalID === "" ||
      !state.app ||
      !state.ptsp ||
      state.visa === "" ||
      state.verve === "" ||
      state.master === ""
    ) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      const inputData = {
        merchant: state.merchant,
        "device-type": state.deviceType,
        "serial-number": state.serialNo,
        "terminal-id": state.terminalID,
        app: state.app,
        ptsp: state.ptsp,
        visa: state.visa,
        verve: state.verve,
        "enable-supervision-pin": state.supervisionPin,
        "enable-admin-pin": state.adminPin,
      };
      console.log(inputData);
      // props.userLogin(credentials);
    }
  };
  const { loading } = props;
  return (
    <div>
      {state.formError === true
        ? renderFieldError(
            !state.merchant
              ? "Merchant name is required"
              : state.deviceType === ""
              ? "Device type is required"
              : state.serialNo === ""
              ? "Serial number is required"
              : state.terminalID === ""
              ? "Terminal ID is required"
              : !state.app
              ? "App name is required"
              : !state.ptsp
              ? "PTSP name is required"
              : state.visa === ""
              ? "Visa number is required"
              : state.verve === ""
              ? "Verveis required"
              : state.masterCard === ""
              ? "Master card is required"
              : ""
          )
        : null}
      <div className={classes.addCard}>
        <Grid container spacing={0}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <div className={classes.logo}>
              <img
                src={jaiz}
                alt="jaiz logo"
                style={{ width: 20, height: 20, marginRight: "1rem" }}
              />
              Add single merchant terminal
            </div>
          </Grid>
          <div>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <form
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
                className={classes.container}
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Select
                    id="merchant"
                    htmlFor="merchant"
                    label="Merchant"
                    name="merchant"
                    value={state.merchant}
                    onChange={handleChange}
                  >
                    <option value="Aliyu Musa Aliyu">Aliyu Musa Aliyu</option>
                    <option value="El-Ameen Abdullahi">
                      El-Ameen Abdullahi
                    </option>
                  </Select>
                </Grid>
                {state.merchant ? (
                  <>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={6}
                      lg={6}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>
                          Merchant ID:{" "}
                        </span>
                        JZ00012
                      </Text>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={6}
                      lg={6}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>Contact: </span>
                        aisha.abdulkadir@stetis.com
                      </Text>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={6}
                      lg={6}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>Phone: </span>
                        +2348142445807
                      </Text>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={6}
                      lg={6}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>Email: </span>
                        aisha.abdulkadir@stetis.com
                      </Text>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={12}
                      md={12}
                      lg={12}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>Address: </span>
                        No. 53 mambolo street
                      </Text>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={6}
                      lg={6}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>State: </span>
                        FCT Abuja
                      </Text>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={6}
                      lg={6}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>LGA: </span>
                        Abuja municapal city (AMAC)
                      </Text>
                    </Grid>
                  </>
                ) : null}
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Title style={{ display: "flex", margin: "10px 0" }}>
                    Enable email alert{" "}
                    <span className={classes.checkbox}>
                      <CheckBox
                        id="emailAlert"
                        htmlFor="emailAlert"
                        type="checkbox"
                        name="emailAlert"
                        checked={state.emailAlert}
                        value={state.emailAlert}
                        onChange={handleCheckbox}
                      />
                    </span>
                  </Title>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Title style={{ marginTop: 5 }}>Terminal</Title>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <TextField
                    id="terminalID"
                    htmlFor="terminalID"
                    label="Terminal ID"
                    name="terminalID"
                    value={state.terminalID}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <TextField
                    id="serialNo"
                    htmlFor="serialNo"
                    label="Serial Number"
                    name="serialNo"
                    value={state.serialNo}
                    onChange={handleChange}
                    grouped
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <Select
                    id="deviceType"
                    htmlFor="deviceType"
                    label="Device type"
                    name="deviceType"
                    value={state.deviceType}
                    onChange={handleChange}
                  >
                    <option value="Andriod">Andriod</option>
                    <option value="IOS">IOS</option>
                  </Select>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <Select
                    id="app"
                    htmlFor="app"
                    label="App"
                    name="app"
                    value={state.app}
                    onChange={handleChange}
                    grouped
                  >
                    <option value="Andriod">Andriod</option>
                    <option value="IOS">IOS</option>
                  </Select>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <Select
                    id="ptsp"
                    htmlFor="ptsp"
                    label="PTSP"
                    name="ptsp"
                    value={state.ptsp}
                    onChange={handleChange}
                    grouped
                  >
                    <option value="intellicense solution">
                      intellicense solution
                    </option>
                    <option value="local farmers">local farmers</option>
                  </Select>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Title style={{ marginTop: 5 }}>Acquirer numbers</Title>
                </Grid>
                <Grid item xs={12} sm={4} md={4} lg={4}>
                  <TextField
                    id="visa"
                    htmlFor="visa"
                    label="Visa"
                    name="visa"
                    value={state.visa}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={4} md={4} lg={4}>
                  <TextField
                    id="verve"
                    htmlFor="verve"
                    label="Verve"
                    name="verve"
                    value={state.verve}
                    onChange={handleChange}
                    grouped
                  />
                </Grid>
                <Grid item xs={12} sm={4} md={4} lg={4}>
                  <TextField
                    id="masterCard"
                    htmlFor="masterCard"
                    label="Master card"
                    name="masterCard"
                    value={state.masterCard}
                    onChange={handleChange}
                    grouped
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Title style={{ marginTop: 5 }}>Security</Title>
                </Grid>
                <Grid item xs={12} sm={5} md={5} lg={5}>
                  <Text style={{ display: "flex", margin: "10px 0" }}>
                    Enable supervisor PIN{" "}
                    <span className={classes.checkbox}>
                      <CheckBox
                        id="supervisionPin"
                        htmlFor="supervisionPin"
                        name="supervisionPin"
                        value={state.supervisionPin}
                        checked={state.supervisionPin}
                        onChange={handleSuperVisionbox}
                      />
                    </span>
                  </Text>
                </Grid>
                <Grid item xs={12} sm={5} md={5} lg={5}>
                  <Text style={{ display: "flex", margin: "10px -27px" }}>
                    Enable admin PIN{" "}
                    <span className={classes.checkbox}>
                      <CheckBox
                        id="adminPin"
                        htmlFor="adminPin"
                        name="adminPin"
                        type="checkbox"
                        value={state.adminPin}
                        checked={state.adminPin}
                        onChange={handleAdminPinbox}
                      />
                    </span>
                  </Text>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      onClick={handleClose}
                      style={{ margin: "3px 10px 0 0" }}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        backgroundColor: loading ? Colors.primary : "",
                        marginTop: 0,
                        width: 65,
                      }}
                      disableRipple={loading}
                    >
                      <span className="submit-btn">
                        {loading ? (
                          <Loader
                            type="ThreeDots"
                            color="#B28B0A"
                            height="15"
                            width="30"
                          />
                        ) : (
                          "save"
                        )}
                      </span>
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </div>
        </Grid>
      </div>
    </div>
  );
}
export default AddSingleTerminal;
