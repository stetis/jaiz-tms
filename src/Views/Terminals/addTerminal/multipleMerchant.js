import { Grid } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import ArrowDropDownOutlinedIcon from "@material-ui/icons/ArrowDropDownOutlined";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import { clearMessages } from "../../../actions/terminals.actions";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import {
  DeleteButton,
  SubmitButton,
  Fab,
} from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Title } from "../../../components/contentHolders/Card";
import { Text } from "../../../components/Forms/index";
import { Select, TextField } from "../../../components/TextField";
import { AutoComplete } from "../../../components/TextField/autotcomplete/autocomplete";
import { getAllMerchantsHelper } from "../../../helpers/mechants.helper";
import { getPTSPHelper } from "../../../helpers/ptsp.helper";
import { addMerchantTerminalHelper } from "../../../helpers/terminals.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "../styles";

export default function AddSingleTerminal(props) {
  const classes = styles();
  const [merchant, setMerchant] = useState({
    text: "",
    suggestions: [],
  });
  const [isComponentVisible, setIsComponentVisible] = useState(true);

  const [state, setState] = useState({
    ptsp: "",
    first_serialNo: "",
    first_terminalID: "",
    first_ptsp: "",
    terminals: [
      {
        "serial-number": "",
        "terminal-id": "",
      },
    ],
    formError: false,
  });
  const dispatch = useDispatch();
  const merchants = useSelector(
    (state) => state.merchantsReducer.allMerchantsData.merchants
  );
  const ptsps = useSelector((state) => state.ptspReducer.data.ptsps);
  const loading = useSelector((state) => state.terminalsReducer.actionLoading);
  const status = useSelector((state) => state.terminalsReducer.status);
  const statusMessage = useSelector(
    (state) => state.terminalsReducer.statusMessage
  );
  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  useEffect(() => {
    document.title = "Add merchant terminal | Terminals";
    dispatch(getPTSPHelper());
    dispatch(getAllMerchantsHelper());
    dispatch(
      updateBreadcrumb({
        parent: "Terminals",
        child: "Add merchant terminal",
        homeLink: "/tms",
        childLink: "/tms/terminals",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const onTextChanged = (e) => {
    const value = e.target.value;
    let suggestions = [];
    if (value.length > 0) {
      const regex = new RegExp(`^${value}`, "i");
      suggestions =
        merchants && merchants.length !== 0
          ? merchants.sort().filter((merchant) => regex.test(merchant.name))
          : [];
    }
    setIsComponentVisible(true);
    setMerchant({ suggestions, text: value });
  };
  const suggestionSelected = (value) => {
    setIsComponentVisible(false);
    setMerchant({
      text: value.name,
      suggestions: [],
    });
  };
  // handle click event of the Add button
  const handleAddTerminal = (e) => {
    e.preventDefault();
    setState((prev) => ({
      ...prev,
      terminals: [
        ...state.terminals,
        { "serial-number": "", "terminal-terminal": "" },
      ],
    }));
  };
  // handle click event of the Remove button
  const handleRemoveTerminal = (e, index) => {
    e.preventDefault();
    const list = [...state.terminals];
    list.splice(index, 1);
    setState((prev) => ({
      ...prev,
      terminals: list,
    }));
  };
  const onChangeInput = (e, index) => {
    const { name, value } = e.target;
    const list = [...state.terminals];
    list[index][name] = value;
    setState((prev) => ({
      ...prev,
      terminals: list,
    }));
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  const handleClose = (e) => {
    e.preventDefault();
    props.history.push("/tms/terminals");
  };
  const handleCloseSnack = () => dispatch(clearMessages());

  const { suggestions } = merchant;
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const ptspDetail = ptsps && ptsps.find((pt) => pt.id === state.ptsp);

    let allTerminals = [];
    allTerminals.push(...state.terminals, {
      "serial-number": state.first_serialNo,
      "terminal-id": state.first_terminalID,
    });
    const merchantDetail =
      merchants && merchants.find((ag) => ag.name === merchant.text);
    if (
      merchant.text === "" ||
      state.first_serialNo === "" ||
      state.first_terminalID === "" ||
      !state.ptsp
    ) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      const inputData = {
        terminals: allTerminals,
        "merchant-id": merchantDetail["merchant-id"],
        "ptsp-id": ptspDetail.id,
      };
      console.log(inputData);
      dispatch(addMerchantTerminalHelper(inputData));
    }
  };
  const merchantDetail =
    merchants && merchants.find((ag) => ag.name === merchant.text);
  return (
    <div>
      {state.formError === true
        ? renderFieldError(
            merchant.text === ""
              ? "Merchant name is required"
              : !state.ptsp
              ? "PTSP is required"
              : state.first_serialNo === ""
              ? "Serial number is required"
              : state.first_terminalID === ""
              ? "Terminal ID is required"
              : ""
          )
        : null}
      <div className={classes.addTerminalCard}>
        <Grid container spacing={0}>
          <div>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <form
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
                className={classes.container}
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Add merchant terminal
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <AutoComplete
                    id="merchant"
                    htmlFor="merchant"
                    label="Merchant"
                    onChange={onTextChanged}
                    value={merchant.text}
                    setIsComponentVisible={setIsComponentVisible}
                    suggestions={suggestions}
                    isComponentVisible={isComponentVisible}
                    suggestionSelected={suggestionSelected}
                    Icon={
                      <ArrowDropDownOutlinedIcon style={{ fontSize: 34 }} />
                    }
                  />
                </Grid>
                {merchantDetail && merchantDetail !== null ? (
                  <>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={6}
                      lg={6}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>
                          Agent/Merchant ID:{" "}
                        </span>
                        {merchantDetail["merchant-id"]}
                      </Text>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={6}
                      lg={6}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>Phone: </span>
                        {merchantDetail.phone}
                      </Text>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={6}
                      lg={6}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>Email: </span>
                        {merchantDetail.email}
                      </Text>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={12}
                      md={12}
                      lg={12}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>Address: </span>
                        {merchantDetail.address}
                      </Text>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={6}
                      lg={6}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>State: </span>
                        {merchantDetail["state-name"]}
                      </Text>
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      sm={6}
                      md={6}
                      lg={6}
                      style={{ display: "flex", marginBottom: 8 }}
                    >
                      <Text>
                        <span className={classes.merchantInfo}>LGA: </span>
                        {merchantDetail["lg-name"]}
                      </Text>
                    </Grid>
                  </>
                ) : null}
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Select
                    id="ptsp"
                    htmlFor="ptsp"
                    label="PTSP"
                    name="ptsp"
                    value={state.ptsp}
                    onChange={handleChange}
                  >
                    {ptsps &&
                      ptsps.map((ptsp) => {
                        return (
                          <option value={ptsp.id} key={ptsp.id}>
                            {ptsp.name}
                          </option>
                        );
                      })}
                  </Select>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Title style={{ marginTop: 5 }}>Terminal</Title>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Fab
                    onClick={handleAddTerminal}
                    className={classes.AddTerminalFab}
                  >
                    <AddIcon className={classes.fabAddIcon} />
                  </Fab>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <label className={classes.label} htmlFor="first_serialNo">
                    Serial number
                  </label>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <label className={classes.label} htmlFor="first_terminalID">
                    Terminal ID
                  </label>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <hr
                    style={{
                      margin: "8px 0",
                      height: 0.5,
                      border: "none",
                      color: Colors.greyLight,
                      backgroundColor: Colors.greyLight,
                    }}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <TextField
                    id="first_serialNo"
                    htmlFor="first_serialNo"
                    label=""
                    name="first_serialNo"
                    value={state.first_serialNo}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <TextField
                    id="first_terminalID"
                    htmlFor="first_terminalID"
                    label=""
                    name="first_terminalID"
                    value={state.first_terminalID}
                    onChange={handleChange}
                    grouped
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <hr
                    style={{
                      margin: "8px 0 8px",
                      height: 0.5,
                      border: "none",
                      color: Colors.greyLight,
                      backgroundColor: Colors.greyLight,
                    }}
                  />
                </Grid>
                {state.terminals.map((terminal, i) => {
                  return (
                    <Grid container spacing={0} key={i}>
                      <Grid item xs={12} sm={6} md={6} lg={6}>
                        <TextField
                          id="serial-number"
                          htmlFor="serial-number"
                          label=""
                          name="serial-number"
                          value={terminal["serial-number"]}
                          onChange={(e) => onChangeInput(e, i)}
                        />
                      </Grid>
                      <Grid item xs={12} sm={6} md={6} lg={6}>
                        <TextField
                          id="terminal-id"
                          htmlFor="terminal-id"
                          label=""
                          name="terminal-id"
                          value={terminal["terminal-id"]}
                          onChange={(e) => onChangeInput(e, i)}
                          grouped
                        />
                        <Fab
                          onClick={(e) => handleRemoveTerminal(e, i)}
                          className={classes.removeFab}
                          ghost
                        >
                          <RemoveIcon className={classes.removeFabIcon} />
                        </Fab>
                      </Grid>
                      <Grid item xs={12} sm={12} md={12} lg={12}>
                        <hr
                          style={{
                            margin: "-18px 0 0px",
                            height: 0.5,
                            border: "none",
                            color: Colors.greyLight,
                            backgroundColor: Colors.greyLight,
                          }}
                        />
                      </Grid>
                    </Grid>
                  );
                })}
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      onClick={handleClose}
                      style={{ margin: "3px 10px 0 0" }}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        backgroundColor: loading ? Colors.primary : "",
                        marginTop: 0,
                        width: 65,
                      }}
                      disableRipple={loading}
                    >
                      <span className="submit-btn">
                        {loading ? (
                          <Loader
                            type="ThreeDots"
                            color="#B28B0A"
                            height="15"
                            width="30"
                          />
                        ) : (
                          "save"
                        )}
                      </span>
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </div>
        </Grid>
      </div>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
