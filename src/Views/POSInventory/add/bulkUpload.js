import React, { useState, useEffect } from "react";
import XLSX from "xlsx";
import { MuiThemeProvider, Grid } from "@material-ui/core";
import Loader from "react-loader-spinner";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import { useSelector, useDispatch } from "react-redux";
import { wideTable } from "../../../assets/MUI/Table";
import MUIDataTable from "mui-datatables";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { clearMessages } from "../../../actions/posInventory.actions";
import { SubmitButton, DeleteButton } from "../../../components/Buttons/index";
import { Select } from "../../../components/TextField";
import Snackbars from "../../../assets/Snackbars/index";
import { styles } from "../styles";
import { getDeviceHelper } from "../../../helpers/Configurations/devices.helper";
import { getSoftwareVersionsHelper } from "../../../helpers/Configurations/versions.helper";
import { uploadBulkPOSInventoryHelper } from "../../../helpers/posInventory.heper";
import { Colors } from "../../../assets/themes/theme";
import jaiz from "../../../images/jaiz-logo.png";

export default function BulkUpload(props) {
  const classes = styles();
  const fileUploader = React.useRef();
  const [state, setState] = useState({
    data: [],
    cols: [],
    type: "",
    app: "",
  });
  const dispatch = useDispatch();
  const devices = useSelector((state) => state.devicesReducer.data.devices);
  const softwares = useSelector(
    (state) => state.softwareReducer.data.softwares
  );
  const loading = useSelector(
    (state) => state.posInventoryReducer.actionLoading
  );
  const status = useSelector((state) => state.posInventoryReducer.status);
  const statusMessage = useSelector(
    (state) => state.posInventoryReducer.statusMessage
  );
  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);
  useEffect(() => {
    document.title = "Devices | Jaiz bank TMS";
    dispatch(getDeviceHelper());
    dispatch(getSoftwareVersionsHelper());
    dispatch(
      updateBreadcrumb({
        parent: "POS inventory",
        child: "Bulk upload",
        homeLink: "/tms",
        childLink: "/tms/pos-inventory",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const handleSelectChange = (event) => {
    const { name, value } = event.target;
    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };
  const handleFile = (file) => {
    /* Boilerplate to set up FileReader */
    const reader = new FileReader();
    const rABS = !!reader.readAsBinaryString;
    reader.onload = (e) => {
      /* Parse data */
      const { result } = e.target;
      const bstr = result;
      const wb = XLSX.read(bstr, { type: rABS ? "binary" : "array" });
      /* Get first worksheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      /* Convert array of arrays */
      const data = XLSX.utils.sheet_to_json(ws, {
        header: 1,
      });
      /* Update state */
      setState((prev) => ({
        ...prev,
        data: data,
        cols: ["Serial number", "IMEI-one", "IMEI-two"],
      }));
    };
    if (rABS) reader.readAsBinaryString(file);
    else reader.readAsArrayBuffer(file);
  };
  const handleChange = (e) => {
    const { files } = e.target;
    if (files && files[0]) handleFile(files[0]);
  };
  const handleFileUpload = (e) => {
    e.preventDefault();
    fileUploader.current.click();
  };
  const suppress = (evt) => {
    evt.persist();
    evt.stopPropagation();
    evt.preventDefault();
  };
  const onDrop = (evt) => {
    evt.persist();
    evt.stopPropagation();
    evt.preventDefault();
    const { files } = evt.dataTransfer;
    if (files && files[0]) handleFile(files[0]);
  };
  const handleClose = (e) => {
    e.preventDefault();
    props.history.goBack();
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const device = devices && devices.find((dv) => dv.name === state.type);
    const app = softwares && softwares.find((ap) => ap.name === state.app);
    if (!state.type || !state.app || state.data === null) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      let inputData = state.data.map((el) => {
        return {
          "serial-number": el[0],
          "imei-one": el[1],
          "imei-two": el[2],
          os: device.os,
          type: device.type,
          model: device.model,
          "app-name": app.name,
          "app-id": app.id,
          "app-version": app.version,
        };
      });
      inputData.shift();
      dispatch(uploadBulkPOSInventoryHelper(inputData));
    }
  };
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: false,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    rowsPerPageOptions: [],
    customToolbar: () =>
      state.data.length === 0 ? (
        ""
      ) : (
        <ButtonsRow
          style={{
            marginTop: 25,
          }}
        >
          <DeleteButton
            style={{
              marginRight: 10,
            }}
            onClick={handleClose}
            // disabled={loading}
          >
            Cancel
          </DeleteButton>
          <SubmitButton
            type="submit"
            disabled={loading}
            style={{
              marginTop: 0,
            }}
            onClick={handleSubmit}
          >
            {loading ? (
              <Loader
                type="ThreeDots"
                color={Colors.secondary}
                height="15"
                width="30"
              />
            ) : (
              "Save"
            )}
          </SubmitButton>
        </ButtonsRow>
      ),
    textLabels: {
      body: {
        noMatch:
          "No excel file uploaded yet. Either click on the button above to upload or drag and drop the file into this message",
      },
    },
  };
  return (
    <div>
      {state.formError === true
        ? renderFieldError(
            !state.type
              ? "Device type is required"
              : !state.app
              ? "App is required"
              : state.data === null
              ? "Please upload excel file"
              : ""
          )
        : null}
      <div className={classes.bulkCard}>
        <Grid container spacing={0}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <div className={classes.logo}>
              <img
                src={jaiz}
                alt="jaiz logo"
                style={{ width: 20, height: 20, marginRight: "1rem" }}
              />
              POS inventory bulk upload
            </div>
          </Grid>
          <Grid
            item
            xs={6}
            sm={4}
            md={4}
            lg={4}
            className={classes.merchantGrid}
          >
            <Select
              id="type"
              htmlFor="type"
              label="Select device type"
              name="type"
              value={state.type}
              onChange={handleSelectChange}
            >
              {devices &&
                devices.map((device) => {
                  return (
                    <option value={device.name} key={device.id}>
                      {device.name}
                    </option>
                  );
                })}
            </Select>
          </Grid>
          <Grid item xs={6} sm={4} md={4} lg={4} className={classes.inputGrid}>
            <Select
              id="app"
              htmlFor="app"
              label="Select app"
              name="app"
              value={state.app}
              onChange={handleSelectChange}
              grouped
            >
              {softwares &&
                softwares.map((app) => {
                  return (
                    <option value={app.name} key={app.id}>
                      {app.name}
                    </option>
                  );
                })}
            </Select>
          </Grid>
          <Grid item xs={4} sm={2} md={2} lg={2} className={classes.fileGrid}>
            <input
              type="file"
              id="file"
              accept={SheetJSFT}
              onChange={handleChange}
              ref={fileUploader}
              style={{
                display: "none",
              }}
            />
            <ButtonsRow style={{ marginTop: 18 }}>
              <SubmitButton onClick={handleFileUpload}>upload</SubmitButton>
            </ButtonsRow>
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <div onDrop={onDrop} onDragEnter={suppress} onDragOver={suppress}>
              <MuiThemeProvider theme={wideTable}>
                <MUIDataTable
                  data={state.data.slice(1)}
                  columns={state.cols}
                  options={options}
                />
              </MuiThemeProvider>
            </div>
          </Grid>
        </Grid>
      </div>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}

/* list of supported file types */
const SheetJSFT = ["xlsx", "xls", "csv"]
  .map(function (x) {
    return "." + x;
  })
  .join(",");
