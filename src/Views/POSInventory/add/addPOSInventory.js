import { Grid, List, ListItemIcon } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import AddSharpIcon from "@material-ui/icons/AddSharp";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../actions/posInventory.actions";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Text } from "../../../components/Forms/index";
import { Select, TextField } from "../../../components/TextField/index";
import { getDeviceHelper } from "../../../helpers/Configurations/devices.helper";
import { getSoftwareVersionsHelper } from "../../../helpers/Configurations/versions.helper";
import { addPOSInventoryHelper } from "../../../helpers/posInventory.heper";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "../styles";

export default function AddPOSInventory() {
  const classes = styles();
  const [state, setState] = useState({
    open: false,
    serialNo: "",
    IMEI1: "",
    IMEI2: "",
    type: "",
    app: "",
    formError: false,
  });
  const dispatch = useDispatch();
  const devices = useSelector((state) => state.devicesReducer.data.devices);
  const softwares = useSelector(
    (state) => state.softwareReducer.data.softwares
  );
  const loading = useSelector(
    (state) => state.posInventoryReducer.actionLoading
  );
  const status = useSelector((state) => state.posInventoryReducer.status);
  const statusMessage = useSelector(
    (state) => state.posInventoryReducer.statusMessage
  );
  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);
  useEffect(() => {
    dispatch(getDeviceHelper());
    dispatch(getSoftwareVersionsHelper());
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };
  const handleClickOpen = () => {
    setState((prev) => ({
      ...prev,
      open: true,
    }));
  };
  const handleClose = (e) => {
    e.preventDefault();
    setState((prev) => ({
      ...prev,
      open: false,
    }));
  };
  const handleKeyPress = (event) => {
    if (event.which === 10 || event.which === 13) {
      event.preventDefault();
    }
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const handleSubmit = (e) => {
    e.preventDefault();
    const device = devices && devices.find((dv) => dv.name === state.type);
    const app = softwares && softwares.find((ap) => ap.name === state.app);
    if (!state.type || !state.app || state.data === null) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      let inputData = {
        "serial-number": state.serialNo,
        name: state.type,
        "imei-one": state.IMEI1,
        "imei-two": state.IMEI2,
        os: device.os,
        type: device.type,
        model: device.model,
        "app-name": app.name,
        "app-id": app.id,
        "app-version": app.version,
      };
      dispatch(addPOSInventoryHelper(inputData));
    }
  };
  const renderFieldError = (formErrorMessage) => {
    <Snackbars
      variant="error"
      handleClose={handleCloseSnack}
      message={formErrorMessage}
      isOpen={state.formError}
    />;
  };
  return (
    <div>
      <List classes={{ root: classes.button }} onClick={handleClickOpen}>
        <ListItemIcon classes={{ root: classes.listItemIcon }}>
          <AddSharpIcon className={classes.editIcon} />
        </ListItemIcon>
        <Text className={classes.editText}>Add single POS</Text>
      </List>
      {state.formError === true
        ? renderFieldError(
            state.serialNo === ""
              ? "POS serial number is required"
              : state.IMEI1
              ? "POS IMEI is required"
              : !state.type
              ? "Device type is required"
              : !state.app
              ? "App name is required"
              : ""
          )
        : null}
      <Dialog
        open={state.open}
        keepMounted
        aria-labelledby="alert-dialog-slide-title"
        className="add-branch-dialog"
        aria-describedby="alert-dialog-slide-description"
      >
        <div className={classes.actionsRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12}>
              <form
                className={classes.container}
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Add POS inventory
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextField
                    id="serialNo"
                    htmlFor="serialNo"
                    type="text"
                    label="Serial No."
                    name="serialNo"
                    value={state.serialNo}
                    onChange={handleChange}
                    onKeyPress={handleKeyPress}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <TextField
                    id="IMEI1"
                    htmlFor="IMEI1"
                    type="text"
                    label="IMEI 1"
                    name="IMEI1"
                    value={state.IMEI1}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <TextField
                    id="IMEI2"
                    htmlFor="IMEI2"
                    type="text"
                    label="IMEI 2"
                    name="IMEI2"
                    value={state.IMEI2}
                    onChange={handleChange}
                    grouped
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <Select
                    id="type"
                    htmlFor="type"
                    label="Select device type"
                    name="type"
                    value={state.type}
                    onChange={handleChange}
                  >
                    {devices &&
                      devices.map((device) => {
                        return (
                          <option value={device.name} key={device.id}>
                            {device.name}
                          </option>
                        );
                      })}
                  </Select>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <Select
                    id="app"
                    htmlFor="app"
                    label="Select app"
                    name="app"
                    value={state.app}
                    onChange={handleChange}
                    grouped
                  >
                    {softwares &&
                      softwares.map((app) => {
                        return (
                          <option value={app.name} key={app.id}>
                            {app.name}
                          </option>
                        );
                      })}
                  </Select>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      style={{
                        marginRight: 10,
                      }}
                      onClick={handleClose}
                      disabled={loading}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        marginTop: 0,
                        width: 65,
                      }}
                    >
                      {loading ? (
                        <Loader
                          type="ThreeDots"
                          color={Colors.secondary}
                          height="15"
                          width="30"
                        />
                      ) : (
                        "Submit"
                      )}
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </div>
      </Dialog>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
