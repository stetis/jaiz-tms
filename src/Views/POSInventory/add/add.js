import { List, ListItemIcon, MenuItem } from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import Popover from "@material-ui/core/Popover";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";
import AddSharpIcon from "@material-ui/icons/AddSharp";
import React, { useState } from "react";
import { withRouter } from "react-router";
import { Text } from "../../../components/Forms/index";
import { SubmitButton } from "../../../components/Buttons";
import { styles } from "../styles";
import AddPOSInventory from "./addPOSInventory";

function AddPOS(props) {
  const classes = styles();
  const [anchorEl, setAnchorEl] = useState(null);
  const handleClick = (event) => setAnchorEl(event.currentTarget);
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleAddBulk = () => props.history.push("/tms/pos-bulkupload");
  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  return (
    <div>
      <SubmitButton onClick={handleClick} className={classes.fabIcon}>
        <AddOutlinedIcon className={classes.icon} />
        Add POS
      </SubmitButton>
      <Popover
        id={id}
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
      >
        <MenuItem>
          <AddPOSInventory />
        </MenuItem>
        <Divider />
        <MenuItem>
          <List classes={{ root: classes.button }} onClick={handleAddBulk}>
            <ListItemIcon classes={{ root: classes.listItemIcon }}>
              <AddSharpIcon className={classes.editIcon} />
            </ListItemIcon>
            <Text className={classes.editText}>Upload bulk pos</Text>
          </List>
        </MenuItem>
        <Divider />
      </Popover>
    </div>
  );
}

export default withRouter(AddPOS);
