import { Grid, MuiThemeProvider } from "@material-ui/core";
import TablePagination from "@material-ui/core/TablePagination";
import FilterListOutlinedIcon from "@material-ui/icons/FilterListOutlined";
import MUIDataTable from "mui-datatables";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../actions/breadcrumbAction";
import { clearMessages } from "../../actions/posInventory.actions";
import { POS } from "../../assets/icons/Svg";
import { theme } from "../../assets/MUI/Table";
import Snackbars from "../../assets/Snackbars/index";
import Spinner from "../../assets/Spinner";
import { SubmitButton } from "../../components/Buttons";
import { ButtonsRow } from "../../components/Buttons/styles";
import CallOut from "../../components/CallOut/CallOut";
import { Title } from "../../components/contentHolders/Card";
import { Select } from "../../components/TextField";
import Search from "../../components/TextField/search";
import {
  getPOSesHelper,
  getPOSInventoriesHelper,
} from "../../helpers/posInventory.heper";
import jaiz from "../../images/jaiz-logo.png";
import AddPOSInventory from "./add/add";
import DeletePOSInventory from "./delete";
import EditPOSInventory from "./edit";
import { styles } from "./styles";

const styleProps = {
  height: 50,
  width: 50,
};
function capitalizeFirstLetters(str) {
  var splitStr = str.toLowerCase().split(" ");
  for (var i = 0; i < splitStr.length; i++) {
    splitStr[i] =
      splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  return splitStr.join(" ");
}

function POSInventoryTable() {
  const classes = styles();
  const [showButton, setShowButton] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [search, setSearch] = useState("");
  const [status, setStatus] = useState("all");
  const dispatch = useDispatch();
  const summary = useSelector((state) => state.posInventoryReducer.data);
  const error = useSelector((state) => state.posInventoryReducer.error);
  const errorMessage = useSelector(
    (state) => state.posInventoryReducer.errorMessage
  );
  const posLoading = useSelector(
    (state) => state.posInventoryReducer.posesLoading
  );
  const paginatedPoses = useSelector(
    (state) => state.posInventoryReducer.posesData
  );
  const paginatedPosesPages = useSelector(
    (state) => state.posInventoryReducer.posesData.paging
  );

  const posError = useSelector((state) => state.posInventoryReducer.posesError);
  const posErrorMessage = useSelector(
    (state) => state.posInventoryReducer.posesErrorMessage
  );
  useEffect(() => {
    document.title = "POS Invetory | Jaiz bank TMS";
    dispatch(getPOSInventoriesHelper());
    const credentials = {
      page,
      pagesize: rowsPerPage,
      search,
      status,
    };
    dispatch(getPOSesHelper(credentials));
    dispatch(
      updateBreadcrumb({
        parent: "POS inventory",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const handleCloseSnack = (e) => dispatch(clearMessages());
  const handleStatusChange = (e) => setStatus(e.target.value);
  const handleSearchChange = (event) => setSearch(event.target.value);
  const handleSearch = (e) => {
    e.preventDefault();
    const credentials = {
      page: page,
      pagesize: rowsPerPage,
      search,
      status,
    };
    dispatch(getPOSesHelper(credentials));
  };
  const handleKeyPress = (event) => {
    const credentials = {
      page: page,
      pagesize: rowsPerPage,
      search,
      status,
    };
    if (event.keyCode === 13) {
      dispatch(getPOSesHelper(credentials));
      setSearch(search);
    }
  };
  const handleChangePage = (event, newPage) => {
    const credentials = {
      page: newPage,
      pagesize: rowsPerPage,
      search,
      status,
    };
    dispatch(getPOSesHelper(credentials));
    setPage(newPage);
    setSearch(search);
    setRowsPerPage(rowsPerPage);
    setStatus(status);
  };
  const handleChangeRowsPerPage = (event) => {
    const credentials = {
      page: page,
      pagesize: event.target.value,
      search,
      status,
    };
    dispatch(getPOSesHelper(credentials));
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
    setSearch(search);
    setStatus(status);
  };
  const handleClickOpen = (e) => {
    e.preventDefault();
    setShowButton(!showButton);
  };
  const columns = [
    "",
    "Name",
    "Serial number",
    "IMEI one",
    "IMEI two",
    "Status",
    "OS",
    "Type",
    " ",
  ];
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    rowsPerPage: rowsPerPage,
    page: page,
    serverSide: true,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    customToolbar: () => (
      <ButtonsRow className={classes.buttonRow}>
        <SubmitButton
          onClick={handleClickOpen}
          className={classes.fabIcon}
          style={{ marginRight: 15 }}
        >
          <FilterListOutlinedIcon className={classes.icon} />
          Filters
        </SubmitButton>
        <AddPOSInventory />
      </ButtonsRow>
    ),
    textLabels: {
      body: {
        noMatch: "No pos inventory record found",
      },
    },
  };
  return (
    <div className={classes.posRoot}>
      {!posLoading ? (
        <>
          <Grid container spacing={0}>
            <Grid item xs={12}>
              <div className={classes.summaryHolder}>
                <Grid item xs={12} sm={3} md={3} lg={3}>
                  <div className={classes.SummaryCard}>
                    <Title style={{ textAlign: "center" }}>
                      Total POS
                      <span className={classes.summaryDigits}>
                        {" "}
                        <POS style={{ marginRight: 5 }} /> {summary.total}
                      </span>
                    </Title>
                  </div>
                </Grid>
                <Grid item xs={12} sm={3} md={3} lg={3}>
                  <div className={classes.SummaryCard}>
                    <Title style={{ textAlign: "center" }}>
                      Available POS{" "}
                      <span className={classes.summaryDigits}>
                        {" "}
                        <POS style={{ marginRight: 5 }} /> {summary.available}
                      </span>
                    </Title>
                  </div>
                </Grid>
                <Grid item xs={12} sm={3} md={3} lg={3}>
                  <div className={classes.SummaryCard}>
                    <Title style={{ textAlign: "center" }}>
                      Allocated{" "}
                      <span className={classes.summaryDigits}>
                        {" "}
                        <POS style={{ marginRight: 5 }} />
                        {summary.allocated}
                      </span>
                    </Title>
                  </div>
                </Grid>
                <Grid item xs={12} sm={3} md={3} lg={3}>
                  <div className={classes.SummaryCard}>
                    <Title style={{ textAlign: "center" }}>
                      Faulty
                      <span className={classes.summaryDigits}>
                        {" "}
                        <POS style={{ marginRight: 5 }} /> {summary.faulty}
                      </span>
                    </Title>
                  </div>
                </Grid>
              </div>
            </Grid>
          </Grid>
          <div className={classes.card}>
            <Grid container spacing={0}>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <MuiThemeProvider theme={theme}>
                  <MUIDataTable
                    data={
                      paginatedPoses.poses && paginatedPoses.poses !== null
                        ? paginatedPoses.poses.map((pos, i) => {
                            return {
                              "": rowsPerPage * page + i + 1,
                              Name: pos.name,
                              "Serial number": pos.id,
                              "IMEI one": pos["imei-one"],
                              "IMEI two": pos["imei-two"],
                              Status: capitalizeFirstLetters(pos.status),
                              OS: pos.os,
                              Type: pos.type,
                              " ": (
                                <CallOut
                                  TopAction={<EditPOSInventory row={pos} />}
                                  BottomAction={
                                    <DeletePOSInventory row={pos} />
                                  }
                                />
                              ),
                            };
                          })
                        : []
                    }
                    columns={columns}
                    options={options}
                    title={
                      showButton ? (
                        <Grid container spacing={4}>
                          <Grid item xs={12} sm={6} md={6} lg={6}>
                            <Search
                              id="search"
                              htmlFor="search"
                              name="search"
                              value={search}
                              placeholder="search merchant"
                              searchlabel="Search"
                              onChange={handleSearchChange}
                              onKeyUp={handleKeyPress}
                              onClickIcon={handleSearch}
                            />
                          </Grid>
                          <Grid item xs={12} sm={6} md={6} lg={6}>
                            <Select
                              id="status"
                              htmlFor="status"
                              name="status"
                              placeholder="status"
                              label="Status"
                              value={status}
                              onChange={handleStatusChange}
                            >
                              <option value="all">All</option>
                              <option value="available">Available</option>
                              <option value="faulty">Faulty</option>
                              <option value="allocated">Allocated</option>
                            </Select>
                          </Grid>
                        </Grid>
                      ) : (
                        <div className={classes.tableLogo}>
                          <img
                            src={jaiz}
                            alt="jaiz logo"
                            style={{
                              width: 20,
                              height: 20,
                              marginRight: "1rem",
                            }}
                          />
                          POS inventory
                        </div>
                      )
                    }
                  />
                  <TablePagination
                    component="div"
                    count={
                      paginatedPosesPages && paginatedPosesPages["total-record"]
                    }
                    page={page}
                    onPageChange={handleChangePage}
                    rowsPerPage={rowsPerPage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    rowsPerPageOptions={[5, 10]}
                  />
                </MuiThemeProvider>
              </Grid>
            </Grid>
          </div>
        </>
      ) : (
        <Spinner {...styleProps} />
      )}
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={posErrorMessage}
        isOpen={posError}
      />
    </div>
  );
}
export default POSInventoryTable;
