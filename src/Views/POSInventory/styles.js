import { makeStyles } from "@material-ui/core";
import { Colors, Fonts } from "../../assets/themes/theme";

export const styles = makeStyles(() => ({
  posRoot: {
    width: "90%",
    margin: "26px auto",
  },
  card: {
    width: "100%",
    marginTop: 26,
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 20px 25px",
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
  },
  summaryHolder: {
    width: "100%",
    "@media only screen and (min-width: 600px)": {
      display: "flex",
    },
  },
  SummaryCard: {
    width: "100%",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "10px 15px",
    borderRadius: 5,
    margin: 10,
    justifyContent: "center",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 125,
      display: "flex",
      flexGrow: 1,
    },
    "@media only screen and (min-width: 960px)": {
      width: "80%",
      margin: "0 auto",
      flexGrow: 1,
    },
  },
  logo: {
    color: Colors.primary,
    display: "flex",
    fontSize: 16,
    fontWeight: 700,
    margin: "-5px 0 0px",
    fontFamily: Fonts.secondary,
  },
  summaryDigits: {
    color: Colors.secondary,
    display: "flex",
    justifyContent: "center",
    marginTop: 5,
  },
  searchGrid: {
    "@media only screen and (min-width: 600px)": {
      display: "flex",
    },
  },
  bulkCard: {
    width: "auto",
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: 25,
    borderRadius: 5,
    cursor: "pointer",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 580,
    },
    "@media only screen and (min-width: 960px)": {
      width: 940,
    },
  },
  fileGrid: {
    marginTop: 23,
    "@media only screen and (min-width: 960px)": {
      margin: "16px 0 10px -68px",
    },
  },
  container: {
    display: "flex",
    padding: 25,
    flexWrap: "wrap",
  },
  addIcon: {
    fontSize: 20,
    color: Colors.secondary,
  },
  fabIcon: {
    color: Colors.secondary,
    fontWeight: 600,
  },
  icon: {
    fontSize: 18,
    marginRight: 5,
  },
  tableLogo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
    "@media only screen and (max-width: 540px)": {
      marginTop: -25,
    },
  },
  buttonRow: {
    "@media only screen and (max-width: 540px)": {
      position: "absolute",
      zIndex: 1000,
      top: 0,
    },
  },
  deleteRoot: {
    width: 320,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    display: "flex",
    padding: 25,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 350,
    },
  },
  actionsRoot: {
    width: 320,
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 400,
    },
  },
  listItemIcon: {
    color: Colors.menuListColor,
    minWidth: 22,
  },
  editIcon: {
    margin: "1px 0 0 -10px",
    color: Colors.secondary,
    cursor: "pointer",
  },
  editText: {
    color: Colors.primary,
    margin: "2px 0 0px -10px",
    cursor: "pointer",
  },
  button: {
    cursor: "pointer",
    width: "100%",
    display: "flex",
    marginLeft: 5,
    marginRight: 5,
    padding: 2,
    color: Colors.menuListColor,
    fontSize: Fonts.font,
  },
  DeleteIcon: {
    color: Colors.buttonError,
    margin: "1px 0 0 -10px",
    cursor: "pointer",
  },
  deleteText: {
    color: Colors.buttonError,
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
    margin: "2px 0 0px -10px",
    cursor: "pointer",
  },
}));
