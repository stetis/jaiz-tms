import { Grid, MuiThemeProvider } from "@material-ui/core";
import TablePagination from "@material-ui/core/TablePagination";
import CloudDownloadOutlinedIcon from "@material-ui/icons/CloudDownloadOutlined";
import FilterListOutlinedIcon from "@material-ui/icons/FilterListOutlined";
import MUIDataTable from "mui-datatables";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import XLSX from "xlsx";
import { updateBreadcrumb } from "../../actions/breadcrumbAction";
import { clearMessages } from "../../actions/transactions.actions";
import { wideTable } from "../../assets/MUI/Table";
import Snackbars from "../../assets/Snackbars/index";
import Spinner from "../../assets/Spinner";
import { Colors } from "../../assets/themes/theme";
import { SubmitButton } from "../../components/Buttons";
import { ButtonsRow } from "../../components/Buttons/styles";
import { Text } from "../../components/Forms";
import { Select, TextField } from "../../components/TextField";
import Search from "../../components/TextField/search";
import {
  getAgentsCommissionsHelper,
  getAllAgentsCommissionsHelper,
} from "../../helpers/transactions.helper";
import jaiz from "../../images/jaiz-logo.png";
import { styles } from "./styles";

const styleProps = {
  height: 50,
  width: 50,
};
function capitalizeFirstLetters(str) {
  var splitStr = str.toLowerCase().split(" ");
  for (var i = 0; i < splitStr.length; i++) {
    // You do not need to check if i is larger than splitStr length, as your for does that for you
    // Assign it back to the array
    splitStr[i] =
      splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  // Directly return the joined string
  return splitStr.join(" ");
}
function removeUnderscore(str) {
  if (typeof str !== "string") return "";
  const regex = /_/g;
  const newStr = capitalizeFirstLetters(str);
  const update = newStr.replace(regex, " ");
  return update;
}
export default function AgentsCommissionsTable() {
  const classes = styles();
  let today = new Date();
  let startDate =
    today.getFullYear() +
    "/" +
    ("0" + (today.getMonth() - 5)).slice(-2) +
    "/" +
    ("0" + today.getDate()).slice(-2);
  let endDate =
    today.getFullYear() +
    "/" +
    ("0" + (today.getMonth() + 1)).slice(-2) +
    "/" +
    ("0" + today.getDate()).slice(-2);
  const [showButton, setShowButton] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [search, setSearch] = useState("");
  const [status, setStatus] = useState("all");
  const [from, setFrom] = useState(startDate);
  const [to, setTo] = useState(endDate);
  const [type, setType] = useState("text");
  const [toType, setToType] = useState("text");
  const toOnFocus = () => setToType("date");
  const toOnBlur = () => setToType("text");
  const onFocus = () => setType("date");
  const onBlur = () => setType("text");

  const dispatch = useDispatch();
  const commissions = useSelector(
    (state) => state.transactionsReducer.commissionData
  );
  const loading = useSelector(
    (state) => state.transactionsReducer.commissionLoading
  );
  const error = useSelector(
    (state) => state.transactionsReducer.commissionError
  );
  const errorMessage = useSelector(
    (state) => state.transactionsReducer.commissionErrorMessage
  );
  const isLoadingData = useSelector(
    (state) => state.transactionsReducer.transactionsLoading
  );

  const transError = useSelector(
    (state) => state.transactionsReducer.transactionsError
  );
  const transErrorMessage = useSelector(
    (state) => state.transactionsReducer.transactionsErrorMessage
  );

  useEffect(() => {
    document.title = "Agents commissions | Jaiz bank TMS";
    dispatch(
      updateBreadcrumb({
        parent: "Agents commissions",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );
    const credentials = {
      page,
      pagesize: rowsPerPage,
      search: search,
      status,
      from,
      to,
    };
    dispatch(getAgentsCommissionsHelper(credentials));
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const handleClickOpen = (e) => {
    e.preventDefault();
    setShowButton(!showButton);
  };
  const handleChangePage = (event, newPage) => {
    let fromDate = new Date(from);
    let toDate = new Date(to);
    let startDate =
      fromDate.getFullYear() +
      "/" +
      ("0" + (fromDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + fromDate.getDate()).slice(-2);
    let endDate =
      toDate.getFullYear() +
      "/" +
      ("0" + (toDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + toDate.getDate()).slice(-2);
    const credentials = {
      page: newPage,
      pagesize: rowsPerPage,
      search,
      status,
      from: startDate,
      to: endDate,
    };
    dispatch(getAgentsCommissionsHelper(credentials));
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    let fromDate = new Date(from);
    let toDate = new Date(to);
    let startDate =
      fromDate.getFullYear() +
      "/" +
      ("0" + (fromDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + fromDate.getDate()).slice(-2);
    let endDate =
      toDate.getFullYear() +
      "/" +
      ("0" + (toDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + toDate.getDate()).slice(-2);
    const credentials = {
      page: page,
      pagesize: event.target.value,
      search,
      status,
      from: startDate,
      to: endDate,
    };
    dispatch(getAgentsCommissionsHelper(credentials));
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFromChange = (e) => setFrom(e.target.value);
  const handleToChange = (e) => setTo(e.target.value);
  const handleSearchChange = (e) => setSearch(e.target.value);
  const handleStatusChange = (e) => setStatus(e.target.value);
  const exportFile = async () => {
    const credentials = {
      page: 0,
      pagesize: commissions.paging["total-record"],
      search,
      status,
      from,
      to,
    };
    const res = await dispatch(getAllAgentsCommissionsHelper(credentials));
    if (res.payload) {
      let data = res.payload.data.body;
      data.unshift(res.payload.data.header);
      let wb = XLSX.utils.book_new(); // create workbook
      let ws = XLSX.utils.aoa_to_sheet(data); // create worksheet
      let ws_name = "worksheet";
      XLSX.utils.book_append_sheet(wb, ws, ws_name); // add worksheet to workbook
      XLSX.writeFile(wb, "Agents commissions.xlsx"); // write xls to file
    }
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const handleSearch = (e) => {
    e.preventDefault();
    let fromDate = new Date(from);
    let toDate = new Date(to);
    let startDate =
      fromDate.getFullYear() +
      "/" +
      ("0" + (fromDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + fromDate.getDate()).slice(-2);
    let endDate =
      toDate.getFullYear() +
      "/" +
      ("0" + (toDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + toDate.getDate()).slice(-2);
    const credentials = {
      page: page,
      pagesize: rowsPerPage,
      search,
      status,
      from: startDate,
      to: endDate,
    };
    dispatch(getAgentsCommissionsHelper(credentials));
    setSearch(search);
  };
  const handleKeyPress = (event) => {
    let fromDate = new Date(from);
    let toDate = new Date(to);
    let startDate =
      fromDate.getFullYear() +
      "/" +
      ("0" + (fromDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + fromDate.getDate()).slice(-2);
    let endDate =
      toDate.getFullYear() +
      "/" +
      ("0" + (toDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + toDate.getDate()).slice(-2);
    const credentials = {
      page: page,
      pagesize: rowsPerPage,
      search,
      status,
      from: startDate,
      to: endDate,
    };
    if (event.keyCode === 13) {
      dispatch(getAgentsCommissionsHelper(credentials));
    }
  };
  const newColumns =
    commissions.header &&
    commissions.header.map((dat, i) => {
      return dat === "ID"
        ? {
            name: dat,
            options: {
              display: false,
            },
          }
        : dat === "Agent"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 130,
                    }}
                  >
                    {capitalizeFirstLetters(dataIndex)}
                  </Text>
                );
              },
            },
          }
        : dat === "Branch"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 100,
                    }}
                  >
                    {capitalizeFirstLetters(dataIndex)}
                  </Text>
                );
              },
            },
          }
        : dat === "Account number"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 90,
                    }}
                  >
                    {capitalizeFirstLetters(dataIndex)}
                  </Text>
                );
              },
            },
          }
        : dat === "Region"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 120,
                    }}
                  >
                    {capitalizeFirstLetters(dataIndex)}
                  </Text>
                );
              },
            },
          }
        : dat === "Merchant ID"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 100,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : dat === "Amount (₦)"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 80,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : dat === "Commission (₦)"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 85,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : {
            name: removeUnderscore(dat),
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      minWidth: 120,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          };
    });

  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: false,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    customToolbar: () => (
      <ButtonsRow className={classes.buttonRow}>
        <SubmitButton
          onClick={handleClickOpen}
          className={classes.fabIcon}
          style={{ marginRight: 15 }}
        >
          <FilterListOutlinedIcon className={classes.icon} />
          Filters
        </SubmitButton>
        <SubmitButton
          onClick={exportFile}
          className={classes.fabIcon}
          disabled={isLoadingData}
          style={{ marginTop: 0 }}
        >
          {isLoadingData ? (
            <Loader
              type="ThreeDots"
              color={Colors.secondary}
              height="30"
              width="20"
            />
          ) : (
            <>
              {" "}
              <CloudDownloadOutlinedIcon className={classes.icon} />
              Download excel
            </>
          )}
        </SubmitButton>
      </ButtonsRow>
    ),
    textLabels: {
      body: {
        noMatch: "No Agents commissions records found",
      },
    },
  };
  return (
    <div className={classes.tableCard}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!loading ? (
            <MuiThemeProvider theme={wideTable}>
              <MUIDataTable
                data={commissions && commissions ? commissions.body : []}
                columns={newColumns}
                options={options}
                title={
                  showButton ? (
                    <Grid container spacing={0}>
                      <Grid item xs={12} sm={3} md={3} lg={3}>
                        <Search
                          id="search"
                          htmlFor="search"
                          name="search"
                          value={search}
                          placeholder="search merchant ID, agent"
                          searchlabel="Search"
                          onChange={handleSearchChange}
                          onKeyUp={handleKeyPress}
                          onClickIcon={handleSearch}
                        />
                      </Grid>
                      <Grid item xs={12} sm={3} md={3} lg={3}>
                        <Select
                          id="status"
                          htmlFor="status"
                          name="status"
                          placeholder="status"
                          value={status}
                          label="Response status"
                          onChange={handleStatusChange}
                          onKeyUp={handleKeyPress}
                          grouped
                        >
                          <option value="all">All</option>
                          <option value="declined">Declined</option>
                          <option value="approved">Approved</option>
                        </Select>
                      </Grid>
                      <Grid item xs={12} sm={3} md={3} lg={3}>
                        <TextField
                          id="from"
                          htmlFor="from"
                          name="from"
                          type={type}
                          placeholder="from"
                          onFocus={onFocus}
                          onBlur={onBlur}
                          value={from}
                          label="Start date"
                          onChange={handleFromChange}
                          onKeyUp={handleKeyPress}
                          grouped
                        />
                      </Grid>
                      <Grid item xs={12} sm={3} md={3} lg={3}>
                        <TextField
                          id="to"
                          htmlFor="to"
                          name="to"
                          type={toType}
                          placeholder="To"
                          onFocus={toOnFocus}
                          onBlur={toOnBlur}
                          value={to}
                          label="End date"
                          onChange={handleToChange}
                          onKeyUp={handleKeyPress}
                          grouped
                        />
                      </Grid>
                    </Grid>
                  ) : (
                    <div className={classes.tableLogo}>
                      <img
                        src={jaiz}
                        alt="jaiz logo"
                        style={{ width: 20, height: 20, marginRight: "1rem" }}
                      />
                      Agents commissions
                    </div>
                  )
                }
              />
              <TablePagination
                component="div"
                count={
                  commissions.paging && commissions.paging["total-record"]
                    ? commissions.paging["total-record"]
                    : 0
                }
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={rowsPerPage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                rowsPerPageOptions={[5, 10, 15, 20]}
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={transErrorMessage}
        isOpen={transError}
      />
    </div>
  );
}
