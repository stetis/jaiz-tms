import { makeStyles } from "@material-ui/core";
import { Colors, Fonts } from "../../assets/themes/theme";

export const styles = makeStyles(() => ({
  tableCard: {
    width: "95%",
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 5px 25px",
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 960px)": {
      width: "100%",
    },
  },
  performanceCard: {
    width: "95%",
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 5px 25px",
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 960px)": {
      width: "75%",
    },
  },
  tableLogo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
    "@media only screen and (max-width: 540px)": {
      marginTop: -25,
    },
  },
  buttonRow: {
    "@media only screen and (max-width: 540px)": {
      position: "absolute",
      zIndex: 1000,
      top: 0,
    },
  },
  logo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
  },
  root: {
    width: 320,
    display: "flex",
    flexGrow: 1,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 480px)": {
      width: 350,
    },
  },
  container: {
    display: "flex",
    padding: "8px 25px 20px",
    flexWrap: "wrap",
  },
  fabIcon: {
    color: Colors.secondary,
    fontWeight: 600,
  },
  icon: {
    fontSize: 18,
    marginRight: 5,
  },
}));
