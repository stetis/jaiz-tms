import { Grid, MuiThemeProvider } from "@material-ui/core";
import TablePagination from "@material-ui/core/TablePagination";
import CloudDownloadOutlinedIcon from "@material-ui/icons/CloudDownloadOutlined";
import FilterListOutlinedIcon from "@material-ui/icons/FilterListOutlined";
import MUIDataTable from "mui-datatables";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import XLSX from "xlsx";
import { updateBreadcrumb } from "../../actions/breadcrumbAction";
import { clearMessages } from "../../actions/transactions.actions";
import { wideTable } from "../../assets/MUI/Table";
import Snackbars from "../../assets/Snackbars/index";
import Spinner from "../../assets/Spinner";
import { Colors } from "../../assets/themes/theme";
import { SubmitButton } from "../../components/Buttons";
import { ButtonsRow } from "../../components/Buttons/styles";
import { Text } from "../../components/Forms";
import { Select, TextField } from "../../components/TextField";
import {
  getAllMerchantPerformanceHelper,
  getMerchantPerformanceHelper,
} from "../../helpers/transactions.helper";
import jaiz from "../../images/jaiz-logo.png";
import { styles } from "./styles";

const styleProps = {
  height: 50,
  width: 50,
};
function capitalizeFirstLetters(str) {
  var splitStr = str.toLowerCase().split(" ");
  for (var i = 0; i < splitStr.length; i++) {
    // You do not need to check if i is larger than splitStr length, as your for does that for you
    // Assign it back to the array
    splitStr[i] =
      splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  // Directly return the joined string
  return splitStr.join(" ");
}
export default function MerchantsPerformanceTable() {
  const classes = styles();
  let today = new Date();
  let startDate =
    today.getFullYear() +
    "/" +
    ("0" + (today.getMonth() - 5)).slice(-2) +
    "/" +
    ("0" + today.getDate()).slice(-2);
  let endDate =
    today.getFullYear() +
    "/" +
    ("0" + (today.getMonth() + 1)).slice(-2) +
    "/" +
    ("0" + today.getDate()).slice(-2);
  const [showButton, setShowButton] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [sort, setSort] = useState("default");
  const [from, setFrom] = useState(startDate);
  const [to, setTo] = useState(endDate);
  const [type, setType] = useState("text");
  const [toType, setToType] = useState("text");
  const toOnFocus = () => setToType("date");
  const toOnBlur = () => setToType("text");
  const onFocus = () => setType("date");
  const onBlur = () => setType("text");

  const dispatch = useDispatch();
  const performances = useSelector(
    (state) => state.transactionsReducer.merchantPerformanceData
  );
  const loading = useSelector(
    (state) => state.transactionsReducer.merchantPerformanceLoading
  );
  const error = useSelector(
    (state) => state.transactionsReducer.merchantPerformanceError
  );
  const errorMessage = useSelector(
    (state) => state.transactionsReducer.merchantPerformanceErrorMessage
  );
  const isLoadingData = useSelector(
    (state) => state.transactionsReducer.transactionsLoading
  );
  const transError = useSelector(
    (state) => state.transactionsReducer.transactionsError
  );
  const transErrorMessage = useSelector(
    (state) => state.transactionsReducer.transactionsErrorMessage
  );
  useEffect(() => {
    document.title = "Merchant performance | Jaiz bank TMS";
    dispatch(
      updateBreadcrumb({
        parent: "Merchant performance",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );
    const credentials = {
      page,
      pagesize: rowsPerPage,
      from: startDate,
      to: endDate,
      sort,
    };
    dispatch(getMerchantPerformanceHelper(credentials));
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const handleChangePage = (event, newPage) => {
    const credentials = {
      page: newPage,
      pagesize: rowsPerPage,
      from,
      to,
    };
    dispatch(getMerchantPerformanceHelper(credentials));
    setPage(newPage);
    setRowsPerPage(rowsPerPage);
  };
  const handleChangeRowsPerPage = (event) => {
    let fromDate = new Date(from);
    let toDate = new Date(to);
    let startDate =
      fromDate.getFullYear() +
      "/" +
      ("0" + (fromDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + fromDate.getDate()).slice(-2);
    let endDate =
      toDate.getFullYear() +
      "/" +
      ("0" + (toDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + toDate.getDate()).slice(-2);
    const credentials = {
      page: page,
      pagesize: event.target.value,
      from: startDate,
      to: endDate,
    };
    dispatch(getMerchantPerformanceHelper(credentials));
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleSortChange = (e) => setSort(e.target.value);
  const handleFromChange = (e) => setFrom(e.target.value);
  const handleToChange = (e) => setTo(e.target.value);
  const handleKeyPress = (event) => {
    let fromDate = new Date(from);
    let toDate = new Date(to);
    let startDate =
      fromDate.getFullYear() +
      "/" +
      ("0" + (fromDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + fromDate.getDate()).slice(-2);
    let endDate =
      toDate.getFullYear() +
      "/" +
      ("0" + (toDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + toDate.getDate()).slice(-2);
    const credentials = {
      page,
      pagesize: rowsPerPage,
      sort,
      from: startDate,
      to: endDate,
    };
    if (event.keyCode === 13) {
      dispatch(getMerchantPerformanceHelper(credentials));
    }
  };
  const handleClickOpen = (e) => {
    e.preventDefault();
    setShowButton(!showButton);
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const exportFile = async () => {
    const credentials = {
      page: 0,
      pagesize: performances.paging["total-record"],
      from,
      to,
    };
    const res = await dispatch(getAllMerchantPerformanceHelper(credentials));
    if (res.payload) {
      let data = res.payload.data.body;
      data.unshift(res.payload.data.header);
      let wb = XLSX.utils.book_new(); // create workbook
      let ws = XLSX.utils.aoa_to_sheet(data); // create worksheet
      let ws_name = "worksheet";
      XLSX.utils.book_append_sheet(wb, ws, ws_name); // add worksheet to workbook
      XLSX.writeFile(wb, "Merchant performance.xlsx"); // write xls to file
    }
  };
  const newColumns =
    performances.header &&
    performances.header.map((dat, i) => {
      return dat === "Merchant"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return <Text>{capitalizeFirstLetters(dataIndex)}</Text>;
              },
            },
          }
        : dat === "Transaction volume (₦)"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return <Text>{dataIndex}</Text>;
              },
            },
          }
        : dat === "Transaction count"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return <Text>{dataIndex}</Text>;
              },
            },
          }
        : dat === "Merchant ID"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return <Text>{capitalizeFirstLetters(dataIndex)}</Text>;
              },
            },
          }
        : dat;
    });
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: false,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    customToolbar: () => (
      <ButtonsRow className={classes.buttonRow}>
        <SubmitButton
          onClick={handleClickOpen}
          className={classes.fabIcon}
          style={{ marginRight: 15 }}
        >
          <FilterListOutlinedIcon className={classes.icon} />
          Filters
        </SubmitButton>
        <SubmitButton
          onClick={exportFile}
          className={classes.fabIcon}
          disabled={isLoadingData}
          style={{ marginTop: 0 }}
        >
          {isLoadingData ? (
            <Loader
              type="ThreeDots"
              color={Colors.secondary}
              height="30"
              width="20"
            />
          ) : (
            <>
              <CloudDownloadOutlinedIcon className={classes.icon} />
              Download excel
            </>
          )}
        </SubmitButton>
      </ButtonsRow>
    ),
    textLabels: {
      body: {
        noMatch: "No merchants performance records found",
      },
    },
  };
  return (
    <div className={classes.performanceCard}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!loading ? (
            <MuiThemeProvider theme={wideTable}>
              <MUIDataTable
                data={performances && performances ? performances.body : []}
                columns={newColumns}
                options={options}
                title={
                  showButton ? (
                    <Grid container spacing={0}>
                      <Grid item xs={12} sm={4} md={4} lg={4}>
                        <Select
                          id="sort"
                          htmlFor="sort"
                          name="sort"
                          placeholder="sort"
                          value={sort}
                          label="Sort table"
                          onChange={handleSortChange}
                          onKeyUp={handleKeyPress}
                        >
                          <option value="default">Default</option>
                          <option value="ascending">Ascending order</option>
                          <option value="desceding">Descending order</option>
                        </Select>
                      </Grid>
                      <Grid item xs={12} sm={4} md={4} lg={4}>
                        <TextField
                          id="from"
                          htmlFor="from"
                          name="from"
                          type={type}
                          placeholder="from"
                          onFocus={onFocus}
                          onBlur={onBlur}
                          value={from}
                          label="Start"
                          onChange={handleFromChange}
                          onKeyUp={handleKeyPress}
                          grouped
                        />
                      </Grid>
                      <Grid item xs={12} sm={4} md={4} lg={4}>
                        <TextField
                          id="to"
                          htmlFor="to"
                          name="to"
                          type={toType}
                          placeholder="To"
                          onFocus={toOnFocus}
                          onBlur={toOnBlur}
                          value={to}
                          label="End date"
                          onChange={handleToChange}
                          onKeyUp={handleKeyPress}
                          grouped
                        />
                      </Grid>
                    </Grid>
                  ) : (
                    <div className={classes.tableLogo}>
                      <img
                        src={jaiz}
                        alt="jaiz logo"
                        style={{ width: 20, height: 20, marginRight: "1rem" }}
                      />
                      Merchants performances
                    </div>
                  )
                }
              />
              <TablePagination
                component="div"
                count={
                  performances.paging && performances.paging["total-record"]
                    ? performances.paging["total-record"]
                    : 0
                }
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={rowsPerPage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                rowsPerPageOptions={[5, 10]}
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={transErrorMessage}
        isOpen={transError}
      />
    </div>
  );
}
