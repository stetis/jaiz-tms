import { Grid, MuiThemeProvider } from "@material-ui/core";
import TablePagination from "@material-ui/core/TablePagination";
import CloudDownloadOutlinedIcon from "@material-ui/icons/CloudDownloadOutlined";
import MUIDataTable from "mui-datatables";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import XLSX from "xlsx";
import { updateBreadcrumb } from "../../actions/breadcrumbAction";
import { clearMessages } from "../../actions/transactions.actions";
import { wideTable } from "../../assets/MUI/Table";
import Snackbars from "../../assets/Snackbars/index";
import Spinner from "../../assets/Spinner";
import { Colors } from "../../assets/themes/theme";
import { SubmitButton } from "../../components/Buttons";
import { ButtonsRow } from "../../components/Buttons/styles";
import { Text } from "../../components/Forms";
import {
  getAllBranchAnalyticsHelper,
  getBranchAnalyticsHelper,
} from "../../helpers/transactions.helper";
import jaiz from "../../images/jaiz-logo.png";
import { styles } from "./styles";

const styleProps = {
  height: 50,
  width: 50,
};

function capitalizeFirstLetters(str) {
  var splitStr = str.toLowerCase().split(" ");
  for (var i = 0; i < splitStr.length; i++) {
    // You do not need to check if i is larger than splitStr length, as your for does that for you
    // Assign it back to the array
    splitStr[i] =
      splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  // Directly return the joined string
  return splitStr.join(" ");
}
function removeUnderscore(str) {
  if (typeof str !== "string") return "";
  const regex = /_/g;
  const newStr = capitalizeFirstLetters(str);
  const update = newStr.replace(regex, " ");
  return update;
}
export default function BranchAnalyticsTable() {
  const classes = styles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const dispatch = useDispatch();
  const branchAnalytics = useSelector(
    (state) => state.transactionsReducer.branchAnalyticsData
  );
  const loading = useSelector(
    (state) => state.transactionsReducer.branchAnalyticsLoading
  );
  const error = useSelector(
    (state) => state.transactionsReducer.branchAnalyticsError
  );
  const errorMessage = useSelector(
    (state) => state.transactionsReducer.branchAnalyticsErrorMessage
  );
  const isLoadingData = useSelector(
    (state) => state.transactionsReducer.transactionsLoading
  );

  const transError = useSelector(
    (state) => state.transactionsReducer.transactionsError
  );
  const transErrorMessage = useSelector(
    (state) => state.transactionsReducer.transactionsErrorMessage
  );

  useEffect(() => {
    document.title = "Branch analytics | Jaiz bank TMS";
    dispatch(
      updateBreadcrumb({
        parent: "Branch analytics",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );
    dispatch(getBranchAnalyticsHelper());
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const exportFile = async () => {
    const res = await dispatch(getAllBranchAnalyticsHelper());
    if (res.payload) {
      let data = res.payload.data.body;
      data.unshift(res.payload.data.header);
      let wb = XLSX.utils.book_new(); // create workbook
      let ws = XLSX.utils.aoa_to_sheet(data); // create worksheet
      let ws_name = "worksheet";
      XLSX.utils.book_append_sheet(wb, ws, ws_name); // add worksheet to workbook
      XLSX.writeFile(wb, "Branch analytics.xlsx"); // write xls to file
    }
  };

  const handleCloseSnack = () => dispatch(clearMessages());
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const newColumns =
    branchAnalytics.header &&
    branchAnalytics.header.map((dat, i) => {
      return dat === "Region"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 120,
                    }}
                  >
                    {capitalizeFirstLetters(dataIndex)}
                  </Text>
                );
              },
            },
          }
        : dat === "Branch"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 120,
                    }}
                  >
                    {capitalizeFirstLetters(dataIndex)}
                  </Text>
                );
              },
            },
          }
        : dat === "Branch code"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 70,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : removeUnderscore(dat);
    });

  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: false,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    customToolbar: () => (
      <ButtonsRow style={{ marginTop: 0 }}>
        <SubmitButton
          onClick={exportFile}
          className={classes.fabIcon}
          disabled={isLoadingData}
          style={{ marginTop: 0 }}
        >
          {isLoadingData ? (
            <Loader
              type="ThreeDots"
              color={Colors.secondary}
              height="30"
              width="20"
            />
          ) : (
            <>
              <CloudDownloadOutlinedIcon className={classes.icon} />
              Download excel
            </>
          )}
        </SubmitButton>
      </ButtonsRow>
    ),
    textLabels: {
      body: {
        noMatch: "No branch analytics records found",
      },
    },
  };
  return (
    <div className={classes.tableCard}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!loading ? (
            <MuiThemeProvider theme={wideTable}>
              <MUIDataTable
                data={
                  branchAnalytics.body && branchAnalytics.body
                    ? branchAnalytics.body.slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )
                    : []
                }
                title={
                  <div className={classes.tableLogo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Branch analytics
                  </div>
                }
                columns={newColumns}
                options={options}
              />
              <TablePagination
                component="div"
                count={
                  branchAnalytics.body && branchAnalytics.body.length
                    ? branchAnalytics.body.length
                    : 0
                }
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={rowsPerPage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                rowsPerPageOptions={[5, 10]}
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={transErrorMessage}
        isOpen={transError}
      />
    </div>
  );
}
