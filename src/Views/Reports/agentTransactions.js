import { Grid, MuiThemeProvider } from "@material-ui/core";
import TablePagination from "@material-ui/core/TablePagination";
import CloudDownloadOutlinedIcon from "@material-ui/icons/CloudDownloadOutlined";
import FilterListOutlinedIcon from "@material-ui/icons/FilterListOutlined";
import MUIDataTable from "mui-datatables";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import ArrowDropDownOutlinedIcon from "@material-ui/icons/ArrowDropDownOutlined";
import { withRouter } from "react-router";
import XLSX from "xlsx";
import { updateBreadcrumb } from "../../actions/breadcrumbAction";
import { clearMessages } from "../../actions/transactions.actions";
import { wideTable } from "../../assets/MUI/Table";
import Snackbars from "../../assets/Snackbars/index";
import Spinner from "../../assets/Spinner";
import { Colors } from "../../assets/themes/theme";
import { SubmitButton } from "../../components/Buttons";
import { ButtonsRow } from "../../components/Buttons/styles";
import { Text } from "../../components/Forms";
import { Select, TextField } from "../../components/TextField";
import Search from "../../components/TextField/search";
import { getBranchesHelper } from "../../helpers/Configurations/branches.helper";
import {
  getAgentsTransactionsHelper,
  getAllAgentsTransactionsHelper,
} from "../../helpers/transactions.helper";
import jaiz from "../../images/jaiz-logo.png";
import { styles } from "./styles";
import { AutoComplete } from "../../components/TextField/autotcomplete/autocomplete";

const styleProps = {
  height: 50,
  width: 50,
};
function capitalizeFirstLetters(str) {
  var splitStr = str.toLowerCase().split(" ");
  for (var i = 0; i < splitStr.length; i++) {
    // You do not need to check if i is larger than splitStr length, as your for does that for you
    // Assign it back to the array
    splitStr[i] =
      splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  // Directly return the joined string
  return splitStr.join(" ");
}
function removeUnderscore(str) {
  if (typeof str !== "string") return "";
  const regex = /_/g;
  const newStr = capitalizeFirstLetters(str);
  const update = newStr.replace(regex, " ");
  return update;
}
function AgentsTransactionsTable() {
  const classes = styles();
  let today = new Date();
  let startDate =
    today.getFullYear() +
    "/" +
    ("0" + (today.getMonth() - 5)).slice(-2) +
    "/" +
    ("0" + today.getDate()).slice(-2);
  let endDate =
    today.getFullYear() +
    "/" +
    ("0" + (today.getMonth() + 1)).slice(-2) +
    "/" +
    ("0" + today.getDate()).slice(-2);
  const [branch, setBranch] = useState({
    text: "",
    suggestions: [],
  });
  const [isComponentVisible, setIsComponentVisible] = useState(true);

  const [showButton, setShowButton] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [search, setSearch] = useState("");
  const [status, setStatus] = useState("all");
  const [from, setFrom] = useState(startDate);
  const [to, setTo] = useState(endDate);
  const [type, setType] = useState("text");
  const [toType, setToType] = useState("text");
  const toOnFocus = () => setToType("date");
  const toOnBlur = () => setToType("text");
  const onFocus = () => setType("date");
  const onBlur = () => setType("text");
  const dispatch = useDispatch();
  const branches = useSelector((state) => state.branchesReducer.data.branches);
  const transactions = useSelector(
    (state) => state.transactionsReducer.agentData
  );
  const loading = useSelector(
    (state) => state.transactionsReducer.agentLoading
  );
  const error = useSelector((state) => state.transactionsReducer.agentError);
  const errorMessage = useSelector(
    (state) => state.transactionsReducer.agentErrorMessage
  );
  const isLoadingData = useSelector(
    (state) => state.transactionsReducer.transactionsLoading
  );

  const transError = useSelector(
    (state) => state.transactionsReducer.transactionsError
  );
  const transErrorMessage = useSelector(
    (state) => state.transactionsReducer.transactionsErrorMessage
  );
  useEffect(() => {
    document.title = "Agents transactions | Jaiz bank TMS";
    dispatch(
      updateBreadcrumb({
        parent: "Agents transactions",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );
    const credentials = {
      page,
      pagesize: rowsPerPage,
      search: search,
      status,
      branch: "",
      from: startDate,
      to: endDate,
    };
    dispatch(getBranchesHelper());
    dispatch(getAgentsTransactionsHelper(credentials));
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleChangePage = (event, newPage) => {
    const branchDetail = branches.find((item) => item.name === branch.text);
    let fromDate = new Date(from);
    let toDate = new Date(to);
    let startDate =
      fromDate.getFullYear() +
      "/" +
      ("0" + (fromDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + fromDate.getDate()).slice(-2);
    let endDate =
      toDate.getFullYear() +
      "/" +
      ("0" + (toDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + toDate.getDate()).slice(-2);
    const credentials = {
      page: newPage,
      pagesize: rowsPerPage,
      search,
      status,
      branch: branchDetail ? branchDetail.id : "",
      from: startDate,
      to: endDate,
    };
    dispatch(getAgentsTransactionsHelper(credentials));
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    const branchDetail = branches.find((item) => item.name === branch.text);

    let fromDate = new Date(from);
    let toDate = new Date(to);
    let startDate =
      fromDate.getFullYear() +
      "/" +
      ("0" + (fromDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + fromDate.getDate()).slice(-2);
    let endDate =
      toDate.getFullYear() +
      "/" +
      ("0" + (toDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + toDate.getDate()).slice(-2);
    const credentials = {
      page,
      pagesize: event.target.value,
      search,
      status,
      branch: branchDetail,
      from: startDate,
      to: endDate,
    };
    dispatch(getAgentsTransactionsHelper(credentials));
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleClickOpen = (e) => {
    e.preventDefault();
    setShowButton(!showButton);
  };
  const handleFromChange = (e) => setFrom(e.target.value);
  const handleToChange = (e) => setTo(e.target.value);
  const handleSearchChange = (e) => setSearch(e.target.value);
  const handleStatusChange = (e) => setStatus(e.target.value);
  const onTextChanged = (e) => {
    const value = e.target.value;
    let suggestions = [];
    if (value.length > 0) {
      const regex = new RegExp(`^${value}`, "i");
      suggestions =
        branches && branches.length !== 0
          ? branches.sort().filter((branch) => regex.test(branch.name))
          : [];
    }
    setIsComponentVisible(true);
    setBranch({ suggestions, text: value });
  };
  const suggestionSelected = (value) => {
    setIsComponentVisible(false);
    setBranch({
      text: value.name,
      suggestions: [],
    });
  };
  const exportFile = async () => {
    const branchDetail = branches.find((item) => item.name === branch.text);
    const credentials = {
      page: 0,
      pagesize: transactions.paging["total-record"],
      search,
      branch: branchDetail ? branchDetail.id : "",
      status,
      from,
      to,
    };
    const res = await dispatch(getAllAgentsTransactionsHelper(credentials));
    if (res.payload) {
      let data = res.payload.data.body;
      data.unshift(res.payload.data.header);
      let wb = XLSX.utils.book_new(); // create workbook
      let ws = XLSX.utils.aoa_to_sheet(data); // create worksheet
      let ws_name = "worksheet";
      XLSX.utils.book_append_sheet(wb, ws, ws_name); // add worksheet to workbook
      XLSX.writeFile(wb, "Agents transactions.xlsx"); // write xls to file
    }
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const handleSearch = (e) => {
    e.preventDefault();
    const branchDetail = branches.find((item) => item.name === branch.text);
    let fromDate = new Date(from);
    let toDate = new Date(to);
    let startDate =
      fromDate.getFullYear() +
      "/" +
      ("0" + (fromDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + fromDate.getDate()).slice(-2);
    let endDate =
      toDate.getFullYear() +
      "/" +
      ("0" + (toDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + toDate.getDate()).slice(-2);
    const credentials = {
      page,
      pagesize: rowsPerPage,
      search,
      status,
      branch: branchDetail ? branchDetail.id : "",
      from: startDate,
      to: endDate,
    };
    dispatch(getAgentsTransactionsHelper(credentials));
    setSearch(search);
  };
  const handleKeyPress = (event) => {
    const branchDetail = branches.find((item) => item.name === branch.text);
    let fromDate = new Date(from);
    let toDate = new Date(to);
    let startDate =
      fromDate.getFullYear() +
      "/" +
      ("0" + (fromDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + fromDate.getDate()).slice(-2);
    let endDate =
      toDate.getFullYear() +
      "/" +
      ("0" + (toDate.getMonth() + 1)).slice(-2) +
      "/" +
      ("0" + toDate.getDate()).slice(-2);
    const credentials = {
      page,
      pagesize: rowsPerPage,
      search,
      status,
      branch: branchDetail ? branchDetail.id : "",
      from: startDate,
      to: endDate,
    };
    if (event.keyCode === 13) {
      dispatch(getAgentsTransactionsHelper(credentials));
    }
  };
  const { suggestions } = branch;
  const newColumns =
    transactions.header &&
    transactions.header.map((dat, i) => {
      return dat === "ID"
        ? {
            name: dat,
            options: {
              display: false,
            },
          }
        : dat === "Agent"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 190,
                    }}
                  >
                    {capitalizeFirstLetters(dataIndex)}
                  </Text>
                );
              },
            },
          }
        : dat === "Branch"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 100,
                    }}
                  >
                    {capitalizeFirstLetters(dataIndex)}
                  </Text>
                );
              },
            },
          }
        : dat === "Amount (₦)"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 80,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : dat === "Date"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 70,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : dat === "Region"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 120,
                    }}
                  >
                    {capitalizeFirstLetters(dataIndex)}
                  </Text>
                );
              },
            },
          }
        : dat === "Terminal ID"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 100,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : dat === "Type"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 150,
                    }}
                  >
                    {removeUnderscore(dataIndex)}
                  </Text>
                );
              },
            },
          }
        : dat === "Response code"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 80,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 120,
                    }}
                  >
                    {removeUnderscore(dataIndex)}
                  </Text>
                );
              },
            },
          };
    });
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: false,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    customToolbar: () => (
      <ButtonsRow className={classes.buttonRow}>
        <SubmitButton
          onClick={handleClickOpen}
          className={classes.fabIcon}
          style={{ marginRight: 15 }}
        >
          <FilterListOutlinedIcon className={classes.icon} />
          Filters
        </SubmitButton>
        <SubmitButton
          onClick={exportFile}
          className={classes.fabIcon}
          disabled={isLoadingData}
          style={{ marginTop: 0 }}
        >
          {isLoadingData ? (
            <Loader
              type="ThreeDots"
              color={Colors.secondary}
              height="30"
              width="20"
            />
          ) : (
            <>
              {" "}
              <CloudDownloadOutlinedIcon className={classes.icon} />
              Download excel
            </>
          )}
        </SubmitButton>
      </ButtonsRow>
    ),
    textLabels: {
      body: {
        noMatch: "No Agents transactions records found",
      },
    },
  };
  return (
    <div className={classes.tableCard}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!loading ? (
            <MuiThemeProvider theme={wideTable}>
              <MUIDataTable
                data={transactions && transactions ? transactions.body : []}
                columns={newColumns}
                options={options}
                title={
                  showButton ? (
                    <Grid container spacing={0}>
                      <Grid item xs={12} sm={2} md={2} lg={2}>
                        <Search
                          id="search"
                          htmlFor="search"
                          name="search"
                          value={search}
                          placeholder="search terminal ID, agent"
                          searchlabel="Search"
                          onChange={handleSearchChange}
                          onKeyUp={handleKeyPress}
                          onClickIcon={handleSearch}
                        />
                      </Grid>
                      <Grid item xs={12} sm={2} md={2} lg={2}>
                        <Select
                          id="status"
                          htmlFor="status"
                          name="status"
                          placeholder="status"
                          value={status}
                          label="Response status"
                          onChange={handleStatusChange}
                          onKeyUp={handleKeyPress}
                          grouped
                        >
                          <option value="all">All</option>
                          <option value="declined">Declined</option>
                          <option value="approved">Approved</option>
                        </Select>
                      </Grid>
                      <Grid item xs={12} sm={2} md={2} lg={2}>
                        <AutoComplete
                          id="branch"
                          htmlFor="branch"
                          label="Branch"
                          onChange={onTextChanged}
                          value={branch.text}
                          setIsComponentVisible={setIsComponentVisible}
                          suggestions={suggestions}
                          isComponentVisible={isComponentVisible}
                          suggestionSelected={suggestionSelected}
                          Icon={
                            <ArrowDropDownOutlinedIcon
                              style={{ fontSize: 34 }}
                            />
                          }
                          grouped
                          onKeyUp={handleKeyPress}
                        />
                      </Grid>
                      <Grid item xs={12} sm={3} md={3} lg={3}>
                        <TextField
                          id="from"
                          htmlFor="from"
                          name="from"
                          type={type}
                          placeholder="from"
                          onFocus={onFocus}
                          onBlur={onBlur}
                          value={from}
                          label="Start date"
                          onChange={handleFromChange}
                          onKeyUp={handleKeyPress}
                          grouped
                        />
                      </Grid>
                      <Grid item xs={12} sm={3} md={3} lg={3}>
                        <TextField
                          id="to"
                          htmlFor="to"
                          name="to"
                          type={toType}
                          placeholder="To"
                          onFocus={toOnFocus}
                          onBlur={toOnBlur}
                          value={to}
                          label="End date"
                          onChange={handleToChange}
                          onKeyUp={handleKeyPress}
                          grouped
                        />
                      </Grid>
                    </Grid>
                  ) : (
                    <div className={classes.tableLogo}>
                      <img
                        src={jaiz}
                        alt="jaiz logo"
                        style={{ width: 20, height: 20, marginRight: "1rem" }}
                      />
                      Agents transactions
                    </div>
                  )
                }
              />
              <TablePagination
                component="div"
                count={
                  transactions.paging && transactions.paging["total-record"]
                    ? transactions.paging["total-record"]
                    : 0
                }
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={rowsPerPage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                rowsPerPageOptions={[5, 10, 15, 20]}
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={transErrorMessage}
        isOpen={transError}
      />
    </div>
  );
}
export default withRouter(AgentsTransactionsTable);
