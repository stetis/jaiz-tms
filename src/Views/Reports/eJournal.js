import { Grid, MuiThemeProvider } from "@material-ui/core";
import TablePagination from "@material-ui/core/TablePagination";
import MUIDataTable from "mui-datatables";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../actions/breadcrumbAction";
import { clearMessages } from "../../actions/transactions.actions";
import { wideTable } from "../../assets/MUI/Table";
import Snackbars from "../../assets/Snackbars/index";
import Spinner from "../../assets/Spinner";
import { Text } from "../../components/Forms";
import { getEjournalHelper } from "../../helpers/transactions.helper";
import jaiz from "../../images/jaiz-logo.png";
import { styles } from "./styles";
const styleProps = {
  height: 50,
  width: 50,
};

function capitalizeFirstLetters(str) {
  var splitStr = str.toLowerCase().split(" ");
  for (var i = 0; i < splitStr.length; i++) {
    // You do not need to check if i is larger than splitStr length, as your for does that for you
    // Assign it back to the array
    splitStr[i] =
      splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  // Directly return the joined string
  return splitStr.join(" ");
}
function removeUnderscore(str) {
  if (typeof str !== "string") return "";
  const regex = /_/g;
  const newStr = capitalizeFirstLetters(str);
  const update = newStr.replace(regex, " ");
  return update;
}
export default function EJournalTable() {
  const classes = styles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const dispatch = useDispatch();
  const eJournal = useSelector(
    (state) => state.transactionsReducer.eJournalData
  );
  const loading = useSelector(
    (state) => state.transactionsReducer.eJournalLoading
  );
  const error = useSelector((state) => state.transactionsReducer.eJournalError);
  const errorMessage = useSelector(
    (state) => state.transactionsReducer.eJournalErrorMessage
  );
  useEffect(() => {
    document.title = "E-journal | Jaiz bank TMS";
    dispatch(
      updateBreadcrumb({
        parent: "E-journal",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );
    dispatch(getEjournalHelper());
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    eJournal.header && eJournal.header.unshift("");
    eJournal.body && eJournal.body.map((dat, i) => dat.unshift(i + 1));
  }, [eJournal]);
  const handleCloseSnack = () => dispatch(clearMessages());
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const newColumns =
    eJournal.header &&
    eJournal.header.map((dat, i) => {
      return dat === "Region"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 120,
                    }}
                  >
                    {capitalizeFirstLetters(dataIndex)}
                  </Text>
                );
              },
            },
          }
        : dat === "Branch"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 120,
                    }}
                  >
                    {capitalizeFirstLetters(dataIndex)}
                  </Text>
                );
              },
            },
          }
        : dat === "Branch code"
        ? {
            name: dat,
            options: {
              customBodyRender: (dataIndex, rowIndex) => {
                return (
                  <Text
                    style={{
                      width: 70,
                    }}
                  >
                    {dataIndex}
                  </Text>
                );
              },
            },
          }
        : removeUnderscore(dat);
    });

  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: false,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    textLabels: {
      body: {
        noMatch: "No e-journal records found",
      },
    },
  };
  return (
    <div className={classes.tableCard}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!loading ? (
            <MuiThemeProvider theme={wideTable}>
              <MUIDataTable
                data={
                  eJournal.body && eJournal.body
                    ? eJournal.body.slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )
                    : []
                }
                columns={newColumns}
                options={options}
                title={
                  <div className={classes.tableLogo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    E-journal
                  </div>
                }
              />
              <TablePagination
                component="div"
                count={
                  eJournal.body && eJournal.body.length !== 0
                    ? eJournal.body.length
                    : 0
                }
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={rowsPerPage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                rowsPerPageOptions={[5, 10]}
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
    </div>
  );
}
