import { makeStyles, withStyles, Tooltip } from "@material-ui/core";
import { Colors, Fonts } from "../../../assets/themes/theme";

export const LightTooltip = withStyles(() => ({
  tooltip: {
    backgroundColor: Colors.light,
    color: Colors.textColor,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
  },
}))(Tooltip);
export const styles = makeStyles(() => ({
  card: {
    width: 320,
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 5px 25px",
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 580,
    },
  },
  container: {
    display: "flex",
    padding: "15px 25px 25px",
    flexWrap: "wrap",
  },
  addIcon: {
    fontSize: 20,
    color: Colors.secondary,
  },
  bulkFabIcon: {
    position: "absolute",
    top: 19,
    left: 201,
    zIndex: 200,
    "@media only screen and (min-width: 600px)": {
      top: 42,
      left: 395,
    },
  },
  bulkCard: {
    width: "90%",
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "25px 25px 25px",
    borderRadius: 5,
    cursor: "pointer",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: "70%",
    },
    "@media only screen and (min-width: 960px)": {
      width: "45%",
    },
  },

  fabIcon: {
    color: Colors.secondary,
    fontWeight: 600,
  },
  icon: {
    fontSize: 18,
    marginRight: 5,
  },
  tableLogo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
    "@media only screen and (max-width: 540px)": {
      marginTop: -25,
    },
  },
  buttonRow: {
    "@media only screen and (max-width: 540px)": {
      position: "absolute",
      zIndex: 1000,
      top: -3,
      left: -10,
    },
  },
  logo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
  },
  deleteRoot: {
    width: 320,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    display: "flex",
    padding: "12px 18px 20px",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 350,
    },
  },
  actionsRoot: {
    width: 320,
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 350,
    },
  },
  listItemIcon: {
    color: Colors.menuListColor,
    minWidth: 22,
    cursor: "pointer",
  },
  editIcon: {
    margin: "1px 0 0 -10px",
    color: Colors.secondary,
    cursor: "pointer",
  },
  editText: {
    color: Colors.primary,
    margin: "2px 0 0 -10px",
    cursor: "pointer",
  },
  button: {
    width: "100%",
    display: "flex",
    marginLeft: 5,
    marginRight: 5,
    padding: 2,
    cursor: "pointer",
    color: Colors.menuListColor,
    fontSize: Fonts.font,
  },
  DeleteIcon: {
    color: Colors.buttonError,
    margin: "1px 0 0 -10px",
    cursor: "pointer",
  },
  deleteText: {
    color: Colors.buttonError,
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
    margin: "2px 0 0px -10px",
    cursor: "pointer",
  },
}));
