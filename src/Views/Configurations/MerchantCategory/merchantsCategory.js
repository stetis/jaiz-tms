import { Grid } from "@material-ui/core";
import { MuiThemeProvider } from "@material-ui/core/styles";
import TablePagination from "@material-ui/core/TablePagination";
import BackupOutlinedIcon from "@material-ui/icons/BackupOutlined";
import MUIDataTable from "mui-datatables";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import { clearMessages } from "../../../actions/Configurations/merchantCategory.actions";
import { theme } from "../../../assets/MUI/Table";
import Snackbars from "../../../assets/Snackbars/index";
import Spinner from "../../../assets/Spinner";
import { SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import CallOut from "../../../components/CallOut/CallOut";
import { mCatgoryHelper } from "../../../helpers/Configurations/merchant.helper";
import jaiz from "../../../images/jaiz-logo.png";
import DeleteCategory from "./delete";
import EditCategory from "./edit";
import NewCatgory from "./newCategory";
import { styles } from "./styles";

const styleProps = {
  height: 50,
  width: 50,
};
function MerchantsCategory(props) {
  const classes = styles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.categoryReducer.loading);
  const records = useSelector(
    (state) => state.categoryReducer.data && state.categoryReducer.data.records
  );
  const error = useSelector((state) => state.categoryReducer.error);
  const errorMessage = useSelector(
    (state) => state.categoryReducer.errorMessage
  );
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleGotoBulkupload = () => {
    props.history.push("/tms/merchant-categories-bulkupload");
  };
  useEffect(() => {
    document.title = "Merchant categories | Jaiz bank TMS";
    dispatch(mCatgoryHelper());
    dispatch(
      updateBreadcrumb({
        parent: "Merchant category",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const handleCloseSnack = () => {
    dispatch(clearMessages());
  };
  const columns = ["", "Code", "Category", " "];
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    rowsPerPage: rowsPerPage,
    page: page,
    serverSide: true,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    rowsPerPageOptions: [5, 10],
    customToolbar: () => {
      return (
        <ButtonsRow className={classes.buttonRow}>
          <SubmitButton
            onClick={handleGotoBulkupload}
            className={classes.fabIcon}
            style={{ marginRight: 15 }}
          >
            <BackupOutlinedIcon className={classes.icon} />
            Bulk upload
          </SubmitButton>
          <NewCatgory />
        </ButtonsRow>
      );
    },
    textLabels: {
      body: {
        noMatch: "No merchant category records found",
      },
    },
  };
  return (
    <div className={classes.card}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!loading ? (
            <MuiThemeProvider theme={theme}>
              <MUIDataTable
                data={
                  records && records.constructor === Array
                    ? records
                        .slice(
                          page * rowsPerPage,
                          page * rowsPerPage + rowsPerPage
                        )
                        .map((category, i) => {
                          return {
                            "": rowsPerPage * page + i + 1,
                            Code: category.id,
                            Category: category.name,
                            " ": (
                              <CallOut
                                TopAction={<EditCategory row={category} />}
                                BottomAction={<DeleteCategory row={category} />}
                              />
                            ),
                          };
                        })
                    : []
                }
                columns={columns}
                options={options}
                title={
                  <div className={classes.tableLogo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Merchant categories
                  </div>
                }
              />
              <TablePagination
                component="div"
                count={records && records.length}
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={rowsPerPage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                rowsPerPageOptions={[5, 10]}
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
    </div>
  );
}
export default MerchantsCategory;
