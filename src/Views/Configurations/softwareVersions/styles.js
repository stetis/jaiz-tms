import { makeStyles } from "@material-ui/core";
import { Colors, Fonts } from "../../../assets/themes/theme";
import { lighten } from "polished";

export const styles = makeStyles(() => ({
  historyRoot: {
    width: "95%",
    margin: "26px auto",
    display: "block",
    "@media only screen and (min-width: 600px)": {
      width: "50%",
    },
  },
  tableCard: {
    width: "95%",
    margin: "26px auto",
    display: "block",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 20px 25px",
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: "50%",
    },
  },
  card: {
    width: "100%",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 20px 25px",
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
  },

  version: {
    width: "100%",
    display: "flex",
    marginTop: 26,
  },
  versionCard: {
    width: "100%",
    display: "block",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    marginTop: 26,
    padding: "13px 20px 25px",
    borderRadius: 5,
    cursor: "pointer",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
  },
  infoText: {
    margin: "1px 0 0 -10px",
    "@media only screen and (min-width: 600px)": {
      margin: "5px 0px",
    },
  },
  fabIcon: {
    position: "absolute",
    top: 20,
    "@media only screen and (max-width: 540px)": {
      top: 8,
      left: 174,
    },
  },
  softwareCard: {
    width: "95%",
    margin: "26px auto",
    display: "flex",
    paddingRight: 10,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 500,
    },
  },
  outletCard: {
    width: "100%",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 5px 25px",
    borderRadius: 5,
    margin: "-10px auto",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
  },
  container: {
    display: "flex",
    padding: "8px 25px 25px",
    flexWrap: "wrap",
  },
  infoCard: {
    display: "flex",
    flexWrap: "wrap",
  },
  fabAddIcon: {
    fontSize: 14,
    color: Colors.secondary,
  },
  apkgrid: {
    "@media only screen and (min-width: 600px)": {
      marginLeft: -25,
      marginTop: -5,
    },
  },
  sectiongrid: {
    display: "flex",
    justifyContent: "start",
  },
  sectiongrid4: {
    display: "flex",
    justifyContent: "end",
  },
  sectionbg: {
    background: lighten(0.8, Colors.primary),
    padding: "10px 15px 10px 14px",
    marginBottom: 10,
  },
  sectiontitle: {
    padding: "10px 15px",
  },
  gutters: {
    paddingLeft: 0,
    paddingRight: 0,
  },
  editOutletFab: {
    width: 25,
    height: 20,
    padding: 15,
    float: "right",
    "@media only screen and (min-width: 600px)": {
      left: -5,
      top: -28,
    },
    "@media only screen and (min-width: 960px)": {
      left: 8,
      top: -30,
    },
    "@media only screen and (min-width: 1280)": {
      // left: 594,
      top: 15,
    },
    "@media only screen and (min-width: 1920)": {
      // left: 594,
      top: 15,
    },
  },
  AddOutletButton: {
    position: "relative",
    top: -8,
    left: 0,
    float: "right",
    "@media only screen and (min-width: 600px)": {
      left: -5,
      top: -28,
    },
    "@media only screen and (min-width: 960px)": {
      left: 8,
      top: -30,
    },
    "@media only screen and (min-width: 1280)": {
      // left: 594,
      top: 15,
    },
    "@media only screen and (min-width: 1920)": {
      // left: 594,
      top: 15,
    },
  },
  AddOutletFab: {
    width: 25,
    height: 20,
    padding: 15,
    float: "right",
    marginTop: 0,
    "@media only screen and (min-width: 960px)": {
      position: "absolute",
      left: 695,
      top: 23,
    },
  },
  EditRemoveFab: {
    width: 20,
    height: 18,
    padding: 10,
    margin: 8,
    "@media only screen and (min-width: 600px)": {
      position: "relative",
      top: -52,
      left: 115,
    },
    "@media only screen and (min-width: 960px)": {
      left: 110,
    },
  },
  removeFab: {
    width: 20,
    height: 18,
    padding: 10,
    margin: 8,
    float: "right",
    "@media only screen and (min-width: 600px)": {
      position: "relative",
      top: -50,
      left: 37,
    },
    "@media only screen and (min-width: 960px)": {
      position: "relative",
      top: -50,
      left: 47,
    },
  },
  removeFabIcon: {
    fontSize: 14,
  },
  transLabel: {
    cursor: "pointer",
    fontFamily: Fonts.secondary,
  },
  tableLogo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
    position: "relative",
    top: 5,
    left: -5,
    marginBottom: 20,
  },
  detail: {
    color: Colors.secondary,
  },
  versionLogo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
    "@media only screen and (max-width: 540px)": {
      marginLeft: "-20px",
    },
  },
  appUpdatelogo: {
    color: Colors.primary,
    display: "flex",
    margin: "12px 0px",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
  },
  logo: {
    color: Colors.primary,
    display: "flex",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
    margin: "12px 0px",
  },

  addIcon: {
    fontSize: 20,
    color: Colors.secondary,
  },
  actionsRoot: {
    width: 350,
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (max-width: 359px)": {
      width: "100%",
    },
    "@media only screen and (min-width: 360px) and (max-width: 700px)": {
      width: "100%",
    },
    "@media only screen and (min-width: 768px) and (max-width: 1200px)": {
      width: 350,
    },
  },
  deleteRoot: {
    width: "95%",
    display: "flex",
    padding: "8px 20px 25px",
    flexWrap: "wrap",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 480px)": {
      width: 350,
    },
  },
  listItemIcon: {
    color: Colors.secondary,
    minWidth: 22,
    cursor: "pointer",
  },
  moreIcon: {
    margin: "1px 0 0 -10px",
    color: Colors.secondary,
    cursor: "pointer",
  },
  moreText: {
    margin: "2px 0 0px 5px",
    color: Colors.primary,
    cursor: "pointer",
  },
  editIcon: {
    color: Colors.secondary,
    margin: "1px 0 0 -10px",
    cursor: "pointer",
  },
  editText: {
    color: Colors.primary,
    margin: "2px 0 0px -10px",
    cursor: "pointer",
  },
  button: {
    width: "100%",
    display: "flex",
    marginLeft: 5,
    marginRight: 5,
    padding: 2,
    cursor: "pointer",
    color: Colors.menuListColor,
    fontSize: Fonts.font,
  },
  DeleteIcon: {
    color: Colors.buttonError,
    margin: "1px 0 0 -10px",
    cursor: "pointer",
  },
  deleteText: {
    margin: "2px 0 0px -10px",
    color: Colors.buttonError,
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
    cursor: "pointer",
  },
  circle: {
    color: Colors.primary,
    fontSize: 6,
  },
}));
