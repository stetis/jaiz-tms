import { Grid, List, ListItemIcon, MuiThemeProvider } from "@material-ui/core";
import { CloudUploadSharp, EditSharp, HistorySharp } from "@material-ui/icons";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";
import MUIDataTable from "mui-datatables";
import React, { useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import { clearMessages } from "../../../actions/Configurations/versions.actions";
import { theme } from "../../../assets/MUI/Table";
import Snackbars from "../../../assets/Snackbars/index";
import Spinner from "../../../assets/Spinner";
import { SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import CallOut from "../../../components/CallOut/CallOut";
import { Text } from "../../../components/Forms/index";
import { getSoftwareVersionsHelper } from "../../../helpers/Configurations/versions.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "./styles";
import { history } from "../../../App";

const styleProps = {
  height: 50,
  width: 50,
};
function SoftwareVersions() {
  const classes = styles();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.softwareReducer.loading);
  const softwareVersions = useSelector(
    (state) => state.softwareReducer.data.softwares
  );
  const error = useSelector((state) => state.softwareReducer.error);
  const errorMessage = useSelector(
    (state) => state.softwareReducer.errorMessage
  );

  useEffect(() => {
    document.title = "Software versions | Jaiz bank TMS";
    dispatch(getSoftwareVersionsHelper());
    dispatch(
      updateBreadcrumb({
        parent: "Software versions",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const handleCloseSnack = () => {
    dispatch(clearMessages());
  };
  const handleHistoryClick = (id) => {
    return history.push({
      pathname: `/tms/software-history`,
      state: { id },
    });
  };
  const handleClickOpen = () => {
    return history.push("/tms/add-software-version");
  };
  const handleEditClick = useCallback(
    (id) => {
      return history.push({
        pathname: `/tms/manage-software`,
        state: { id },
      });
    },
    [] // Tells React to memoize regardless of arguments.
  );
  const handleUploadClick = useCallback(
    (id) => {
      return history.push({
        pathname: `/tms/software-update`,
        state: { id },
      });
    },
    [] // Tells React to memoize regardless of arguments.
  );
  const columns = [
    "",
    {
      name: "id",
      options: {
        display: false,
      },
    },
    "App ID",
    "Name",
    "Current version",
    " ",
  ];
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: true,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    customToolbar: () => {
      return (
        <ButtonsRow>
          <SubmitButton onClick={handleClickOpen} className={classes.fabIcon}>
            <AddOutlinedIcon className={classes.addIcon} />
            Add software
          </SubmitButton>
        </ButtonsRow>
      );
    },
    textLabels: {
      body: {
        noMatch: "No software version record found",
      },
    },
  };
  return (
    <div className={classes.tableCard}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!loading ? (
            <MuiThemeProvider theme={theme}>
              <MUIDataTable
                data={
                  softwareVersions && softwareVersions.constructor === Array
                    ? softwareVersions.map((version, i) => {
                        return {
                          "": i + 1,
                          id: version.id,
                          "App ID": version.id,
                          Name: version.name,
                          "Current version": version.version,
                          Description: version.note,
                          " ": (
                            <CallOut
                              TopAction={
                                <List
                                  classes={{ root: classes.button }}
                                  onClick={() => handleEditClick(version.id)}
                                >
                                  <ListItemIcon
                                    classes={{ root: classes.listItemIcon }}
                                  >
                                    <EditSharp className={classes.moreIcon} />
                                  </ListItemIcon>
                                  <Text className={classes.moreText}>
                                    Edit info
                                  </Text>
                                </List>
                              }
                              BottomAction={
                                <List
                                  classes={{ root: classes.button }}
                                  onClick={() => handleUploadClick(version.id)}
                                >
                                  <ListItemIcon
                                    classes={{ root: classes.listItemIcon }}
                                  >
                                    <CloudUploadSharp
                                      className={classes.moreIcon}
                                    />
                                  </ListItemIcon>
                                  <Text className={classes.moreText}>
                                    Upload new version
                                  </Text>
                                </List>
                              }
                              ThirdAction={
                                <List
                                  classes={{ root: classes.button }}
                                  onClick={() => handleHistoryClick(version.id)}
                                >
                                  <ListItemIcon
                                    classes={{ root: classes.listItemIcon }}
                                  >
                                    <HistorySharp
                                      className={classes.moreIcon}
                                    />
                                  </ListItemIcon>
                                  <Text className={classes.moreText}>
                                    History
                                  </Text>
                                </List>
                              }
                            />
                          ),
                        };
                      })
                    : []
                }
                title={
                  <div className={classes.versionLogo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Software versions
                  </div>
                }
                columns={columns}
                options={options}
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
    </div>
  );
}
export default SoftwareVersions;
