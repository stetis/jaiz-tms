import { Grid, List, ListItem, MuiThemeProvider } from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../../actions/breadcrumbAction";
import { wideTable } from "../../../../assets/MUI/Table";
import Spinner from "../../../../assets/Spinner";
import { Title } from "../../../../components/contentHolders/Card";
import { Text } from "../../../../components/Forms/index";
import { getVersionsHelper } from "../../../../helpers/Configurations/versions.helper";
import jaiz from "../../../../images/jaiz-logo.png";
import { styles } from "../styles";

const styleProps = {
  height: 50,
  width: 50,
};
export default function SoftwareHistory(props) {
  const classes = styles();
  const [showDetail, setShowDetail] = useState(false);
  const [data, setData] = useState([]);
  const dispatch = useDispatch();
  const loading = useSelector(
    (state) => state.softwareReducer.versionIsLoading
  );
  const versionData = useSelector((state) => state.softwareReducer.versionData);
  // const versions = useSelector(
  //   (state) => state.softwareReducer.versionData.versions
  // );
  const error = useSelector((state) => state.softwareReducer.versionsError);
  const errorMessage = useSelector(
    (state) => state.softwareReducer.versionsErrorMessage
  );

  useEffect(() => {
    document.title = "Software versions | Jaiz bank TMS";
    dispatch(
      updateBreadcrumb({
        parent: "Software version",
        child: "Manage version",
        homeLink: "/tms",
        childLink: "/tms/software-versioning",
      })
    );
    dispatch(getVersionsHelper(props.location.state.id));
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const handleRowClick = (data) => {
    console.log(data, "i amthe row data");
    setData(data);
    setShowDetail(true);
  };
  const columns = [
    "",
    "Version",
    "Date",
    {
      name: "Note",
      options: {
        display: false,
      },
    },
  ];
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: true,
    sort: false,
    rowHover: false,
    onRowClick: (data) => handleRowClick(data),
    selectableRows: "none",
    textLabels: {
      body: {
        noMatch: "No software version records found",
      },
    },
  };
  // console.log(data, "i am the state data");
  return (
    <div className={classes.historyRoot}>
      {loading ? (
        <Spinner {...styleProps} />
      ) : error ? (
        <div className={classes.card}>{errorMessage}</div>
      ) : (
        <>
          <div className={classes.appUpdatelogo}>
            <img
              src={jaiz}
              alt="jaiz logo"
              style={{ width: 20, height: 20, marginRight: "1rem" }}
            />
            Software history
          </div>
          <div className={classes.card}>
            <Grid container spacing={0}>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <div className={classes.infoCard}>
                  <Grid
                    item
                    xs={12}
                    sm={4}
                    md={4}
                    lg={4}
                    style={{ display: "flex", marginBottom: 8 }}
                  >
                    <Text className={classes.infoText}>
                      <span className={classes.detail}>Code: </span>
                      {versionData.id}
                    </Text>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={6}
                    md={6}
                    lg={6}
                    style={{ display: "flex", marginBottom: 8 }}
                  >
                    <Text className={classes.infoText}>
                      <span className={classes.detail}>Name: </span>
                      {versionData.name}
                    </Text>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={2}
                    md={2}
                    lg={2}
                    style={{ display: "flex", marginBottom: 8 }}
                  >
                    <Text className={classes.infoText}>
                      <span className={classes.detail}>Version: </span>
                      {versionData["current-version"]}
                    </Text>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={12}
                    md={12}
                    lg={12}
                    style={{ display: "flex", marginBottom: 8 }}
                  >
                    <Text className={classes.infoText}>
                      <span className={classes.detail}>Note: </span>
                      {versionData.note}
                    </Text>
                  </Grid>
                </div>
              </Grid>
            </Grid>
          </div>
          <div className={classes.version}>
            <Grid container spacing={0}>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.appUpdatelogo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Versions
                  </div>
                </Grid>
                {showDetail ? (
                  <Grid container spacing={3}>
                    <Grid item xs={12} sm={6} md={6} lg={6}>
                      <div className={classes.versionCard}>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                          <MuiThemeProvider theme={wideTable}>
                            <MUIDataTable
                              data={
                                versionData.versions &&
                                versionData.versions.constructor === Array
                                  ? versionData.versions.map((version, i) => {
                                      return {
                                        "": i + 1,
                                        Version: version.version,
                                        Date: version[version["date-modified"]],
                                        Note: version.note,
                                      };
                                    })
                                  : []
                              }
                              columns={columns}
                              options={options}
                            />
                          </MuiThemeProvider>
                        </Grid>
                      </div>
                    </Grid>
                    <Grid item xs={12} sm={6} md={6} lg={6}>
                      <Grid item xs={12} sm={12} md={12} lg={12}>
                        <div className={classes.versionCard}>
                          <Grid item xs={12} sm={12} md={12} lg={12}>
                            <Title>Note</Title>
                          </Grid>
                          <Grid item xs={12} sm={12} md={12} lg={12}>
                            <Text>{data[3]}</Text>
                          </Grid>
                        </div>
                      </Grid>
                      <Grid item xs={12} sm={12} md={12} lg={12}>
                        <div className={classes.versionCard}>
                          <Grid item xs={12} sm={12} md={12} lg={12}>
                            <Title>Sections</Title>
                          </Grid>
                          <Grid item xs={12} sm={12} md={12} lg={12}>
                            {versionData.sections.map((section, index) => {
                              return (
                                <Grid container spacing={0} key={index + 1}>
                                  <Grid item xs={12} sm={12} md={12} lg={12}>
                                    <List component="div" disablePadding>
                                      <Grid
                                        item
                                        xs={12}
                                        sm={12}
                                        md={12}
                                        lg={12}
                                      >
                                        <ListItem
                                          key={section.name}
                                          classes={{
                                            gutters: classes.gutters,
                                          }}
                                        >
                                          <Grid
                                            item
                                            xs={8}
                                            sm={8}
                                            md={8}
                                            lg={8}
                                            className={classes.sectiongrid}
                                          >
                                            <Text>{section.name}</Text>
                                          </Grid>
                                          <Grid
                                            item
                                            xs={4}
                                            sm={4}
                                            md={4}
                                            lg={4}
                                            className={classes.sectiongrid4}
                                          >
                                            <span className={classes.checkbox}>
                                              {section.affected ? (
                                                <span
                                                  className={classes.circle}
                                                >
                                                  &#11044;
                                                </span>
                                              ) : null}
                                            </span>
                                          </Grid>
                                        </ListItem>
                                      </Grid>
                                    </List>
                                  </Grid>
                                </Grid>
                              );
                            })}
                          </Grid>
                        </div>
                      </Grid>
                    </Grid>
                  </Grid>
                ) : (
                  <Grid item xs={12} sm={6} md={6} lg={6}>
                    <div className={classes.versionCard}>
                      <Grid item xs={12} sm={12} md={12} lg={12}>
                        <MuiThemeProvider theme={wideTable}>
                          <MUIDataTable
                            data={
                              versionData.versions &&
                              versionData.versions.constructor === Array
                                ? versionData.versions.map((version, i) => {
                                    console.log(version, "software version");
                                    return {
                                      "": i + 1,
                                      Version: version.version,
                                      Date: version["date-modified"],
                                      Note: version.note,
                                    };
                                  })
                                : []
                            }
                            columns={columns}
                            options={options}
                          />
                        </MuiThemeProvider>
                      </Grid>
                    </div>
                  </Grid>
                )}
              </Grid>
            </Grid>
          </div>
        </>
      )}
    </div>
  );
}
