import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../../actions/breadcrumbAction";
import Spinner from "../../../../assets/Spinner";
import { getVersionsHelper } from "../../../../helpers/Configurations/versions.helper";
import EditSoftwareVersion from "./editInfo";

export default function ManageSoftware(props) {
  const dispatch = useDispatch();
  const loading = useSelector(
    (state) => state.softwareReducer.versionIsLoading
  );
  const singleVersion = useSelector(
    (state) => state.softwareReducer.versionData
  );

  useEffect(() => {
    document.title = "Manage software info | Software versioning";
    dispatch(
      updateBreadcrumb({
        parent: "Software versioning",
        child: "Manage software info",
        homeLink: "/tms",
        childLink: "/tms/software-versioning",
      })
    );
    dispatch(getVersionsHelper(props.location.state.id));
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  console.log(singleVersion, "single");
  return (
    <div>
      {loading ? (
        <Spinner />
      ) : singleVersion ? (
        <EditSoftwareVersion row={singleVersion} />
      ) : null}
    </div>
  );
}
