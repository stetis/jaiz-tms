import { Grid } from "@material-ui/core";
import { SaveSharp } from "@material-ui/icons";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../../actions/Configurations/versions.actions";
import Snackbars from "../../../../assets/Snackbars/index";
import { Colors } from "../../../../assets/themes/theme";
import { SubmitButton } from "../../../../components/Buttons/index";
import { ButtonsRow } from "../../../../components/Buttons/styles";
import { Title } from "../../../../components/contentHolders/Card";
import { TextArea, TextField } from "../../../../components/TextField/index";
import {
  editVersionHelper,
  editSectionHelper,
} from "../../../../helpers/Configurations/versions.helper";
import jaiz from "../../../../images/jaiz-logo.png";
import { styles } from "../styles";
import AddSection from "./addSection";
import DeleteSection from "./deleteSection";

export default function EditSoftwareVersion(props) {
  const classes = styles();
  const [state, setState] = useState({
    name: props.row.name,
    initialVersion: props.row["current-version"],
    descripton: props.row.note,
    sections: props.row.sections ? [...props.row.sections] : [],
    formError: false,
  });
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.softwareReducer.actionLoading);
  const status = useSelector((state) => state.softwareReducer.status);
  const statusMessage = useSelector(
    (state) => state.softwareReducer.statusMessage
  );

  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  const onChangeInput = (e, index) => {
    const { name, value } = e.target;
    const list = [...state.sections];
    list[index][name] = value;
    setState((prev) => ({
      ...prev,
      sections: list,
    }));
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  const handleCloseSnack = () => {
    dispatch(clearMessages());
  };
  const handleSaveSection = (section) => {
    let inputData = {
      id: section.id,
      name: section.name,
    };
    dispatch(editSectionHelper(inputData));
  };
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    let sections = [];
    sections.push(...state.sections, {
      name: state.section,
    });

    if (state.name === "" || state.descripton === "") {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      const inputData = {
        id: props.row.id,
        name: state.name,
        version: state.initialVersion,
        note: state.descripton,
        sections,
      };
      dispatch(editVersionHelper(inputData));
    }
  };
  return (
    <div>
      {state.formError === true
        ? renderFieldError(
            state.name === ""
              ? "Software name is required"
              : state.descripton === ""
              ? "Decription of the software is required"
              : ""
          )
        : null}
      <div className={classes.softwareCard}>
        <Grid container spacing={0}>
          <div>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <form
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
                className={classes.container}
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Manage software info
                  </div>
                </Grid>
                <Grid item xs={12} sm={8} md={8} lg={8}>
                  <TextField
                    id="name"
                    htmlFor="name"
                    label="Name"
                    name="name"
                    value={state.name}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={4} md={4} lg={4}>
                  <TextField
                    id="initialVersion"
                    htmlFor="initialVersion"
                    type="text"
                    label="Version"
                    name="initialVersion"
                    value={state.initialVersion}
                    onChange={handleChange}
                    grouped
                    disabled
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextArea
                    id="descripton"
                    htmlFor="descripton"
                    type="text"
                    label="Note"
                    name="descripton"
                    value={state.descripton}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow style={{ marginBottom: 15 }}>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        backgroundColor: loading ? Colors.primary : "",
                        marginTop: 0,
                        width: 65,
                      }}
                      disableRipple={loading}
                    >
                      <span className="submit-btn">
                        {loading ? (
                          <Loader
                            type="ThreeDots"
                            color="#B28B0A"
                            height="15"
                            width="30"
                          />
                        ) : (
                          "save"
                        )}
                      </span>
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Title style={{ margin: "5px 0 -20px" }}>
                    Software sections
                  </Title>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <AddSection id={props.row.id} />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <label className={classes.label} htmlFor="first_section">
                    Sections
                  </label>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <hr
                    style={{
                      margin: "8px 0 0",
                      height: 0.5,
                      border: "none",
                      color: Colors.greyLight,
                      backgroundColor: Colors.greyLight,
                    }}
                  />
                </Grid>
                {state.sections.map((section, i) => {
                  return (
                    <Grid container spacing={0} key={i + 1}>
                      <Grid item xs={8} sm={10} md={10} lg={10}>
                        <TextField
                          id="name"
                          htmlFor="name"
                          label=""
                          name="name"
                          value={section.name}
                          onChange={(e) => onChangeInput(e, i)}
                        />
                      </Grid>
                      <Grid item xs={4} sm={2} md={2} lg={2}>
                        <ButtonsRow
                          style={{ position: "relative", top: 25, left: -5 }}
                        >
                          <SaveSharp
                            style={{
                              fontSize: 25,
                              marginRight: 10,
                              color: Colors.primary,
                              cursor: "pointer",
                            }}
                            onClick={() => handleSaveSection(section)}
                          />
                          <DeleteSection id={section.id} />
                        </ButtonsRow>
                      </Grid>
                    </Grid>
                  );
                })}
              </form>
            </Grid>
          </div>
        </Grid>
      </div>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
