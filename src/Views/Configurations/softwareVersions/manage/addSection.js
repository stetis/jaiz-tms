import { Grid } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import AddIcon from "@material-ui/icons/Add";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../../actions/Configurations/titles.actions";
import Snackbars from "../../../../assets/Snackbars/index";
import { Colors } from "../../../../assets/themes/theme";
import {
  DeleteButton,
  Fab,
  SubmitButton,
} from "../../../../components/Buttons/index";
import { ButtonsRow } from "../../../../components/Buttons/styles";
import { TextField } from "../../../../components/TextField/index";
import { addSectionHelper } from "../../../../helpers/Configurations/versions.helper";
import jaiz from "../../../../images/jaiz-logo.png";
import { styles } from "../styles";

export default function AddSection(props) {
  const classes = styles();
  const [open, setOpen] = useState(false);
  const [name, setName] = useState("");
  const [formError, setFormError] = useState(false);
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.softwareReducer.actionLoading);

  useEffect(() => {
    const timer = setTimeout(() => {
      setFormError(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [formError]);

  const handleChange = (e) => setName(e.target.value);
  const handleClickOpen = (e) => {
    e.preventDefault();
    setOpen(true);
  };
  const handleClose = (e) => {
    e.preventDefault();
    setOpen(false);
    setName("");
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (name === "") {
      setFormError(true);
    } else {
      const inputData = {
        "software-id": props.id,
        name: name,
      };
      dispatch(addSectionHelper(inputData));
    }
  };
  const handleCloseSnack = () => dispatch(clearMessages());

  const renderFieldError = (formErrorMessage) => {
    <Snackbars
      variant="error"
      handleClose={handleCloseSnack}
      message={formErrorMessage}
      isOpen={formError}
    />;
  };
  return (
    <div>
      <ButtonsRow>
        <Fab onClick={handleClickOpen} className={classes.AddFab}>
          <AddIcon className={classes.fabAddIcon} />
        </Fab>
      </ButtonsRow>
      {formError === true
        ? renderFieldError(name === "" ? "Section name is required" : "")
        : null}
      <Dialog
        open={open}
        keepMounted
        aria-labelledby="alert-dialog-slide-title"
        className="add-branch-dialog"
        aria-describedby="alert-dialog-slide-description"
      >
        <div className={classes.actionsRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12}>
              <form
                className={classes.container}
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Add Section
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextField
                    id="name"
                    htmlFor="name"
                    name="name"
                    type="text"
                    label="Section"
                    value={name}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      style={{
                        marginRight: 10,
                      }}
                      onClick={handleClose}
                      disabled={loading}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        marginTop: 0,
                      }}
                    >
                      {loading ? (
                        <Loader
                          type="ThreeDots"
                          color={Colors.secondary}
                          height="15"
                          width="30"
                        />
                      ) : (
                        "Save"
                      )}
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </div>
      </Dialog>
    </div>
  );
}
