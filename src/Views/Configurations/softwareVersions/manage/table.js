import { Grid, MuiThemeProvider } from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import React, { useEffect } from "react";
import { wideTable } from "../../../../assets/MUI/Table";
import Spinner from "../../../../assets/Spinner";
import { Title } from "../../../../components/contentHolders/Card";
import { Text } from "../../../../components/Forms/index";
import { updateBreadcrumb } from "../../../../actions/breadcrumbAction";
import { useSelector, useDispatch } from "react-redux";
import jaiz from "../../../../images/jaiz-logo.png";
import { styles } from "../styles";
import NewVersion from "./editInfo";
import { getVersionsHelper } from "../../../../helpers/Configurations/versions.helper";

const styleProps = {
  height: 50,
  width: 50,
};
export default function SoftwareVersions(props) {
  const classes = styles();
  const dispatch = useDispatch();
  const loading = useSelector(
    (state) => state.softwareReducer.versionIsLoading
  );
  const versionData = useSelector((state) => state.softwareReducer.versionData);
  const versions = useSelector(
    (state) => state.softwareReducer.versionData.versions
  );
  const error = useSelector((state) => state.softwareReducer.versionsError);
  const errorMessage = useSelector(
    (state) => state.softwareReducer.versionsErrorMessage
  );
  useEffect(() => {
    document.title = "Software versions | Jaiz bank TMS";
    dispatch(
      updateBreadcrumb({
        parent: "Software version",
        child: "Manage version",
        homeLink: "/tms",
        childLink: "/tms/software-versioning",
      })
    );
    dispatch(getVersionsHelper(props.location.state.id));
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const columns = ["", "Version", "Last updated", "Note"];
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: true,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    textLabels: {
      body: {
        noMatch: "No software version records found",
      },
    },
  };
  return (
    <div className={classes.card}>
      {error ? (
        <Text>{errorMessage}</Text>
      ) : (
        <Grid container spacing={0}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <div className={classes.tableLogo}>
              <img
                src={jaiz}
                alt="jaiz logo"
                style={{ width: 20, height: 20, marginRight: "1rem" }}
              />
              Software versions
            </div>
          </Grid>
          <Grid container>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <Text className={classes.infoText}>
                <span className={classes.merchantInfo}>Name: </span>
                {versionData.name}
              </Text>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <Text className={classes.infoText}>
                <span className={classes.merchantInfo}>Version: </span>
                {versionData.version}
              </Text>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Text className={classes.infoText}>
                <span className={classes.merchantInfo}>Note: </span>
                {versionData.note}
              </Text>
            </Grid>
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <NewVersion
              id={props.location.state.id}
              versionData={versionData}
            />
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Title vmargin="8px">Previous versions</Title>
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            {!loading ? (
              <MuiThemeProvider theme={wideTable}>
                <MUIDataTable
                  data={
                    versions && versions.constructor === Array
                      ? versions.map((version, i) => {
                          return {
                            "": i + 1,
                            Version: version.version,
                            "Last updated": version.updated,
                            Note: version.note,
                          };
                        })
                      : []
                  }
                  columns={columns}
                  options={options}
                />
              </MuiThemeProvider>
            ) : (
              <Spinner {...styleProps} />
            )}
          </Grid>
        </Grid>
      )}
    </div>
  );
}
