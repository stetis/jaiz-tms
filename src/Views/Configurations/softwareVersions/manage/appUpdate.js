import { Grid, ListItem, List } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../../actions/breadcrumbAction";
import { clearMessages } from "../../../../actions/Configurations/versions.actions";
import Snackbars from "../../../../assets/Snackbars/index";
import { Colors } from "../../../../assets/themes/theme";
import {
  DeleteButton,
  SubmitButton,
} from "../../../../components/Buttons/index";
import { ButtonsRow } from "../../../../components/Buttons/styles";
import { Title } from "../../../../components/contentHolders/Card";
import { Text } from "../../../../components/Forms/index";
import {
  TextField,
  CheckBox,
  TextArea,
} from "../../../../components/TextField/index";
import {
  addNewVersionHelper,
  getVersionsHelper,
} from "../../../../helpers/Configurations/versions.helper";
import jaiz from "../../../../images/jaiz-logo.png";
import { styles } from "../styles";
import Spinner from "../../../../assets/Spinner";

const styleProps = {
  height: 50,
  width: 50,
};

export default function UpdateSoftwareVersion(props) {
  const classes = styles();
  const fileUploader = React.useRef();
  const [state, setState] = useState({
    name: "",
    initialVersion: "",
    uploadApk: "",
    apkName: "",
    descripton: "",
    section: "",
    checkedItems: new Map(),
    sections: [],
    formError: false,
  });
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.softwareReducer.actionLoading);
  const status = useSelector((state) => state.softwareReducer.status);
  const statusMessage = useSelector(
    (state) => state.softwareReducer.statusMessage
  );
  const versionLoading = useSelector(
    (state) => state.softwareReducer.versionIsLoading
  );
  const singleVersion = useSelector(
    (state) => state.softwareReducer.versionData
  );

  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  useEffect(() => {
    document.title = "Software version update| Software versioning";
    dispatch(getVersionsHelper(props.location.state.id));

    dispatch(
      updateBreadcrumb({
        parent: "Software versioning",
        child: "Software version update",
        homeLink: "/tms",
        childLink: "/tms/software-versioning",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleFile = (e) => {
    e.preventDefault();
    const { files } = e.target;
    let file = files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setState((prev) => ({
        ...prev,
        apkName: files[0].name,
        uploadApk: reader.result,
      }));
    };
  };
  const handleCheckboxChange = (e, section) => {
    const item = e.target.name;
    const isChecked = e.target.checked;
    const selectedSections = [...state.sections, section];
    setState((prev) => ({
      ...prev,
      checkedItems: prev.checkedItems.set(item, isChecked),
      sections: selectedSections,
    }));
  };
  const handleFileUpload = (e) => {
    e.preventDefault();
    fileUploader.current.click();
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  const handleClose = (e) => {
    e.preventDefault();
    props.history.push("/tms/software-versioning");
  };
  const handleCloseSnack = () => {
    dispatch(clearMessages());
  };
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    let allSections = state.sections.map((section) => {
      return {
        name: section.name,
        updated: true,
      };
    });

    if (!state.initialVersion || !state.uploadApk || state.descripton === "") {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      const inputData = {
        id: singleVersion.id,
        name: singleVersion.name,
        version: state.initialVersion,
        binary: state.uploadApk,
        note: state.descripton,
        sections: allSections,
      };
      dispatch(addNewVersionHelper(inputData));
    }
  };
  return (
    <div>
      {state.formError === true
        ? renderFieldError(
            state.initialVersion === ""
              ? "Initial version is required"
              : !state.uploadApk
              ? "APK file is required"
              : state.descripton === ""
              ? "Decription of the software is required"
              : ""
          )
        : null}
      <div className={classes.softwareCard}>
        {versionLoading ? (
          <Spinner {...styleProps} />
        ) : (
          <Grid container spacing={0}>
            <div>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <form
                  noValidate
                  autoComplete="off"
                  onSubmit={handleSubmit}
                  className={classes.container}
                >
                  <Grid item xs={12} sm={12} md={12} lg={12}>
                    <div className={classes.appUpdatelogo}>
                      <img
                        src={jaiz}
                        alt="jaiz logo"
                        style={{ width: 20, height: 20, marginRight: "1rem" }}
                      />
                      Software version update
                    </div>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={8}
                    md={8}
                    lg={8}
                    style={{ display: "flex", marginBottom: 8 }}
                  >
                    <Text className={classes.infoText}>
                      <span className={classes.detail}>Software: </span>
                      {singleVersion.name}
                    </Text>
                  </Grid>
                  <Grid item xs={12} sm={4} md={4} lg={4}>
                    <Text className={classes.infoText}>
                      <span className={classes.detail}>Current version: </span>
                      {singleVersion["current-version"]}
                    </Text>
                  </Grid>
                  <Grid item xs={12} sm={8} md={8} lg={8}>
                    <Text style={{ display: "flex", marginTop: 10 }}>
                      New version
                      <span style={{ marginTop: -30, width: "70%" }}>
                        <TextField
                          id="initialVersion"
                          htmlFor="initialVersion"
                          type="text"
                          label=""
                          name="initialVersion"
                          value={state.initialVersion}
                          onChange={handleChange}
                          grouped
                        />
                      </span>
                    </Text>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={4}
                    md={4}
                    lg={4}
                    className={classes.apkgrid}
                  >
                    <input
                      type="file"
                      id="file"
                      accept=".apk"
                      onChange={handleFile}
                      ref={fileUploader}
                      style={{
                        display: "none",
                      }}
                    />
                    <ButtonsRow>
                      <SubmitButton
                        onClick={handleFileUpload}
                        ghost
                        style={{ borderColor: Colors.secondary }}
                      >
                        upload apk file
                      </SubmitButton>
                    </ButtonsRow>
                  </Grid>
                  <Grid item xs={12} sm={12} md={12} lg={12}>
                    <Text>{state.apkName}</Text>
                  </Grid>
                  <Grid item xs={12} sm={12} md={12} lg={12}>
                    <hr
                      style={{
                        margin: "5px 0",
                        height: 0.5,
                        border: "none",
                        color: Colors.greyLight,
                        backgroundColor: Colors.greyLight,
                      }}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} md={12} lg={12}>
                    <Text style={{ margin: "15px 0 15px" }}>
                      Which section of this app is updated
                    </Text>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={8}
                    md={8}
                    lg={8}
                    className={classes.sectiongrid}
                  >
                    <Title className={classes.sectiontitle}>Sections</Title>
                  </Grid>
                  <Grid
                    item
                    xs={12}
                    sm={4}
                    md={4}
                    lg={4}
                    className={classes.sectiongrid4}
                  >
                    <Title className={classes.sectiontitle}>Updated</Title>
                  </Grid>
                  <Grid item xs={12} sm={12} md={12} lg={12}>
                    <div className={classes.sectionbg}>
                      {singleVersion.sections &&
                        singleVersion.sections.map((item, index) => {
                          return (
                            <Grid container spacing={0} key={index + 1}>
                              <Grid item xs={12} sm={12} md={12} lg={12}>
                                <List component="div" disablePadding>
                                  <Grid item xs={12} sm={12} md={12} lg={12}>
                                    <ListItem
                                      key={item.name}
                                      classes={{
                                        gutters: classes.gutters,
                                      }}
                                    >
                                      <Grid
                                        item
                                        xs={8}
                                        sm={8}
                                        md={8}
                                        lg={8}
                                        className={classes.sectiongrid}
                                      >
                                        <Title>{item.name}</Title>
                                      </Grid>
                                      <Grid
                                        item
                                        xs={4}
                                        sm={4}
                                        md={4}
                                        lg={4}
                                        className={classes.sectiongrid4}
                                      >
                                        <span className={classes.checkbox}>
                                          <CheckBox
                                            id={item.name}
                                            htmlFor={item.name}
                                            type="checkbox"
                                            name={item.name}
                                            value={item.name}
                                            checked={state.checkedItems.get(
                                              item.name
                                            )}
                                            onChange={(e) =>
                                              handleCheckboxChange(e, item)
                                            }
                                          />
                                        </span>
                                      </Grid>
                                    </ListItem>
                                  </Grid>
                                </List>
                              </Grid>
                            </Grid>
                          );
                        })}
                    </div>
                  </Grid>
                  <Grid item xs={12} sm={12} md={12} lg={12}>
                    <TextArea
                      id="descripton"
                      htmlFor="descripton"
                      type="text"
                      label="Note"
                      name="descripton"
                      value={state.descripton}
                      onChange={handleChange}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} md={12} lg={12}>
                    <ButtonsRow>
                      <DeleteButton
                        onClick={handleClose}
                        style={{ margin: "3px 10px 0 0" }}
                      >
                        Cancel
                      </DeleteButton>
                      <SubmitButton
                        type="submit"
                        disabled={loading}
                        style={{
                          backgroundColor: loading ? Colors.primary : "",
                          marginTop: 0,
                          width: 65,
                        }}
                        disableRipple={loading}
                      >
                        <span className="submit-btn">
                          {loading ? (
                            <Loader
                              type="ThreeDots"
                              color="#B28B0A"
                              height="15"
                              width="30"
                            />
                          ) : (
                            "save"
                          )}
                        </span>
                      </SubmitButton>
                    </ButtonsRow>
                  </Grid>
                </form>
              </Grid>
            </div>
          </Grid>
        )}
      </div>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
