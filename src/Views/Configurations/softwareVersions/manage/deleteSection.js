import { Grid } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import React, { useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { DeleteIcon } from "../../../../assets/icons/Svg";
import { Colors } from "../../../../assets/themes/theme";
import {
  DeleteButton,
  SubmitButton,
} from "../../../../components/Buttons/index";
import { ButtonsRow } from "../../../../components/Buttons/styles";
import { Text } from "../../../../components/Forms/index";
import { deleteSectionHelper } from "../../../../helpers/Configurations/versions.helper";
import jaiz from "../../../../images/jaiz-logo.png";
import { styles } from "../styles";

export default function DeleteSection(props) {
  const classes = styles();
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.softwareReducer.actionLoading);

  const handleClickOpen = () => setOpen(true);
  const handleClose = (e) => {
    e.preventDefault();
    setOpen(false);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(deleteSectionHelper(props.id));
  };
  return (
    <div>
      <DeleteIcon onClick={handleClickOpen} />
      <Dialog
        open={open}
        keepMounted
        aria-labelledby="alert-dialog-slide-title"
        className="add-department-dialog"
        aria-describedby="alert-dialog-slide-description"
      >
        <div className={classes.deleteRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <div className={classes.logo}>
                <img
                  src={jaiz}
                  alt="jaiz logo"
                  style={{ width: 20, height: 20, marginRight: "1rem" }}
                />
                Delete section
              </div>
            </Grid>
            <Grid
              item
              xs={12}
              sm={12}
              md={12}
              lg={12}
              className={classes.deleteText}
            >
              <Text style={{ width: "80%", margin: "0 auto" }}>
                Are you sure you want to delete this section
              </Text>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <ButtonsRow>
                <DeleteButton onClick={handleClose} style={{ marginRight: 10 }}>
                  Cancel
                </DeleteButton>
                <SubmitButton
                  type="submit"
                  disabled={loading}
                  style={{
                    marginTop: 0,
                  }}
                  onClick={handleSubmit}
                >
                  <span className="submit-btn">
                    {loading ? (
                      <Loader
                        type="ThreeDots"
                        color={Colors.secondary}
                        height="15"
                        width="30"
                      />
                    ) : (
                      "Delete"
                    )}
                  </span>
                </SubmitButton>
              </ButtonsRow>
            </Grid>
          </Grid>
        </div>
      </Dialog>
    </div>
  );
}
