import { Grid } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import { clearMessages } from "../../../actions/Configurations/versions.actions";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import {
  DeleteButton,
  Fab,
  SubmitButton,
} from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Title } from "../../../components/contentHolders/Card";
import { Text } from "../../../components/Forms/index";
import { TextArea, TextField } from "../../../components/TextField/index";
import { addversionHelper } from "../../../helpers/Configurations/versions.helper";
import { getPTSPHelper } from "../../../helpers/ptsp.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "./styles";

export default function AddSoftwareVersion(props) {
  const classes = styles();
  const fileUploader = React.useRef();
  const [state, setState] = useState({
    name: "",
    initialVersion: "",
    uploadApk: "",
    apkName: "",
    descripton: "",
    section: "",
    versionID: "",
    sections: [
      {
        name: "",
        update: true,
      },
    ],
    formError: false,
  });
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.softwareReducer.actionLoading);
  const status = useSelector((state) => state.softwareReducer.status);
  const statusMessage = useSelector(
    (state) => state.softwareReducer.statusMessage
  );

  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  useEffect(() => {
    document.title = "Add software version | Software versioning";
    dispatch(getPTSPHelper());
    dispatch(
      updateBreadcrumb({
        parent: "Software versioning",
        child: "Add software version",
        homeLink: "/tms",
        childLink: "/tms/software-versioning",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const handleFile = (e) => {
    e.preventDefault();
    const { files } = e.target;
    let file = files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
      setState((prev) => ({
        ...prev,
        apkName: files[0].name,
        uploadApk: reader.result,
      }));
    };
  };
  const handleFileUpload = (e) => {
    e.preventDefault();
    fileUploader.current.click();
  };
  // handle click event of the Add button
  const handleAddSection = (e) => {
    e.preventDefault();
    setState((prev) => ({
      ...prev,
      sections: [...state.sections, { name: "", updated: true }],
    }));
  };
  // handle click event of the Remove button
  const handleRemoveSection = (e, index) => {
    e.preventDefault();
    const list = [...state.sections];
    list.splice(index, 1);
    setState((prev) => ({
      ...prev,
      sections: list,
    }));
  };
  const onChangeInput = (e, index) => {
    const { name, value } = e.target;
    const list = [...state.sections];
    list[index][name] = value;
    setState((prev) => ({
      ...prev,
      sections: list,
    }));
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  const handleClose = (e) => {
    e.preventDefault();
    props.history.push("/tms/software-versioning");
  };
  const handleCloseSnack = () => {
    dispatch(clearMessages());
  };
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    let allSections = [];
    allSections.push(...state.sections, {
      name: state.section,
      update: true,
    });

    if (
      state.versionID === "" ||
      state.name === "" ||
      !state.initialVersion ||
      !state.uploadApk ||
      state.descripton === ""
    ) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      const inputData = {
        id: state.versionID,
        name: state.name,
        version: state.initialVersion,
        binary: state.uploadApk,
        note: state.descripton,
        sections: allSections,
      };
      dispatch(addversionHelper(inputData));
    }
  };
  return (
    <div>
      {state.formError === true
        ? renderFieldError(
            state.appID === ""
              ? "App ID is required"
              : state.name === ""
              ? "Software name is required"
              : state.initialVersion === ""
              ? "Initial version is required"
              : !state.uploadApk
              ? "APK file is required"
              : state.descripton === ""
              ? "Decription of the software is required"
              : ""
          )
        : null}
      <div className={classes.softwareCard}>
        <Grid container spacing={0}>
          <div>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <form
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
                className={classes.container}
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    New software
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextField
                    id="name"
                    htmlFor="name"
                    label="Name"
                    name="name"
                    value={state.name}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={8} md={8} lg={8}>
                  <TextField
                    id="versionID"
                    htmlFor="versionID"
                    label="ID"
                    name="versionID"
                    value={state.versionID}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={4} md={4} lg={4}>
                  <TextField
                    id="initialVersion"
                    htmlFor="initialVersion"
                    type="text"
                    label="Version"
                    name="initialVersion"
                    value={state.initialVersion}
                    onChange={handleChange}
                    grouped
                  />
                </Grid>

                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <input
                      type="file"
                      id="file"
                      accept=".apk"
                      onChange={handleFile}
                      ref={fileUploader}
                      style={{
                        display: "none",
                      }}
                    />
                    <SubmitButton
                      onClick={handleFileUpload}
                      ghost
                      style={{ borderColor: Colors.secondary }}
                    >
                      upload apk file
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Text>{state.apkName}</Text>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextArea
                    id="descripton"
                    htmlFor="descripton"
                    type="text"
                    label="Description"
                    name="descripton"
                    value={state.descripton}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Title style={{ margin: "5px 0 -20px" }}>
                    Software sections
                  </Title>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <Fab onClick={handleAddSection} className={classes.AddFab}>
                      <AddIcon className={classes.fabAddIcon} />
                    </Fab>
                  </ButtonsRow>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <label className={classes.label} htmlFor="first_section">
                    Section
                  </label>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <hr
                    style={{
                      margin: "8px 0 0",
                      height: 0.5,
                      border: "none",
                      color: Colors.greyLight,
                      backgroundColor: Colors.greyLight,
                    }}
                  />
                </Grid>
                <Grid item xs={12} sm={11} md={11} lg={11}>
                  <TextField
                    id="section"
                    htmlFor="section"
                    label=""
                    name="section"
                    value={state.section}
                    onChange={handleChange}
                  />
                </Grid>
                {state.sections.map((section, i) => {
                  return (
                    <Grid
                      item
                      xs={12}
                      sm={11}
                      md={11}
                      lg={11}
                      key={i}
                      style={{ margin: "-20px 0" }}
                    >
                      <TextField
                        id="name"
                        htmlFor="name"
                        label=""
                        name="name"
                        value={section.name}
                        onChange={(e) => onChangeInput(e, i)}
                      />
                      <span>
                        <Fab
                          onClick={(e) => handleRemoveSection(e, i)}
                          className={classes.removeFab}
                          ghost
                        >
                          <RemoveIcon className={classes.removeFabIcon} />
                        </Fab>
                      </span>
                    </Grid>
                  );
                })}
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      onClick={handleClose}
                      style={{ margin: "3px 10px 0 0" }}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        backgroundColor: loading ? Colors.primary : "",
                        marginTop: 0,
                        width: 65,
                      }}
                      disableRipple={loading}
                    >
                      <span className="submit-btn">
                        {loading ? (
                          <Loader
                            type="ThreeDots"
                            color="#B28B0A"
                            height="15"
                            width="30"
                          />
                        ) : (
                          "save"
                        )}
                      </span>
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </div>
        </Grid>
      </div>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
