import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../../actions/breadcrumbAction";
import { Text } from "../../../../components/Forms/index";
import { getRoleHelper } from "../../../../helpers/profile.helper";
import EditRole from "./edit";
import Spinner from "../../../../assets/Spinner";

const styleProps = {
  height: 50,
  width: 50,
};
export default function SoftwareVersions(props) {
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.profileReducer.roleLoading);
  const role = useSelector((state) => state.profileReducer.role);
  const error = useSelector((state) => state.profileReducer.roleError);
  const errorMessage = useSelector(
    (state) => state.profileReducer.roleErrorMessage
  );
  useEffect(() => {
    document.title = "Manage role | Jaiz bank TMS";
    dispatch(getRoleHelper(props.location.state.id));
    dispatch(
      updateBreadcrumb({
        parent: "Roles",
        child: "Manage role",
        homeLink: "/tms",
        childLink: "/tms/roles",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleModules = (modules) => {
    const newModules = [];
    if (modules) {
      for (let i = 0; i < modules.length; i++) {
        if (modules[i]["pages"]) {
          newModules.push({
            "module-code": modules[i]["module-code"],
            module: modules[i]["module"],
            page: modules[i]["module"],
            code: modules[i]["module-code"],
            child: modules[i]["child"],
            children: modules[i]["pages"].map((page) => ({
              "module-code": modules[i]["module-code"],
              module: modules[i]["module"],
              child: page.child,
              page: page["page"],
              code: page["page-code"],
            })),
          });
        } else {
          newModules.push({
            "module-code": modules[i]["module-code"],
            module: modules[i]["module"],
            child: modules[i]["child"],
          });
        }
      }
    }
    return newModules;
  };
  return (
    <div>
      {loading ? (
        <Spinner {...styleProps} />
      ) : error ? (
        <Text>{errorMessage}</Text>
      ) : (
        <EditRole
          modules={handleModules(props.location.state.modules)}
          role={role}
        />
      )}
    </div>
  );
}
