import { makeStyles } from "@material-ui/core";
import { Colors, Fonts } from "../../../assets/themes/theme";
import { lighten } from "polished";

export const styles = makeStyles((theme) => ({
  card: {
    width: 320,
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "8px 20px 25px",
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 500,
    },
  },
  fabIcon: {
    position: "absolute",
    top: 20,
    "@media only screen and (max-width: 480px)": {
      left: "60%",
      zIndex: 200,
    },
  },
  container: {
    display: "flex",
    padding: "15px 25px 25px",
    flexWrap: "wrap",
  },
  fabAddIcon: {
    fontSize: 14,
    color: Colors.secondary,
  },

  tableLogo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
  },
  logo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
  },
  listItem: {
    paddingLeft: 0,
    paddingRight: 0,
  },
  deleteRoot: {
    width: 320,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    display: "flex",
    padding: "15px 18px 25px",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 350,
    },
  },
  addIcon: {
    fontSize: 20,
    color: Colors.secondary,
  },
  actionsRoot: {
    width: "95%",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    margin: "26px auto",
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 480px)": {
      width: 400,
    },
  },
  listItemIcon: {
    color: Colors.secondary,
    minWidth: 22,
    cursor: "pointer",
  },
  editIcon: {
    color: Colors.secondary,
    margin: "1px 0 0 -10px",
    cursor: "pointer",
  },
  editText: {
    color: Colors.primary,
    margin: "2px 0 0 -10px",
    cursor: "pointer",
  },
  button: {
    width: "100%",
    display: "flex",
    marginLeft: 5,
    marginRight: 5,
    padding: 2,
    cursor: "pointer",
    color: Colors.menuListColor,
    fontSize: Fonts.font,
  },
  DeleteIcon: {
    color: Colors.buttonError,
    margin: "1px 0 0 -10px",
    cursor: "pointer",
  },
  deleteText: {
    margin: "2px 0 0px -10px",
    color: Colors.buttonError,
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
    cursor: "pointer",
  },
  checkbox: {
    float: "right",
  },
  gutter: {
    paddingLeft: 0,
  },
  appbg: {
    background: lighten(0.8, Colors.primary),
    padding: "10px 15px",
  },
  nested: {
    [theme.breakpoints.up("sm")]: {
      cursor: "pointer",
    },
    cursor: "pointer",
  },
  nestedIcon: {
    [theme.breakpoints.up("sm")]: {
      position: "absolute",
      left: "91%",
      color: Colors.secondary,
    },
    position: "absolute",
    left: "74%",
    color: Colors.secondary,
  },
  icon: {
    [theme.breakpoints.up("sm")]: {
      fontSize: 25,
    },
    fontSize: 16,
  },
}));
