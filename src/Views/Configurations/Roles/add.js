import {
  Collapse,
  Grid,
  List,
  ListItem,
  ListItemIcon,
} from "@material-ui/core";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ArrowDropDownOutlinedIcon from "@material-ui/icons/ArrowDropDownOutlined";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import { clearMessages } from "../../../actions/Configurations/versions.actions";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Title } from "../../../components/contentHolders/Card";
import { addRoleHelper } from "../../../helpers/profile.helper";
import { CheckBox, TextField } from "../../../components/TextField/index";
import { styles } from "./styles";
import jaiz from "../../../images/jaiz-logo.png";

export default function AddRole(props) {
  const classes = styles();
  const [state, setState] = useState({
    title: "",
    openNest: "",
    prevNest: "",
    pages: [],
    appModules: [...props.location.state.pages],
    checkedItems: new Map(),
    formError: false,
  });
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.softwareReducer.actionLoading);
  const status = useSelector((state) => state.softwareReducer.status);
  const statusMessage = useSelector(
    (state) => state.softwareReducer.statusMessage
  );
  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  useEffect(() => {
    document.title = "Add role | Jaiz bank TMS";
    dispatch(
      updateBreadcrumb({
        parent: "Role",
        child: "Add role",
        homeLink: "/tms",
        childLink: "/tms/roles",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const handleClose = () => {
    props.history.goBack();
  };
  const handleCheckboxChange = (e, page) => {
    const item = e.target.name;
    const isChecked = e.target.checked;
    const selectedPages = [...state.pages, page];
    setState((prev) => ({
      ...prev,
      checkedItems: prev.checkedItems.set(item, isChecked),
      pages: selectedPages,
    }));
  };
  const handleClick = (item, index) => {
    if (item.children && state.openNest === index) {
      setState((prev) => ({
        ...prev,
        openNest: "",
      }));
    } else if (item.children) {
      setState((prev) => ({
        ...prev,
        openNest: index,
      }));
    }
  };
  const handleCloseSnack = () => {
    dispatch(clearMessages());
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (state.name === "") {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      let inputData = {
        title: state.title,
        pages: state.pages,
      };
      dispatch(addRoleHelper(inputData));
    }
  };
  let parentWithPages = state.appModules.filter(
    (item) => !item.child && item.children.length !== 0
  );
  let parentWithOutPages = state.appModules.filter(
    (item) => !item.child && item.children.length === 0
  );
  return (
    <div>
      {state.formError === true
        ? renderFieldError(state.title === "" ? "Title is required" : "")
        : null}
      <div className={classes.actionsRoot}>
        <Grid container spacing={0}>
          <Grid item xs={12} sm={12}>
            <form
              className={classes.container}
              noValidate
              onSubmit={handleSubmit}
              autoComplete="off"
            >
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <div className={classes.logo}>
                  <img
                    src={jaiz}
                    alt="jaiz logo"
                    style={{ width: 20, height: 20, marginRight: "1rem" }}
                  />
                  Add role
                </div>
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <TextField
                  id="title"
                  htmlFor="title"
                  type="text"
                  label="Title"
                  name="title"
                  value={state.title}
                  onChange={handleChange}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <Title vmargin="5px">Modules</Title>
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <div className={classes.appbg}>
                  <div>
                    {parentWithOutPages &&
                      parentWithOutPages.map((item, index) => {
                        return (
                          <Grid container spacing={0} key={index + 1}>
                            <Grid item xs={12} sm={12} md={12} lg={12}>
                              <List component="div" disablePadding>
                                <Grid item xs={12} sm={12} md={12} lg={12}>
                                  <ListItem
                                    className={classes.nested}
                                    key={item.module}
                                    classes={{ gutters: classes.listItem }}
                                  >
                                    <Grid item xs={8} sm={8} md={8} lg={8}>
                                      <Title>{item.module}</Title>
                                    </Grid>
                                    <Grid item xs={4} sm={4} md={4} lg={4}>
                                      <span className={classes.checkbox}>
                                        <CheckBox
                                          id={item.module}
                                          htmlFor={item.module}
                                          type="checkbox"
                                          name={item.module}
                                          value={item.module}
                                          checked={state.checkedItems.get(
                                            item.page
                                          )}
                                          onChange={(e) =>
                                            handleCheckboxChange(e, item)
                                          }
                                        />
                                      </span>
                                    </Grid>
                                  </ListItem>
                                </Grid>
                              </List>
                            </Grid>
                          </Grid>
                        );
                      })}
                  </div>
                  <div>
                    {parentWithPages &&
                      parentWithPages.map((item, index) => {
                        return (
                          <Grid container spacing={0} key={index + 1}>
                            <Grid item xs={12} sm={12} md={12} lg={12}>
                              <List
                                component="nav"
                                aria-labelledby="nested-list-subheader"
                                className={classes.listRoot}
                              >
                                <Grid item xs={12} sm={12} md={12} lg={12}>
                                  <ListItem
                                    button
                                    onClick={() => handleClick(item, index)}
                                    classes={{ gutters: classes.listItem }}
                                  >
                                    <Title>{item.module}</Title>
                                    {item.children && (
                                      <ListItemIcon
                                        className={classes.nestedIcon}
                                      >
                                        {state.openNest === index ? (
                                          <ExpandLessIcon
                                            className={classes.icon}
                                          />
                                        ) : (
                                          <ArrowDropDownOutlinedIcon
                                            className={classes.icon}
                                          />
                                        )}
                                      </ListItemIcon>
                                    )}
                                  </ListItem>
                                </Grid>
                                <Grid item xs={12} sm={12} md={12} lg={12}>
                                  <Collapse
                                    in={state.openNest === index}
                                    timeout="auto"
                                    unmountOnExit
                                  >
                                    <Grid item xs={12} sm={12} md={12} lg={12}>
                                      <List component="div" disablePadding>
                                        {item.children &&
                                          item.children.map((item, index) => {
                                            return (
                                              <ListItem
                                                className={classes.nested}
                                                onClick={() =>
                                                  handleClick(item, index)
                                                }
                                                key={item.page}
                                              >
                                                <Grid
                                                  item
                                                  xs={8}
                                                  sm={8}
                                                  md={8}
                                                  lg={8}
                                                >
                                                  <Title
                                                    style={{
                                                      marginLeft: 20,
                                                      fontSize: 11,
                                                    }}
                                                  >
                                                    {item.page}
                                                  </Title>
                                                </Grid>
                                                <Grid
                                                  item
                                                  xs={4}
                                                  sm={8}
                                                  md={8}
                                                  lg={8}
                                                >
                                                  <span
                                                    className={classes.checkbox}
                                                  >
                                                    <CheckBox
                                                      id={item.page}
                                                      htmlFor={item.page}
                                                      type="checkbox"
                                                      name={item.page}
                                                      value={item.page}
                                                      // checked={item.isChecked}
                                                      checked={state.checkedItems.get(
                                                        item.page
                                                      )}
                                                      onChange={(e) =>
                                                        handleCheckboxChange(
                                                          e,
                                                          item
                                                        )
                                                      }
                                                    />
                                                  </span>
                                                </Grid>
                                              </ListItem>
                                            );
                                          })}
                                      </List>
                                    </Grid>
                                  </Collapse>
                                </Grid>
                              </List>
                            </Grid>
                          </Grid>
                        );
                      })}
                  </div>
                </div>
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <ButtonsRow style={{ marginTop: 20 }}>
                  <DeleteButton
                    onClick={handleClose}
                    style={{ marginRight: 15 }}
                  >
                    Cancel
                  </DeleteButton>
                  <SubmitButton
                    type="submit"
                    className={classes.submitAddButton}
                    disabled={loading}
                    style={{
                      backgroundColor: loading ? Colors.primary : "",
                      marginTop: 0,
                    }}
                  >
                    {loading ? (
                      <Loader
                        type="ThreeDots"
                        color={Colors.secondary}
                        height="15"
                        width="30"
                      />
                    ) : (
                      "Save"
                    )}
                  </SubmitButton>
                </ButtonsRow>
              </Grid>
            </form>
          </Grid>
        </Grid>
      </div>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
