import { Grid, MuiThemeProvider } from "@material-ui/core";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";
import NavigateNextOutlinedIcon from "@material-ui/icons/NavigateNextOutlined";
import MUIDataTable from "mui-datatables";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import { clearMessages } from "../../../actions/profile.action";
import { history } from "../../../App";
import { theme } from "../../../assets/MUI/Table";
import Snackbars from "../../../assets/Snackbars/index";
import Spinner from "../../../assets/Spinner";
import { SubmitButton } from "../../../components/Buttons";
import { ButtonsRow } from "../../../components/Buttons/styles";
import {
  getModuleHelper,
  getRolesHelper,
} from "../../../helpers/profile.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "./styles";
const styleProps = {
  height: 50,
  width: 50,
};
function Table(props) {
  const classes = styles();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.profileReducer.rolesLoading);
  const roles = useSelector((state) => state.profileReducer.roles.roles);
  const modules = useSelector((state) => state.profileReducer.module.modules);
  const error = useSelector((state) => state.profileReducer.rolesError);
  const errorMessage = useSelector(
    (state) => state.profileReducer.rolesErrorMessage
  );

  useEffect(() => {
    document.title = "Roles | Jaiz bank TMS";
    dispatch(getRolesHelper());
    dispatch(getModuleHelper());
    dispatch(
      updateBreadcrumb({
        parent: "Roles",
        child: "",
        homeLink: "/tms",
        childLink: "/tms/roles",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const handleCloseSnack = () => dispatch(clearMessages());

  const handleClickOpen = (pages) => {
    history.push({
      pathname: `/tms/add-role`,
      state: { pages },
    });
  };
  const gotoManageRole = (id) => {
    history.push({
      pathname: `/tms/manage-role`,
      state: {
        id,
        modules,
      },
    });
  };
  const rowClickGotoEditVersion = (rowData) => {
    return history.push({
      pathname: `/tms/manage-role`,
      state: {
        id: rowData[1],
        modules,
      },
    });
  };

  const handleModules = (modules) => {
    const newModules = [];
    if (modules) {
      for (let i = 0; i < modules.length; i++) {
        if (modules[i]["pages"]) {
          newModules.push({
            "module-code": modules[i]["module-code"],
            module: modules[i]["module"],
            page: modules[i]["module"],
            code: modules[i]["module-code"],
            child: modules[i]["child"],
            children: modules[i]["pages"].map((page) => ({
              "module-code": modules[i]["module-code"],
              module: modules[i]["module"],
              child: page.child,
              page: page["page"],
              code: page["page-code"],
            })),
          });
        } else {
          newModules.push({
            "module-code": modules[i]["module-code"],
            module: modules[i]["module"],
            child: modules[i]["child"],
          });
        }
      }
    }
    return newModules;
  };
  const columns = [
    "",
    {
      name: "id",
      options: {
        display: false,
      },
    },
    "Role",
    " ",
  ];
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: true,
    sort: false,
    rowHover: false,
    onRowClick: (rowData) => rowClickGotoEditVersion(rowData),
    selectableRows: "none",
    customToolbar: () => {
      return (
        <ButtonsRow>
          <SubmitButton
            onClick={() => handleClickOpen(handleModules(modules))}
            className={classes.fabIcon}
          >
            <AddOutlinedIcon className={classes.addIcon} />
            Add role
          </SubmitButton>
        </ButtonsRow>
      );
    },
    textLabels: {
      body: {
        noMatch: "No role records found",
      },
    },
  };
  return (
    <div className={classes.card}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!loading ? (
            <MuiThemeProvider theme={theme}>
              <MUIDataTable
                data={
                  roles && roles.constructor === Array
                    ? roles.map((role, i) => {
                        return {
                          "": i + 1,
                          id: role["role-id"],
                          Role: role.title,
                          " ":
                            role.system === true ? (
                              ""
                            ) : (
                              <NavigateNextOutlinedIcon
                                style={{ fontSize: 20 }}
                                onClick={gotoManageRole.bind(
                                  this,
                                  role["role-id"]
                                )}
                              />
                            ),
                        };
                      })
                    : []
                }
                title={
                  <div className={classes.tableLogo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Roles
                  </div>
                }
                columns={columns}
                options={options}
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
    </div>
  );
}
export default Table;
