import { Grid, List, ListItemIcon } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import EditSharpIcon from "@material-ui/icons/EditSharp";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../actions/Configurations/accountType.actions";
import Snackbars from "../../../assets/Snackbars/index";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Text } from "../../../components/Forms/index";
import { TextField } from "../../../components/TextField/index";
import { editAccountTypeHelper } from "../../../helpers/Configurations/accountType.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "./styles";

export default function EditAccountType(props) {
  const classes = styles();
  const [state, setState] = useState({
    open: false,
    code: props.row.id,
    name: props.row.name,
    formError: false,
  });
  const dispatch = useDispatch();
  const loading = useSelector(
    (state) => state.accountTypeReducer.actionLoading
  );
  const status = useSelector((state) => state.accountTypeReducer.status);
  const statusMessage = useSelector(
    (state) => state.accountTypeReducer.statusMessage
  );
  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };
  const handleClickOpen = () => {
    setState((prev) => ({
      ...prev,
      open: true,
    }));
  };
  const handleClose = (e) => {
    e.preventDefault();
    setState((prev) => ({
      ...prev,
      open: false,
      code: "",
      name: "",
    }));
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const handleSubmit = (e) => {
    e.preventDefault();
    if (state.code === "" || state.name === "") {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      const inputData = {
        id: state.code,
        name: state.name,
      };
      dispatch(editAccountTypeHelper(inputData));
    }
  };
  const renderFieldError = (formErrorMessage) => {
    <Snackbars
      variant="error"
      handleClose={handleCloseSnack}
      message={formErrorMessage}
      isOpen={state.formError}
    />;
  };
  return (
    <div>
      <List classes={{ root: classes.button }} onClick={handleClickOpen}>
        <ListItemIcon classes={{ root: classes.listItemIcon }}>
          <EditSharpIcon className={classes.editIcon} />
        </ListItemIcon>
        <Text className={classes.editText}>Edit</Text>
      </List>
      {state.formError === true
        ? renderFieldError(
            state.code === ""
              ? "Account type code is required"
              : state.name === ""
              ? "Account type name is required"
              : ""
          )
        : null}
      <Dialog
        open={state.open}
        keepMounted
        aria-labelledby="alert-dialog-slide-title"
        className="add-branch-dialog"
        aria-describedby="alert-dialog-slide-name"
      >
        <div className={classes.actionsRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12}>
              <form
                className={classes.container}
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Edit account type
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextField
                    id="code"
                    htmlFor="code"
                    name="code"
                    type="text"
                    label="Code"
                    value={state.code}
                    onChange={handleChange}
                    disabled
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextField
                    id="name"
                    htmlFor="name"
                    name="name"
                    type="text"
                    label="Name"
                    value={state.name}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      style={{
                        marginRight: 10,
                      }}
                      onClick={handleClose}
                      disabled={loading}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        marginTop: 0,
                        width: 65,
                      }}
                    >
                      {loading ? (
                        <Loader
                          type="ThreeDots"
                          color="#f3f3f3"
                          height="15"
                          width="30"
                        />
                      ) : (
                        "Save"
                      )}
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </div>
      </Dialog>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
    </div>
  );
}
