import { makeStyles } from "@material-ui/core";
import { Colors, Fonts } from "../../../assets/themes/theme";

export const styles = makeStyles(() => ({
  ManagegroupCard: {
    width: "100%",
    margin: "0 auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 15px 12px",
    borderRadius: 5,
    cursor: "pointer",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
  },
  addFeeFabIcon: {
    position: "relative",
    top: 8,
    left: 140,
    zIndex: 200,
    "@media only screen and (max-width: 320px)": {
      position: "relative",
      top: 8,
      left: 114,
      zIndex: 200,
    },
    "@media only screen and (min-width: 340px) and (max-width: 700px)": {
      position: "relative",
      top: 8,
      left: 154,
      zIndex: 200,
    },
    "@media only screen and (min-width: 768px) and (max-width: 1200px)": {
      position: "relative",
      top: 8,
      left: 126,
      zIndex: 200,
    },
  },
  card: {
    width: 320,
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 5px 25px",
    borderRadius: 5,
    cursor: "pointer",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 500,
    },
  },
  TransactionTableCard: {
    width: "100%",
    marginTop: 30,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 5px 25px",
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
  },
  transactionCard: {
    width: "95%",
    margin: "26px auto",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 5px 25px",
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: "95%",
    },
    "@media only screen and (min-width: 960px)": {
      width: 650,
    },
    "@media only screen and (min-width: 1280)": {
      width: 650,
    },
    "@media only screen and (min-width: 1920px)": {
      width: 650,
    },
  },
  container: {
    display: "flex",
    padding: "8px 25px 25px",
    flexWrap: "wrap",
  },
  managegroupRoot: {
    width: "90%",
    margin: "26px auto",
    "@media only screen and (min-width: 600px)": {
      width: "70%",
    },
  },
  fabAddIcon: {
    fontSize: 14,
    color: Colors.secondary,
  },
  fabIcon: {
    position: "absolute",
    top: 10,
    "@media only screen and (min-width: 600px)": {
      position: "absolute",
      right: 15,
      top: 15,
    },
  },
  transLabel: {
    cursor: "pointer",
    fontFamily: Fonts.secondary,
  },
  tableLogo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
    position: "absolute",
    top: 15,
  },
  logo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
  },
  deleteRoot: {
    width: 350,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    display: "flex",
    padding: "12px 18px 20px",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (max-width: 359px)": {
      width: "100%",
    },
    "@media only screen and (min-width: 360px) and (max-width: 700px)": {
      width: "100%",
    },
    "@media only screen and (min-width: 768px) and (max-width: 1200px)": {
      width: 350,
    },
  },
  addIcon: {
    fontSize: 20,
    color: Colors.secondary,
  },
  actionsRoot: {
    width: 350,
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (max-width: 359px)": {
      width: "100%",
    },
    "@media only screen and (min-width: 360px) and (max-width: 700px)": {
      width: "100%",
    },
    "@media only screen and (min-width: 768px) and (max-width: 1200px)": {
      width: 350,
    },
  },
  listItemIcon: {
    color: Colors.menuListColor,
    minWidth: 22,
    cursor: "pointer",
  },
  editIcon: {
    margin: "1px 0 0 -10px",
    color: Colors.secondary,
    cursor: "pointer",
  },
  editText: {
    margin: "1px 0 0 -10px",
    color: Colors.primary,
    cursor: "pointer",
  },
  button: {
    width: "100%",
    display: "flex",
    marginLeft: 5,
    marginRight: 5,
    padding: 2,
    cursor: "pointer",
    color: Colors.menuListColor,
    fontSize: Fonts.font,
  },
  DeleteIcon: {
    color: Colors.buttonError,
    margin: "1px 0 0 -10px",
    cursor: "pointer",
  },
  deleteText: {
    color: Colors.buttonError,
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
    margin: "2px 0 0px -10px",
    cursor: "pointer",
  },
}));
