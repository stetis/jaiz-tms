import { Grid } from "@material-ui/core";
import { MuiThemeProvider } from "@material-ui/core/styles";
import MUIDataTable from "mui-datatables";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import { clearMessages } from "../../../actions/Configurations/accountType.actions";
import { theme } from "../../../assets/MUI/Table";
import Snackbars from "../../../assets/Snackbars/index";
import Spinner from "../../../assets/Spinner";
import CallOut from "../../../components/CallOut/CallOut";
import { getAccountTypeHelper } from "../../../helpers/Configurations/accountType.helper";
import jaiz from "../../../images/jaiz-logo.png";
import AddAccountype from "./add";
import DeleteAccoutType from "./delete";
import EditAccountType from "./edit";
import { styles } from "./styles";

const styleProps = {
  height: 50,
  width: 50,
};
export default function AccountTypes() {
  const classes = styles();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.accountTypeReducer.loading);
  const accounttypes = useSelector(
    (state) => state.accountTypeReducer.data["account-types"]
  );
  const error = useSelector((state) => state.accountTypeReducer.error);
  const errorMessage = useSelector(
    (state) => state.accountTypeReducer.errorMessage
  );
  useEffect(() => {
    document.title = "Account types | Jaiz bank TMS";
    dispatch(getAccountTypeHelper());
    dispatch(
      updateBreadcrumb({
        parent: "Account types",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const handleCloseSnack = () => {
    dispatch(clearMessages());
  };
  const columns = ["", "Code", "Type", " "];
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: true,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    customToolbar: () => {
      return <AddAccountype />;
    },
    textLabels: {
      body: {
        noMatch: "No account type records found",
      },
    },
  };
  return (
    <div className={classes.card}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!loading ? (
            <MuiThemeProvider theme={theme}>
              <MUIDataTable
                data={
                  accounttypes && accounttypes.constructor === Array
                    ? accounttypes.map((type, i) => {
                        return {
                          "": i + 1,
                          Code: type.id,
                          Type: type.name,
                          " ": (
                            <CallOut
                              TopAction={<EditAccountType row={type} />}
                              BottomAction={<DeleteAccoutType row={type} />}
                            />
                          ),
                        };
                      })
                    : []
                }
                columns={columns}
                options={options}
                title={
                  <div className={classes.tableLogo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Account type
                  </div>
                }
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
    </div>
  );
}
