import { Grid } from "@material-ui/core";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import { clearMessages } from "../../../actions/Configurations/commissions.actions";
import Snackbars from "../../../assets/Snackbars/index";
import Spinner from "../../../assets/Spinner";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Title } from "../../../components/contentHolders/Card";
import { Text } from "../../../components/Forms/index";
import { singleFeeGroupHelper } from "../../../helpers/Configurations/commissions.helper";
import DeleteFeeGroup from "./deleteFeeGroup";
import EditFeeGroup from "./editFeeGroup";
import { styles } from "./styles";
import TransactonTab from "./TransactionTypes/tab";

const styleProps = {
  height: 50,
  width: 50,
};
export default function ManageGroups(props) {
  const classes = styles();
  const dispatch = useDispatch();
  const loading = useSelector(
    (state) => state.commissionsReducer.singleFeeGroupLoading
  );
  const singleFeeGroup = useSelector(
    (state) => state.commissionsReducer.singleFeeGroupData["fee-group"]
  );
  const error = useSelector(
    (state) => state.commissionsReducer.singleFeeGroupError
  );
  const errorMessage = useSelector(
    (state) => state.commissionsReducer.singleFeeGroupErrorMessage
  );
  useEffect(() => {
    document.title = `${props.location.state.row.name} | Manage fee groups`;
    dispatch(singleFeeGroupHelper(props.location.state.row.id));
    dispatch(
      updateBreadcrumb({
        parent: "Commission",
        child: "Manage group",
        homeLink: "/tms",
        childLink: "/tms/commissions",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleCloseSnack = () => dispatch(clearMessages());
  return (
    <div>
      {loading ? (
        <Spinner {...styleProps} />
      ) : singleFeeGroup ? (
        <div className={classes.managegroupRoot}>
          <Grid container spacing={0}>
            <div className={classes.manageGroupCard}>
              <Grid container spacing={0}>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <Title>{singleFeeGroup.name}</Title>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <ButtonsRow style={{ marginTop: 0 }}>
                    <EditFeeGroup row={singleFeeGroup} />
                    <DeleteFeeGroup row={singleFeeGroup} />
                  </ButtonsRow>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <Text
                    style={{
                      width: "95%",
                      textOverflow: "ellipsis",
                    }}
                  >
                    {singleFeeGroup.description}
                  </Text>
                </Grid>
              </Grid>
            </div>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <TransactonTab fees={singleFeeGroup.fees} />
            </Grid>
          </Grid>
        </div>
      ) : (
        <div className={classes.managegroupRoot}>
          <Text>{errorMessage}</Text>
        </div>
      )}
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
    </div>
  );
}
