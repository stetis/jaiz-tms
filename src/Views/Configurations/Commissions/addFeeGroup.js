import { Grid, Popover } from "@material-ui/core";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../actions/Configurations/commissions.actions";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { TextArea, TextField } from "../../../components/TextField/index";
import { addFeeGroupHelper } from "../../../helpers/Configurations/commissions.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "./styles";

export default function AddFeeGroup() {
  const classes = styles();
  const [anchorEl, setAnchorEl] = useState(null);
  const [state, setState] = useState({
    group: "",
    description: "",
    formError: false,
  });
  const dispatch = useDispatch();
  const loading = useSelector(
    (state) => state.commissionsReducer.actionLoading
  );
  const status = useSelector((state) => state.commissionsReducer.status);
  const statusMessage = useSelector(
    (state) => state.commissionsReducer.statusMessage
  );
  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        FormError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  const handleClickOpen = (e) => setAnchorEl(e.currentTarget);

  const handleClose = (e) => {
    e.persist();
    e.preventDefault();
    setAnchorEl(null);
    setState((prev) => ({
      ...prev,
      group: "",
      description: "",
    }));
  };
  const handlePopperClose = () => setAnchorEl(null);
  const handleCloseSnack = () => dispatch(clearMessages());
  const handleChange = (e) => {
    e.persist();
    const { name, value } = e.target;
    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (state.group === "") {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      let inputData = {
        name: state.group,
        description: state.description,
      };
      dispatch(addFeeGroupHelper(inputData));
    }
  };
  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  return (
    <div>
      <SubmitButton onClick={handleClickOpen} className={classes.fabIcon}>
        <AddOutlinedIcon className={classes.addIcon} />
        Add fee group
      </SubmitButton>
      {state.formError === true
        ? renderFieldError(state.group === "" ? "Fee group is required" : "")
        : null}
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handlePopperClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <div className={classes.actionsRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12}>
              <form
                className={classes.container}
                noValidate
                onSubmit={handleSubmit}
                autoComplete="off"
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Fee group
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextField
                    id="group"
                    htmlFor="group"
                    type="text"
                    label="Group"
                    name="group"
                    value={state.group}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextArea
                    id="description"
                    htmlFor="description"
                    type="text"
                    label="Description"
                    name="description"
                    value={state.description}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      onClick={handleClose}
                      style={{ marginRight: 10, marginTop: 12 }}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      className={classes.submitAddButton}
                      disabled={loading}
                      style={{
                        backgroundColor: loading ? Colors.primary : "",
                        width: 65,
                      }}
                    >
                      {loading ? (
                        <Loader
                          type="ThreeDots"
                          color={Colors.secondary}
                          height="15"
                          width="30"
                        />
                      ) : (
                        "Save"
                      )}
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </div>
      </Popover>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
