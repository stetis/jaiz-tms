import { Grid, Popover } from "@material-ui/core";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../../actions/Configurations/commissions.actions";
import Snackbars from "../../../../assets/Snackbars/index";
import { Colors } from "../../../../assets/themes/theme";
import {
  DeleteButton,
  SubmitButton,
} from "../../../../components/Buttons/index";
import { ButtonsRow } from "../../../../components/Buttons/styles";
import { MoneyTextField } from "../../../../components/TextField/index";
import { addBandHelper } from "../../../../helpers/Configurations/commissions.helper";
import jaiz from "../../../../images/jaiz-logo.png";
import { styles } from "../styles";

export default function AddFeeBand(props) {
  const classes = styles();
  const [anchorEl, setAnchorEl] = useState(null);
  const [state, setState] = useState({
    lowerlimit: "",
    upperlimit: "",
    charge: "",
    agentcommission: "",
    superAgentcommission: "0",
    formError: false,
  });
  const dispatch = useDispatch();
  const loading = useSelector(
    (state) => state.commissionsReducer.actionLoading
  );
  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  const handleClickOpen = (e) => {
    setAnchorEl(e.currentTarget);
  };
  const handleClose = (e) => {
    e.preventDefault();
    setAnchorEl(null);
    setState((prev) => ({
      ...prev,
      lowerlimit: "",
      upperlimit: "",
      charge: "",
      agentcommission: "",
      superAgentcommission: "",
    }));
  };
  const handlePopperClose = () => setAnchorEl(null);
  const handleCloseSnack = () => dispatch(clearMessages());
  const handleChange = (e) => {
    const { name, value } = e.target;
    let numberValue = value.substring(1);
    // eslint-disable-next-line
    numberValue = numberValue.replace(/\,/g, "");
    numberValue = parseFloat(numberValue, 10);
    let finalNumber = numberValue ? numberValue : numberValue === 0 ? 0 : "";
    setState((prev) => ({
      ...prev,
      [name]: finalNumber,
    }));
  };
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (
      state.lowerlimit === "" ||
      state.upperlimit === "" ||
      state.charge === "" ||
      state.agentcomission === ""
    ) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      const inputData = {
        "lower-limit": state.lowerlimit,
        "upper-limit": state.upperlimit,
        "agent-commission": state.agentcommission,
        "super-agent-commission": state.superAgentcommission,
        charge: state.charge,
        "fee-id": props.id,
      };
      dispatch(addBandHelper(inputData));
    }
  };
  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  return (
    <div>
      <SubmitButton onClick={handleClickOpen} className={classes.fabIcon}>
        <AddOutlinedIcon className={classes.addIcon} />
        Add fee band
      </SubmitButton>
      {state.formError === true
        ? renderFieldError(
            state.lowerlimit === ""
              ? "Lower limit is required"
              : state.upperlimit === ""
              ? "Upper limit is required"
              : state.charge === ""
              ? "Transaction charge limit is required"
              : state.agentcommission === ""
              ? "Agent commission is required"
              : ""
          )
        : null}
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handlePopperClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <div className={classes.actionsRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12}>
              <form
                className={classes.container}
                noValidate
                onSubmit={handleSubmit}
                autoComplete="off"
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    New fee band
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <MoneyTextField
                    id="lowerlimit"
                    htmlFor="lowerlimit"
                    label="Lower limit"
                    type="text"
                    name="lowerlimit"
                    value={state.lowerlimit}
                    onChange={handleChange}
                    thousandsGroupStyle="thousand"
                    prefix={"₦ "}
                    thousandSeparator={true}
                    allowEmptyFormatting={true}
                    allowNegative={false}
                    fixedDecimalScale={true}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <MoneyTextField
                    id="upperlimit"
                    htmlFor="upperlimit"
                    label="Upper limit"
                    name="upperlimit"
                    type="text"
                    value={state.upperlimit}
                    onChange={handleChange}
                    thousandsGroupStyle="thousand"
                    prefix={"₦ "}
                    thousandSeparator={true}
                    allowEmptyFormatting={true}
                    allowNegative={false}
                    fixedDecimalScale={true}
                    grouped
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <MoneyTextField
                    id="charge"
                    htmlFor="charge"
                    label="Charge"
                    name="charge"
                    type="text"
                    value={state.charge}
                    onChange={handleChange}
                    thousandsGroupStyle="thousand"
                    prefix={"₦ "}
                    thousandSeparator={true}
                    allowEmptyFormatting={true}
                    allowNegative={false}
                    fixedDecimalScale={true}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <MoneyTextField
                    id="agentcommission"
                    htmlFor="agentcommission"
                    label="Agent commission"
                    type="text"
                    name="agentcommission"
                    value={state.agentcommission}
                    onChange={handleChange}
                    thousandsGroupStyle="thousand"
                    prefix={"₦ "}
                    thousandSeparator={true}
                    allowEmptyFormatting={true}
                    allowNegative={false}
                    fixedDecimalScale={true}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <MoneyTextField
                    id="superAgentcommission"
                    htmlFor="superAgentcommission"
                    label="Super agent commission"
                    name="superAgentcommission"
                    type="text"
                    value={state.superAgentcommission}
                    onChange={handleChange}
                    thousandsGroupStyle="thousand"
                    prefix={"₦ "}
                    thousandSeparator={true}
                    allowEmptyFormatting={true}
                    allowNegative={false}
                    fixedDecimalScale={true}
                    grouped
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      onClick={handleClose}
                      style={{ marginRight: 10 }}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        backgroundColor: loading ? Colors.primary : "",
                        marginTop: 0,
                      }}
                      disableRipple={loading}
                    >
                      <span className="submit-btn">
                        {loading ? (
                          <Loader
                            type="ThreeDots"
                            color={Colors.secondary}
                            height="15"
                            width="30"
                          />
                        ) : (
                          "save"
                        )}
                      </span>
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </div>
      </Popover>
    </div>
  );
}
