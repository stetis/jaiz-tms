import { Grid, List, ListItemIcon } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import EditSharpIcon from "@material-ui/icons/EditSharp";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../../actions/Configurations/commissions.actions";
import Snackbars from "../../../../assets/Snackbars/index";
import { Colors } from "../../../../assets/themes/theme";
import {
  DeleteButton,
  SubmitButton,
} from "../../../../components/Buttons/index";
import { ButtonsRow } from "../../../../components/Buttons/styles";
import { Text } from "../../../../components/Forms/index";
import { MoneyTextField } from "../../../../components/TextField/index";
import { editBandHelper } from "../../../../helpers/Configurations/commissions.helper";
import jaiz from "../../../../images/jaiz-logo.png";
import { styles } from "../styles";

export default function EditTransactionType(props) {
  const classes = styles();
  const [state, setState] = useState({
    open: false,
    lowerlimit: props.row["lower-limit"],
    upperlimit: props.row["upper-limit"],
    charge: props.row["charge"],
    agentcommission: props.row["agent-commission"],
    superAgentcommission: props.row["super-agent-commission"],
    formError: false,
  });
  const dispatch = useDispatch();
  const loading = useSelector(
    (state) => state.commissionsReducer.actionLoading
  );
  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    let numberValue = value.substring(1);
    // eslint-disable-next-line
    numberValue = numberValue.replace(/\,/g, "");
    numberValue = parseFloat(numberValue, 10);
    let finalNumber = numberValue ? numberValue : numberValue === 0 ? 0 : "";
    setState((prev) => ({
      ...prev,
      [name]: finalNumber,
    }));
  };
  const handleClickOpen = () => {
    setState((prev) => ({
      ...prev,
      open: true,
    }));
  };
  const handleClose = (e) => {
    e.preventDefault();
    setState((prev) => ({
      ...prev,
      open: false,
      code: "",
      category: "",
    }));
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const { lowerlimit, upperlimit, charge, agentcomission } = state;

    if (
      lowerlimit === "" ||
      upperlimit === "" ||
      charge === "" ||
      agentcomission === ""
    ) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      const inputData = {
        "lower-limit": state.lowerlimit,
        "upper-limit": state.upperlimit,
        "agent-commission": state.agentcommission,
        "super-agent-commission": state.superAgentcommission,
        charge: state.charge,
        "fee-id": props.row["fee-id"],
        "group-id": props.row["group-id"],
        id: props.row.id,
      };
      dispatch(editBandHelper(inputData));
    }
  };

  const handleCloseSnack = () => dispatch(clearMessages());
  const renderFieldError = (formErrorMessage) => {
    <Snackbars
      variant="error"
      handleClose={handleCloseSnack}
      message={formErrorMessage}
      isOpen={state.formError}
    />;
  };
  return (
    <div>
      <List classes={{ root: classes.button }} onClick={handleClickOpen}>
        <ListItemIcon classes={{ root: classes.listItemIcon }}>
          <EditSharpIcon className={classes.editIcon} />
        </ListItemIcon>
        <Text className={classes.editText}>Edit</Text>
      </List>
      {state.formError === true
        ? renderFieldError(
            state.lowerlimit === ""
              ? "Lower limit is required"
              : state.upperlimit === ""
              ? "Upper limit is required"
              : state.charge === ""
              ? "Transaction charge limit is required"
              : state.agentcommission === ""
              ? "Agent commission is required"
              : ""
          )
        : null}
      <Dialog
        open={state.open}
        keepMounted
        aria-labelledby="alert-dialog-slide-title"
        className="add-branch-dialog"
        aria-describedby="alert-dialog-slide-description"
      >
        <div className={classes.actionsRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12}>
              <form
                className={classes.container}
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Edit fee band
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <MoneyTextField
                    id="lowerlimit"
                    htmlFor="lowerlimit"
                    label="Lower limit"
                    type="text"
                    name="lowerlimit"
                    value={state.lowerlimit}
                    onChange={handleChange}
                    thousandsGroupStyle="thousand"
                    prefix={"₦ "}
                    thousandSeparator={true}
                    allowEmptyFormatting={true}
                    allowNegative={false}
                    fixedDecimalScale={true}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <MoneyTextField
                    id="upperlimit"
                    htmlFor="upperlimit"
                    label="Upper limit"
                    name="upperlimit"
                    type="text"
                    value={state.upperlimit}
                    onChange={handleChange}
                    thousandsGroupStyle="thousand"
                    prefix={"₦ "}
                    thousandSeparator={true}
                    allowEmptyFormatting={true}
                    allowNegative={false}
                    fixedDecimalScale={true}
                    grouped
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <MoneyTextField
                    id="charge"
                    htmlFor="charge"
                    label="Charge"
                    name="charge"
                    type="text"
                    value={state.charge}
                    onChange={handleChange}
                    thousandsGroupStyle="thousand"
                    prefix={"₦ "}
                    thousandSeparator={true}
                    allowEmptyFormatting={true}
                    allowNegative={false}
                    fixedDecimalScale={true}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <MoneyTextField
                    id="agentcommission"
                    htmlFor="agentcommission"
                    label="Agent commission"
                    type="text"
                    name="agentcommission"
                    value={state.agentcommission}
                    onChange={handleChange}
                    thousandsGroupStyle="thousand"
                    prefix={"₦ "}
                    thousandSeparator={true}
                    allowEmptyFormatting={true}
                    allowNegative={false}
                    fixedDecimalScale={true}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <MoneyTextField
                    id="superAgentcommission"
                    htmlFor="superAgentcommission"
                    label="Super agent commission"
                    name="superAgentcommission"
                    type="text"
                    value={state.superAgentcommission}
                    onChange={handleChange}
                    thousandsGroupStyle="thousand"
                    prefix={"₦ "}
                    thousandSeparator={true}
                    allowEmptyFormatting={true}
                    allowNegative={false}
                    fixedDecimalScale={true}
                    grouped
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      onClick={handleClose}
                      style={{ marginRight: 10 }}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        backgroundColor: loading ? Colors.primary : "",
                        marginTop: 0,
                      }}
                      disableRipple={loading}
                    >
                      <span className="submit-btn">
                        {loading ? (
                          <Loader
                            type="ThreeDots"
                            color="#B28B0A"
                            height="15"
                            width="30"
                          />
                        ) : (
                          "save"
                        )}
                      </span>
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </div>
      </Dialog>
    </div>
  );
}
