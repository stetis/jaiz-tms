import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Box from "@material-ui/core/Box";
import TransactionTypeTable from "./table";
import { Colors, Fonts } from "../../../../assets/themes/theme";

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  const classes = useStyles();
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box component="div" className={classes.box}>
          {children}
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
    backgroundColor: "transparent",
    display: "flex",
    width: "100%",
    marginTop: 25,
    "@media only screen and (max-width: 480px)": {
      width: "100%",
      display: "inline-block",
    },
  },
  tabRoot: {
    minHeight: 45,
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
    fontWeight: 500,
    textTransform: "inherit",
    color: Colors.primary,
  },
  tabs: {
    borderLeft: `1px solid ${Colors.secondary}`,
    width: 405,
    backgroundColor: Colors.light,
    borderRadius: 5,
    cursor: "pointer",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (max-width: 480px)": {
      width: "100%",
      display: "inline-flex",
    },
  },
  wrapper: {
    alignItems: "flex-start",
  },
  tabSelected: {
    color: Colors.secondary,
  },
  panel: {
    width: "100%",
  },
  box: {
    padding: 0,
    margin: "0 0 0 20px",
    "@media only screen and (max-width: 480px)": {
      margin: 0,
    },
  },
}));

export default function TransactonTab(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Transaction tab"
        className={classes.tabs}
      >
        {props.fees &&
          props.fees.map((fee, i) => {
            return (
              <Tab
                label={fee["display-name"]}
                {...a11yProps(i)}
                classes={{
                  root: classes.tabRoot,
                  selected: classes.tabSelected,
                  wrapper: classes.wrapper,
                }}
                key={fee.id}
              />
            );
          })}
      </Tabs>
      {props.fees &&
        props.fees.map((fee, i) => {
          return (
            <TabPanel
              value={value}
              index={i}
              className={classes.panel}
              key={fee.id}
            >
              <TransactionTypeTable
                name={fee["display-name"]}
                data={fee.bands}
                id={fee.id}
                limit={fee["daily-limit"]}
              />
            </TabPanel>
          );
        })}
    </div>
  );
}
