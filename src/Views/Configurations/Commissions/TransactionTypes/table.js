import { Grid, MuiThemeProvider } from "@material-ui/core";
import SaveIcon from "@material-ui/icons/Save";
import MUIDataTable from "mui-datatables";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../../actions/Configurations/commissions.actions";
import { theme } from "../../../../assets/MUI/Table";
import Snackbars from "../../../../assets/Snackbars/index";
import Spinner from "../../../../assets/Spinner";
import { Colors } from "../../../../assets/themes/theme";
import CallOut from "../../../../components/CallOut/CallOut";
import { Title } from "../../../../components/contentHolders/Card";
import { MoneyTextField } from "../../../../components/TextField";
import {
  bandHelper,
  editFeeHelper,
} from "../../../../helpers/Configurations/commissions.helper";
import jaiz from "../../../../images/jaiz-logo.png";
import { LightTooltip } from "../../../Terminals/styles";
import { styles } from "../styles";
import AddFeeBand from "./addFeeBand";
import DeleteTransactionType from "./delete";
import EditTransactionType from "./edit";

const styleProps = {
  height: 50,
  width: 50,
};
function TransactionTypeTable(props) {
  const classes = styles();
  const [dailylimit, setDailylimit] = useState(props.limit);
  const [formError, setFormError] = useState(false);
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.commissionsReducer.bandLoading);
  const bands = useSelector((state) => state.commissionsReducer.bandData.bands);
  const error = useSelector((state) => state.commissionsReducer.bandError);
  const errorMessage = useSelector(
    (state) => state.commissionsReducer.bandErrorMessage
  );
  useEffect(() => {
    const timer = setTimeout(() => {
      setFormError(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [formError]);
  useEffect(() => {
    dispatch(bandHelper(props.id));
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const handleCloseSnack = () => dispatch(clearMessages());
  const handleChange = (e) => {
    const { value } = e.target;
    let numberValue = value.substring(1);
    // eslint-disable-next-line
    numberValue = numberValue.replace(/\,/g, "");
    numberValue = parseFloat(numberValue, 10);
    let finalNumber = numberValue ? numberValue : numberValue === 0 ? 0 : "";
    setDailylimit(finalNumber);
  };
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={formError}
      />
    );
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (dailylimit === "") {
      setFormError(true);
    } else {
      const inputData = {
        id: props.id,
        "daily-limit": dailylimit,
      };
      dispatch(editFeeHelper(inputData));
    }
  };
  const columns = [
    "",
    "Lower limit",
    "Upper limit",
    "Charge",
    "Agent's commission %",
    "Super agent's commission %",
    " ",
  ];
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: true,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    rowsPerPageOptions: [],
    customToolbar: () => <AddFeeBand id={props.id} />,
    textLabels: {
      body: {
        noMatch: "No fee bands record found",
      },
    },
  };
  return (
    <div>
      {formError === true
        ? renderFieldError(dailylimit === "" ? "Daily limit is required" : "")
        : null}
      <div className={classes.TransactionTableCard}>
        {!loading ? (
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <div className={classes.transLogo}>
                <img
                  src={jaiz}
                  alt="jaiz logo"
                  style={{
                    width: 20,
                    height: 20,
                    marginRight: "1rem",
                  }}
                />
                {props.name}
              </div>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Title
                style={{
                  display: "flex",
                  // margin: "10px 0",
                  color: Colors.textColor,
                }}
              >
                <span className={classes.label}>Daily limit</span>
                <MoneyTextField
                  id="dailylimit"
                  htmlFor="dailylimit"
                  label=""
                  type="text"
                  name="dailylimit"
                  value={dailylimit}
                  onChange={handleChange}
                  thousandsGroupStyle="thousand"
                  prefix={"₦ "}
                  thousandSeparator={true}
                  allowEmptyFormatting={true}
                  allowNegative={false}
                  fixedDecimalScale={true}
                />
                <LightTooltip title="Save daily limit" position="right">
                  <SaveIcon
                    className={classes.saveIcon}
                    onClick={handleSubmit}
                  />
                </LightTooltip>
              </Title>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <hr
                style={{
                  margin: "8px 0 0",
                  height: 0.5,
                  border: "none",
                  color: Colors.greyLight,
                  backgroundColor: Colors.greyLight,
                }}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <MuiThemeProvider theme={theme}>
                <MUIDataTable
                  data={
                    bands && bands.constructor === Array
                      ? bands.map((band, i) => {
                          return {
                            "": i + 1,
                            "Lower limit": band["lower-limit"],
                            "Upper limit": band["upper-limit"],
                            Charge: band["charge"],
                            "Agent's commission %": band["agent-commission"],
                            "Super agent's commission %":
                              band["super-agent-commission"],
                            " ": (
                              <CallOut
                                TopAction={<EditTransactionType row={band} />}
                                BottomAction={
                                  <DeleteTransactionType row={band} />
                                }
                              />
                            ),
                          };
                        })
                      : []
                  }
                  title="Bands"
                  columns={columns}
                  options={options}
                />
              </MuiThemeProvider>
            </Grid>
          </Grid>
        ) : (
          <Spinner {...styleProps} />
        )}
        <Snackbars
          variant="error"
          handleClose={handleCloseSnack}
          message={errorMessage}
          isOpen={error}
        />
      </div>
    </div>
  );
}
export default TransactionTypeTable;
