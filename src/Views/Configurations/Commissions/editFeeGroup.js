import { Grid, List, ListItemIcon } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../actions/Configurations/commissions.actions";
import { EditIcon } from "../../../assets/icons/Svg";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { TextField } from "../../../components/TextField/index";
import { editFeeGroupHelper } from "../../../helpers/Configurations/commissions.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "./styles";

function EditFeeGroup(props) {
  const classes = styles();
  const [state, setState] = useState({
    open: false,
    group: props.row.name,
    description: props.row.description,
    formError: false,
  });
  const dispatch = useDispatch();
  const loading = useSelector(
    (state) => state.commissionsReducer.actionLoading
  );
  const status = useSelector((state) => state.commissionsReducer.status);
  const statusMessage = useSelector(
    (state) => state.commissionsReducer.statusMessage
  );

  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };
  const handleClickOpen = () => {
    setState((prev) => ({
      ...prev,
      open: true,
    }));
  };
  const handleClose = (e) => {
    e.preventDefault();
    setState((prev) => ({
      ...prev,
      open: false,
      group: "",
      description: "",
    }));
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (state.group === "") {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      const inputData = {
        id: props.row.id,
        name: state.group,
        description: state.description,
      };
      dispatch(editFeeGroupHelper(inputData));
    }
  };

  const handleCloseSnack = () => dispatch(clearMessages());
  const renderFieldError = (formErrorMessage) => {
    <Snackbars
      variant="error"
      handleClose={handleCloseSnack}
      message={formErrorMessage}
      isOpen={state.formError}
    />;
  };
  return (
    <div>
      <List classes={{ root: classes.button }} onClick={handleClickOpen}>
        <ListItemIcon classes={{ root: classes.listItemIcon }}>
          <EditIcon className={classes.editIcon} />
        </ListItemIcon>
      </List>
      {state.formError === true
        ? renderFieldError(
            state.group === "" ? "Fee group name is required" : ""
          )
        : null}
      <Dialog
        open={state.open}
        keepMounted
        aria-labelledby="alert-dialog-slide-title"
        className="add-branch-dialog"
        aria-describedby="alert-dialog-slide-description"
      >
        <div className={classes.actionsRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12}>
              <form
                className={classes.container}
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Edit fee group
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextField
                    id="group"
                    htmlFor="group"
                    name="group"
                    type="text"
                    label="Group"
                    value={state.group}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextField
                    id="description"
                    htmlFor="description"
                    name="description"
                    type="text"
                    label="Decription"
                    value={state.description}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      style={{
                        marginRight: 10,
                      }}
                      onClick={handleClose}
                      disabled={loading}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        marginTop: 0,
                      }}
                    >
                      {loading ? (
                        <Loader
                          type="ThreeDots"
                          color={Colors.secondary}
                          height="15"
                          width="30"
                        />
                      ) : (
                        "Save"
                      )}
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </div>
      </Dialog>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}

// const mapStateToProps = (state) => {
//   return {
//     editCategoryState: state.branchesReducer,
//   };
// };

// const mapDispatchToProps = (dispatch) => {
//   return {
//     editCategory: (editCategoryData) =>
//       dispatch(fetchEditCategory(editCategoryData)),
//     refreshRequest: () => dispatch(editCategoryRefresh()),
//   };
// };

export default EditFeeGroup;
