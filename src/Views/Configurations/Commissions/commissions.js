import { Grid, MuiThemeProvider } from "@material-ui/core";
import NavigateNextOutlinedIcon from "@material-ui/icons/NavigateNextOutlined";
import MUIDataTable from "mui-datatables";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import { clearMessages } from "../../../actions/Configurations/commissions.actions";
import { theme } from "../../../assets/MUI/Table";
import Snackbars from "../../../assets/Snackbars/index";
import Spinner from "../../../assets/Spinner";
import { feeGroupHelper } from "../../../helpers/Configurations/commissions.helper";
import jaiz from "../../../images/jaiz-logo.png";
import AddFeeGroup from "./addFeeGroup";
import { styles } from "./styles";

const styleProps = {
  height: 50,
  width: 50,
};
function Commissions(props) {
  const classes = styles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowPerPage] = useState(10);

  const dispatch = useDispatch();
  const loading = useSelector(
    (state) => state.commissionsReducer.feeGroupLoading
  );
  const feeGroups = useSelector(
    (state) => state.commissionsReducer.feeGroupData.groups
  );
  const error = useSelector((state) => state.commissionsReducer.error);
  const errorMessage = useSelector(
    (state) => state.commissionsReducer.errorMessage
  );
  useEffect(() => {
    document.title = "Commission | Jaiz bank TMS";
    dispatch(feeGroupHelper());
    dispatch(
      updateBreadcrumb({
        parent: "Commission",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const onPageChange = (page) => setPage(page);
  const onChangeRowsPerPage = (numberOfRows) => setRowPerPage(numberOfRows);
  const handleCloseSnack = () => {
    dispatch(clearMessages());
  };
  const gotoManagegroup = (id) => {
    props.history.push({
      pathname: `/tms/manage-group`,
      state: {
        id: id,
      },
    });
  };
  const rowClickGotoManagegroup = (rowData) => {
    return props.history.push({
      pathname: `/tms/manage-group`,
      state: {
        row: { id: rowData[1], name: rowData[2] },
      },
    });
  };
  const columns = [
    "",
    {
      name: "id",
      options: {
        display: false,
      },
    },
    "Group",
    "Description",
    " ",
  ];
  const options = {
    pagination: true,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    rowsPerPage: rowsPerPage,
    page: page,
    serverSide: true,
    sort: false,
    rowHover: false,
    onRowClick: (rowData) => rowClickGotoManagegroup(rowData),
    selectableRows: "none",
    rowsPerPageOptions: [5, 10],
    customToolbar: () => {
      return <AddFeeGroup />;
    },
    textLabels: {
      body: {
        noMatch: "No fee group record found",
      },
    },
    onTableChange: (action, tableState) => {
      switch (action) {
        case "onRowsPerPageChange":
          onChangeRowsPerPage(tableState.rowsPerPage);
          break;
        case "onPageChange":
          onPageChange(tableState.page);
          break;
        default:
      }
    },
  };
  return (
    <div className={classes.card}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!loading ? (
            <MuiThemeProvider theme={theme}>
              <MUIDataTable
                data={
                  feeGroups && feeGroups.constructor === Array
                    ? feeGroups
                        .slice(
                          page * rowsPerPage,
                          page * rowsPerPage + rowsPerPage
                        )
                        .map((group, i) => {
                          return {
                            "": rowsPerPage * page + i + 1,
                            id: group.id,
                            Group: group.name,
                            Description: group.description,
                            " ": (
                              <NavigateNextOutlinedIcon
                                style={{ fontSize: 20 }}
                                onClick={gotoManagegroup.bind(this, group.id)}
                              />
                            ),
                          };
                        })
                    : []
                }
                columns={columns}
                options={options}
                title={
                  <div className={classes.tableLogo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Commision fees group
                  </div>
                }
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
    </div>
  );
}
export default Commissions;
