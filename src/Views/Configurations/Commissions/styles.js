import { makeStyles } from "@material-ui/core";
import { Colors, Fonts } from "../../../assets/themes/theme";

export const styles = makeStyles(() => ({
  manageGroupCard: {
    width: "90%",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "10px 15px 18px",
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 238,
    },
    "@media only screen and (min-width: 960px)": {
      width: 283,
    },
  },
  addFeebandFabIcon: {
    position: "absolute",
    top: 24,
    left: 272,
    zIndex: 200,
    "@media only screen and (min-width: 600px)": {
      left: 300,
      top: 30,
    },
    "@media only screen and (min-width: 960px)": {
      left: 592,
      top: 20,
    },
  },
  addFeeFabIcon: {
    position: "absolute",
    top: 34,
    left: 266,
    zIndex: 200,
    "@media only screen and (min-width: 600px)": {
      left: 494,
    },
    "@media only screen and (min-width: 960px)": {
      left: 644,
    },
  },
  card: {
    width: 320,
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 5px 25px",
    borderRadius: 5,
    cursor: "pointer",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 500,
    },
  },
  TransactionTableCard: {
    width: "100%",
    background: Colors.light,
    padding: "13px 5px 25px",
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
  },
  transactionCard: {
    width: 320,
    margin: "26px auto",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 5px 25px",
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 580,
    },
    "@media only screen and (min-width: 960px)": {
      width: 650,
    },
  },
  container: {
    display: "flex",
    padding: "8px 25px 25px",
    flexWrap: "wrap",
  },
  transcontainer: {
    display: "flex",
    padding: 5,
    flexWrap: "wrap",
  },
  saveIcon: {
    color: Colors.primary,
    fontSize: 35,
    cursor: "pointer",
    position: "relative",
    top: 26,
    left: 8,
  },
  label: {
    margin: "27px 8px 0 16px",
  },
  managegroupRoot: {
    width: 320,
    margin: "26px auto",
    "@media only screen and (min-width: 600px)": { width: 550 },
    "@media only screen and (min-width: 960px)": {
      width: 950,
    },
  },
  fabAddIcon: {
    fontSize: 14,
    color: Colors.secondary,
  },
  TransactionFabIcon: {
    position: "relative",
    top: 5,
    left: 240,
    minWidth: 15,
    minHeight: 15,
    width: 15,
    height: 12,
    padding: 10,
    lineHeight: 12,
    "@media only screen and (min-width: 600px)": {
      left: 524,
      top: 5,
    },
    "@media only screen and (min-width: 960px)": {
      left: 591,
      top: 0,
    },
  },
  removeFab: {
    position: "relative",
    top: 0,
    left: 238,
    minWidth: 10,
    minHeight: 10,
    width: 10,
    height: 12,
    padding: 8,
    lineHeight: 12,
    "@media only screen and (min-width: 600px)": {
      left: 135,
      top: -37,
    },
    "@media only screen and (min-width: 960px)": {
      left: 154,
      top: -37,
    },
  },
  removeFabIcon: {
    fontSize: 14,
  },
  fabIcon: {
    color: Colors.secondary,
    fontWeight: 600,
    "@media only screen and (max-width: 540px)": {
      position: "absolute",
      left: 170,
      top: 16,
    },
  },
  icon: {
    fontSize: 18,
    marginRight: 5,
  },

  transLabel: {
    cursor: "pointer",
    fontFamily: Fonts.secondary,
  },
  tableLogo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
    "@media only screen and (max-width: 540px)": {
      margin: "-15px 0 0 -10px",
    },
  },
  transLogo: {
    color: Colors.primary,
    display: "inline-block",
    cursor: "pointer",
    padding: "0px 24px 0px",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    marginTop: 15,
    // margin: "10px -25px 68px",
    fontFamily: Fonts.secondary,
  },
  logo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    padding: "0px 24px 0px",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    margin: "10px -25px 8px",
    fontFamily: Fonts.secondary,
  },
  deleteRoot: {
    width: 350,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    display: "flex",
    padding: "12px 18px 20px",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (max-width: 359px)": {
      width: "100%",
    },
    "@media only screen and (min-width: 360px) and (max-width: 700px)": {
      width: "100%",
    },
    "@media only screen and (min-width: 768px) and (max-width: 1200px)": {
      width: 350,
    },
  },
  addIcon: {
    fontSize: 20,
    color: Colors.secondary,
  },
  actionsRoot: {
    width: 350,
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (max-width: 359px)": {
      width: "100%",
    },
    "@media only screen and (min-width: 360px) and (max-width: 700px)": {
      width: "100%",
    },
    "@media only screen and (min-width: 768px) and (max-width: 1200px)": {
      width: 350,
    },
  },
  listItemIcon: {
    color: Colors.menuListColor,
    minWidth: 22,
    cursor: "pointer",
  },
  editIcon: {
    margin: "1px 0 0 -10px",
    color: Colors.secondary,
    cursor: "pointer",
  },
  editText: {
    color: Colors.primary,
    margin: "2px 0 0 -10px",
    cursor: "pointer",
  },
  button: {
    width: "100%",
    display: "flex",
    cursor: "pointer",
    marginLeft: 5,
    marginRight: 5,
    padding: 2,
    color: Colors.menuListColor,
    fontSize: Fonts.font,
  },
  DeleteIcon: {
    color: Colors.buttonError,
    margin: "1px 0 0 -10px",
    cursor: "pointer",
  },
  deleteText: {
    color: Colors.buttonError,
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
    margin: "2px 0 0px -10px",
    cursor: "pointer",
  },
}));
