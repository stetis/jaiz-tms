import { Grid, List, ListItemIcon } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import EditSharpIcon from "@material-ui/icons/EditSharp";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../actions/Configurations/titles.actions";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Text } from "../../../components/Forms/index";
import { TextField } from "../../../components/TextField/index";
import { editRegionHelper } from "../../../helpers/Configurations/regions.helper";
import { styles } from "./styles";
import jaiz from "../../../images/jaiz-logo.png";

export default function EditRegion(props) {
  const classes = styles();
  const [code, setCode] = useState(props.row.id);
  const [name, setName] = useState(props.row.name);
  const [open, setOpen] = useState(false);
  const [formError, setFormError] = useState(false);
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.regionsReducer.actionLoading);
  const status = useSelector((state) => state.regionsReducer.status);
  const statusMessage = useSelector(
    (state) => state.regionsReducer.statusMessage
  );

  useEffect(() => {
    const timer = setTimeout(() => {
      setFormError(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [formError]);
  const handleCodeChange = (e) => setCode(e.target.value);
  const handleChange = (e) => setName(e.target.value);
  const handleClickOpen = () => setOpen(true);
  const handleClose = (e) => {
    e.preventDefault();
    setOpen(false);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (code === "" || name === "") {
      setFormError(true);
    } else {
      const inputData = {
        id: props.row.id,
        name: name,
      };
      dispatch(editRegionHelper(inputData));
    }
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const renderFieldError = (formErrorMessage) => {
    <Snackbars
      variant="error"
      handleClose={handleCloseSnack}
      message={formErrorMessage}
      isOpen={formError}
    />;
  };
  return (
    <div>
      <List classes={{ root: classes.button }} onClick={handleClickOpen}>
        <ListItemIcon classes={{ root: classes.listItemIcon }}>
          <EditSharpIcon className={classes.editIcon} />
        </ListItemIcon>
        <Text className={classes.editText}>Edit</Text>
      </List>
      {formError === true
        ? renderFieldError(
            code === ""
              ? "Region code is required"
              : name === ""
              ? "Region name is required"
              : ""
          )
        : null}
      <Dialog
        open={open}
        keepMounted
        aria-labelledby="alert-dialog-slide-title"
        className="add-branch-dialog"
        aria-describedby="alert-dialog-slide-description"
      >
        <div className={classes.actionsRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12}>
              <form
                className={classes.container}
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Edit region
                  </div>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <TextField
                    id="code"
                    htmlFor="code"
                    name="code"
                    type="text"
                    label="Code"
                    value={code}
                    onChange={handleCodeChange}
                    disabled
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <TextField
                    id="name"
                    htmlFor="name"
                    name="name"
                    type="text"
                    label="Name"
                    value={name}
                    onChange={handleChange}
                    grouped
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      style={{
                        marginRight: 10,
                      }}
                      onClick={handleClose}
                      disabled={loading}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        marginTop: 0,
                      }}
                    >
                      {loading ? (
                        <Loader
                          type="ThreeDots"
                          color={Colors.secondary}
                          height="15"
                          width="30"
                        />
                      ) : (
                        "Save"
                      )}
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </div>
      </Dialog>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
