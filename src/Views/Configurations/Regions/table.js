import { Grid } from "@material-ui/core";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { useSelector, useDispatch } from "react-redux";
import Snackbars from "../../../assets/Snackbars/index";
import { clearMessages } from "../../../actions/Configurations/region.actions";
import MUIDataTable from "mui-datatables";
import React, { useEffect } from "react";
import { theme } from "../../../assets/MUI/Table";
import Spinner from "../../../assets/Spinner";
import CallOut from "../../../components/CallOut/CallOut";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import { getRegionHelper } from "../../../helpers/Configurations/regions.helper";
import jaiz from "../../../images/jaiz-logo.png";
import DeleteRegion from "./delete";
import EditRegion from "./edit";
import AddRegion from "./add";
import { styles } from "./styles";

const styleProps = {
  height: 50,
  width: 50,
};
function RegionTable() {
  const classes = styles();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.regionsReducer.loading);
  const regions = useSelector((state) => state.regionsReducer.data.records);
  const error = useSelector((state) => state.regionsReducer.error);
  const errorMessage = useSelector(
    (state) => state.regionsReducer.errorMessage
  );
  useEffect(() => {
    document.title = "Regions | Jaiz bank TMS";
    dispatch(getRegionHelper());
    dispatch(
      updateBreadcrumb({
        parent: "Regions",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleCloseSnack = () => dispatch(clearMessages());
  const columns = ["", "Code", "Name", " "];
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: true,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    customToolbar: () => {
      return <AddRegion />;
    },
    textLabels: {
      body: {
        noMatch: "No region records found",
      },
    },
  };
  return (
    <div className={classes.card}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!loading ? (
            <MuiThemeProvider theme={theme}>
              <MUIDataTable
                data={
                  regions && regions.constructor === Array
                    ? regions.map((region, i) => {
                        return {
                          "": i + 1,
                          Code: region.id,
                          Name: region.name,
                          " ": (
                            <CallOut
                              TopAction={<EditRegion row={region} />}
                              BottomAction={<DeleteRegion row={region} />}
                            />
                          ),
                        };
                      })
                    : []
                }
                columns={columns}
                options={options}
                title={
                  <div className={classes.tableLogo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Regions
                  </div>
                }
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
    </div>
  );
}
export default RegionTable;
