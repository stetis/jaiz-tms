import { Grid, Popover } from "@material-ui/core";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../actions/Configurations/branch.actions";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Select, TextField } from "../../../components/TextField/index";
import { addBranchHelper } from "../../../helpers/Configurations/branches.helper";
import { getRegionHelper } from "../../../helpers/Configurations/regions.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { states } from "../../../utils/data";
import { styles } from "./styles";

export default function AddBranch() {
  const classes = styles();
  const [anchorEl, setAnchorEl] = useState(null);
  const [state, setState] = useState({
    code: "",
    name: "",
    state: "",
    region: "",
    formError: false,
  });
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.branchesReducer.actionLoading);
  const status = useSelector((state) => state.branchesReducer.status);
  const statusMessage = useSelector(
    (state) => state.branchesReducer.statusMessage
  );
  const regions = useSelector(
    (state) => state.regionsReducer.data && state.regionsReducer.data.records
  );

  useEffect(() => {
    dispatch(getRegionHelper());
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        FormError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  const handleClickOpen = (e) => setAnchorEl(e.currentTarget);
  const handleClose = (e) => {
    e.preventDefault();
    setAnchorEl(null);
    setState((prev) => ({
      ...prev,
      name: "",
      state: "",
      region: "",
    }));
  };
  const handlePopperClose = () => setAnchorEl(null);
  const handleCloseSnack = () => dispatch(clearMessages());
  const handleChange = (e) => {
    const { name, value } = e.target;
    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    const stateDetail = states.find((st) => st.name === state.state);
    const regionDetail = regions.find((region) => region.name === state.region);

    if (
      state.code === "" ||
      state.name === "" ||
      !state.state ||
      !state.region
    ) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      let inputData = {
        id: state.code,
        name: state.name,
        "state-code": stateDetail.code,
        "state-name": state.state,
        "region-id": regionDetail.id,
        region: state.region,
      };
      dispatch(addBranchHelper(inputData));
    }
  };
  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  return (
    <div>
      <SubmitButton
        onClick={handleClickOpen}
        className={classes.fabIcon}
        style={{ marginTop: 0 }}
      >
        <AddOutlinedIcon className={classes.icon} />
        Add branch
      </SubmitButton>
      {state.formError === true
        ? renderFieldError(
            state.code === ""
              ? "Branch code is required"
              : state.name === ""
              ? "Branch name is required"
              : !state.state
              ? "State is required"
              : !state.region
              ? "Region is required"
              : ""
          )
        : null}
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handlePopperClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <div className={classes.actionsRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12}>
              <form
                className={classes.container}
                noValidate
                onSubmit={handleSubmit}
                autoComplete="off"
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Add branch
                  </div>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <TextField
                    id="code"
                    htmlFor="code"
                    type="text"
                    label="Code"
                    name="code"
                    value={state.code}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <TextField
                    id="name"
                    htmlFor="name"
                    type="text"
                    label="Name"
                    name="name"
                    value={state.name}
                    onChange={handleChange}
                    grouped
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <Select
                    id="state"
                    htmlFor="state"
                    name="state"
                    label="State"
                    value={state.state}
                    onChange={handleChange}
                  >
                    {states.map((state) => {
                      return (
                        <option key={state.name} value={state.name}>
                          {state.name}
                        </option>
                      );
                    })}
                  </Select>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <Select
                    id="region"
                    htmlFor="region"
                    name="region"
                    label="Region"
                    value={state.region}
                    onChange={handleChange}
                    grouped
                  >
                    {regions &&
                      regions.map((region) => {
                        return (
                          <option key={region.id} value={region.name}>
                            {region.name}
                          </option>
                        );
                      })}
                  </Select>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      onClick={handleClose}
                      style={{ marginRight: 15 }}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      className={classes.submitAddButton}
                      disabled={loading}
                      style={{
                        marginTop: 0,
                        width: 65,
                      }}
                    >
                      {loading ? (
                        <Loader
                          type="ThreeDots"
                          color={Colors.secondary}
                          height="15"
                          width="30"
                        />
                      ) : (
                        "Save"
                      )}
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </div>
      </Popover>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
