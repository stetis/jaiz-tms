import { Grid, MuiThemeProvider } from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import XLSX from "xlsx";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import { clearMessages } from "../../../actions/posInventory.actions";
import { wideTable } from "../../../assets/MUI/Table";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { branchBulkUploadHelper } from "../../../helpers/Configurations/branches.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "./styles";

export default function BulkUpload(props) {
  const classes = styles();
  const fileUploader = React.useRef();
  const [state, setState] = useState({
    data: [],
    cols: [],
  });
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.categoryReducer.actionLoading);
  const status = useSelector((state) => state.categoryReducer.status);
  const statusMessage = useSelector(
    (state) => state.categoryReducer.statusMessage
  );
  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        formError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);
  useEffect(() => {
    document.title = "Branch bulk uppload | Jaiz bank TMS";
    dispatch(
      updateBreadcrumb({
        parent: "Branches",
        child: "Bulk upload",
        homeLink: "/tms",
        childLink: "/tms/branches",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const handleFile = (file) => {
    /* Boilerplate to set up FileReader */
    const reader = new FileReader();
    const rABS = !!reader.readAsBinaryString;
    reader.onload = (e) => {
      /* Parse data */
      const { result } = e.target;
      const bstr = result;
      const wb = XLSX.read(bstr, { type: rABS ? "binary" : "array" });
      /* Get first worksheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      /* Convert array of arrays */
      const data = XLSX.utils.sheet_to_json(ws, {
        header: 1,
      });
      /* Update state */
      setState({
        data: data,
        cols: make_cols(ws["!ref"]),
      });
    };
    if (rABS) reader.readAsBinaryString(file);
    else reader.readAsArrayBuffer(file);
  };
  const handleChange = (e) => {
    const { files } = e.target;
    if (files && files[0]) handleFile(files[0]);
  };
  const handleFileUpload = (e) => {
    e.preventDefault();
    fileUploader.current.click();
  };
  const suppress = (evt) => {
    evt.persist();
    evt.stopPropagation();
    evt.preventDefault();
  };
  const onDrop = (evt) => {
    evt.persist();
    evt.stopPropagation();
    evt.preventDefault();
    const { files } = evt.dataTransfer;
    if (files && files[0]) handleFile(files[0]);
  };
  const handleClose = (e) => {
    e.preventDefault();
    props.history.goBack();
  };
  const handleCloseSnack = () => dispatch(clearMessages());
  const handleSubmit = (e) => {
    e.preventDefault();
    let inputData = state.data.map((el) => {
      return {
        id: el[0],
        name: el[1],
        "state-code": "",
        "state-name": "",
        "region-id": el[2],
        region: el[3],
      };
    });
    inputData.shift();
    dispatch(branchBulkUploadHelper(inputData));
  };
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: false,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    rowsPerPageOptions: [],
    customToolbar: () => (
      <ButtonsRow>
        <DeleteButton
          style={{
            marginRight: 10,
          }}
          onClick={handleClose}
        >
          Cancel
        </DeleteButton>
        <SubmitButton type="submit" disabled={loading} onClick={handleSubmit}>
          {loading ? (
            <Loader
              type="ThreeDots"
              color={Colors.secondary}
              height="15"
              width="30"
            />
          ) : (
            "Save"
          )}
        </SubmitButton>
      </ButtonsRow>
    ),
    textLabels: {
      body: {
        noMatch:
          "No excel file uploaded yet. Either click on the button above to upload or drag and drop the file into this message",
      },
    },
  };
  return (
    <div>
      <div className={classes.bulkCard}>
        <Grid container spacing={0}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <input
              type="file"
              className="form-control"
              id="file"
              accept={SheetJSFT}
              onChange={handleChange}
              ref={fileUploader}
              style={{
                display: "none",
              }}
            />
            <SubmitButton
              onClick={handleFileUpload}
              ghost
              style={{ float: "right", marginTop: 15 }}
            >
              upload Brnaches
            </SubmitButton>
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <div onDrop={onDrop} onDragEnter={suppress} onDragOver={suppress}>
              <MuiThemeProvider theme={wideTable}>
                <MUIDataTable
                  data={state.data.slice(1)}
                  columns={state.cols}
                  options={options}
                  title={
                    <div className={classes.logo}>
                      <img
                        src={jaiz}
                        alt="jaiz logo"
                        style={{ width: 20, height: 20, marginRight: "1rem" }}
                      />
                      Merchant categories bulk upload
                    </div>
                  }
                />
              </MuiThemeProvider>
            </div>
          </Grid>
        </Grid>
      </div>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}

/* list of supported file types */
const SheetJSFT = ["xlsx", "xls", "csv"]
  .map(function (x) {
    return "." + x;
  })
  .join(",");
/* generate an array of column objects */
const make_cols = (refstr) => {
  let o = [],
    C = XLSX.utils.decode_range(refstr).e.c + 1;
  for (var i = 0; i < C; ++i)
    o[i] = {
      name: XLSX.utils.encode_col(i),
      key: i,
    };
  return o;
};
