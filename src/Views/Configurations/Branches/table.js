import { Grid, MuiThemeProvider } from "@material-ui/core";
import TablePagination from "@material-ui/core/TablePagination";
import BackupOutlinedIcon from "@material-ui/icons/BackupOutlined";
import MUIDataTable from "mui-datatables";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import { clearMessages } from "../../../actions/Configurations/branch.actions";
import { history } from "../../../App";
import { theme } from "../../../assets/MUI/Table";
import Snackbars from "../../../assets/Snackbars/index";
import Spinner from "../../../assets/Spinner";
import { SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import CallOut from "../../../components/CallOut/CallOut";
import { getBranchesHelper } from "../../../helpers/Configurations/branches.helper";
import jaiz from "../../../images/jaiz-logo.png";
import AddBranch from "./add";
import DeleteBranch from "./delete";
import EditBranch from "./edit";
import { styles } from "./styles";

const styleProps = {
  height: 50,
  width: 50,
};
function capitalizeFirstLetters(str) {
  var splitStr = str.toLowerCase().split(" ");
  for (var i = 0; i < splitStr.length; i++) {
    splitStr[i] =
      splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  return splitStr.join(" ");
}
export default function BranchTable() {
  const classes = styles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.branchesReducer.loading);
  const branches = useSelector((state) => state.branchesReducer.data.branches);
  const error = useSelector((state) => state.branchesReducer.error);
  const errorMessage = useSelector(
    (state) => state.branchesReducer.errorMessage
  );
  useEffect(() => {
    document.title = "Branches | Jaiz bank TMS";
    dispatch(getBranchesHelper());
    dispatch(
      updateBreadcrumb({
        parent: "Branches",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleGotoBulkupload = () => history.push("/tms/branches-bulkupload");
  const handleCloseSnack = () => dispatch(clearMessages());
  const columns = ["", "Code", "Name", "State", "Region", " "];
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: true,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    customToolbar: () => {
      return (
        <ButtonsRow className={classes.buttonRow}>
          <SubmitButton
            onClick={handleGotoBulkupload}
            className={classes.fabIcon}
            style={{ marginRight: 15 }}
          >
            <BackupOutlinedIcon className={classes.icon} />
            Bulk upload
          </SubmitButton>
          <AddBranch />
        </ButtonsRow>
      );
    },
    textLabels: {
      body: {
        noMatch: "No branch records found",
      },
    },
  };
  return (
    <div className={classes.card}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!loading ? (
            <MuiThemeProvider theme={theme}>
              <MUIDataTable
                data={
                  branches && branches.constructor === Array
                    ? branches
                        .slice(
                          page * rowsPerPage,
                          page * rowsPerPage + rowsPerPage
                        )
                        .map((branch, i) => {
                          return {
                            "": rowsPerPage * page + i + 1,
                            Code: branch.id,
                            Name: capitalizeFirstLetters(branch.name),
                            State: capitalizeFirstLetters(branch["state-name"]),
                            Region: capitalizeFirstLetters(branch.region),
                            " ": (
                              <CallOut
                                TopAction={<EditBranch row={branch} />}
                                BottomAction={<DeleteBranch row={branch} />}
                              />
                            ),
                          };
                        })
                    : []
                }
                columns={columns}
                options={options}
                title={
                  <div className={classes.tableLogo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Branches
                  </div>
                }
              />
              <TablePagination
                component="div"
                count={branches && branches.length !== 0 ? branches.length : 0}
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={rowsPerPage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                rowsPerPageOptions={[5, 10, 15, 20]}
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
    </div>
  );
}
