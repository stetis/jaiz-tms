import { makeStyles, Tooltip, withStyles } from "@material-ui/core";
import { Colors, Fonts } from "../../../assets/themes/theme";

export const LightTooltip = withStyles(() => ({
  tooltip: {
    backgroundColor: Colors.light,
    color: Colors.textColor,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
  },
}))(Tooltip);
export const styles = makeStyles(() => ({
  card: {
    width: "95%",
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 5px 25px",
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 960px)": {
      width: "60%",
    },
  },
  logo: {
    color: Colors.primary,
    display: "flex",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
  },
  container: {
    display: "flex",
    padding: "15px 25px 25px",
    flexWrap: "wrap",
  },
  addIcon: {
    fontSize: 20,
    color: Colors.secondary,
  },
  fabIcon: {
    color: Colors.secondary,
    fontWeight: 600,
  },
  icon: {
    fontSize: 18,
    marginRight: 5,
  },
  tableLogo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
    "@media only screen and (max-width: 540px)": {
      marginTop: -25,
    },
  },
  buttonRow: {
    "@media only screen and (max-width: 540px)": {
      position: "absolute",
      zIndex: 1000,
      top: 0,
    },
  },
  deleteRoot: {
    width: 320,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    display: "flex",
    padding: 25,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 350,
    },
  },
  actionsRoot: {
    width: 320,
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 350,
    },
  },
  bulkFabIcon: {
    position: "absolute",
    left: 240,
    "@media only screen and (min-width: 600px)": {
      position: "absolute",
      top: 30,
      left: "83%",
    },
  },
  listItemIcon: {
    color: Colors.menuListColor,
    minWidth: 22,
  },
  editIcon: {
    color: Colors.secondary,
    margin: "1px 0 0 -10px",
    cursor: "pointer",
  },
  editText: {
    color: Colors.primary,
    margin: "2px 0 0 -10px",
    cursor: "pointer",
  },
  button: {
    cursor: "pointer",
    width: "100%",
    display: "flex",
    marginLeft: 5,
    marginRight: 5,
    padding: 2,
    color: Colors.menuListColor,
    fontSize: Fonts.font,
  },
  DeleteIcon: {
    color: Colors.buttonError,
    margin: "1px 0 0 -10px",
    cursor: "pointer",
  },
  deleteText: {
    color: Colors.buttonError,
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
    margin: "2px 0 0px -10px",
    cursor: "pointer",
  },
}));
