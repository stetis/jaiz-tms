import { Grid, Popover } from "@material-ui/core";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../actions/Configurations/titles.actions";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import {
  DeleteButton,
  Fab,
  SubmitButton,
} from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { TextField } from "../../../components/TextField/index";
import { addTitleHelper } from "../../../helpers/Configurations/titles.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "./styles";

export default function AddTitle() {
  const classes = styles();
  const [title, setTitle] = useState("");
  const [anchorEl, setAnchorEl] = useState(null);
  const [formError, setFormError] = useState(false);
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.titleReducer.actionLoading);
  const status = useSelector((state) => state.titleReducer.status);
  const statusMessage = useSelector(
    (state) => state.titleReducer.statusMessage
  );

  useEffect(() => {
    const timer = setTimeout(() => {
      setFormError(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [formError]);
  const handleClickOpen = (e) => {
    setAnchorEl(e.currentTarget);
  };
  const handleClose = (e) => {
    e.preventDefault();
    setAnchorEl(null);
    setTitle("");
    setFormError("");
  };
  const handlePopperClose = () => {
    setAnchorEl(null);
  };
  const handleCloseSnack = () => {
    dispatch(clearMessages());
  };
  const handleChange = (e) => {
    setTitle(e.target.value);
  };
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={formError}
      />
    );
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (title === "") {
      setFormError(true);
    } else {
      let inputData = {
        name: title,
      };
      dispatch(addTitleHelper(inputData));
    }
  };
  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  return (
    <div>
      <Fab onClick={handleClickOpen} className={classes.fabIcon}>
        <AddOutlinedIcon className={classes.addIcon} />
      </Fab>
      {formError === true
        ? renderFieldError(title === "" ? "Title is required" : "")
        : null}
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handlePopperClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <div className={classes.actionsRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12}>
              <form
                className={classes.container}
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    New title
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextField
                    id="title"
                    htmlFor="title"
                    type="text"
                    label="Title"
                    title="title"
                    value={title}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      onClick={handleClose}
                      style={{ marginRight: 15, marginTop: 12 }}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      className={classes.submitAddButton}
                      disabled={loading}
                      style={{
                        backgroundColor: loading ? Colors.primary : "",
                        width: 65,
                      }}
                    >
                      {loading ? (
                        <Loader
                          type="ThreeDots"
                          color={Colors.secondary}
                          height="15"
                          width="30"
                        />
                      ) : (
                        "Save"
                      )}
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </div>
      </Popover>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
