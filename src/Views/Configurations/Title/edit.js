import { Grid, List, ListItemIcon } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import EditSharpIcon from "@material-ui/icons/EditSharp";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../actions/Configurations/titles.actions";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { Text } from "../../../components/Forms/index";
import { TextField } from "../../../components/TextField/index";
import { editTitleHelper } from "../../../helpers/Configurations/titles.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "./styles";

export default function EditTitle(props) {
  const classes = styles();
  const [title, setTitle] = useState(props.row.name);
  const [open, setOpen] = useState(false);
  const [formError, setFormError] = useState(false);
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.titleReducer.actionLoading);
  const status = useSelector((state) => state.titleReducer.status);
  const statusMessage = useSelector(
    (state) => state.titleReducer.statusMessage
  );
  useEffect(() => {
    const timer = setTimeout(() => {
      setFormError(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [formError]);

  const handleChange = (e) => setTitle(e.target.value);
  const handleClickOpen = () => setOpen(true);
  const handleClose = (e) => {
    e.preventDefault();
    setOpen(false);
    setTitle("");
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (title === "") {
      setFormError(true);
    } else {
      const inputData = {
        id: props.row.id,
        name: title,
      };
      dispatch(editTitleHelper(inputData));
    }
  };

  const handleCloseSnack = () => dispatch(clearMessages());

  const renderFieldError = (formErrorMessage) => {
    <Snackbars
      variant="error"
      handleClose={handleCloseSnack}
      message={formErrorMessage}
      isOpen={formError}
    />;
  };
  return (
    <div>
      <List classes={{ root: classes.button }} onClick={handleClickOpen}>
        <ListItemIcon classes={{ root: classes.listItemIcon }}>
          <EditSharpIcon className={classes.editIcon} />
        </ListItemIcon>
        <Text className={classes.editText}>Edit</Text>
      </List>
      {formError === true
        ? renderFieldError(title === "" ? "Title is required" : "")
        : null}
      <Dialog
        open={open}
        keepMounted
        aria-labelledby="alert-dialog-slide-title"
        className="add-branch-dialog"
        aria-describedby="alert-dialog-slide-description"
      >
        <div className={classes.actionsRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12}>
              <form
                className={classes.container}
                noValidate
                autoComplete="off"
                onSubmit={handleSubmit}
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Edit title
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextField
                    id="title"
                    htmlFor="title"
                    name="title"
                    type="text"
                    label="Title"
                    value={title}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      style={{
                        marginRight: 10,
                      }}
                      onClick={handleClose}
                      disabled={loading}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      disabled={loading}
                      style={{
                        marginTop: 0,
                      }}
                    >
                      {loading ? (
                        <Loader
                          type="ThreeDots"
                          color={Colors.secondary}
                          height="15"
                          width="30"
                        />
                      ) : (
                        "Save"
                      )}
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </div>
      </Dialog>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
