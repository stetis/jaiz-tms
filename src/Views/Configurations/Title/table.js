import { Grid } from "@material-ui/core";
import { MuiThemeProvider } from "@material-ui/core/styles";
import MUIDataTable from "mui-datatables";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../actions/Configurations/titles.actions";
import { theme } from "../../../assets/MUI/Table";
import Snackbars from "../../../assets/Snackbars/index";
import Spinner from "../../../assets/Spinner";
import CallOut from "../../../components/CallOut/CallOut";
import { getTitleHelper } from "../../../helpers/Configurations/titles.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import AddTitle from "./add";
import DeleteTitle from "./delete";
import EditTitle from "./edit";
import { styles } from "./styles";

const styleProps = {
  height: 50,
  width: 50,
};
export default function TitleTable() {
  const classes = styles();

  const dispatch = useDispatch();
  const loading = useSelector((state) => state.titleReducer.loading);
  const titles = useSelector((state) => state.titleReducer.data.titles);
  const error = useSelector((state) => state.titleReducer.error);
  const errorMessage = useSelector((state) => state.titleReducer.errorMessage);

  useEffect(() => {
    document.title = "Titles | Jaiz bank TMS";
    dispatch(getTitleHelper());
    dispatch(
      updateBreadcrumb({
        parent: "Titles",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  const handleCloseSnack = () => {
    dispatch(clearMessages());
  };
  const columns = ["", "Title", " "];
  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: true,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    rowsPerPageOptions: [5, 10],
    customToolbar: () => {
      return <AddTitle />;
    },
    textLabels: {
      body: {
        noMatch: "No title records found",
      },
    },
  };
  return (
    <div className={classes.card}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!loading ? (
            <MuiThemeProvider theme={theme}>
              <MUIDataTable
                data={
                  titles && titles.constructor === Array
                    ? titles.map((title, i) => {
                        return {
                          "": i + 1,
                          Title: title.name,
                          " ": (
                            <CallOut
                              TopAction={<EditTitle row={title} />}
                              BottomAction={<DeleteTitle row={title} />}
                            />
                          ),
                        };
                      })
                    : []
                }
                columns={columns}
                options={options}
                title={
                  <div className={classes.tableLogo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Titles
                  </div>
                }
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
    </div>
  );
}
