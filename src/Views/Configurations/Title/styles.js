import { makeStyles } from "@material-ui/core";
import { Colors, Fonts } from "../../../assets/themes/theme";

export const styles = makeStyles(() => ({
  card: {
    width: 320,
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "15px 18px 25px",
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 500,
    },
  },
  container: {
    display: "flex",
    padding: "15px 25px 25px",
    flexWrap: "wrap",
  },
  logo: {
    color: Colors.primary,
    display: "flex",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
  },
  addIcon: {
    fontSize: 20,
    color: Colors.secondary,
  },
  fabIcon: {
    position: "absolute",
    top: 19,
    left: 271,
    zIndex: 200,
    "@media only screen and (min-width: 600px)": {
      top: 42,
      left: 423,
    },
  },
  tableLogo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
    position: "relative",
    top: 0,
    left: 10,
  },
  deleteRoot: {
    width: 320,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    display: "flex",
    padding: "12px 18px 20px",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 350,
    },
  },
  actionsRoot: {
    width: 320,
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 350,
    },
  },
  listItemIcon: {
    color: Colors.menuListColor,
    minWidth: 22,
    cursor: "pointer",
  },
  editIcon: {
    margin: "1px 0 0px -10px",
    color: Colors.secondary,
    cursor: "pointer",
  },
  editText: {
    color: Colors.primary,
    margin: "2px 0 0 -10px",
    cursor: "pointer",
  },
  button: {
    width: "100%",
    display: "flex",
    marginLeft: 5,
    marginRight: 5,
    padding: 2,
    cursor: "pointer",
    color: Colors.menuListColor,
    fontSize: Fonts.font,
  },
  DeleteIcon: {
    color: Colors.buttonError,
    margin: "1px 0 0 -10px",
    cursor: "pointer",
  },
  deleteText: {
    color: Colors.buttonError,
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
    margin: "2px 0 0px -10px",
    cursor: "pointer",
  },
}));
