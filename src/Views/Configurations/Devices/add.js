import { Grid, Popover } from "@material-ui/core";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";
import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { clearMessages } from "../../../actions/Configurations/devices.actions";
import Snackbars from "../../../assets/Snackbars/index";
import { Colors } from "../../../assets/themes/theme";
import { DeleteButton, SubmitButton } from "../../../components/Buttons/index";
import { ButtonsRow } from "../../../components/Buttons/styles";
import { TextField } from "../../../components/TextField/index";
import { addDeviceHelper } from "../../../helpers/Configurations/devices.helper";
import jaiz from "../../../images/jaiz-logo.png";
import { styles } from "./styles";

export default function AddDevice() {
  const classes = styles();
  const [anchorEl, setAnchorEl] = useState(null);
  const [state, setState] = useState({
    name: "",
    os: "",
    deviceID: "",
    model: "",
    type: "",
    formError: false,
  });
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.devicesReducer.actionLoading);
  const status = useSelector((state) => state.devicesReducer.status);
  const statusMessage = useSelector(
    (state) => state.devicesReducer.statusMessage
  );

  useEffect(() => {
    const timer = setTimeout(() => {
      setState((prev) => ({
        ...prev,
        FormError: false,
      }));
    }, 3000);
    return () => clearTimeout(timer);
  }, [state.formError]);

  const handleClick = (event) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };
  const handleClose = (e) => {
    e.persist();
    e.preventDefault();
    setAnchorEl(null);
    setState((prev) => ({
      ...prev,
      name: "",
      deviceID: "",
      os: "",
      model: "",
      type: "",
    }));
  };
  const handlePopperClose = () => setAnchorEl(null);
  const handleCloseSnack = () => dispatch(clearMessages());

  const handleChange = (e) => {
    e.persist();
    const { name, value } = e.target;
    setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  };
  const renderFieldError = (formErrorMessage) => {
    return (
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={formErrorMessage}
        isOpen={state.formError}
      />
    );
  };
  const handleSubmit = (e) => {
    e.persist();
    e.preventDefault();
    if (
      state.deviceID === "" ||
      state.os === "" ||
      state.name === "" ||
      state.model === "" ||
      state.type === ""
    ) {
      setState((prev) => ({
        ...prev,
        formError: true,
      }));
    } else {
      let inputData = {
        id: state.deviceID,
        os: state.os,
        name: state.name,
        model: state.model,
        type: state.type,
      };
      dispatch(addDeviceHelper(inputData));
    }
  };
  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  return (
    <div>
      <SubmitButton onClick={handleClick} className={classes.fabIcon}>
        <AddOutlinedIcon className={classes.addIcon} />
        Add device
      </SubmitButton>
      {state.formError === true
        ? renderFieldError(
            state.deviceID === ""
              ? "Device ID is required"
              : state.os === ""
              ? "Operating system is required"
              : state.name === ""
              ? "Device name is required"
              : state.model === ""
              ? "Device model is required"
              : state.type === ""
              ? "Device type is required"
              : ""
          )
        : null}
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handlePopperClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <div className={classes.actionsRoot}>
          <Grid container spacing={0}>
            <Grid item xs={12} sm={12}>
              <form
                className={classes.container}
                noValidate
                onSubmit={handleSubmit}
                autoComplete="off"
              >
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <div className={classes.logo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Add device
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <TextField
                    id="name"
                    htmlFor="name"
                    type="text"
                    label="Name"
                    name="name"
                    value={state.name}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <TextField
                    id="deviceID"
                    htmlFor="deviceID"
                    type="text"
                    label="Device ID"
                    name="deviceID"
                    value={state.deviceID}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <TextField
                    id="os"
                    htmlFor="os"
                    type="text"
                    label="OS"
                    name="os"
                    value={state.os}
                    onChange={handleChange}
                    grouped
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <TextField
                    id="model"
                    htmlFor="model"
                    type="text"
                    label="Model"
                    name="model"
                    value={state.model}
                    onChange={handleChange}
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                  <TextField
                    id="type"
                    htmlFor="type"
                    type="text"
                    label="Type"
                    name="type"
                    value={state.type}
                    onChange={handleChange}
                    grouped
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <ButtonsRow>
                    <DeleteButton
                      onClick={handleClose}
                      style={{ marginRight: 15, marginTop: 5 }}
                    >
                      Cancel
                    </DeleteButton>
                    <SubmitButton
                      type="submit"
                      className={classes.submitAddButton}
                      disabled={loading}
                      style={{
                        width: 65,
                        marginTop: 0,
                      }}
                    >
                      {loading ? (
                        <Loader
                          type="ThreeDots"
                          color={Colors.secondary}
                          height="15"
                          width="30"
                        />
                      ) : (
                        "Save"
                      )}
                    </SubmitButton>
                  </ButtonsRow>
                </Grid>
              </form>
            </Grid>
          </Grid>
        </div>
      </Popover>
      <Snackbars
        variant="success"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 1}
      />
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={statusMessage}
        isOpen={status === 0}
      />
    </div>
  );
}
