import { Grid } from "@material-ui/core";
import { MuiThemeProvider } from "@material-ui/core/styles";
import MUIDataTable from "mui-datatables";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateBreadcrumb } from "../../../actions/breadcrumbAction";
import { clearMessages } from "../../../actions/Configurations/titles.actions";
import { theme } from "../../../assets/MUI/Table";
import Snackbars from "../../../assets/Snackbars/index";
import Spinner from "../../../assets/Spinner";
import CallOut from "../../../components/CallOut/CallOut";
import { getDeviceHelper } from "../../../helpers/Configurations/devices.helper";
import jaiz from "../../../images/jaiz-logo.png";
import AddDevice from "./add";
import DeleteDevice from "./delete";
import EditDevice from "./edit";
import { styles } from "./styles";

const styleProps = {
  height: 50,
  width: 50,
};
export default function Devices() {
  const classes = styles();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.devicesReducer.loading);
  const devices = useSelector((state) => state.devicesReducer.data.devices);
  const error = useSelector((state) => state.devicesReducer.error);
  const errorMessage = useSelector(
    (state) => state.devicesReducer.errorMessage
  );

  useEffect(() => {
    document.title = "Devices | Jaiz bank TMS";
    dispatch(getDeviceHelper());
    dispatch(
      updateBreadcrumb({
        parent: "Devices",
        child: "",
        homeLink: "/tms",
        childLink: "",
      })
    );
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleCloseSnack = () => {
    dispatch(clearMessages());
  };
  const columns = ["", "Device ID", "Name", "Model", "Type", "OS", " "];

  const options = {
    pagination: false,
    filter: false,
    search: false,
    print: false,
    download: false,
    viewColumns: false,
    responsive: "standard",
    serverSide: true,
    sort: false,
    rowHover: false,
    selectableRows: "none",
    customToolbar: () => {
      return <AddDevice />;
    },
    textLabels: {
      body: {
        noMatch: "No device records found",
      },
    },
  };
  return (
    <div className={classes.card}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {!loading ? (
            <MuiThemeProvider theme={theme}>
              <MUIDataTable
                data={
                  devices && devices.constructor === Array
                    ? devices.map((device, i) => {
                        return {
                          "": i + 1,
                          "Device ID": device.id,
                          Name: device.name,
                          Model: device.model,
                          Type: device.type,
                          OS: device.os,
                          " ": (
                            <CallOut
                              TopAction={<EditDevice row={device} />}
                              BottomAction={<DeleteDevice row={device} />}
                            />
                          ),
                        };
                      })
                    : []
                }
                columns={columns}
                options={options}
                title={
                  <div className={classes.tableLogo}>
                    <img
                      src={jaiz}
                      alt="jaiz logo"
                      style={{ width: 20, height: 20, marginRight: "1rem" }}
                    />
                    Devices (POS terminals)
                  </div>
                }
              />
            </MuiThemeProvider>
          ) : (
            <Spinner {...styleProps} />
          )}
        </Grid>
      </Grid>
      <Snackbars
        variant="error"
        handleClose={handleCloseSnack}
        message={errorMessage}
        isOpen={error}
      />
    </div>
  );
}
