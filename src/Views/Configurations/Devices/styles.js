import { makeStyles } from "@material-ui/core";
import { Colors, Fonts } from "../../../assets/themes/theme";

export const styles = makeStyles(() => ({
  card: {
    width: 320,
    margin: "26px auto",
    display: "flex",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    padding: "13px 5px 25px",
    borderRadius: 5,
    cursor: "pointer",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 500,
    },
  },
  logo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
  },
  container: {
    display: "flex",
    padding: "15px 25px 25px",
    flexWrap: "wrap",
  },
  fabAddIcon: {
    fontSize: 14,
    color: Colors.secondary,
  },
  fabIcon: {
    position: "relative",
    top: 8,
    left: 0,
    "@media only screen and (max-width: 540px)": {
      left: 90,
    },
  },
  tableLogo: {
    color: Colors.primary,
    display: "flex",
    cursor: "pointer",
    textDecoration: "none",
    fontSize: 16,
    fontWeight: 700,
    fontFamily: Fonts.secondary,
    position: "absolute",
    top: 15,
    "@media only screen and (max-width: 540px)": {
      marginTop: -25,
      marginLeft: -10,
    },
  },
  deleteRoot: {
    width: 320,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    display: "flex",
    padding: "12px 18px 20px",
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 350,
    },
  },
  addIcon: {
    fontSize: 20,
    color: Colors.secondary,
  },
  actionsRoot: {
    width: 320,
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
    background: Colors.light,
    borderRadius: 5,
    boxShadow:
      "rgba(9, 30, 66, 0.08) 0px 0px 0px 0px, rgba(9, 30, 66, 0.08) 0px 3px 3px 0px",
    "@media only screen and (min-width: 600px)": {
      width: 350,
    },
  },
  listItemIcon: {
    color: Colors.menuListColor,
    minWidth: 22,
  },
  editIcon: {
    margin: "1px 0 0 -10px",
    color: Colors.secondary,
    cursor: "pointer",
  },
  editText: {
    margin: "2px 0 0 -10px",
    color: Colors.primary,
    cursor: "pointer",
  },
  button: {
    width: "100%",
    display: "flex",
    margin: "0 5px",
    padding: 2,
    cursor: "pointer",
    color: Colors.menuListColor,
    fontSize: Fonts.font,
  },
  DeleteIcon: {
    color: Colors.buttonError,
    margin: "1px 0 0 -10px",
    cursor: "pointer",
  },
  deleteText: {
    color: Colors.buttonError,
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
    margin: "2px 0 0px -10px",
    cursor: "pointer",
  },
}));
