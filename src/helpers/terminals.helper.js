import {
  singleTerminalRequest,
  singleTerminalSuccess,
  singleTerminalError,
  agentTerminalRequest,
  agentTerminalSuccess,
  agentTerminalError,
  terminalsRequest,
  terminalsSuccess,
  terminalsError,
  actionsRequest,
  actionsSuccess,
  actionsError,
  clearMessages,
} from "../actions/terminals.actions";
import { timeout, dispatchAndRefresh } from "../utils/Utils";
import {
  singleTerminalAPI,
  terminalsAPI,
  agentTerminalsAPI,
  addAgentTerminalAPI,
  addMerchantTerminalAPI,
  uploadmerchantTerminalsAPI,
  uploadAgentTerminalsAPI,
  decommissionTerminalAPI,
  editTerminalLocationAPI,
  editTerminalSlipAPI,
} from "../services/terminal.service";
import { refreshToken } from "./login.helper";
import { history } from "../App";

export function getTerminalsHelper(credentials) {
  return (dispatch) => {
    dispatch(terminalsRequest());
    return timeout(10000, terminalsAPI(credentials))
      .then((data) => dispatch(terminalsSuccess(data)))
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                terminalsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                terminalsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                terminalsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(terminalsError(error.data.error)));
      });
  };
}
export function getAgentTerminalsHelper(credentials) {
  return (dispatch) => {
    dispatch(agentTerminalRequest());
    return timeout(10000, agentTerminalsAPI(credentials))
      .then((data) => dispatch(agentTerminalSuccess(data)))
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                agentTerminalError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                agentTerminalError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                agentTerminalError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(agentTerminalError(error.data.error)));
      });
  };
}
export function getSingleTerminalHelper(id) {
  return (dispatch) => {
    dispatch(singleTerminalRequest());
    return timeout(10000, singleTerminalAPI(id))
      .then((data) => {
        dispatch(singleTerminalSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                singleTerminalError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                singleTerminalError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                singleTerminalError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(singleTerminalError(error.data.error)));
      });
  };
}
export function addAgentTerminalHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addAgentTerminalAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return history.push("/tms/terminals");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function decommissionTerminalHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, decommissionTerminalAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return history.push("/tms/terminals");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function addMerchantTerminalHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addMerchantTerminalAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return history.push("/tms/terminals");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
/* BULK UPLOADS */
export function uploadBulkMerchantTerminalsHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, uploadmerchantTerminalsAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return history.push({
          pathname: `/tms/add-bulkmerchant-terminals`,
          state: { data },
        });
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function uploadBulkAgentTerminalsHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, uploadAgentTerminalsAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return history.push({
          pathname: `/tms/add-bulkagent-terminals`,
          state: { data },
        });
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function editTerminalLocationHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editTerminalLocationAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function editTerminalSlipHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editTerminalSlipAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
