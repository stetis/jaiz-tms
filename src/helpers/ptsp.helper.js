import {
  ptspSingleRequest,
  ptspSingleSuccess,
  ptspSingleError,
  ptspRequest,
  ptspSuccess,
  ptspError,
  ptspContactRequest,
  ptspContactSuccess,
  ptspContactError,
  actionsRequest,
  actionsSuccess,
  actionsError,
  clearMessages,
} from "../actions/ptsp.actions";
import { timeout, dispatchAndRefresh } from "../utils/Utils";
import {
  ptspAPI,
  enablePTSPProfileAPI,
  revokePTSPProfileAPI,
  initiatePTSPResetPinAPI,
  singlePtspAPI,
  addPTSPAPI,
  editPTSPAPI,
  deletePTSPAPI,
  ptspContactAPI,
  addPTSPContactAPI,
  editPTSPContactAPI,
  deletePTSPContactAPI,
} from "../services/ptsp.service";
import { refreshToken } from "./login.helper";
import { history } from "../App";

export function getSinglePTSPHelper(id) {
  return (dispatch) => {
    dispatch(ptspSingleRequest());
    return timeout(10000, singlePtspAPI(id))
      .then((data) => {
        dispatch(ptspSingleSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                ptspSingleError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                ptspSingleError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                ptspSingleError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(ptspSingleError(error.data.error)));
      });
  };
}
export function getPTSPHelper() {
  return (dispatch) => {
    dispatch(ptspRequest());
    return timeout(10000, ptspAPI())
      .then((data) => {
        dispatch(ptspSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                ptspError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                ptspError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(ptspError(error.data.message), clearMessages())
            )
          : dispatch(dispatch(ptspError(error.data.error)));
      });
  };
}
export function addPTSPHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addPTSPAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getPTSPHelper());
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function editPTSPHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editPTSPAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getSinglePTSPHelper(inputData.id));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function deletePTSPHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, deletePTSPAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return history.push("/tms/ptsp");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function getPTSPContactHelper(id) {
  return (dispatch) => {
    dispatch(ptspContactRequest());
    return timeout(10000, ptspContactAPI(id))
      .then((data) => {
        return dispatch(ptspContactSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                ptspContactError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                ptspContactError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                ptspContactError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(ptspContactError(error.data.error)));
      });
  };
}
export function enablePTSPProfileHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, enablePTSPProfileAPI(inputData.id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getPTSPContactHelper(inputData["ptsp-id"]));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function revokePTSPProfileHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, revokePTSPProfileAPI(inputData.id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getPTSPContactHelper(inputData["ptsp-id"]));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function initiatePTSPResetPinHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, initiatePTSPResetPinAPI(inputData.id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getPTSPContactHelper(inputData["ptsp-id"]));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function addPTSPContactHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addPTSPContactAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getPTSPContactHelper(inputData["ptsp-id"]));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function editPTSPContactHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editPTSPContactAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getPTSPContactHelper(inputData["ptsp-id"]));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function deletePTSPContactHelper(row) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, deletePTSPContactAPI(row.id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getPTSPContactHelper(row["ptsp-id"]));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
