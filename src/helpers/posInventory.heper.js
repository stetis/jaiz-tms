import {
  posInventoryRequest,
  posInventorySuccess,
  posInventoryError,
  actionsRequest,
  actionsSuccess,
  actionsError,
  singlePosRequest,
  singlePosSuccess,
  singlePosError,
  posesRequest,
  posesSuccess,
  posesError,
  clearMessages,
} from "../actions/posInventory.actions";
import { timeout, dispatchAndRefresh } from "../utils/Utils";
import {
  posInventoriesAPI,
  addPOSInventoryAPI,
  editPOSInventoryAPI,
  deletePOSInventoryAPI,
  uploadBulkPOSInventoryAPI,
  singlePosAPI,
  allPosesAPI,
} from "../services/posInventory.service";
import { refreshToken } from "./login.helper";
import { history } from "../App";

export function getPOSInventoriesHelper() {
  return (dispatch) => {
    dispatch(posInventoryRequest());
    return timeout(10000, posInventoriesAPI())
      .then((data) => {
        dispatch(posInventorySuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                posInventoryError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                posInventoryError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                posInventoryError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(posInventoryError(error.data.error)));
      });
  };
}
export function getPOSesHelper(credentials) {
  return (dispatch) => {
    dispatch(posesRequest());
    return timeout(10000, allPosesAPI(credentials))
      .then((data) => {
        dispatch(posesSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                posesError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                posesError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                posesError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(posesError(error.data.error)));
      });
  };
}
export function getSinglePosHelper() {
  return (dispatch) => {
    dispatch(singlePosRequest());
    return timeout(10000, singlePosAPI())
      .then((data) => {
        dispatch(singlePosSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                singlePosError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                singlePosError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                singlePosError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(singlePosError(error.data.error)));
      });
  };
}
export function addPOSInventoryHelper(inputData) {
  const credentials = {
    page: 0,
    pagesize: 10,
    search: "",
    status: "all",
  };
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addPOSInventoryAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getPOSesHelper(credentials));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function uploadBulkPOSInventoryHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, uploadBulkPOSInventoryAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return history.goBack();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function editPOSInventoryHelper(inputData) {
  const credentials = {
    page: 0,
    pagesize: 10,
    search: "",
    status: "all",
  };
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editPOSInventoryAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getPOSesHelper(credentials));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function deletePOSInventoryHelper(id) {
  const credentials = {
    page: 0,
    pagesize: 10,
    search: "",
    status: "all",
  };
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, deletePOSInventoryAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getPOSesHelper(credentials));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
