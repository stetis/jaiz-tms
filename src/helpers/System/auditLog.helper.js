import {
  auditlogsRequest,
  auditlogsSuccess,
  auditlogsError,
  clearMessages,
} from "../../actions/System/auditlogs.actions";
import { timeout, dispatchAndRefresh } from "../../utils/Utils";
import { auditlogAPI } from "../../services/system.service";
import { refreshToken } from "../login.helper";

export function getAuditlogHelper() {
  return (dispatch) => {
    dispatch(auditlogsRequest());
    return timeout(10000, auditlogAPI())
      .then((data) => {
        dispatch(auditlogsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                auditlogsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                auditlogsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                auditlogsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(auditlogsError(error.data.error)));
      });
  };
}
