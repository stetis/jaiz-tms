import {
  usersRequest,
  usersSuccess,
  usersError,
  actionsRequest,
  actionsSuccess,
  actionsError,
  clearMessages,
} from "../../actions/System/users.actions";
import { timeout, dispatchAndRefresh } from "../../utils/Utils";
import {
  getPaginatedUsersAPI,
  getAllUsersAPI,
  addUserAPI,
  editUserAPI,
  deleteUserAPI,
  suspendRecallUserAPI,
  changeRoleAPI,
} from "../../services/system.service";
import { refreshToken } from "../login.helper";

export function getPaginatedUsersHelper(credentials) {
  return (dispatch) => {
    dispatch(usersRequest());
    return timeout(10000, getPaginatedUsersAPI(credentials))
      .then((data) => {
        return dispatch(usersSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                usersError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                usersError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                usersError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(usersError(error.data.error)));
      });
  };
}
export function getAllUsersHelper() {
  return (dispatch) => {
    dispatch(usersRequest());
    return timeout(10000, getAllUsersAPI())
      .then((data) => {
        return dispatch(usersSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                usersError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                usersError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                usersError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(usersError(error.data.error)));
      });
  };
}
export function addUserHelper(inputData) {
  let credentials = {
    search: "",
    page: 0,
    pagesize: 10,
  };
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addUserAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getPaginatedUsersHelper(credentials));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function editUserHelper(inputData) {
  let credentials = {
    search: "",
    page: 0,
    pagesize: 10,
  };
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editUserAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getPaginatedUsersHelper(credentials));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function deleteUserHelper(id) {
  let credentials = {
    search: "",
    page: 0,
    pagesize: 10,
  };
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, deleteUserAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getPaginatedUsersHelper(credentials));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function suspendRecallUserHelper(id) {
  let credentials = {
    search: "",
    page: 0,
    pagesize: 10,
  };
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, suspendRecallUserAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getPaginatedUsersHelper(credentials));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function changeRoleHelper(inputData) {
  let credentials = {
    search: "",
    page: 0,
    pagesize: 10,
  };
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, changeRoleAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getPaginatedUsersHelper(credentials));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
