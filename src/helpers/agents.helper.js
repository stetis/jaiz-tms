import {
  singleAgentRequest,
  singleAgentSuccess,
  singleAgentError,
  allAgentsRequest,
  allAgentsSuccess,
  allAgentsError,
  agentsError,
  agentsRequest,
  agentsSuccess,
  clearMessages,
  actionsError,
  actionsRequest,
  actionsSuccess,
} from "../actions/agents.actions";
import {
  singleAgentAPI,
  allAgentsAPI,
  getPaginatedAgentsAPI,
  addAgentAPI,
  deleteAgentAPI,
  editAgentAPI,
  superAgentsAPI,
  enableAgentProfileAPI,
  revokeAgentProfileAPI,
  initiateAgentResetPinAPI,
  singleSuperAgentsAPI,
  allSuperAgentsAPI,
  addSuperAgentAPI,
  deleteSuperAgentAPI,
  editSuperAgentAPI,
  enableSuperAgentProfileAPI,
  revokeSuperAgentProfileAPI,
  initiateSuperAgentResetPinAPI,
} from "../services/agent.service";
import { dispatchAndRefresh, timeout } from "../utils/Utils";
import { refreshToken } from "./login.helper";
import { history } from "../App";

export function getPaginatedAgentsHelper(credentials) {
  return (dispatch) => {
    dispatch(agentsRequest());
    return timeout(10000, getPaginatedAgentsAPI(credentials))
      .then((data) => {
        dispatch(agentsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                agentsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                agentsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                agentsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(agentsError(error.data.error)));
      });
  };
}
export function getAllAgentsHelper() {
  return (dispatch) => {
    dispatch(allAgentsRequest());
    return timeout(10000, allAgentsAPI())
      .then((data) => {
        dispatch(allAgentsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                allAgentsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                allAgentsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                allAgentsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(allAgentsError(error.data.error)));
      });
  };
}
export function getSingleAgentHelper(id) {
  return (dispatch) => {
    dispatch(singleAgentRequest());
    return timeout(10000, singleAgentAPI(id))
      .then((data) => {
        dispatch(singleAgentSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                singleAgentError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                singleAgentError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                singleAgentError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(singleAgentError(error.data.error)));
      });
  };
}
export function addAgentHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addAgentAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return (window.location = "/tms/manage-agents");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function editAgentHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editAgentAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function deleteAgentHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, deleteAgentAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return history.push("/tms/agents");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function revokeAgentProfileHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, revokeAgentProfileAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function initiateAgentResetPinHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, initiateAgentResetPinAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function enableAgentProfileHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, enableAgentProfileAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function getSuperAgentHelper(credentials) {
  return (dispatch) => {
    dispatch(agentsRequest());
    return timeout(10000, superAgentsAPI(credentials))
      .then((data) => {
        dispatch(agentsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                agentsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                agentsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                agentsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(agentsError(error.data.error)));
      });
  };
}
export function getAllSuperAgentsHelper() {
  return (dispatch) => {
    dispatch(allAgentsRequest());
    return timeout(10000, allSuperAgentsAPI())
      .then((data) => {
        dispatch(allAgentsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                allAgentsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                allAgentsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                allAgentsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(allAgentsError(error.data.error)));
      });
  };
}
export function getSingleSuperAgentHelper(id) {
  return (dispatch) => {
    dispatch(singleAgentRequest());
    return timeout(10000, singleSuperAgentsAPI(id))
      .then((data) => {
        dispatch(singleAgentSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                singleAgentError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                singleAgentError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                singleAgentError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(singleAgentError(error.data.error)));
      });
  };
}
export function addSuperAgentHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addSuperAgentAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return history.push("/tms/super-agents");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function editSuperAgentHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editSuperAgentAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function enableSuperAgentProfileHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, enableSuperAgentProfileAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function revokeSuperAgentProfileHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, revokeSuperAgentProfileAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function initiateSuperAgentResetPinHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, initiateSuperAgentResetPinAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function deleteSuperAgentHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, deleteSuperAgentAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
