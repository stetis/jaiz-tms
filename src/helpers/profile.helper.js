import {
  passportRequest,
  passportSuccess,
  passportError,
  getPassportRequest,
  getPassportSuccess,
  getPassportError,
  rolesRequest,
  rolesSuccess,
  rolesError,
  roleRequest,
  roleSuccess,
  roleError,
  getModuleRequest,
  getModuleSuccess,
  getModuleError,
  actionsRequest,
  actionsSuccess,
  actionsError,
  clearMessages,
} from "../actions/profile.action";
import { timeout, dispatchAndRefresh } from "../utils/Utils";
import {
  updateProfileAPI,
  changePasswordAPI,
  changePassportAPI,
  getPassportAPI,
  singleRoleAPI,
  rolesAPI,
  getModuleAPI,
  addRoleAPI,
  editRoleAPI,
  deleteRoleAPI,
} from "../services/profile.service";
import { refreshToken } from "./login.helper";
import { history } from "../App";

export function updateProfileHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, updateProfileAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function changePasswordHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, changePasswordAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return history.push("/");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function updatePhotoHelper(passport) {
  return (dispatch) => {
    dispatch(passportRequest());
    return timeout(10000, changePassportAPI(passport))
      .then((data) => {
        return dispatch(passportSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                passportError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                passportError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                passportError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(passportError(error.data.error)));
      });
  };
}
export function getPhotoHelper() {
  return (dispatch) => {
    dispatch(getPassportRequest());
    return timeout(10000, getPassportAPI())
      .then((data) => {
        return dispatch(getPassportSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                getPassportError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                getPassportError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                getPassportError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(getPassportError(error.data.error)));
      });
  };
}

/* ROLES */
export function getRoleHelper(id) {
  return (dispatch) => {
    dispatch(roleRequest());
    return timeout(10000, singleRoleAPI(id))
      .then((data) => dispatch(roleSuccess(data)))
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                roleError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                roleError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(roleError(error.data.message), clearMessages())
            )
          : dispatch(dispatch(roleError(error.data.error)));
      });
  };
}
export function getModuleHelper() {
  return (dispatch) => {
    dispatch(getModuleRequest());
    return timeout(10000, getModuleAPI())
      .then((data) => dispatch(getModuleSuccess(data)))
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                getModuleError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                getModuleError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                getModuleError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(getModuleError(error.data.error)));
      });
  };
}
export function getRolesHelper() {
  return (dispatch) => {
    dispatch(rolesRequest());
    return timeout(10000, rolesAPI())
      .then((data) => dispatch(rolesSuccess(data)))
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                rolesError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                rolesError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                rolesError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(rolesError(error.data.error)));
      });
  };
}
export function addRoleHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addRoleAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return (window.location = "/tms/roles");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function editRoleHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editRoleAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        // return (window.location = "/tms/roles");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function deleteRoleHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, deleteRoleAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return (window.location = "/tms/roles");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
