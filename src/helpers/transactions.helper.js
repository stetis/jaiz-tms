import {
  merchantTransactionsRequest,
  merchantTransactionsSuccess,
  merchantTransactionsError,
  agentTrasactionsRequest,
  agentTrasactionsSuccess,
  agentTrasactionsError,
  agentCommisssionsRequest,
  agentCommisssionsSuccess,
  agentCommisssionsError,
  merchantPerformanceRequest,
  merchantPerformanceSuccess,
  merchantPerformanceError,
  branchAnalyticsRequest,
  branchAnalyticsSuccess,
  branchAnalyticsError,
  eJournalSuccess,
  eJournalRequest,
  eJournalError,
  transactionsRequest,
  transactionsSuccess,
  transactionsError,
  clearMessages,
} from "../actions/transactions.actions";
import { timeout, dispatchAndRefresh } from "../utils/Utils";
import {
  merchantTransactionsAPI,
  getAgentTransactionsAPI,
  getAgentCommissionsAPI,
  getMerchantsPerformanceAPI,
  getBranchAnalyticsAPI,
  getEjournalAPI,
} from "../services/transactions.service";
import { refreshToken } from "./login.helper";

export function getMerchantsTransactionsHelper(credentials) {
  return (dispatch) => {
    dispatch(merchantTransactionsRequest());
    return timeout(10000, merchantTransactionsAPI(credentials))
      .then((data) => dispatch(merchantTransactionsSuccess(data)))
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                merchantTransactionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                merchantTransactionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                merchantTransactionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(merchantTransactionsError(error.data.error)));
      });
  };
}
export function getAllMerchantsTransactionsHelper(credentials) {
  return (dispatch) => {
    dispatch(transactionsRequest());
    return timeout(10000, merchantTransactionsAPI(credentials))
      .then((data) => dispatch(transactionsSuccess(data)))
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                transactionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                transactionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                transactionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(transactionsError(error.data.error)));
      });
  };
}

export function getAgentsTransactionsHelper(credentials) {
  return (dispatch) => {
    dispatch(agentTrasactionsRequest());
    return timeout(10000, getAgentTransactionsAPI(credentials))
      .then((data) => dispatch(agentTrasactionsSuccess(data)))
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                agentTrasactionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                agentTrasactionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                agentTrasactionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(agentTrasactionsError(error.data.error)));
      });
  };
}

export function getAllAgentsTransactionsHelper(credentials) {
  return (dispatch) => {
    dispatch(transactionsRequest());
    return timeout(10000, getAgentTransactionsAPI(credentials))
      .then((data) => dispatch(transactionsSuccess(data)))
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                transactionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                transactionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                transactionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(transactionsError(error.data.error)));
      });
  };
}
export function getAgentsCommissionsHelper(credentials) {
  return (dispatch) => {
    dispatch(agentCommisssionsRequest());
    return timeout(10000, getAgentCommissionsAPI(credentials))
      .then((data) => dispatch(agentCommisssionsSuccess(data)))
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                agentCommisssionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                agentCommisssionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                agentCommisssionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(agentCommisssionsError(error.data.error)));
      });
  };
}
export function getAllAgentsCommissionsHelper(credentials) {
  return (dispatch) => {
    dispatch(transactionsRequest());
    return timeout(10000, getAgentCommissionsAPI(credentials))
      .then((data) => dispatch(transactionsSuccess(data)))
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                transactionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                transactionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                transactionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(transactionsError(error.data.error)));
      });
  };
}
export function getMerchantPerformanceHelper(credentials) {
  return (dispatch) => {
    dispatch(merchantPerformanceRequest());
    return timeout(10000, getMerchantsPerformanceAPI(credentials))
      .then((data) => dispatch(merchantPerformanceSuccess(data)))
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                merchantPerformanceError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                merchantPerformanceError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                merchantPerformanceError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(merchantPerformanceError(error.data.error)));
      });
  };
}
export function getAllMerchantPerformanceHelper(credentials) {
  return (dispatch) => {
    dispatch(transactionsRequest());
    return timeout(10000, getMerchantsPerformanceAPI(credentials))
      .then((data) => dispatch(transactionsSuccess(data)))
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                transactionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                transactionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                transactionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(transactionsError(error.data.error)));
      });
  };
}
export function getBranchAnalyticsHelper() {
  return (dispatch) => {
    dispatch(branchAnalyticsRequest());
    return timeout(10000, getBranchAnalyticsAPI())
      .then((data) => dispatch(branchAnalyticsSuccess(data)))
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                branchAnalyticsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                branchAnalyticsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                branchAnalyticsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(branchAnalyticsError(error.data.error)));
      });
  };
}
export function getAllBranchAnalyticsHelper() {
  return (dispatch) => {
    dispatch(transactionsRequest());
    return timeout(10000, getBranchAnalyticsAPI())
      .then((data) => dispatch(transactionsSuccess(data)))
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                transactionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                transactionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                transactionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(transactionsError(error.data.error)));
      });
  };
}
export function getEjournalHelper() {
  return (dispatch) => {
    dispatch(eJournalRequest());
    return timeout(10000, getEjournalAPI())
      .then((data) => dispatch(eJournalSuccess(data)))
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                eJournalError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                eJournalError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                eJournalError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(eJournalError(error.data.error)));
      });
  };
}
