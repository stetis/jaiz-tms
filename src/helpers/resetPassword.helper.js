import {
  initiatePasswordResetRequest,
  initiatePasswordResetSuccess,
  initiatePasswordResetError,
  validateEmailRequest,
  validateEmailSuccess,
  validateEmailError,
  completePasswordResetRequest,
  completePasswordResetSuccess,
  completePasswordResetError,
  clearMessages,
} from "../actions/fortgotPassword.action";
import { timeout, dispatchAndRefresh } from "../utils/Utils";
import {
  initiatePasswordResetAPI,
  validateUserEmailAPI,
  completePasswordResetAPI,
} from "../services/resetPassword.service";
import { history } from "../App";

export const initPasswordReset = (email) => {
  return (dispatch) => {
    dispatch(initiatePasswordResetRequest());
    return timeout(10000, initiatePasswordResetAPI(email))
      .then((data) => {
        return dispatch(initiatePasswordResetSuccess(data));
      })
      .catch((error) => {
        console.log(error, "erre");
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                initiatePasswordResetError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                initiatePasswordResetError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                initiatePasswordResetError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(initiatePasswordResetError(error.data.error)));
      });
  };
};
export const validatePasswordReset = (token) => {
  return (dispatch) => {
    dispatch(validateEmailRequest());
    return timeout(10000, validateUserEmailAPI(token))
      .then((data) => {
        return dispatch(validateEmailSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(dispatch(validateEmailError("Server can't be reached")))
          : error === "Access time out"
          ? dispatch(dispatch(validateEmailError("Response time out")))
          : error.data && error.data.message
          ? dispatch(dispatch(validateEmailError(error.data.message)))
          : dispatch(dispatch(validateEmailError(error.data.error)));
      });
  };
};
export const completePasswordReset = (inputData) => {
  return (dispatch) => {
    dispatch(completePasswordResetRequest());
    return timeout(10000, completePasswordResetAPI(inputData))
      .then((data) => {
        dispatch(completePasswordResetSuccess(data));
        return history.push("/");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                completePasswordResetError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                completePasswordResetError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                completePasswordResetError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(completePasswordResetError(error.data.error)));
      });
  };
};
