import {
  installationRequest,
  installationSucess,
  installationError,
  checkInstallationRequest,
  checkInstallationSucess,
  checkInstallationError,
  clearMessages,
} from "../actions/install.action";
import { timeout, dispatchAndRefresh } from "../utils/Utils";
import { installAPI, checkInstallationAPI } from "../services/install.service";
import { history } from "../App";

export function installHelper(inputData) {
  return (dispatch) => {
    dispatch(installationRequest());
    return timeout(10000, installAPI(inputData))
      .then((data) => {
        dispatch(installationSucess(data));
        return history.push("/");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                installationError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                installationError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                installationError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(installationError(error.data.error)));
      });
  };
}
export function checkInstallationHelper() {
  return (dispatch) => {
    dispatch(checkInstallationRequest());
    return timeout(10000, checkInstallationAPI())
      .then((data) => {
        return dispatch(checkInstallationSucess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                checkInstallationError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                checkInstallationError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 403
          ? history.push("/")
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                checkInstallationError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(checkInstallationError(error.data.error)));
      });
  };
}
