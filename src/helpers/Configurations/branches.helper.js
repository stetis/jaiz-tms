import {
  branchesRequest,
  branchesSuccess,
  branchesError,
  actionsRequest,
  actionsSuccess,
  actionsError,
  clearMessages,
} from "../../actions/Configurations/branch.actions";
import { timeout, dispatchAndRefresh } from "../../utils/Utils";
import {
  paginatedBranchesAPI,
  branchesAPI,
  bulkUploadBranchAPI,
  addBranchAPI,
  editBranchAPI,
  deleteBranchAPI,
} from "../../services/configurations.service";
import { refreshToken } from "../login.helper";

export function getBranchesHelper() {
  return (dispatch) => {
    dispatch(branchesRequest());
    return timeout(10000, branchesAPI())
      .then((data) => {
        dispatch(branchesSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                branchesError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                branchesError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                branchesError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(branchesError(error.data.error)));
      });
  };
}
export function getPaginatedBranchesHelper(credentials) {
  return (dispatch) => {
    dispatch(branchesRequest());
    return timeout(10000, paginatedBranchesAPI(credentials))
      .then((data) => {
        dispatch(branchesSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                branchesError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                branchesError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                branchesError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(branchesError(error.data.error)));
      });
  };
}
export function branchBulkUploadHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, bulkUploadBranchAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return (window.location = "/tms/branches");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function addBranchHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addBranchAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getBranchesHelper());
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function editBranchHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editBranchAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getBranchesHelper());
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function deleteBranchHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, deleteBranchAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getBranchesHelper());
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
