import {
  softwareRequest,
  softwareSuccess,
  softwareError,
  versionsRequest,
  versionsSuccess,
  versionsError,
  actionsRequest,
  actionsSuccess,
  actionsError,
  clearMessages,
} from "../../actions/Configurations/versions.actions";
import { timeout, dispatchAndRefresh } from "../../utils/Utils";
import {
  softwareAPI,
  addSoftwareAPI,
  editSoftwareAPI,
  addSectionAPI,
  editSectionAPI,
  deleteSectionAPI,
  getVersionsAPI,
  addNewVersionAPI,
} from "../../services/configurations.service";

export function getSoftwareVersionsHelper() {
  return (dispatch) => {
    dispatch(softwareRequest());
    return timeout(10000, softwareAPI())
      .then((data) => {
        dispatch(softwareSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                softwareError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                softwareError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                softwareError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(softwareError(error.data.error)));
      });
  };
}
export function addversionHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addSoftwareAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return (window.location = "/tms/software-versioning");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function addSectionHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addSectionAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function editSectionHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editSectionAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function deleteSectionHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, deleteSectionAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function editVersionHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editSoftwareAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function getVersionsHelper(id) {
  return (dispatch) => {
    dispatch(versionsRequest());
    return timeout(10000, getVersionsAPI(id))
      .then((data) => {
        dispatch(versionsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                versionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                versionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                versionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(versionsError(error.data.error)));
      });
  };
}
export function addNewVersionHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addNewVersionAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return (window.location = "/tms/software-versioning");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
