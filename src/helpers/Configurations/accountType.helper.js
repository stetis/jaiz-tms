import {
  accountTypesRequest,
  accountTypesSuccess,
  accountTypesError,
  actionsRequest,
  actionsSuccess,
  actionsError,
  clearMessages,
} from "../../actions/Configurations/accountType.actions";
import { timeout, dispatchAndRefresh } from "../../utils/Utils";
import {
  accountTypeAPI,
  addAccountTypeAPI,
  editAccountTypeAPI,
  deleteAccountTypeAPI,
} from "../../services/configurations.service";
import { refreshToken } from "../login.helper";

export function getAccountTypeHelper() {
  return (dispatch) => {
    dispatch(accountTypesRequest());
    return timeout(10000, accountTypeAPI())
      .then((data) => {
        dispatch(accountTypesSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                accountTypesError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                accountTypesError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                accountTypesError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(accountTypesError(error.data.error)));
      });
  };
}
export function addAccountTypeHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addAccountTypeAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getAccountTypeHelper());
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function editAccountTypeHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editAccountTypeAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getAccountTypeHelper());
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function deleteAccountTypeHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, deleteAccountTypeAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getAccountTypeHelper());
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
