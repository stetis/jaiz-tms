import {
  zonesRequest,
  zonesSuccess,
  zonesError,
  actionsRequest,
  actionsSuccess,
  actionsError,
  clearMessages,
} from "../../actions/Configurations/deploymentZones.actions";
import { timeout, dispatchAndRefresh } from "../../utils/Utils";
import {
  getStateZonesAPI,
  getPaginatedZonesAPI,
  zonesAPI,
  addZoneAPI,
  editZoneAPI,
  deleteZoneAPI,
} from "../../services/configurations.service";

export function getStateZonesHelper(credentials) {
  return (dispatch) => {
    dispatch(zonesRequest());
    return timeout(10000, getStateZonesAPI(credentials))
      .then((data) => {
        dispatch(zonesSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                zonesError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                zonesError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                zonesError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(
              zonesError(error.data.message),
              dispatch(error.data.error)
            );
      });
  };
}
export function getPaginatedZonesHelper(credentials) {
  return (dispatch) => {
    dispatch(zonesRequest());
    return timeout(10000, getPaginatedZonesAPI(credentials))
      .then((data) => {
        dispatch(zonesSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                zonesError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                zonesError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                zonesError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(
              zonesError(error.data.message),
              dispatch(error.data.error)
            );
      });
  };
}
export function getZoneHelper() {
  return (dispatch) => {
    dispatch(zonesRequest());
    return timeout(10000, zonesAPI())
      .then((data) => {
        dispatch(zonesSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                zonesError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                zonesError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                zonesError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(
              zonesError(error.data.message),
              dispatch(error.data.error)
            );
      });
  };
}
export function addregionHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addZoneAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function editZoneHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editZoneAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function deleteZoneHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, deleteZoneAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
