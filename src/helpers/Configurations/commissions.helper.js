import {
  commissionsRequest,
  commissionsSuccess,
  commissionsError,
  transactionTypeRequest,
  transactionTypeSuccess,
  transactionTypeError,
  singleFeeGroupRequest,
  singleFeeGroupSuccess,
  singleFeeGroupError,
  feeGroupRequest,
  feeGroupSuccess,
  feeGroupError,
  feeRequest,
  feeSuccess,
  feeError,
  singleFeeRequest,
  singleFeeSuccess,
  singleFeeError,
  bandRequest,
  bandSuccess,
  bandError,
  actionsRequest,
  actionsSuccess,
  actionsError,
  clearMessages,
} from "../../actions/Configurations/commissions.actions";
import { timeout, dispatchAndRefresh } from "../../utils/Utils";
import { history } from "../../App";

import {
  commissionsAPI,
  addCommissionAPI,
  editCommissionAPI,
  deleteCommissionAPI,
  FeeTypeAPI,
  FeeGroupsAPI,
  singleFeeGroupAPI,
  addFeeGroupAPI,
  editFeeGroupAPI,
  deleteFeeGroupAPI,
  feeAPI,
  singleFeeAPI,
  addFeeAPI,
  editFeeAPI,
  deleteFeeAPI,
  bandAPI,
  addBandAPI,
  editBandAPI,
  deleteBandAPI,
} from "../../services/configurations.service";
import { refreshToken } from "../login.helper";

export function getCommissionHelper() {
  return (dispatch) => {
    dispatch(commissionsRequest());
    return timeout(10000, commissionsAPI())
      .then((data) => {
        dispatch(commissionsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                commissionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                commissionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                commissionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(commissionsError(error.data.error)));
      });
  };
}
export function addCommissionHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addCommissionAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function editCommissionHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editCommissionAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function deleteCommissionHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, deleteCommissionAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
/* TYPE TRANSACTION */
export function feeTypeHelper() {
  return (dispatch) => {
    dispatch(transactionTypeRequest());
    return timeout(10000, FeeTypeAPI())
      .then((data) => {
        dispatch(transactionTypeSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                transactionTypeError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                transactionTypeError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                transactionTypeError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(transactionTypeError(error.data.error)));
      });
  };
}

/* FEE GROUPS */
export function singleFeeGroupHelper(id) {
  return (dispatch) => {
    dispatch(singleFeeGroupRequest());
    return timeout(10000, singleFeeGroupAPI(id))
      .then((data) => {
        dispatch(singleFeeGroupSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                singleFeeGroupError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                singleFeeGroupError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                singleFeeGroupError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(singleFeeGroupError(error.data.error)));
      });
  };
}
export function feeGroupHelper() {
  return (dispatch) => {
    dispatch(feeGroupRequest());
    return timeout(10000, FeeGroupsAPI())
      .then((data) => {
        dispatch(feeGroupSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                feeGroupError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                feeGroupError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                feeGroupError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(feeGroupError(error.data.error)));
      });
  };
}
export function addFeeGroupHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addFeeGroupAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(feeGroupHelper());
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function editFeeGroupHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editFeeGroupAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function deleteFeeGroupHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, deleteFeeGroupAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return history.push("/tms/commissions");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

/* FEE  */
export function singleFeeHelper(id) {
  return (dispatch) => {
    dispatch(singleFeeRequest());
    return timeout(10000, singleFeeAPI(id))
      .then((data) => {
        dispatch(singleFeeSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                singleFeeError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                singleFeeError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                singleFeeError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(singleFeeError(error.data.error)));
      });
  };
}
export function feeHelper() {
  return (dispatch) => {
    dispatch(feeRequest());
    return timeout(10000, feeAPI())
      .then((data) => {
        dispatch(feeSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                feeError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(feeError("Response time out"), clearMessages())
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(feeError(error.data.message), clearMessages())
            )
          : dispatch(dispatch(feeError(error.data.error)));
      });
  };
}
export function addFeeHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addFeeAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function editFeeHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editFeeAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(bandHelper(inputData.id));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function deleteFeeHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, deleteFeeAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

/* BAND  */
export function bandHelper(id) {
  return (dispatch) => {
    dispatch(bandRequest());
    return timeout(10000, bandAPI(id))
      .then((data) => {
        dispatch(bandSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                bandError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                bandError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(bandError(error.data.message), clearMessages())
            )
          : dispatch(dispatch(bandError(error.data.error)));
      });
  };
}
export function addBandHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addBandAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(bandHelper(inputData["fee-id"]));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function editBandHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editBandAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(bandHelper(inputData["fee-id"]));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function deleteBandHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, deleteBandAPI(inputData.id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(bandHelper(inputData["fee-id"]));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
