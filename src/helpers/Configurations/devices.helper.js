import {
  devicesRequest,
  devicesSuccess,
  devicesError,
  actionsRequest,
  actionsSuccess,
  actionsError,
  clearMessages,
} from "../../actions/Configurations/devices.actions";
import { timeout, dispatchAndRefresh } from "../../utils/Utils";
import {
  DeviceAPI,
  addDeviceAPI,
  editDeviceAPI,
  deleteDeviceAPI,
} from "../../services/configurations.service";
import { refreshToken } from "../login.helper";

export function getDeviceHelper() {
  return (dispatch) => {
    dispatch(devicesRequest());
    return timeout(10000, DeviceAPI())
      .then((data) => {
        dispatch(devicesSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                devicesError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                devicesError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                devicesError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(devicesError(error.data.error)));
      });
  };
}
export function addDeviceHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addDeviceAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getDeviceHelper());
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function editDeviceHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editDeviceAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getDeviceHelper());
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function deleteDeviceHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, deleteDeviceAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return dispatch(getDeviceHelper());
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
