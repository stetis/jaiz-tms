import {
  merchantsRequest,
  merchantsSuccess,
  merchantsError,
  allMerchantsRequest,
  allMerchantsSuccess,
  allMerchantsError,
  singleMerchantRequest,
  singleMerchantSuccess,
  singleMerchantError,
  businessesRequest,
  businessesSuccess,
  businessesError,
  allBusinessesRequest,
  allBusinessesSuccess,
  allBusinessesError,
  actionsRequest,
  actionsSuccess,
  actionsError,
  clearMessages,
} from "../actions/merchants.actions";
import { timeout, dispatchAndRefresh } from "../utils/Utils";
import {
  singleMerchantsAPI,
  allMerchantsAPI,
  merchantsAPI,
  addMerchantAPI,
  enableMerchantProfileAPI,
  revokeMerchantProfileAPI,
  initiateMerchantResetPinAPI,
  editMerchantAPI,
  deleteMerchantAPI,
  allBusinessesAPI,
  businessesAPI,
  addBusinessAPI,
  editBusinessAPI,
  deleteBusinessAPI,
} from "../services/merchant.service";
import { refreshToken } from "./login.helper";
import { history } from "../App";

export function getSingleMerchantsHelper(id) {
  return (dispatch) => {
    dispatch(singleMerchantRequest());
    return timeout(10000, singleMerchantsAPI(id))
      .then((data) => {
        dispatch(singleMerchantSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                singleMerchantError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                singleMerchantError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                singleMerchantError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(singleMerchantError(error.data.error)));
      });
  };
}
export function getAllMerchantsHelper() {
  return (dispatch) => {
    dispatch(allMerchantsRequest());
    return timeout(10000, allMerchantsAPI())
      .then((data) => {
        dispatch(allMerchantsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                allMerchantsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                allMerchantsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                allMerchantsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(allMerchantsError(error.data.error)));
      });
  };
}
export function getMerchantsHelper(credentials) {
  return (dispatch) => {
    dispatch(merchantsRequest());
    return timeout(10000, merchantsAPI(credentials))
      .then((data) => {
        return dispatch(merchantsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                merchantsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                merchantsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                merchantsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(merchantsError(error.data.error)));
      });
  };
}
export function addMerchantHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addMerchantAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return history.push("/tms/merchants");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function editMerchantHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editMerchantAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return history.push("/tms/merchants");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function enableMerchantProfileHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, enableMerchantProfileAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function revokeMerchantProfileHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, revokeMerchantProfileAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function initiateMerchantResetPinHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, initiateMerchantResetPinAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return window.location.reload();
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function deleteMerchantHelper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, deleteMerchantAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
        return history.push("/tms/merchants");
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
/* BUSINESSES */
export function getAllBusinessesHelper() {
  return (dispatch) => {
    dispatch(allBusinessesRequest());
    return timeout(10000, allBusinessesAPI())
      .then((data) => {
        dispatch(allBusinessesSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                allBusinessesError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                allBusinessesError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                allBusinessesError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(allBusinessesError(error.data.error)));
      });
  };
}
export function getBusinessesHelper(credentials) {
  return (dispatch) => {
    dispatch(businessesRequest());
    return timeout(10000, businessesAPI(credentials))
      .then((data) => {
        dispatch(businessesSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                businessesError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                businessesError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                businessesError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(businessesError(error.data.error)));
      });
  };
}
export function addBusinessHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, addBusinessAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}

export function editBusinessHelper(inputData) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, editBusinessAPI(inputData))
      .then((data) => {
        dispatch(actionsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
export function deleteBusinesselper(id) {
  return (dispatch) => {
    dispatch(actionsRequest());
    return timeout(10000, deleteBusinessAPI(id))
      .then((data) => {
        dispatch(actionsSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                actionsError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                actionsError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(actionsError(error.data.error)));
      });
  };
}
