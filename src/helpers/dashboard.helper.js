import {
  dashboardRequest,
  dashboardSuccess,
  dashboardError,
  clearMessages,
} from "../actions/dashboard.actions";
import { timeout, dispatchAndRefresh } from "../utils/Utils";
import { dashboardAPI } from "../services/dashboard.service";
import { refreshToken } from "./login.helper";

export function dashboardHelper(credentials) {
  return (dispatch) => {
    dispatch(dashboardRequest());
    return timeout(10000, dashboardAPI(credentials))
      .then((data) => {
        return dispatch(dashboardSuccess(data));
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                dashboardError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                dashboardError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.statuscode === 401
          ? dispatch(refreshToken())
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                dashboardError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(dashboardError(error.data.error)));
      });
  };
}
