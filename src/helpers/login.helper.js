import Cookies from "js-cookie";
import {
  clearMessages,
  loginError,
  loginRequest,
  loginSuccess,
  logoutError,
  logoutRequest,
  logoutSuccess,
  refreshRequest,
  refreshSuccess,
} from "../actions/login.action";
import { history } from "../App";
import {
  loginAPI,
  logoutAPI,
  refreshTokenAPI,
} from "../services/login.service";
import { dispatchAndRefresh, timeout } from "../utils/Utils";

/**
 * - Remove tokens from cookie
 */

export const clearCookies = () => {
  try {
    Cookies.remove("access-token");
    Cookies.remove("refresh-token");
  } catch (error) {}
};
export const userLogin = (credentials) => {
  return (dispatch, getState) => {
    dispatch(loginRequest());
    return timeout(10000, loginAPI(credentials))
      .then((res) => {
        dispatch(loginSuccess(res));
        Cookies.set("access-token", res.data.tokens["access-token"]);
        Cookies.set("refresh-token", res.data.tokens["refresh-token"]);
        const { profile } = getState().loginReducer.data;
        if (profile.status === "reset_pin") {
          return history.push("/message");
        } else {
          return history.push("/tms");
        }
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                loginError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                loginError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                loginError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(loginError(error.data.error)));
      });
  };
};

export const userLogout = () => {
  return (dispatch, getState) => {
    dispatch(logoutRequest());
    return timeout(10000, logoutAPI())
      .then((res) => {
        dispatch(logoutSuccess(res));
        clearCookies();
        const { loggedout } = getState().loginReducer;
        if (loggedout) {
          return history.push("/");
        }
      })
      .catch((error) => {
        error.message === "Failed to fetch"
          ? dispatch(
              dispatchAndRefresh(
                logoutError("Server can't be reached"),
                clearMessages()
              )
            )
          : error === "Access time out"
          ? dispatch(
              dispatchAndRefresh(
                logoutError("Response time out"),
                clearMessages()
              )
            )
          : error.data && error.data.message
          ? dispatch(
              dispatchAndRefresh(
                logoutError(error.data.message),
                clearMessages()
              )
            )
          : dispatch(dispatch(logoutError(error.data.error)));
      });
  };
};

export const refreshToken = () => {
  return (dispatch, getState) => {
    dispatch(refreshRequest());
    return timeout(10000, refreshTokenAPI())
      .then((res) => {
        dispatch(refreshSuccess());
        Cookies.set("access-token", res.data["access-token"]);
        Cookies.set("refresh-token", res.data["refresh-token"]);
        const { refreshed } = getState().loginReducer;
        if (refreshed) {
          return window.location.reload();
        }
      })
      .catch(() => history.push("/"));
  };
};
