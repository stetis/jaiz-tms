import { ActionConstants } from "../../constants/ActionConstants";

export const usersRequest = () => {
  return {
    type: ActionConstants.GET_USERS_REQUEST,
  };
};

export const usersSuccess = (payload) => {
  return {
    type: ActionConstants.GET_USERS_SUCCESS,
    payload: payload,
  };
};

export const usersError = (payload) => {
  return {
    type: ActionConstants.GET_USERS_ERROR,
    payload: payload,
  };
};

export const actionsRequest = () => {
  return {
    type: ActionConstants.ACTIONS_REQUEST,
  };
};

export const actionsSuccess = (payload) => {
  return {
    type: ActionConstants.ACTIONS_SUCCESS,
    payload: payload,
  };
};

export const actionsError = (payload) => {
  return {
    type: ActionConstants.ACTIONS_ERROR,
    payload: payload,
  };
};

export const clearMessages = () => {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
};
