import { ActionConstants } from "../../constants/ActionConstants";

export const auditlogsRequest = () => {
  return {
    type: ActionConstants.AUDIT_LOG_REQUEST,
  };
};

export const auditlogsSuccess = (payload) => {
  return {
    type: ActionConstants.AUDIT_LOG_SUCCESS,
    payload: payload,
  };
};

export const auditlogsError = (payload) => {
  return {
    type: ActionConstants.AUDIT_LOG_ERROR,
    payload: payload,
  };
};

export const clearMessages = () => {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
};
