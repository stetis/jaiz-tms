import { ActionConstants } from "../constants/ActionConstants";

export const singleTerminalRequest = () => {
  return {
    type: ActionConstants.SINGLE_TERMINAL_REQUEST,
  };
};

export const singleTerminalSuccess = (payload) => {
  return {
    type: ActionConstants.SINGLE_TERMINAL_SUCCESS,
    payload: payload,
  };
};

export const singleTerminalError = (payload) => {
  return {
    type: ActionConstants.SINGLE_TERMINAL_ERROR,
    payload: payload,
  };
};

export const terminalsRequest = () => {
  return {
    type: ActionConstants.TERMINALS_REQUEST,
  };
};

export const terminalsSuccess = (payload) => {
  return {
    type: ActionConstants.TERMINALS_SUCCESS,
    payload: payload,
  };
};

export const terminalsError = (payload) => {
  return {
    type: ActionConstants.TERMINALS_ERROR,
    payload: payload,
  };
};

export const agentTerminalRequest = () => {
  return {
    type: ActionConstants.AGENT_TERMINAL_REQUEST,
  };
};

export const agentTerminalSuccess = (payload) => {
  return {
    type: ActionConstants.AGENT_TERMINAL_SUCCESS,
    payload: payload,
  };
};

export const agentTerminalError = (payload) => {
  return {
    type: ActionConstants.AGENT_TERMINAL_ERROR,
    payload: payload,
  };
};
export const actionsRequest = () => {
  return {
    type: ActionConstants.ACTIONS_REQUEST,
  };
};

export const actionsSuccess = (payload) => {
  return {
    type: ActionConstants.ACTIONS_SUCCESS,
    payload: payload,
  };
};

export const actionsError = (payload) => {
  return {
    type: ActionConstants.ACTIONS_ERROR,
    payload: payload,
  };
};

export const clearMessages = () => {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
};
