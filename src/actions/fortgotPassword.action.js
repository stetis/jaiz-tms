import { ActionConstants } from "../constants/ActionConstants";

export function initiatePasswordResetRequest() {
  return {
    type: ActionConstants.INITIATE_PASSWORD_RESET_REQUEST,
  };
}

export function initiatePasswordResetSuccess(payload) {
  return {
    type: ActionConstants.INITIATE_PASSWORD_RESET_SUCCESS,
    payload,
  };
}

export function initiatePasswordResetError(payload) {
  return {
    type: ActionConstants.INITIATE_PASSWORD_RESET_ERROR,
    payload: payload,
  };
}

export function validateEmailRequest() {
  return {
    type: ActionConstants.VALIDATE_PASSWORD_RESET_REQUEST,
  };
}

export function validateEmailSuccess(payload) {
  return {
    type: ActionConstants.VALIDATE_PASSWORD_RESET_SUCCESS,
    payload: payload,
  };
}

export function validateEmailError(payload) {
  return {
    type: ActionConstants.VALIDATE_PASSWORD_RESET_ERROR,
    payload: payload,
  };
}
export function completePasswordResetRequest() {
  return {
    type: ActionConstants.COMPLETE_PASSWORD_RESET_REQUEST,
  };
}

export function completePasswordResetSuccess(payload) {
  return {
    type: ActionConstants.COMPLETE_PASSWORD_RESET_SUCCESS,
    payload: payload,
  };
}

export function completePasswordResetError(payload) {
  return {
    type: ActionConstants.COMPLETE_PASSWORD_RESET_ERROR,
    payload: payload,
  };
}

export function clearMessages() {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
}
