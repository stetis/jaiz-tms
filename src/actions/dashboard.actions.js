import { ActionConstants } from "../constants/ActionConstants";

export const dashboardRequest = () => {
  return {
    type: ActionConstants.DASHBOARD_REQUEST,
  };
};

export const dashboardSuccess = (payload) => {
  return {
    type: ActionConstants.DASHBOARD_SUCCESS,
    payload: payload,
  };
};

export const dashboardError = (payload) => {
  return {
    type: ActionConstants.DASHBOARD_ERROR,
    payload: payload,
  };
};
export const clearMessages = () => {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
};
