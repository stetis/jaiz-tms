import { ActionConstants } from "../../constants/ActionConstants";

export const titlesRequest = () => {
  return {
    type: ActionConstants.GET_TITLE_REQUEST,
  };
};

export const titlesSuccess = (payload) => {
  return {
    type: ActionConstants.GET_TITLE_SUCCESS,
    payload: payload,
  };
};

export const titlesError = (payload) => {
  return {
    type: ActionConstants.GET_TITLE_ERROR,
    payload: payload,
  };
};

export const actionsRequest = () => {
  return {
    type: ActionConstants.ACTIONS_REQUEST,
  };
};

export const actionsSuccess = (payload) => {
  return {
    type: ActionConstants.ACTIONS_SUCCESS,
    payload: payload,
  };
};

export const actionsError = (payload) => {
  return {
    type: ActionConstants.ACTIONS_ERROR,
    payload: payload,
  };
};

export const clearMessages = () => {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
};
