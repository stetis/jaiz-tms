import { ActionConstants } from "../../constants/ActionConstants";

export const regionsRequest = () => {
  return {
    type: ActionConstants.REGION_REQUEST,
  };
};

export const regionsSuccess = (payload) => {
  return {
    type: ActionConstants.REGION_SUCCESS,
    payload: payload,
  };
};

export const regionsError = (payload) => {
  return {
    type: ActionConstants.REGION_ERROR,
    payload: payload,
  };
};

export const actionsRequest = () => {
  return {
    type: ActionConstants.ACTIONS_REQUEST,
  };
};

export const actionsSuccess = (payload) => {
  return {
    type: ActionConstants.ACTIONS_SUCCESS,
    payload: payload,
  };
};

export const actionsError = (payload) => {
  return {
    type: ActionConstants.ACTIONS_ERROR,
    payload: payload,
  };
};

export const clearMessages = () => {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
};
