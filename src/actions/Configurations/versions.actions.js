import { ActionConstants } from "../../constants/ActionConstants";

export const softwareRequest = () => {
  return {
    type: ActionConstants.SOFTWARE_VERSION_REQUEST,
  };
};

export const softwareSuccess = (payload) => {
  return {
    type: ActionConstants.SOFTWARE_VERSION_SUCCESS,
    payload: payload,
  };
};

export const softwareError = (payload) => {
  return {
    type: ActionConstants.SOFTWARE_VERSION_ERROR,
    payload: payload,
  };
};

export const versionsRequest = () => {
  return {
    type: ActionConstants.GET_VERSIONS_REQUEST,
  };
};

export const versionsSuccess = (payload) => {
  return {
    type: ActionConstants.GET_VERSIONS_SUCCESS,
    payload: payload,
  };
};

export const versionsError = (payload) => {
  return {
    type: ActionConstants.GET_VERSIONS_ERROR,
    payload: payload,
  };
};

export const actionsRequest = () => {
  return {
    type: ActionConstants.ACTIONS_REQUEST,
  };
};

export const actionsSuccess = (payload) => {
  return {
    type: ActionConstants.ACTIONS_SUCCESS,
    payload: payload,
  };
};

export const actionsError = (payload) => {
  return {
    type: ActionConstants.ACTIONS_ERROR,
    payload: payload,
  };
};

export const clearMessages = () => {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
};
