import { ActionConstants } from "../../constants/ActionConstants";

export const zonesRequest = () => {
  return {
    type: ActionConstants.ZONES_REQUEST,
  };
};

export const zonesSuccess = (payload) => {
  return {
    type: ActionConstants.ZONES_SUCCESS,
    payload: payload,
  };
};

export const zonesError = (payload) => {
  return {
    type: ActionConstants.ZONES_ERROR,
    payload: payload,
  };
};
export const actionsRequest = () => {
  return {
    type: ActionConstants.ACTIONS_REQUEST,
  };
};

export const actionsSuccess = (payload) => {
  return {
    type: ActionConstants.ACTIONS_SUCCESS,
    payload: payload,
  };
};

export const actionsError = (payload) => {
  return {
    type: ActionConstants.ACTIONS_ERROR,
    payload: payload,
  };
};
export const clearMessages = () => {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
};
