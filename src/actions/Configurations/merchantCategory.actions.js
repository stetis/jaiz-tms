import { ActionConstants } from "../../constants/ActionConstants";

export const categoryRequest = () => {
  return {
    type: ActionConstants.MERCHANT_CATEGORY_REQUEST,
  };
};

export const categorySuccess = (payload) => {
  return {
    type: ActionConstants.MERCHANT_CATEGORY_SUCCESS,
    payload: payload,
  };
};

export const categoryError = (payload) => {
  return {
    type: ActionConstants.MERCHANT_CATEGORY_ERROR,
    payload: payload,
  };
};

export const actionsRequest = () => {
  return {
    type: ActionConstants.ACTIONS_REQUEST,
  };
};

export const actionsSuccess = (payload) => {
  return {
    type: ActionConstants.ACTIONS_SUCCESS,
    payload: payload,
  };
};

export const actionsError = (payload) => {
  return {
    type: ActionConstants.ACTIONS_ERROR,
    payload: payload,
  };
};

export const clearMessages = () => {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
};
