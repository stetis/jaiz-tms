import { ActionConstants } from "../../constants/ActionConstants";

export const branchesRequest = () => {
  return {
    type: ActionConstants.BRANCHES_REQUEST,
  };
};

export const branchesSuccess = (payload) => {
  return {
    type: ActionConstants.BRANCHES_SUCCESS,
    payload: payload,
  };
};

export const branchesError = (payload) => {
  return {
    type: ActionConstants.BRANCHES_ERROR,
    payload: payload,
  };
};

export const actionsRequest = () => {
  return {
    type: ActionConstants.ACTIONS_REQUEST,
  };
};

export const actionsSuccess = (payload) => {
  return {
    type: ActionConstants.ACTIONS_SUCCESS,
    payload: payload,
  };
};

export const actionsError = (payload) => {
  return {
    type: ActionConstants.ACTIONS_ERROR,
    payload: payload,
  };
};
export const clearMessages = () => {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
};
