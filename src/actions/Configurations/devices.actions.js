import { ActionConstants } from "../../constants/ActionConstants";

export const devicesRequest = () => {
  return {
    type: ActionConstants.DEVICES_REQUEST,
  };
};

export const devicesSuccess = (payload) => {
  return {
    type: ActionConstants.DEVICES_SUCCESS,
    payload: payload,
  };
};

export const devicesError = (payload) => {
  return {
    type: ActionConstants.DEVICES_ERROR,
    payload: payload,
  };
};

export const actionsRequest = () => {
  return {
    type: ActionConstants.ACTIONS_REQUEST,
  };
};

export const actionsSuccess = (payload) => {
  return {
    type: ActionConstants.ACTIONS_SUCCESS,
    payload: payload,
  };
};

export const actionsError = (payload) => {
  return {
    type: ActionConstants.ACTIONS_ERROR,
    payload: payload,
  };
};

export const clearMessages = () => {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
};
