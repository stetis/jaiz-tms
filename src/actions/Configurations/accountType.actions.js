import { ActionConstants } from "../../constants/ActionConstants";

export const accountTypesRequest = () => {
  return {
    type: ActionConstants.ACCOUNT_TYPE_REQUEST,
  };
};

export const accountTypesSuccess = (payload) => {
  return {
    type: ActionConstants.ACCOUNT_TYPE_SUCCESS,
    payload: payload,
  };
};

export const accountTypesError = (payload) => {
  return {
    type: ActionConstants.ACCOUNT_TYPE_ERROR,
    payload: payload,
  };
};

export const actionsRequest = () => {
  return {
    type: ActionConstants.ACTIONS_REQUEST,
  };
};

export const actionsSuccess = (payload) => {
  return {
    type: ActionConstants.ACTIONS_SUCCESS,
    payload: payload,
  };
};

export const actionsError = (payload) => {
  return {
    type: ActionConstants.ACTIONS_ERROR,
    payload: payload,
  };
};

export const clearMessages = () => {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
};
