import { ActionConstants } from "../../constants/ActionConstants";

export const commissionsRequest = () => {
  return {
    type: ActionConstants.COMMISSIONS_REQUEST,
  };
};

export const commissionsSuccess = (payload) => {
  return {
    type: ActionConstants.COMMISSIONS_SUCCESS,
    payload: payload,
  };
};

export const commissionsError = (payload) => {
  return {
    type: ActionConstants.COMMISSIONS_ERROR,
    payload: payload,
  };
};

export const transactionTypeRequest = () => {
  return {
    type: ActionConstants.TRANSACTION_TYPE_REQUEST,
  };
};

export const transactionTypeSuccess = (payload) => {
  return {
    type: ActionConstants.TRANSACTION_TYPE_SUCCESS,
    payload: payload,
  };
};

export const transactionTypeError = (payload) => {
  return {
    type: ActionConstants.TRANSACTION_TYPE_ERROR,
    payload: payload,
  };
};

/* FEE GROUP */
export const singleFeeGroupRequest = () => {
  return {
    type: ActionConstants.SINGLE_FEE_GROUP_REQUEST,
  };
};

export const singleFeeGroupSuccess = (payload) => {
  return {
    type: ActionConstants.SINGLE_FEE_GROUP_SUCCESS,
    payload: payload,
  };
};

export const singleFeeGroupError = (payload) => {
  return {
    type: ActionConstants.SINGLE_FEE_GROUP_ERROR,
    payload: payload,
  };
};
export const feeGroupRequest = () => {
  return {
    type: ActionConstants.FEE_GROUP_REQUEST,
  };
};

export const feeGroupSuccess = (payload) => {
  return {
    type: ActionConstants.FEE_GROUP_SUCCESS,
    payload: payload,
  };
};

export const feeGroupError = (payload) => {
  return {
    type: ActionConstants.FEE_GROUP_ERROR,
    payload: payload,
  };
};

/* FEE  */

export const feeRequest = () => {
  return {
    type: ActionConstants.FEE_REQUEST,
  };
};

export const feeSuccess = (payload) => {
  return {
    type: ActionConstants.FEE_SUCCESS,
    payload: payload,
  };
};

export const feeError = (payload) => {
  return {
    type: ActionConstants.FEE_ERROR,
    payload: payload,
  };
};

/* SINGLE FEE */

export const singleFeeRequest = () => {
  return {
    type: ActionConstants.SINGLE_FEE_REQUEST,
  };
};

export const singleFeeSuccess = (payload) => {
  return {
    type: ActionConstants.SINGLE_FEE_SUCCESS,
    payload: payload,
  };
};

export const singleFeeError = (payload) => {
  return {
    type: ActionConstants.SINGLE_FEE_ERROR,
    payload: payload,
  };
};

/* SINGLE FEE */

export const bandRequest = () => {
  return {
    type: ActionConstants.BAND_REQUEST,
  };
};

export const bandSuccess = (payload) => {
  return {
    type: ActionConstants.BAND_SUCCESS,
    payload: payload,
  };
};

export const bandError = (payload) => {
  return {
    type: ActionConstants.BAND_ERROR,
    payload: payload,
  };
};
/* TABLE ACTIONS */
export const actionsRequest = () => {
  return {
    type: ActionConstants.ACTIONS_REQUEST,
  };
};

export const actionsSuccess = (payload) => {
  return {
    type: ActionConstants.ACTIONS_SUCCESS,
    payload: payload,
  };
};

export const actionsError = (payload) => {
  return {
    type: ActionConstants.ACTIONS_ERROR,
    payload: payload,
  };
};

export const clearMessages = () => {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
};
