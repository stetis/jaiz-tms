import { ActionConstants } from "../constants/ActionConstants";

export const ptspSingleRequest = () => {
  return {
    type: ActionConstants.SINGLE_PTSP_REQUEST,
  };
};

export const ptspSingleSuccess = (payload) => {
  return {
    type: ActionConstants.SINGLE_PTSP_SUCCESS,
    payload: payload,
  };
};

export const ptspSingleError = (payload) => {
  return {
    type: ActionConstants.SINGLE_PTSP_ERROR,
    payload: payload,
  };
};
export const ptspRequest = () => {
  return {
    type: ActionConstants.PTSP_REQUEST,
  };
};

export const ptspSuccess = (payload) => {
  return {
    type: ActionConstants.PTSP_SUCCESS,
    payload: payload,
  };
};

export const ptspError = (payload) => {
  return {
    type: ActionConstants.PTSP_ERROR,
    payload: payload,
  };
};
export const ptspContactRequest = () => {
  return {
    type: ActionConstants.PTSP_CONTACT_REQUEST,
  };
};

export const ptspContactSuccess = (payload) => {
  return {
    type: ActionConstants.PTSP_CONTACT_SUCCESS,
    payload: payload,
  };
};

export const ptspContactError = (payload) => {
  return {
    type: ActionConstants.PTSP_CONTACT_ERROR,
    payload: payload,
  };
};
export const actionsRequest = () => {
  return {
    type: ActionConstants.ACTIONS_REQUEST,
  };
};

export const actionsSuccess = (payload) => {
  return {
    type: ActionConstants.ACTIONS_SUCCESS,
    payload: payload,
  };
};

export const actionsError = (payload) => {
  return {
    type: ActionConstants.ACTIONS_ERROR,
    payload: payload,
  };
};

export const clearMessages = () => {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
};
