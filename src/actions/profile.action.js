import { ActionConstants } from "../constants/ActionConstants";

export const passportRequest = () => {
  return {
    type: ActionConstants.PASSPORT_REQUEST,
  };
};

export const passportSuccess = (payload) => {
  return {
    type: ActionConstants.PASSPORT_SUCCESS,
    payload: payload,
  };
};

export const passportError = (payload) => {
  return {
    type: ActionConstants.PASSPORT_ERROR,
    payload: payload,
  };
};
export const getPassportRequest = () => {
  return {
    type: ActionConstants.GET_PASSPORT_REQUEST,
  };
};

export const getPassportSuccess = (payload) => {
  return {
    type: ActionConstants.GET_PASSPORT_SUCCESS,
    payload: payload,
  };
};

export const getPassportError = (payload) => {
  return {
    type: ActionConstants.GET_PASSPORT_ERROR,
    payload: payload,
  };
};

export const rolesRequest = () => {
  return {
    type: ActionConstants.GET_ROLES_REQUEST,
  };
};

export const rolesSuccess = (payload) => {
  return {
    type: ActionConstants.GET_ROLES_SUCCESS,
    payload: payload,
  };
};

export const rolesError = (payload) => {
  return {
    type: ActionConstants.GET_ROLES_ERROR,
    payload: payload,
  };
};

export const roleRequest = () => {
  return {
    type: ActionConstants.GET_ROLE_REQUEST,
  };
};

export const roleSuccess = (payload) => {
  return {
    type: ActionConstants.GET_ROLE_SUCCESS,
    payload: payload,
  };
};

export const roleError = (payload) => {
  return {
    type: ActionConstants.GET_ROLE_ERROR,
    payload: payload,
  };
};

export const getModuleRequest = () => {
  return {
    type: ActionConstants.GET_MODULE_REQUEST,
  };
};

export const getModuleSuccess = (payload) => {
  return {
    type: ActionConstants.GET_MODULE_SUCCESS,
    payload: payload,
  };
};

export const getModuleError = (payload) => {
  return {
    type: ActionConstants.GET_MODULE_ERROR,
    payload: payload,
  };
};
export const actionsRequest = () => {
  return {
    type: ActionConstants.ACTIONS_REQUEST,
  };
};

export const actionsSuccess = (payload) => {
  return {
    type: ActionConstants.ACTIONS_SUCCESS,
    payload: payload,
  };
};

export const actionsError = (payload) => {
  return {
    type: ActionConstants.ACTIONS_ERROR,
    payload: payload,
  };
};

export const clearMessages = () => {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
};
