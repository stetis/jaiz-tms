import { ActionConstants } from "../constants/ActionConstants";

export const merchantsRequest = () => {
  return {
    type: ActionConstants.MERCHANTS_REQUEST,
  };
};

export const merchantsSuccess = (payload) => {
  return {
    type: ActionConstants.MERCHANTS_SUCCESS,
    payload: payload,
  };
};

export const merchantsError = (payload) => {
  return {
    type: ActionConstants.MERCHANTS_ERROR,
    payload: payload,
  };
};
export const allMerchantsRequest = () => {
  return {
    type: ActionConstants.ALL_MERCHANTS_REQUEST,
  };
};

export const allMerchantsSuccess = (payload) => {
  return {
    type: ActionConstants.ALL_MERCHANTS_SUCCESS,
    payload: payload,
  };
};

export const allMerchantsError = (payload) => {
  return {
    type: ActionConstants.ALL_MERCHANTS_ERROR,
    payload: payload,
  };
};

export const singleMerchantRequest = () => {
  return {
    type: ActionConstants.SINGLE_MERCHANT_REQUEST,
  };
};

export const singleMerchantSuccess = (payload) => {
  return {
    type: ActionConstants.SINGLE_MERCHANT_SUCCESS,
    payload: payload,
  };
};

export const singleMerchantError = (payload) => {
  return {
    type: ActionConstants.SINGLE_MERCHANT_ERROR,
    payload: payload,
  };
};
export const businessesRequest = () => {
  return {
    type: ActionConstants.GET_BUSINESS_REQUEST,
  };
};

export const businessesSuccess = (payload) => {
  return {
    type: ActionConstants.GET_BUSINESS_SUCCESS,
    payload: payload,
  };
};

export const businessesError = (payload) => {
  return {
    type: ActionConstants.GET_BUSINESS_ERROR,
    payload: payload,
  };
};
export const allBusinessesRequest = () => {
  return {
    type: ActionConstants.ALL_BUSINESSES_REQUEST,
  };
};

export const allBusinessesSuccess = (payload) => {
  return {
    type: ActionConstants.ALL_BUSINESSES_SUCCESS,
    payload: payload,
  };
};

export const allBusinessesError = (payload) => {
  return {
    type: ActionConstants.ALL_BUSINESSES_ERROR,
    payload: payload,
  };
};
export const actionsRequest = () => {
  return {
    type: ActionConstants.ACTIONS_REQUEST,
  };
};

export const actionsSuccess = (payload) => {
  return {
    type: ActionConstants.ACTIONS_SUCCESS,
    payload: payload,
  };
};

export const actionsError = (payload) => {
  return {
    type: ActionConstants.ACTIONS_ERROR,
    payload: payload,
  };
};

export const clearMessages = () => {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
};
