import { ActionConstants } from "../constants/ActionConstants";

const { UPDATE_BREADCRUMB } = ActionConstants;

export const updateBreadcrumb = (breadcrumbData) => ({
  type: UPDATE_BREADCRUMB,
  payload: breadcrumbData,
});
