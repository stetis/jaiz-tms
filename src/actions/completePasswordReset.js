import { ActionConstants } from "../constants/ActionConstants";

const {
  INITIATE_PASSWORD_RESET_REQUEST,
  INITIATE_PASSWORD_RESET_SUCCESS,
  INITIATE_PASSWORD_RESET_ERROR,
  VALIDATE_PASSWORD_RESET_REQUEST,
  VALIDATE_PASSWORD_RESET_SUCCESS,
  VALIDATE_PASSWORD_RESET_ERROR,
  COMPLETE_PASSWORD_RESET_REQUEST,
  COMPLETE_PASSWORD_RESET_SUCCESS,
  COMPLETE_PASSWORD_RESET_ERROR,
  REFRESH_REQUEST,
} = ActionConstants;

export const initiatePasswordResetRequest = () => {
  return {
    type: INITIATE_PASSWORD_RESET_REQUEST,
  };
};

export const initiatePasswordResetSuccess = (payload) => {
  return {
    type: INITIATE_PASSWORD_RESET_SUCCESS,
    payload,
  };
};

export const initiatePasswordResetError = (payload) => {
  return {
    type: INITIATE_PASSWORD_RESET_ERROR,
    payload: payload,
  };
};

export const validateEmailRequest = () => {
  return {
    type: VALIDATE_PASSWORD_RESET_REQUEST,
  };
};

export const validateEmailSuccess = (payload) => {
  return {
    type: VALIDATE_PASSWORD_RESET_SUCCESS,
    payload: payload,
  };
};

export const validateEmailError = (payload) => {
  return {
    type: VALIDATE_PASSWORD_RESET_ERROR,
    payload: payload,
  };
};
export const completePasswordResetRequest = () => {
  return {
    type: COMPLETE_PASSWORD_RESET_REQUEST,
  };
};

export const completePasswordResetSuccess = (payload) => {
  return {
    type: COMPLETE_PASSWORD_RESET_SUCCESS,
    payload: payload,
  };
};

export const completePasswordResetError = (payload) => {
  return {
    type: COMPLETE_PASSWORD_RESET_ERROR,
    payload: payload,
  };
};

export const refreshRequest = () => {
  return {
    type: REFRESH_REQUEST,
  };
};
