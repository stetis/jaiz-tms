import { ActionConstants } from "../constants/ActionConstants";

export const merchantTransactionsRequest = () => {
  return {
    type: ActionConstants.MERCHANT_TRANSACTIONS_REQUEST,
  };
};

export const merchantTransactionsSuccess = (payload) => {
  return {
    type: ActionConstants.MERCHANT_TRANSACTIONS_SUCCESS,
    payload: payload,
  };
};

export const merchantTransactionsError = (payload) => {
  return {
    type: ActionConstants.MERCHANT_TRANSACTIONS_ERROR,
    payload: payload,
  };
};
export const agentTrasactionsRequest = () => {
  return {
    type: ActionConstants.AGENT_TRANSACTIONS_REQUEST,
  };
};

export const agentTrasactionsSuccess = (payload) => {
  return {
    type: ActionConstants.AGENT_TRANSACTIONS_SUCCESS,
    payload: payload,
  };
};

export const agentTrasactionsError = (payload) => {
  return {
    type: ActionConstants.AGENT_TRANSACTIONS_ERROR,
    payload: payload,
  };
};
export const agentCommisssionsRequest = () => {
  return {
    type: ActionConstants.AGENT_COMMISSIONS_REQUEST,
  };
};

export const agentCommisssionsSuccess = (payload) => {
  return {
    type: ActionConstants.AGENT_COMMISSIONS_SUCCESS,
    payload: payload,
  };
};

export const agentCommisssionsError = (payload) => {
  return {
    type: ActionConstants.AGENT_COMMISSIONS_ERROR,
    payload: payload,
  };
};

export const merchantPerformanceRequest = () => {
  return {
    type: ActionConstants.MERCHANT_PERFORMANCE_REQUEST,
  };
};

export const merchantPerformanceSuccess = (payload) => {
  return {
    type: ActionConstants.MERCHANT_PERFORMANCE_SUCCESS,
    payload: payload,
  };
};

export const merchantPerformanceError = (payload) => {
  return {
    type: ActionConstants.MERCHANT_TRANSACTIONS_ERROR,
    payload: payload,
  };
};
export const branchAnalyticsRequest = () => {
  return {
    type: ActionConstants.BRANCH_ANALYTICS_REQUEST,
  };
};

export const branchAnalyticsSuccess = (payload) => {
  return {
    type: ActionConstants.BRANCH_ANALYTICS_SUCCESS,
    payload: payload,
  };
};

export const branchAnalyticsError = (payload) => {
  return {
    type: ActionConstants.BRANCH_ANALYTICS_ERROR,
    payload: payload,
  };
};
export const eJournalRequest = () => {
  return {
    type: ActionConstants.E_JOURNAL_REQUEST,
  };
};

export const eJournalSuccess = (payload) => {
  return {
    type: ActionConstants.E_JOURNAL_SUCCESS,
    payload: payload,
  };
};

export const eJournalError = (payload) => {
  return {
    type: ActionConstants.E_JOURNAL_ERROR,
    payload: payload,
  };
};
export const transactionsRequest = () => {
  return {
    type: ActionConstants.TRANSACTIONS_REQUEST,
  };
};

export const transactionsSuccess = (payload) => {
  return {
    type: ActionConstants.TRANSACTIONS_SUCCESS,
    payload: payload,
  };
};

export const transactionsError = (payload) => {
  return {
    type: ActionConstants.TRANSACTIONS_ERROR,
    payload: payload,
  };
};
export const clearMessages = () => {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
};
