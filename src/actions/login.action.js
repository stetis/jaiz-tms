import { ActionConstants } from "../constants/ActionConstants";

const {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  LOGOUT_ERROR,
  REFRESH_REQUEST,
  REFRESH_SUCCESS,
  REFRESH_ERROR,
  CLEAR_MESSAGES,
} = ActionConstants;

export const loginRequest = () => {
  return {
    type: LOGIN_REQUEST,
  };
};

export const loginSuccess = (payload) => {
  return {
    type: LOGIN_SUCCESS,
    payload: payload,
  };
};

export const loginError = (payload) => {
  return {
    type: LOGIN_ERROR,
    payload: payload,
  };
};

export const logoutRequest = () => {
  return {
    type: LOGOUT_REQUEST,
  };
};

export const logoutSuccess = (payload) => {
  return {
    type: LOGOUT_SUCCESS,
    payload: payload,
  };
};

export const logoutError = (payload) => {
  return {
    type: LOGOUT_ERROR,
    payload: payload,
  };
};
export const refreshRequest = () => {
  return {
    type: REFRESH_REQUEST,
  };
};

export const refreshSuccess = (payload) => {
  return {
    type: REFRESH_SUCCESS,
    payload: payload,
  };
};

export const refreshError = (payload) => {
  return {
    type: REFRESH_ERROR,
    payload: payload,
  };
};
export const clearMessages = () => {
  return {
    type: CLEAR_MESSAGES,
  };
};
