import { ActionConstants } from "../constants/ActionConstants";

export const posesRequest = () => {
  return {
    type: ActionConstants.POSES_REQUEST,
  };
};

export const posesSuccess = (payload) => {
  return {
    type: ActionConstants.POSES_SUCCESS,
    payload: payload,
  };
};

export const posesError = (payload) => {
  return {
    type: ActionConstants.POSES_ERROR,
    payload: payload,
  };
};
export const singlePosRequest = () => {
  return {
    type: ActionConstants.SINGLE_POS_REQUEST,
  };
};

export const singlePosSuccess = (payload) => {
  return {
    type: ActionConstants.SINGLE_POS_SUCCESS,
    payload: payload,
  };
};

export const singlePosError = (payload) => {
  return {
    type: ActionConstants.SINGLE_POS_ERROR,
    payload: payload,
  };
};
export const posInventoryRequest = () => {
  return {
    type: ActionConstants.POS_INVENORY_REQUEST,
  };
};

export const posInventorySuccess = (payload) => {
  return {
    type: ActionConstants.POS_INVENORY_SUCCESS,
    payload: payload,
  };
};

export const posInventoryError = (payload) => {
  return {
    type: ActionConstants.POS_INVENORY_ERROR,
    payload: payload,
  };
};

export const actionsRequest = () => {
  return {
    type: ActionConstants.ACTIONS_REQUEST,
  };
};

export const actionsSuccess = (payload) => {
  return {
    type: ActionConstants.ACTIONS_SUCCESS,
    payload: payload,
  };
};

export const actionsError = (payload) => {
  return {
    type: ActionConstants.ACTIONS_ERROR,
    payload: payload,
  };
};

export const clearMessages = () => {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
};
