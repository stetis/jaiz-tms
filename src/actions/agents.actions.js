import { ActionConstants } from "../constants/ActionConstants";

export const singleAgentRequest = () => {
  return {
    type: ActionConstants.SINGLE_AGENT_REQUEST,
  };
};

export const singleAgentSuccess = (payload) => {
  return {
    type: ActionConstants.SINGLE_AGENT_SUCCESS,
    payload: payload,
  };
};

export const singleAgentError = (payload) => {
  return {
    type: ActionConstants.SINGLE_AGENT_ERROR,
    payload: payload,
  };
};
export const allAgentsRequest = () => {
  return {
    type: ActionConstants.ALL_AGENTS_REQUEST,
  };
};

export const allAgentsSuccess = (payload) => {
  return {
    type: ActionConstants.ALL_AGENTS_SUCCESS,
    payload: payload,
  };
};

export const allAgentsError = (payload) => {
  return {
    type: ActionConstants.ALL_AGENTS_ERROR,
    payload: payload,
  };
};

export const agentsRequest = () => {
  return {
    type: ActionConstants.AGENTS_REQUEST,
  };
};

export const agentsSuccess = (payload) => {
  return {
    type: ActionConstants.AGENTS_SUCCESS,
    payload: payload,
  };
};

export const agentsError = (payload) => {
  return {
    type: ActionConstants.AGENTS_ERROR,
    payload: payload,
  };
};

export const actionsRequest = () => {
  return {
    type: ActionConstants.ACTIONS_REQUEST,
  };
};

export const actionsSuccess = (payload) => {
  return {
    type: ActionConstants.ACTIONS_SUCCESS,
    payload: payload,
  };
};

export const actionsError = (payload) => {
  return {
    type: ActionConstants.ACTIONS_ERROR,
    payload: payload,
  };
};

export const clearMessages = () => {
  return {
    type: ActionConstants.CLEAR_MESSAGES,
  };
};
