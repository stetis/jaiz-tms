import { ActionConstants } from "../constants/ActionConstants";

const {
  INSTALL_REQUEST,
  INSTALL_SUCCESS,
  INSTALL_ERROR,
  INSTALLATION_CHECK_REQUEST,
  INSTALLATION_CHECK_SUCCESS,
  INSTALLATION_CHECK_ERROR,
  CLEAR_MESSAGES,
} = ActionConstants;

export const installationRequest = () => {
  return {
    type: INSTALL_REQUEST,
  };
};
export const installationSucess = (payload) => {
  return {
    type: INSTALL_SUCCESS,
    payload,
  };
};
export const installationError = (payload) => {
  return {
    type: INSTALL_ERROR,
    payload,
  };
};

export const checkInstallationRequest = () => {
  return {
    type: INSTALLATION_CHECK_REQUEST,
  };
};
export const checkInstallationSucess = (payload) => {
  return {
    type: INSTALLATION_CHECK_SUCCESS,
    payload,
  };
};
export const checkInstallationError = (payload) => {
  return {
    type: INSTALLATION_CHECK_ERROR,
    payload,
  };
};

export const clearMessages = () => {
  return {
    type: CLEAR_MESSAGES,
  };
};
