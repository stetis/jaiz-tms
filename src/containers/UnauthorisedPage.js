import React from "react";
import { Grid } from "@material-ui/core";
import { Unauthorised } from "../assets/icons/Svg";
import { SubmitButton } from "../components/Buttons/index";
import { ButtonsRow } from "../components/Buttons/styles";
import { styles } from "./containerStyles/unauthorisedStyle";

export default function UnauthorisedPage() {
  const classes = styles();
  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item xs={12} sm={6} lg={6} md={6} style={{ margin: "0 auto" }}>
          <Unauthorised />
        </Grid>
        <Grid item xs={12} sm={12} lg={12} md={12}>
          <div className={classes.unauthorisedTxt}>
            Sorry you do not have access to this page. Click the button to login
          </div>
        </Grid>
        <Grid item xs={12} sm={12} lg={12} md={12}>
          <ButtonsRow>
            <SubmitButton
              type="submit"
              className={classes.unauthorisedBtn}
              onClick={() => (window.location = "/")}
            >
              Login
            </SubmitButton>
          </ButtonsRow>
        </Grid>
      </Grid>
    </div>
  );
}
