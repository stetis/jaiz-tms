import { Grid } from "@material-ui/core";
import React, { useEffect } from "react";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { Title } from "../../components/contentHolders/Card";
import jaiz from "../../images/jaiz.png";
import { styles } from "./styles";
import { SubmitButton } from "../../components/Buttons/index";
import { history } from "../../App";

function ChangePasswordMessage() {
  const classes = styles();
  const data = useSelector((state) => state.loginReducer.data["profile"]);
  const handleClose = () => {
    return history.push("/change-password");
  };
  useEffect(() => {
    document.title = "Change password message | Password reset";
  });
  return (
    <div className={classes.card}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <Link to="/" className={classes.logo}>
            <img src={jaiz} alt="jaiz logo" style={{ width: 70, height: 40 }} />
          </Link>
        </Grid>
        <div className={classes.container}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Title>Password change</Title>
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <div className={classes.emailConfirmation}>
              Welcome {data.name}, Click the button below to update your
              password.
            </div>
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <SubmitButton
              type="submit"
              className={classes.submitAddButton}
              style={{
                marginTop: 15,
                width: "100%",
              }}
              onClick={handleClose}
            >
              change password
            </SubmitButton>
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <div className={classes.copyright}>
              <span className={classes.copyrightSymbol}> &#169;</span> Stetis
              Terminal Management System{" "}
              <span className={classes.yearColor}>
                {new Date().getFullYear()}{" "}
              </span>
              .
            </div>
          </Grid>
        </div>
      </Grid>
    </div>
  );
}
export default withRouter(ChangePasswordMessage);
