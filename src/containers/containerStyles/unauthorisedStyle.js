import { Fonts, Colors } from "../../assets/themes/theme";
import { makeStyles } from "@material-ui/core";

export const styles = makeStyles(() => ({
  root: {
    width: 500,
    height: "auto",
    display: "flex",
    margin: "150px auto",
    fontSize: Fonts.font,
    fontFamily: Fonts.primary,
  },
  unauthorisedTxt: {
    width: "75%",
    display: "flex",
    margin: "10px auto",
    fontSize: Fonts.font,
    fontFamily: Fonts.secondary,
  },
  unauthorisedBtn: {
    margin: "0 auto",
    color: Colors.secondary,
    width: "50%",
  },
}));
