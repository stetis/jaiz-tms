import React from "react";
import Loadable from "react-loadable";
import Spinner from "../assets/Spinner";
import { Colors } from "../assets/themes/theme";

const styleProps = {
  color: Colors.primary,
  height: 50,
  width: 50,
  className: "spinner-background-opt",
};
const Profile = Loadable({
  loader: () => import("../Views/Profile/index.js"),
  loading: () => <Spinner {...styleProps} />,
});
const Dashboard = Loadable({
  loader: () => import("../Views/Dashboard/index"),
  loading: () => <Spinner {...styleProps} />,
});
const Merchants = Loadable({
  loader: () => import("../Views/Merchants/table"),
  loading: () => <Spinner {...styleProps} />,
});
const AddMerchant = Loadable({
  loader: () => import("../Views/Merchants/add/add"),
  loading: () => <Spinner {...styleProps} />,
});
const EditMerchant = Loadable({
  loader: () => import("../Views/Merchants/manage/manage"),
  loading: () => <Spinner {...styleProps} />,
});
const Agents = Loadable({
  loader: () => import("../Views/Agents/table"),
  loading: () => <Spinner {...styleProps} />,
});
const AddAgent = Loadable({
  loader: () => import("../Views/Agents/add"),
  loading: () => <Spinner {...styleProps} />,
});
const EditAgent = Loadable({
  loader: () => import("../Views/Agents/manage"),
  loading: () => <Spinner {...styleProps} />,
});
const SuperAgents = Loadable({
  loader: () => import("../Views/Agents/superAgent/table"),
  loading: () => <Spinner {...styleProps} />,
});
const AddSuperAgent = Loadable({
  loader: () => import("../Views/Agents/superAgent/add"),
  loading: () => <Spinner {...styleProps} />,
});
const EditSuperAgent = Loadable({
  loader: () => import("../Views/Agents/superAgent/manage/manage"),
  loading: () => <Spinner {...styleProps} />,
});

const AddRole = Loadable({
  loader: () => import("../Views/Configurations/Roles/add"),
  loading: () => <Spinner {...styleProps} />,
});
const ManageRole = Loadable({
  loader: () => import("../Views/Configurations/Roles/manage/manageRole"),
  loading: () => <Spinner {...styleProps} />,
});
const Roles = Loadable({
  loader: () => import("../Views/Configurations/Roles/table"),
  loading: () => <Spinner {...styleProps} />,
});

const PTSP = Loadable({
  loader: () => import("../Views/PTSP/table"),
  loading: () => <Spinner {...styleProps} />,
});
const ManagePTSP = Loadable({
  loader: () => import("../Views/PTSP/manage/manage"),
  loading: () => <Spinner {...styleProps} />,
});
const POSInventory = Loadable({
  loader: () => import("../Views/POSInventory/table"),
  loading: () => <Spinner {...styleProps} />,
});
const AddBulkPOS = Loadable({
  loader: () => import("../Views/POSInventory/add/bulkUpload"),
  loading: () => <Spinner {...styleProps} />,
});
const Terminals = Loadable({
  loader: () => import("../Views/Terminals/table"),
  loading: () => <Spinner {...styleProps} />,
});
const AgentTerminals = Loadable({
  loader: () => import("../Views/Terminals/Agents/table"),
  loading: () => <Spinner {...styleProps} />,
});
const AddSingleAgentTerminal = Loadable({
  loader: () => import("../Views/Terminals/Agents/addTerminal/agent"),
  loading: () => <Spinner {...styleProps} />,
});
const AddSingleMerchantTerminal = Loadable({
  loader: () => import("../Views/Terminals/addTerminal/single"),
  loading: () => <Spinner {...styleProps} />,
});
const AddMultipleTerminals = Loadable({
  loader: () => import("../Views/Terminals/addTerminal/multipleMerchant"),
  loading: () => <Spinner {...styleProps} />,
});
const BulkUploadMerchantsTerminals = Loadable({
  loader: () => import("../Views/Terminals/addTerminal/bulkMerchants"),
  loading: () => <Spinner {...styleProps} />,
});
const BulkUploadAgentsTerminals = Loadable({
  loader: () => import("../Views/Terminals/Agents/addTerminal/bulkAgents"),
  loading: () => <Spinner {...styleProps} />,
});
const ManageTerminal = Loadable({
  loader: () => import("../Views/Terminals/manage/manage"),
  loading: () => <Spinner {...styleProps} />,
});
const MerchantTransactions = Loadable({
  loader: () => import("../Views/Reports/merchantTransactions"),
  loading: () => <Spinner {...styleProps} />,
});
const AgentTransactions = Loadable({
  loader: () => import("../Views/Reports/agentTransactions"),
  loading: () => <Spinner {...styleProps} />,
});
const AgentsCommissions = Loadable({
  loader: () => import("../Views/Reports/agentComissions"),
  loading: () => <Spinner {...styleProps} />,
});
const MerchantPerformance = Loadable({
  loader: () => import("../Views/Reports/merchantPerfomance"),
  loading: () => <Spinner {...styleProps} />,
});
const BranchAnalytics = Loadable({
  loader: () => import("../Views/Reports/branchAnalytics"),
  loading: () => <Spinner {...styleProps} />,
});
const Ejournal = Loadable({
  loader: () => import("../Views/Reports/eJournal"),
  loading: () => <Spinner {...styleProps} />,
});
const Titles = Loadable({
  loader: () => import("../Views/Configurations/Title/table"),
  loading: () => <Spinner {...styleProps} />,
});
const MerchantCategories = Loadable({
  loader: () =>
    import("../Views/Configurations/MerchantCategory/merchantsCategory"),
  loading: () => <Spinner {...styleProps} />,
});
const BulkUploadMerchantCategories = Loadable({
  loader: () => import("../Views/Configurations/MerchantCategory/bulkUpload"),
  loading: () => <Spinner {...styleProps} />,
});
const Regions = Loadable({
  loader: () => import("../Views/Configurations/Regions/table"),
  loading: () => <Spinner {...styleProps} />,
});
const Branches = Loadable({
  loader: () => import("../Views/Configurations/Branches/table"),
  loading: () => <Spinner {...styleProps} />,
});
const BranchesBulkUpload = Loadable({
  loader: () => import("../Views/Configurations/Branches/bulkUpload"),
  loading: () => <Spinner {...styleProps} />,
});
const Commissions = Loadable({
  loader: () => import("../Views/Configurations/Commissions/commissions"),
  loading: () => <Spinner {...styleProps} />,
});
const ManageGroups = Loadable({
  loader: () => import("../Views/Configurations/Commissions/manageGroup"),
  loading: () => <Spinner {...styleProps} />,
});
const Devices = Loadable({
  loader: () => import("../Views/Configurations/Devices/table"),
  loading: () => <Spinner {...styleProps} />,
});
const AccountTypes = Loadable({
  loader: () => import("../Views/Configurations/accountType/table"),
  loading: () => <Spinner {...styleProps} />,
});
const SoftwareVersioning = Loadable({
  loader: () => import("../Views/Configurations/softwareVersions/table"),
  loading: () => <Spinner {...styleProps} />,
});
const AddSoftwareVersion = Loadable({
  loader: () => import("../Views/Configurations/softwareVersions/add"),
  loading: () => <Spinner {...styleProps} />,
});
const SoftwareUpdate = Loadable({
  loader: () =>
    import("../Views/Configurations/softwareVersions/manage/appUpdate"),
  loading: () => <Spinner {...styleProps} />,
});
const EditSoftwareVersion = Loadable({
  loader: () =>
    import("../Views/Configurations/softwareVersions/manage/manage"),
  loading: () => <Spinner {...styleProps} />,
});
const SoftwareHistory = Loadable({
  loader: () =>
    import("../Views/Configurations/softwareVersions/manage/history"),
  loading: () => <Spinner {...styleProps} />,
});
const Users = Loadable({
  loader: () => import("../Views/System/Users/table"),
  loading: () => <Spinner {...styleProps} />,
});

const AuditLog = Loadable({
  loader: () => import("../Views/System/auditLog"),
  loading: () => <Spinner {...styleProps} />,
});
const routes = [
  {
    path: "/tms",
    exact: true,
    name: "Home",
    component: Dashboard,
  },
  {
    path: "/tms/profile-management",
    name: "Profile",
    component: Profile,
  },
  {
    path: "/tms/merchants",
    name: "Merchants",
    component: Merchants,
  },
  {
    path: "/tms/add-merchant",
    name: "AddMerchant",
    component: AddMerchant,
  },
  {
    path: "/tms/edit-merchant",
    name: "EditMerchant",
    component: EditMerchant,
  },
  {
    path: "/tms/manage-agents",
    name: "Agents",
    component: Agents,
  },
  {
    path: "/tms/add-agent",
    name: "AddAgent",
    component: AddAgent,
  },
  {
    path: "/tms/edit-agent",
    name: "EditAgent",
    component: EditAgent,
  },
  {
    path: "/tms/super-agents",
    name: "SuperAgents",
    component: SuperAgents,
  },
  {
    path: "/tms/add-super-agent",
    name: "AddSuperAgent",
    component: AddSuperAgent,
  },
  {
    path: "/tms/edit-super-agent",
    name: "EditSuperAgent",
    component: EditSuperAgent,
  },
  {
    path: "/tms/pos-inventory",
    name: "POSInventory",
    component: POSInventory,
  },
  {
    path: "/tms/pos-bulkupload",
    name: "AddBulkPOS",
    component: AddBulkPOS,
  },
  {
    path: "/tms/merchant-terminals",
    name: "Terminals",
    component: Terminals,
  },
  {
    path: "/tms/agent-terminals",
    name: "AgentTerminals",
    component: AgentTerminals,
  },
  {
    path: "/tms/add-agent-terminal",
    name: "AddSingleAgentTerminal",
    component: AddSingleAgentTerminal,
  },
  {
    path: "/tms/add-single-merchant-terminal",
    name: "AddSingleMerchantTerminal",
    component: AddSingleMerchantTerminal,
  },
  {
    path: "/tms/add-multiple-terminals",
    name: "AddMultipleTerminals",
    component: AddMultipleTerminals,
  },
  {
    path: "/tms/add-bulkmerchant-terminals",
    name: "BulkUploadMerchantsTerminals",
    component: BulkUploadMerchantsTerminals,
  },
  {
    path: "/tms/add-bulkagent-terminals",
    name: "BulkUploadAgentsTerminals",
    component: BulkUploadAgentsTerminals,
  },

  {
    path: "/tms/manage-terminal",
    name: "ManageTerminal",
    component: ManageTerminal,
  },
  {
    path: "/tms/ptsp",
    name: "PTSP",
    component: PTSP,
  },
  {
    path: "/tms/manage-ptsp",
    name: "ManagePTSP",
    component: ManagePTSP,
  },
  {
    path: "/tms/merchants-transactions",
    name: "MerchantTransactions",
    component: MerchantTransactions,
  },
  {
    path: "/tms/agents-transactions",
    name: "AgentTransactions",
    component: AgentTransactions,
  },
  {
    path: "/tms/agents-commissions",
    name: "AgentsCommissions",
    component: AgentsCommissions,
  },
  {
    path: "/tms/merchants-performance",
    name: "MerchantPerformance",
    component: MerchantPerformance,
  },
  {
    path: "/tms/branch-analytics",
    name: "BranchAnalytics",
    component: BranchAnalytics,
  },
  {
    path: "/tms/e-journal",
    name: "Ejournal",
    component: Ejournal,
  },
  {
    path: "/tms/titles",
    name: "Titles",
    component: Titles,
  },
  {
    path: "/tms/merchant-categories",
    name: "MerchantCategories",
    component: MerchantCategories,
  },
  {
    path: "/tms/merchant-categories-bulkupload",
    name: "BulkUploadMerchantCategories",
    component: BulkUploadMerchantCategories,
  },
  {
    path: "/tms/regions",
    name: "Regions",
    component: Regions,
  },
  {
    path: "/tms/roles",
    name: "Roles",
    component: Roles,
  },
  {
    path: "/tms/add-role",
    name: "AddRole",
    component: AddRole,
  },
  {
    path: "/tms/manage-role",
    name: "ManageRole",
    component: ManageRole,
  },
  {
    path: "/tms/branches-bulkupload",
    name: "BranchesBulkUpload",
    component: BranchesBulkUpload,
  },
  {
    path: "/tms/branches",
    name: "Branches",
    component: Branches,
  },
  {
    path: "/tms/commissions",
    name: "Commissions",
    component: Commissions,
  },
  {
    path: "/tms/manage-group",
    name: "ManageGroups",
    component: ManageGroups,
  },
  {
    path: "/tms/devices",
    name: "Devices",
    component: Devices,
  },
  {
    path: "/tms/account-types",
    name: "AccountTypes",
    component: AccountTypes,
  },
  {
    path: "/tms/software-versioning",
    name: "SoftwareVersioning",
    component: SoftwareVersioning,
  },
  {
    path: "/tms/add-software-version",
    name: "AddSoftwareVersion",
    component: AddSoftwareVersion,
  },
  {
    path: "/tms/software-history",
    name: "SoftwareHistory",
    component: SoftwareHistory,
  },
  {
    path: "/tms/software-update",
    name: "SoftwareUpdate",
    component: SoftwareUpdate,
  },

  {
    path: "/tms/manage-software",
    name: "EditSoftwareVersion",
    component: EditSoftwareVersion,
  },
  {
    path: "/tms/users",
    name: "Users",
    component: Users,
  },
  { path: "/tms/Audit-log", name: "AuditLog", component: AuditLog },
  {
    path: "/tms/dashboard",
    name: "Dashboard",
    component: Dashboard,
  },
];

export default routes;
