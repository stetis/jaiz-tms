import { withStyles } from "@material-ui/core";
import Badge from "@material-ui/core/Badge";
import NotificationsIcon from "@material-ui/icons/Notifications";
import React, { Component } from "react";
import { connect } from "react-redux";
import { history } from "../App";
import { Colors } from "../assets/themes/theme";
// import { fetchInbox } from "../helpers/selfServiceHelper/inboxHelper";
import { styles } from "./containerStyles/styles";

const StyledBadge = withStyles((theme) => ({
  badge: {
    right: 0,
    top: 6,
    border: `1px solid ${Colors.buttonError}`,
    padding: "0 0",
    color: Colors.light,
    width: 10,
    height: 16,
    fontSize: 10,
    backgroundColor: Colors.buttonError,
  },
}))(Badge);

class Notifications extends Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorElement: null,
      page: 0,
      pagesize: 15,
      search: "",
    };
  }
  // componentDidMount() {
  //   const { page, pagesize, search } = this.state;
  //   const { getInbox } = this.props;
  //   const credentials = { page, pagesize, search };

  //   getInbox(credentials);
  // }

  handleIconClick = (event) => {
    event.preventDefault();
    history.push("/selfservice/notifications");
  };
  handleIconClose = () => {
    this.setState({ anchorElement: null });
  };
  // handleRequests = () => {
  //   history.push("/selfservice/requisition-table");
  // };
  render() {
    const { classes, inboxData } = this.props;
    // const { anchorElement } = this.state;
    return (
      <div>
        <div className={classes.notificationIcon}>
          <StyledBadge
            badgeContent={inboxData.tableData.unread}
            max={9}
            onClick={this.handleIconClick}
          >
            <NotificationsIcon className={classes.mailIcon} />
          </StyledBadge>
        </div>
        {/* <Menu
          id="menu"
          anchorEl={anchorElement}
          keepMounted
          open={Boolean(anchorElement)}
          onClose={this.handleIconClose}
          classes={{ paper: classes.paper }}
        >
          <MenuItem
            onClick={this.handleIconClose}
            classes={{ root: classes.menuItemRoot }}
          >
            <Typography
              variant="button"
              onClick={this.handleRequests}
              disabled={isLoading}
              className={classes.notificationMenuList}
            >
              <span className="submit-btn">
                {isLoading ? (
                  <Loader
                    type="ThreeDots"
                    color="#0E4C28"
                    height="15"
                    width="30"
                  />
                ) : (
                  "Respond to requests"
                )}
              </span>
            </Typography>
          </MenuItem>
          <MenuItem
            onClick={this.handleIconClose}
            classes={{ root: classes.menuItemRoot }}
          >
            <Typography
              variant="button"
              onClick={this.handleCompleteSubmit}
              disabled={isLoading}
              className={classes.notificationMenuList}
            >
              Leave request. Click to view details
            </Typography>
          </MenuItem>
        </Menu> */}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    inboxData: state.inboxReducer,
  };
};

// const mapDispatchToProps = (dispatch) => {
//   return {
//     getInbox: (credentials) => dispatch(fetchInbox(credentials)),
//   };
// };

export default connect(
  mapStateToProps,
  null
)(withStyles(styles)(Notifications));
