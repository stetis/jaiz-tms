import { AppBar, Toolbar } from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import AccessTimeSharpIcon from "@material-ui/icons/AccessTimeSharp";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import ArrowDropUpIcon from "@material-ui/icons/ArrowDropUp";
import CalendarTodaySharpIcon from "@material-ui/icons/CalendarTodaySharp";
import clsx from "clsx";
import React, { useEffect, useState } from "react";
import Avatar from "react-avatar";
import Loader from "react-loader-spinner";
import { useDispatch, useSelector } from "react-redux";
import { LogoutIcon, UserIcon } from "../../assets/icons/Svg";
import { Colors } from "../../assets/themes/theme";
import { userLogout } from "../../helpers/login.helper";
import { styles } from "../containerStyles/styles";
import { history } from "../../App";

const nameLoader = (
  <Loader type="Rings" color={Colors.secondary} height="10" width="10" />
);
export default function DefaultHeader(props) {
  const classes = styles();
  const [visible, setVisible] = useState(false);
  const [base64] = useState("");
  const [date, setDate] = useState(new Date());
  const dispatch = useDispatch();
  const data = useSelector((state) => state.loginReducer.data);
  const loading = useSelector((state) => state.loginReducer.loggingout);

  useEffect(() => {
    var timer = setInterval(() => setDate(new Date()), 1000);
    return function cleanup() {
      clearInterval(timer);
    };
  });

  const handleLogout = () => dispatch(userLogout());
  const redirectToProfile = () => {
    return history.push("/tms/profile-management");
  };
  const toggle = () => setVisible(!visible);
  return (
    <div>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: props.open,
          [classes.appbarNormal]: !props.open,
        })}
      >
        <Toolbar classes={{ root: classes.ToolbarRoot }}>
          <div className={classes.grow} />
          <div className={classes.timeDate}>
            <AccessTimeSharpIcon className={classes.accessTimeIcon} />
            <span className={classes.momentTime}>
              {date.toLocaleTimeString()}
            </span>
            <CalendarTodaySharpIcon className={classes.CalendarTodayIcon} />
            <span className={classes.momentDate}>{date.toDateString()}</span>
          </div>
          <div className={classes.profile}>
            <div onClick={toggle}>
              {!base64 ? (
                <AccountCircleIcon className={classes.accountIcon} />
              ) : (
                <Avatar
                  size="30"
                  round={true}
                  src={base64}
                  className={classes.accountIconAvatar}
                />
              )}
              <div className={classes.profileDropdown}>
                <div className={classes.profileTitle}>
                  <div className={classes.profileName}>
                    <h4 className={classes.accountName}>
                      {data.profile && data.profile.name}
                    </h4>
                    <div className={classes.dropdownIcon}>
                      {visible ? (
                        <ArrowDropUpIcon className={classes.profileButton} />
                      ) : (
                        <ArrowDropDownIcon className={classes.profileButton} />
                      )}
                    </div>
                  </div>
                  {visible ? (
                    <div className={classes.dropdownList}>
                      <div
                        className={classes.dropdownAction}
                        style={{ marginTop: -4 }}
                      >
                        <div className={classes.dropdownActionIcon}>
                          <UserIcon
                            style={{
                              color: Colors.secondary,
                            }}
                          />
                        </div>
                        <div
                          className={classes.profileDropdownActionName}
                          onClick={redirectToProfile}
                        >
                          profile
                        </div>
                      </div>
                      <hr className={classes.line1} />
                      {loading ? (
                        nameLoader
                      ) : (
                        <div
                          className={classes.logoutDropdownAction}
                          onClick={handleLogout}
                        >
                          <div
                            className={classes.logoutDropdownActionIcon}
                            onClick={handleLogout}
                          >
                            <LogoutIcon />
                          </div>
                          <div className={classes.logoutDropdownActionName}>
                            logout
                          </div>
                        </div>
                      )}
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}
