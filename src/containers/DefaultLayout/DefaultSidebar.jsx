import {
  Collapse,
  CssBaseline,
  Drawer,
  Fab,
  List,
  ListItemIcon,
  ListItemText,
} from "@material-ui/core";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MenuIcon from "@material-ui/icons/Menu";
import clsx from "clsx";
import React, { useState } from "react";
import Loader from "react-loader-spinner";
import { useSelector } from "react-redux";
import { withRouter } from "react-router";
import { Text } from "../../components/Forms/index";
import { Colors } from "../../assets/themes/theme";
import { StyledListItem, styles } from "../containerStyles/styles";
import { handleSidebarNavItems } from "./uiHelpers";
import jaiz from "../../images/jaiz-logo.png";

const Spinner = (
  <Loader type="Puff" color={Colors.primary} height="40" width="40" />
);

function DefaultSidebar(props) {
  const classes = styles();
  const [state, setState] = useState({
    open: false,
    openNest: "",
    prevNest: "",
  });
  const data = useSelector((state) => state.loginReducer.data);
  const loading = useSelector((state) => state.loginReducer.loading);
  const error = useSelector((state) => state.loginReducer.error);
  const errorMessage = useSelector((state) => state.loginReducer.errorMessage);

  const handleClick = (item, index) => {
    if (item.children && state.openNest === index) {
      setState((prev) => ({
        ...prev,
        openNest: "",
      }));
    } else if (item.children) {
      setState((prev) => ({
        ...prev,
        openNest: index,
      }));
      !state.open && props.handleDrawerOpen();
    } else {
      props.history.push(item.link);
      // eslint-disable-next-line no-unused-expressions
      state.open
        ? state.open && props.handleDrawerClose()
        : !state.open && props.handleDrawerOpen();
    }
  };
  const { handleDrawerClose, handleDrawerOpen, open, location } = props;
  const { openNest } = state;
  const menuList = [...handleSidebarNavItems(data.pages)];
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.drawerToggle}>
          {open ? (
            <Fab
              aria-label="add"
              classes={{ root: classes.fab }}
              size="small"
              onClick={handleDrawerClose}
            >
              <ChevronLeftIcon color="inherit" />
            </Fab>
          ) : (
            ""
          )}
          <div className={classes.appnameLogo}>
            {open ? (
              <img src={jaiz} alt="jaiz logo" className={classes.logoImg} />
            ) : (
              <MenuIcon
                color="inherit"
                onClick={handleDrawerOpen}
                className={classes.menuIcon}
              />
            )}
            {open ? <Text className={classes.appName}>Jaiz bank TMS</Text> : ""}
          </div>
          <List
            component="nav"
            aria-labelledby="nested-list-subheader"
            className={classes.listRoot}
          >
            {!loading ? (
              menuList.map((item, index) => {
                return (
                  <span key={index}>
                    <StyledListItem
                      button
                      onClick={() => handleClick(item, index)}
                      selected={location.pathname === item.link}
                    >
                      <ListItemIcon className={classes.listItemIcon}>
                        {item.icon}
                      </ListItemIcon>
                      <ListItemText
                        primary={item.name}
                        classes={{ primary: classes.sidebarText }}
                      />
                      {item.children && open && (
                        <ListItemIcon className={classes.nestedIcon}>
                          {openNest === index ? (
                            <ExpandLessIcon />
                          ) : (
                            <ExpandMoreIcon />
                          )}
                        </ListItemIcon>
                      )}
                    </StyledListItem>
                    <Collapse
                      in={openNest === index}
                      timeout="auto"
                      unmountOnExit
                    >
                      <List component="div" disablePadding>
                        {item.children &&
                          item.children.map((item, index) => (
                            <StyledListItem
                              className={classes.nested}
                              onClick={() => handleClick(item, index)}
                              selected={location.pathname === item.link}
                              key={item.link}
                            >
                              {
                                <ListItemText
                                  primary={item.name}
                                  classes={{
                                    primary: classes.sidebarNestedText,
                                  }}
                                />
                              }
                            </StyledListItem>
                          ))}
                      </List>
                    </Collapse>
                  </span>
                );
              })
            ) : (
              <div className={classes.sideBarLoader}>{Spinner}</div>
            )}
            {error && (
              <h4 className={classes.sideBarErrorText}>{errorMessage}</h4>
            )}
          </List>
        </div>
      </Drawer>
    </div>
  );
}
export default withRouter(DefaultSidebar);
