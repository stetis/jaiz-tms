import { CssBaseline, Grid } from "@material-ui/core";
import Hidden from "@material-ui/core/Hidden";
import React, { useState, useEffect } from "react";
import { Route, Switch } from "react-router-dom";
import MainBreadcrumbs from "../../assets/BreadCrumbs/Breadcrumbs";
import { styles } from "../containerStyles/styles";
import routes from "../routes";
import Cookies from "js-cookie";
import DefaultSidebar from "./DefaultSidebar";
import DefaultSmallScreenSidebar from "./DefaultSmallScreenSidebar";
import { DefaultHeader } from "./index";

export default function DefaultLayout(props) {
  const classes = styles();
  const [open, setOpen] = useState(true);
  useEffect(() => {
    let accessToken = Cookies.get("access-token");
    let refreshToken = Cookies.get("refresh-token");
    if (!(accessToken && refreshToken))
      return props.history.push("/unauthorised");
  });
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Hidden smUp implementation="css">
        <DefaultSmallScreenSidebar />
      </Hidden>
      <Hidden xsDown implementation="css">
        <DefaultSidebar
          handleDrawerOpen={handleDrawerOpen}
          handleDrawerClose={handleDrawerClose}
          open={open}
        />
        <DefaultHeader
          handleDrawerOpen={handleDrawerOpen}
          handleDrawerClose={handleDrawerClose}
          open={open}
        />
      </Hidden>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <MainBreadcrumbs />
        <Switch>
          {routes.map((route, idx) => {
            return route.component ? (
              <Route
                key={idx}
                path={route.path}
                exact={route.exact}
                name={route.name}
                render={(props) => <route.component {...props} />}
              />
            ) : null;
          })}
        </Switch>
        <Grid container spacing={0}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <div className={classes.copyright}>
              <span className={classes.copyrightSymbol}> &#169;</span> Stetis
              Terminal Management System{" "}
              <span className={classes.yearColor}>
                {new Date().getFullYear()}{" "}
              </span>
              .
            </div>
          </Grid>
        </Grid>
      </main>
    </div>
  );
}
