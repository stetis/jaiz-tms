import { Collapse, useTheme } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Drawer from "@material-ui/core/Drawer";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Toolbar from "@material-ui/core/Toolbar";
import AccessTimeSharpIcon from "@material-ui/icons/AccessTimeSharp";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import ArrowDropUpIcon from "@material-ui/icons/ArrowDropUp";
import CalendarTodaySharpIcon from "@material-ui/icons/CalendarTodaySharp";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MenuIcon from "@material-ui/icons/Menu";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import Avatar from "react-avatar";
import Loader from "react-loader-spinner";
import { withRouter } from "react-router";
import { handleSidebarNavItems } from "./uiHelpers";
import { Colors } from "../../assets/themes/theme";
import jaiz from "../../images/jaiz-logo.png";
import { userLogout } from "../../helpers/login.helper";
import { LogoutIcon, UserIcon } from "../../assets/icons/Svg";
import { StyledListItem, useStyles } from "../containerStyles/styles";
import { history } from "../../App";

const Spinner = (
  <Loader type="Rings" color={Colors.primary} height="40" width="40" />
);

function DefaultSmallSidebar(props) {
  const { window } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [open] = React.useState(true);
  const [visible, setVisible] = useState(false);
  const [date, setDate] = useState(new Date());
  const [base64] = useState("");
  const [state, setState] = useState({
    openNest: "",
    prevNest: "",
  });
  const dispatch = useDispatch();
  const data = useSelector((state) => state.loginReducer.data);
  const loading = useSelector((state) => state.loginReducer.loading);
  const error = useSelector((state) => state.loginReducer.error);
  const errorMessage = useSelector((state) => state.loginReducer.errorMessage);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  useEffect(() => {
    var timer = setInterval(() => setDate(new Date()), 1000);
    return function cleanup() {
      clearInterval(timer);
    };
  });
  const toggle = () => {
    setVisible(!visible);
  };

  const handleClick = (item, index) => {
    if (item.children && state.openNest === index) {
      setState((prev) => ({
        ...prev,
        openNest: "",
      }));
    } else if (item.children) {
      setState((prev) => ({
        ...prev,
        openNest: index,
      }));
      !mobileOpen && handleDrawerToggle();
    } else {
      props.history.push(item.link);
      // eslint-disable-next-line no-unused-expressions
      mobileOpen
        ? mobileOpen && handleDrawerToggle()
        : !mobileOpen && handleDrawerToggle();
    }
  };

  const menuList = [...handleSidebarNavItems(data.pages)];
  const redirectToProfile = () => {
    return history.push("/tms/profile-management");
  };
  const handleLogout = () => dispatch(userLogout());
  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <AccessTimeSharpIcon className={classes.accessTimeIcon} />
          <span className={classes.momentTime}>
            {date.toLocaleTimeString()}
          </span>
          <CalendarTodaySharpIcon className={classes.CalendarTodayIcon} />
          <span className={classes.momentDate}>
            {date.toLocaleDateString()}
          </span>
          <div className={classes.profile}>
            {/* <Notifications /> */}
            <div onClick={toggle}>
              {!base64 ? (
                <AccountCircleIcon className={classes.accountIcon} />
              ) : (
                <Avatar
                  size="30"
                  round={true}
                  src={base64}
                  className={classes.accountIconAvatar}
                />
              )}
              <div className={classes.profileDropdown}>
                <div className={classes.profileTitle}>
                  <div className={classes.profileName}>
                    <div className={classes.dropdownIcon}>
                      {visible ? (
                        <ArrowDropUpIcon className={classes.profileButton} />
                      ) : (
                        <ArrowDropDownIcon className={classes.profileButton} />
                      )}
                    </div>
                  </div>
                  {visible ? (
                    <>
                      {/* <div className={classes.dropdownList}>
                        <div
                          className={classes.dropdownAction}
                          style={{ marginTop: -4 }}
                        >
                          <div className={classes.profileDropdownActionName}>
                            "Aliyu Musa Aliyu"}
                          </div>
                        </div>
                      </div>
                      <hr className={classes.line1} /> */}
                      <div className={classes.dropdownList}>
                        <div
                          className={classes.dropdownAction}
                          style={{ marginTop: -4 }}
                        >
                          <div className={classes.dropdownActionIcon}>
                            <UserIcon
                              style={{
                                color: Colors.menuListColor,
                              }}
                            />
                          </div>
                          <div
                            className={classes.profileDropdownActionName}
                            onClick={redirectToProfile}
                          >
                            profile
                          </div>
                        </div>
                        <hr className={classes.line1} />
                        <div
                          className={classes.logoutDropdownAction}
                          onClick={handleLogout}
                        >
                          <div className={classes.logoutDropdownActionIcon}>
                            <LogoutIcon />
                          </div>
                          <div className={classes.logoutDropdownActionName}>
                            logout
                          </div>
                        </div>
                      </div>
                    </>
                  ) : null}
                </div>
              </div>
            </div>
          </div>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Drawer
          container={container}
          variant="temporary"
          anchor={theme.direction === "rtl" ? "right" : "left"}
          open={mobileOpen}
          onClose={handleDrawerToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          <div className={classes.appnameLogo}>
            <img src={jaiz} alt="jaiz logo" className={classes.logoImg} />
            <span className={classes.appName}>Jaiz bank TMS</span>
          </div>
          <List>
            {!loading ? (
              menuList.map((item, index) => {
                return (
                  <span key={index}>
                    <StyledListItem
                      button
                      onClick={() => handleClick(item, index)}
                      selected={props.location.pathname === item.link}
                    >
                      <ListItemIcon className={classes.listItemIcon}>
                        {item.icon}
                      </ListItemIcon>
                      <ListItemText
                        primary={item.name}
                        classes={{ primary: classes.sidebarText }}
                      />
                      {item.children && open && (
                        <ListItemIcon>
                          {state.openNest === index ? (
                            <ExpandLessIcon
                              className={classes.nestedArrowIcon}
                            />
                          ) : (
                            <ExpandMoreIcon
                              className={classes.nestedArrowIcon}
                            />
                          )}
                        </ListItemIcon>
                      )}
                    </StyledListItem>
                    <Collapse
                      in={state.openNest === index}
                      timeout="auto"
                      unmountOnExit
                    >
                      <List component="div" disablePadding>
                        {item.children &&
                          item.children.map((item, index) => (
                            <StyledListItem
                              className={classes.nested}
                              onClick={() => handleClick(item, index)}
                              selected={props.location.pathname === item.link}
                              key={item.link}
                            >
                              {
                                <ListItemText
                                  primary={item.name}
                                  classes={{
                                    primary: classes.sidebarNestedText,
                                  }}
                                />
                              }
                            </StyledListItem>
                          ))}
                      </List>
                    </Collapse>
                  </span>
                );
              })
            ) : (
              <div className={classes.sideBarLoader}>{Spinner}</div>
            )}
            {error
              ? error && (
                  <h4 className={classes.sideBarErrorText}>{errorMessage}</h4>
                )
              : ""}
          </List>
        </Drawer>
      </nav>
    </div>
  );
}

DefaultSmallSidebar.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default withRouter(DefaultSmallSidebar);
