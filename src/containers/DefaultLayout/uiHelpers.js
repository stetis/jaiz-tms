/* eslint no-useless-concat: 0 */
import {
  AgentIcon,
  ConfigurationIcon,
  DashboardIcon,
  PeopleIcon,
  POS,
  Report,
  TransactionsIcon,
} from "../../assets/icons/Svg";
export const updateKey = (keys) => {
  if (typeof keys !== "string") return "";
  const key = keys.toLowerCase();
  const update = key.replace(" ", "-");
  return update;
};

const capitalize = (s) => {
  if (typeof s !== "string") return "";
  return s.charAt(0).toUpperCase() + s.slice(1);
};

/**
 * name in file: `Handle Side Navigation Items`..
 * @param {Object}
 * take data object from server and
 * @return {Object}
 *  sidebar format object
 * with their corresponding navigation links
 */

export const handleSidebarNavItems = (pages) => {
  const sideNavItems = [];
  if (pages) {
    for (let i = 0; i < pages.length; i++) {
      if (pages[i]["sub-pages"].length !== 0) {
        sideNavItems.push({
          name: pages[i].name,
          icon:
            pages[i].name === "Dashboard" ? (
              <DashboardIcon />
            ) : pages[i].name === "Merchants" ? (
              <PeopleIcon />
            ) : pages[i].name === "Reports" ? (
              <Report />
            ) : pages[i].name === "Agents" ? (
              <AgentIcon />
            ) : pages[i].name === "POS Terminals" ? (
              <POS />
            ) : pages[i].name === "Transactions" ? (
              <TransactionsIcon />
            ) : pages[i].name === "Configuration" ? (
              <ConfigurationIcon />
            ) : (
              ""
            ),
          children: pages[i]["sub-pages"].map((page) => ({
            name: capitalize(page.name),
            link: "/tms/" + `${updateKey(page.name)}`,
          })),
        });
      } else {
        sideNavItems.push({
          name: pages[i].name,
          icon:
            pages[i].name === "Dashboard" ? (
              <DashboardIcon />
            ) : pages[i].name === "Merchants" ? (
              <PeopleIcon />
            ) : pages[i].name === "Agents" ? (
              <AgentIcon />
            ) : pages[i].name === "POS Terminals" ? (
              <POS />
            ) : pages[i].name === "Transactions" ? (
              <TransactionsIcon />
            ) : pages[i].name === "Reports" ? (
              <Report />
            ) : pages[i].name === "Configuration" ? (
              <ConfigurationIcon />
            ) : (
              ""
            ),
          link: "/tms/" + `${updateKey(pages[i].name)}`,
        });
      }
    }
  }
  return sideNavItems;
};
