import { createTheme, MuiThemeProvider } from "@material-ui/core/styles";
import { createBrowserHistory } from "history";
import React from "react";
import { Route, Router, Switch } from "react-router-dom";
import "./App.css";
import { Colors, Fonts } from "./assets/themes/theme";
import { DefaultLayout } from "./containers/DefaultLayout";
import ChangePassword from "./containers/firstTimeLogin/changePassword";
import ChangePasswordMessage from "./containers/firstTimeLogin/message";
import UnauthorisedPage from "./containers/UnauthorisedPage";
import CompleteForgotPassword from "./Views/authentication/forgotpassword/CompleteForgotPassword";
import ConfirmEmailPage from "./Views/authentication/forgotpassword/confirm";
import ForgotPassword from "./Views/authentication/forgotpassword/ForgotPassword";
import ConfirmSuccessPage from "./Views/authentication/forgotpassword/passwordResetSuccess";
import SignIn from "./Views/authentication/signIn/SignIn";
import Installation from "./Views/installation/index";

export const history = createBrowserHistory();
const theme = createTheme({
  palette: {
    primary: {
      main: Colors.primary,
    },
    secondary: {
      light: Colors.secondary,
      main: Colors.secondary,
      dark: Colors.secondary,
      contrastText: "#000",
    },
    textPrimary: {
      main: Colors.textColor,
    },
  },
  overrides: {
    MuiPaper: {
      root: {
        color: Colors.textColor,
      },
    },
    "& $notchedOutline": {
      borderColor: Colors.secondary,
      borderWidth: 1,
    },
    MuiOutlinedInput: {
      root: {
        borderRadius: 5,
        color: "#e8ebee",
        "& $notchedOutline": {
          borderColor: "#e8ebee",
          borderWidth: 1,
        },
        "&:hover $notchedOutline": {
          borderColor: Colors.secondary,
          borderWidth: 1,
        },
        "&$focused $notchedOutline": {
          borderWidth: 1,
          borderColor: Colors.secondary,
        },
      },
      focused: {},
    },
    MuiInputBase: {
      root: {
        fontFamily: Fonts.primary,
        fontSize: Fonts.font,
        background: "#fcfdfe",
        borderWidth: 1,
        borderColor: "#e8ebee",
      },
    },
    MuiList: {
      padding: {
        paddingTop: 0,
        paddingBottom: 0,
      },
    },
  },
});
function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <Router history={history} basename=".">
        <div className="app">
          <Switch>
            <Route exact path="/" name="SignIn" component={SignIn} />
            <Route
              path="/install"
              name="Installation"
              component={Installation}
            />
            <Route
              name="ForgotPassword"
              path="/forgot-password"
              component={ForgotPassword}
            />
            <Route
              name="CompleteForgotPassword"
              path="/reset/:token"
              component={CompleteForgotPassword}
            />
            <Route
              name="ConfirmEmailPage"
              path="/confirm-email"
              component={ConfirmEmailPage}
            />
            <Route
              name="ConfirmSuccessPage"
              path="confirmation"
              component={ConfirmSuccessPage}
            />
            <Route
              path="/message"
              name="ChangePasswordMessage"
              component={ChangePasswordMessage}
            />
            <Route
              path="/change-password"
              name="ChangePassword"
              component={ChangePassword}
            />
            <Route
              path="/unauthorised"
              name="UnauthorisedPage"
              component={UnauthorisedPage}
            />
            <Route path="/tms" name="Home" component={DefaultLayout} />
          </Switch>
        </div>
      </Router>
    </MuiThemeProvider>
  );
}

export default App;
