import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { PersistGate } from "redux-persist/es/integration/react";
import { persistStore } from "redux-persist";
import { Provider } from "react-redux";
import store from "./routes/store";
import Spinner from "./assets/Spinner";

//create the spinner here
// let spinner = "";

//create the persistor
let persistor = persistStore(store);
let spinner = <Spinner />;

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={spinner} persistor={persistor}>
      <App />
    </PersistGate>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
